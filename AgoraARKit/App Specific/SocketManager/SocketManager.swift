//
//  SocketManager.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 21/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import SocketIO

typealias CompletionBlock = ((Any) -> ())?
class SocketIOManager: NSObject {
    
    static let shared = SocketIOManager()
    
    let manager = SocketManager(socketURL: URL(string: Constants.SocketKeys.baseUrl)!, config: [.log(false), .compress])
//    let manager = SocketManager(socketURL: URL(string: Constants.SocketKeys.SocketLiveUrl)!, config: [.log(false), .compress])

    lazy var socket = manager.defaultSocket
    
    var isSocketOn = false
    
    override private init() {
        super.init()
        
        socket.on(clientEvent: .disconnect) { (data, ack) in
            print ("socket is disconnected please reconnect")
//            self.socket.connect()
        }
        
        socket.on(clientEvent: .reconnect) { (data, ack) in
          print ("socket is reconnected please reconnect :: \(data) \n ACK :: \(ack)")
          self.closeConnection()
          self.isSocketOn = false
          if AppGlobalManager.sharedInstance.loggedInUser != nil {
            self.socket.connect()
          }
        }
        
        socket.on(clientEvent: .statusChange) { st, ack in
            print("socket statusChange :: \(st) \n ACK :: \(ack)")
        }
        socket.on(clientEvent: .error) { data , ack in
            print("socket error :: \(data) \n ACK :: \(ack)")
        }
        
        socket.on(clientEvent: .connect) {data, ack in
            print ("\n socket connected :: \(data)\n")
            if !self.isSocketOn {
                self.isSocketOn = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    
                    if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false {
                        
                    }
                    else {
                        if ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as? AppSettingsViewController) != nil || ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as? LoginVC) != nil {
                            return
                        }
                    }
                    
                    var chatVC : ChatUsersListViewController?
                    //            if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
                    //                chatVC = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.children.first as? ChatUsersListViewController
                    //
                    //                }
                    //            else{
                    chatVC = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.children[((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.children.count ?? 0) - 2].children.first as! ChatUsersListViewController
                    //                }
                    self.socketDisconnect()
                    chatVC?.socketOnMethods()
                    //            var statusParam = [String:Any]()
                    //            statusParam["status"] = "online"
                    //            statusParam["mobile"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
                    //            chatVC?.socketEmiter(key: Constants.SocketKeys.userstatus, param: statusParam)
                    //                chatVC.socketEmiter(key: Constants.SocketKeys.useronline, param: AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "")
                    var param = [String:Any]()
                    param["token"] = AppGlobalManager.sharedInstance.loggedInUser?.token ?? ""
                    chatVC?.socketEmiter(key: Constants.SocketKeys.authenticate, param: param)
                    
                    chatVC?.socketEmiter(key: Constants.SocketKeys.joinuser, param: AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "")
                }
//                Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
            }
        }
    }
    func establishConnection() {
        socket.connect()
    }
    @objc func updateCounting(){
      let myJSON = ["mobile" : AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""] as [String : Any]
      socket.emit(Constants.SocketKeys.updateLatLong , with: [myJSON])
      print(myJSON)
    }
    func online(){
        if self.socket.status == .connected {
            var statusParam = [String:Any]()
            statusParam["status"] = "online"
            statusParam["mobile"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
            socket.emit(Constants.SocketKeys.userstatus, with: [statusParam])
        } else {
            
            socket.connect()
            
           var statusParam = [String:Any]()
            statusParam["status"] = "online"
            statusParam["mobile"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
            socket.emit(Constants.SocketKeys.userstatus, with: [statusParam])
        }
    }
    func offline(){
        if self.socket.status == .connected {
            var statusParam = [String:Any]()
            statusParam["status"] = "offline"
            statusParam["mobile"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
            statusParam["at"] = UtilityClass.convertToString(date: Date())
            statusParam["device_type"] = "0"
            socket.emit(Constants.SocketKeys.userstatus, with: [statusParam])
        } else {
            
            socket.connect()
            
            var statusParam = [String:Any]()
            statusParam["status"] = "offline"
            statusParam["mobile"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
            statusParam["at"] = UtilityClass.convertToString(date: Date())
            statusParam["device_type"] = "0"
            socket.emit(Constants.SocketKeys.userstatus, with: [statusParam])
        }
    }
    func closeConnection() {
        self.isSocketOn = false
        socketDisconnect()
        socket.disconnect()
//        if ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as? AppSettingsViewController) != nil || ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as? LoginVC) != nil {
//            return
//        }
//
//        let chatVC = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.children[((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.children.count ?? 0) - 2].children.first as! ChatUsersListViewController
//        chatVC.socketDisconnect()
//        socket.disconnect()
    }
    
    func socketCall(for key: String, completion: CompletionBlock = nil)
    {
        SocketIOManager.shared.socket.on(key, callback: { (data, ack) in
            let result = self.dataSerializationToJson(data: data)
            guard result.status else { return }
            if completion != nil { completion!(result.json) }
        })
    }
    
    func socketEmit(for key: String, with parameter: [String:Any]){
        socket.emit(key, with: [parameter])
        //        print ("Parameter Emitted for key - \(key) :: \(parameter)")
    }
    
    func dataSerializationToJson(data: [Any],_ description : String = "") -> (status: Bool, json: Any){
        let json = data
        //        print (description, ": \(json)")
        
        return (true, json)
    }
    
    func socketDisconnect() {
        
        //        {status : "offline", mobile:state.mobile ,at : "2020-09-25 17:02:02"}
        
        var statusParam = [String:Any]()
        statusParam["status"] = "offline"
        statusParam["mobile"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
        statusParam["at"] = UtilityClass.convertToString(date: Date())
        statusParam["device_type"] = "0"
//        self.socketEmiter(key: Constants.SocketKeys.userstatus, param: statusParam)
        self.socket.emit(Constants.SocketKeys.userstatus, with: [statusParam])
        
        socket.off(Constants.SocketKeys.unauthorized)
        socket.off(Constants.SocketKeys.userconnected)
        socket.off(Constants.SocketKeys.message)
        socket.off(Constants.SocketKeys.typing)
        socket.off(Constants.SocketKeys.userstatus)
        socket.off(Constants.SocketKeys.messageRead)
        socket.off(Constants.SocketKeys.messageReadAll)
        socket.off(Constants.SocketKeys.messageDeliver)
    }
    
}
