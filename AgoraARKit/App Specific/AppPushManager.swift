//
//  AppPushManager.swift
//  AgoraARKit
//
//  Created by Prashant on 12/11/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import Foundation
import PushKit
import CallKit
import AgoraRtmKit
import UIKit
import AVKit
import UserNotifications

enum RTMCallLifeCycle {
    case readyToAcceptTheCall(call:Call,action:CXAnswerCallAction)
    case needToRejectTheCall (call:Call,action:CXEndCallAction)
    case requestTimeOut
    case ideal
}

class AppPushManager : NSObject {
    
    var rtmCallHandler:RTMCallHandler!
    var remoteInvite:AgoraRtmRemoteInvitation? = nil
    var rtmCallLifeCycle = RTMCallLifeCycle.ideal
    var timer:Timer? = nil
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    
    var provider:CXProvider!
    var completionPushHandler:(() -> (Void))?
    
    var userBusyWithCall = false
    
    var status:AppStateMachine  {
        get {
            return AppDelegate.shared.status
        } set {
            AppDelegate.shared.status = newValue
        }
    }
    
    var callStatusMessage : String? = "missedcall"
    
    override init () {
        super.init()
        self.status = .ideal
        registerForPushKit()
        setupCallKit()
        rtmCallHandler = RTMCallHandler(delegate: self)
        
    }
    
    private func registerForPushKit() {
        let voipPushRegistery = PKPushRegistry(queue: .main)
        voipPushRegistery.delegate = self
        voipPushRegistery.desiredPushTypes = [.voIP]
    }
  
    private func setupCallKit() {
        
        let cxProviderConfig = CXProviderConfiguration(localizedName: "ARMS")
        cxProviderConfig.supportsVideo = true
        cxProviderConfig.ringtoneSound = "got.caf"
        cxProviderConfig.maximumCallsPerCallGroup = 4
        
        cxProviderConfig.supportedHandleTypes = [.phoneNumber]
        cxProviderConfig.maximumCallGroups = 4
        cxProviderConfig.iconTemplateImageData = #imageLiteral(resourceName: "ARMS_NAME").pngData()
        
        provider = CXProvider(configuration: cxProviderConfig)
        
        provider.setDelegate(self, queue: .main)
        
        
    }
    
    private func setupForAcceptTheCall(call:Call,action:CXAnswerCallAction) {
        self.completionPushHandler?()
        self.completionPushHandler = nil
        self.navigateToARController(call: call, action: action)
    }
    
    private func setupForRejectCall(call:Call,action:CXEndCallAction) {
       
        callStatusMessage = "missedcall"
        self.callStatusAPI()
        AppDelegate.shared.callManager.remove(call: call)
        call.end()
        
        action.fulfill()
        self.rtmCallHandler.resetData()
    }
    
    private  func navigateToARController (call:Call,action:CXAnswerCallAction)   {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute:  {
            
            if UserDefaults.standard.string(forKey: "USER_PHONE") != nil {
                
                let vc = ARController.viewController()
                vc.modalPresentationStyle = .fullScreen
                vc.call = call
                vc.roomName =  self.rtmCallHandler.callInfo.roomName
                AppDelegate.shared.window?.rootViewController?.present(vc, animated: true, completion: nil)
                
                
            } else {
                let vc = LoginVC.viewController()
                AppDelegate.shared.window?.rootViewController = vc
            }
            
            call.answer()
            action.fulfill()
            
            self.rtmCallHandler.resetData()
            
        })
    }
    
    private func configureAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.videoChat, options: .mixWithOthers)
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print("Speaker error : \(error)")
        }
    }
    
    private func reportIncomingCall(uuid:UUID,handle:String,callerName:String,isVideo:Bool,completion:((Error?)->(Void))? = nil){
        let callUpdate = CXCallUpdate()
        callUpdate.hasVideo = true
        
        callUpdate.localizedCallerName = callerName
        callUpdate.remoteHandle = CXHandle(type: .phoneNumber, value: handle)
        
        provider.reportNewIncomingCall(with: uuid, update: callUpdate){ (error) in
            print("New Call reported")
            if error == nil {
                let call = Call(uuid: uuid, handle: handle, callName: callerName, date: Date())
                AppDelegate.shared.callManager.add(call: call)
            
            }
            completion?(error)
            
        }

    }
    
    //CALLER CANCELS THE CALL
    //TIMEOUT
    fileprivate func rejectCallWithCallKit() {
        let callUUID = self.rtmCallHandler.callInfo.callUUID
        let call = Call(uuid: callUUID, handle: self.rtmCallHandler.callInfo.callFrom, callName: self.rtmCallHandler.callInfo.callerName, date: Date())
        
        CoreDataManager.saveCall(call: call, duration: 0)
        AppDelegate.shared.callManager.remove(call: call) // important , endCallCXAction == CALLS =>   func provider(_ provider: CXProvider, perform action: CXEndCallAction)
        call.end()
        
        AppDelegate.shared.callManager.endCallCXAction(call: call, completion: {[weak self] (error) in
            guard let `self` = self else {
                return
            }
            // Call the completion handler
            self.completionPushHandler?()
            self.completionPushHandler = nil
            self.rtmCallHandler.logout()
        })
    }
    
    /// WHEN PUSH RECV. WE START INCOMING CALL --> START TIMER (30 SEC) --> INVITE RECV --> CANCEL TIMER
    /// WHEN PUSH RECV , WE START INCOMING CALL --> START TIMER (30 SEC) --> NO INVITE RECV --> TIEMR INVALIDATE
    
    func startTimerForIncomingCall() {
        
        self.backgroundTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            print("expired!")
            UIApplication.shared.endBackgroundTask(self.backgroundTask)
            self.backgroundTask = .invalid
        })
        timer = Timer.scheduledTimer(withTimeInterval: 30, repeats: false, block: { (timer) in
            print("Timer Fired", self.rtmCallLifeCycle)
            
            switch self.rtmCallLifeCycle {
                
            case .readyToAcceptTheCall(let call, _), .needToRejectTheCall(let call, _):
                self.provider.reportCall(with: call.uuid, endedAt: nil, reason: .remoteEnded)
                CoreDataManager.saveCall(call: call, duration: 0)
                AppDelegate.shared.callManager.remove(call: call)
                call.end()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast("\(call.callName) is Offline or unreachable", type: .fail)
                    self.rtmCallHandler.logout()
                }
                
                self.rtmCallLifeCycle = .ideal
                self.status = .ideal
                // Call the completion handler
                self.completionPushHandler?()
                self.completionPushHandler = nil
                self.rtmCallHandler.resetData()
                
                
            @unknown default:
                break
            }
            
            if self.backgroundTask != .invalid {
                UIApplication.shared.endBackgroundTask(self.backgroundTask)
                self.backgroundTask = .invalid
            }
        })
        
        
    }
    
    func resetTimer() {
        timer?.invalidate()
        timer = nil
        
    }
    
    /*   private func addItemToPushStack(callID:String) -> PushStack {
     if  let firstObejct =  self.arrPushStack.first(where:{$0.remoteInvite?.content == callID}) {
     firstObejct.callID = callID
     return firstObejct
     } else {
     let pushStack = PushStack()
     pushStack.callID = callID
     self.arrPushStack.append(pushStack)
     return pushStack
     }
     }
     
     private func addItemToPushStack(remoteInviteID:AgoraRtmRemoteInvitation) -> PushStack {
     if  let firstObejct =  self.arrPushStack.first(where:{$0.callID == remoteInviteID.content}) {
     firstObejct.remoteInvite = remoteInvite
     return firstObejct
     
     } else {
     let pushStack = PushStack()
     pushStack.remoteInvite = remoteInvite
     self.arrPushStack.append(pushStack)
     return pushStack
     
     }
     }*/
    func  callStatusAPI(){
        
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        guard let user_call_id = AppGlobalManager.sharedInstance.user_call_id_Global else{
            return
        }
        guard let user_id = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else{
            return
        }
        let requestParameters = [
            "user_call_id": user_call_id,
            "device_id" : device_id,
            "user_id" : user_id,
            "device_type" : "0",
            "status" : callStatusMessage as Any] as [String : Any]
        
        print("\n\n\n \(#function) :: \(user_call_id)\n\n\n")
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.callStatus, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    let userResponse =  CommonCodableHelper<GeneralResponse>.codableObject(from: data)
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        //                        self.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                        return
                    }
                    //                    self.showToast(userResponse!.message ?? "Successfully saved", type: .success)
                    
                }
                
            case .failure( let error):
                print("error",error)
            }
            
        }
    }
    
}

//--------------------------------------------------------------------------------

//MARK:-  CALLKIT DELEGATE Methdos

//--------------------------------------------------------------------------------

extension AppPushManager:CXProviderDelegate {
    func providerDidReset(_ provider: CXProvider) {
        for call in AppDelegate.shared.callManager.calls {
            call.end()
        }
        AppDelegate.shared.callManager.removeAllCalls()
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction)
    {
      // Reset the timer
      //  self.resetTimer()
      
           
          guard let call = AppDelegate.shared.callManager.callWithUUID(uuid: action.callUUID) else {
                print("Failed : \(action.callUUID)")
                action.fail()
                self.completionPushHandler?()
                self.status = .ideal
                return
              }
              
              
              NSLog("AGORA PHONE ACCEPT")
              
              //TWO CASES FOR ACCEPT THE CALL
              // 1) PUSH RECV --> LOGIN --> REMOTE INVITE RECV --> ACCEPT
              // 2) PUSH RECV --> LOGIN --> ACCEPT --> REMOVE INVITE RECV
              if AppGlobalManager.sharedInstance.currentUserID == nil {
                print("AppGlobalManager.sharedInstance.currentUserID == nil")
                AppGlobalManager.sharedInstance.setupDataFromUserDefaultToCodableObject()
              }

        callInviteService(state: .callAccepted) { (status) in
        if status {
            print(call.handle)
           self.navigateToARController(call: call, action: action)

        } else {
          UtilityClass.showAlert(title: "", message: "Something went wrong") { (act) in
             
          }
        }
      }
      
    }
    
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
       
      // Reset the timer
      self.resetTimer()
       
       
          /// This is very important:
          // We have two method for end call, endWithTranscation and endWithAction in call manager .
          // So on call back of end with action we remove call from call manager
          // So this guard condition will not true in that case
          // Now other case is when user press end call button from native callkit action.
          // In that case this condition will true.
           
          guard let call = AppDelegate.shared.callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()

            
            if self.status != .ideal {
                if let topVC = UIApplication.shared.topMostViewController as? ARController {
                    topVC.Endcall()
                    _ = topVC.selfSession.calls.map{$0.end()}
                  topVC.stopAgoraAndDeallocateResources()
                }
                self.completionPushHandler?()
            }
            
            self.status = .ideal
            
           
            return
          }
           
           
          if self.status == .receiveCalling {

            callInviteService(state: .callRejected) { (state) in
                if state {
                    AppGlobalManager.sharedInstance.incommingCall.removeAll()
                } else {
                    UtilityClass.showAlert(title: "", message: "Something went wrong") { (act) in

                    }
                }
            }
//
            action.fulfill()
             
          } else {
            // call.state != .ended checked here because We have two scenario to end the call
            // End call from App button call.state == .ended
            // End call from Call kit View call.state != .ended
            // So if call ended from callkit view then we need to leave the channel as well as send end call text
             
            if self.status == .inCall && call.state != .ended {
              if let topVC = UIApplication.shared.topMostViewController as? ARController {
                topVC.sendText(string: Constants.ChannelMessages.endCall)
                 
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {[weak topVC] in
                   
                  topVC?.leaveChannelSaveRecord {
                    _ = topVC?.selfSession.calls.map{$0.end()}
                    _ = topVC?.selfSession.calls.map{AppDelegate.shared.callManager.endCall(call:$0)}
                    //  AppDelegate.shared.callManager.endCall(call: self.selfSession.call)
                     
                     
                    topVC?.stopAgoraAndDeallocateResources()
                    topVC?.dismiss(animated: true, completion: nil)
                     
                    // Call the completion handler
                    self.completionPushHandler?()
                     
                    self.completionPushHandler = nil
                     
                  }
                }
              }
            }
            AppDelegate.shared.callManager.remove(call: call)
             
            action.fulfill()
             
          }
           
          // Reset the status
//          self.status = .ideal
        
        
        if self.status != .ideal {
            if let topVC = UIApplication.shared.topMostViewController as? ARController {
             
             _ = topVC.selfSession.calls.map{$0.end()}
              topVC.stopAgoraAndDeallocateResources()
             topVC.Endcall()
            }
        }
       
        
        
        // Reset the status
        self.status = .ideal
//        if let topVC = UIApplication.shared.topMostViewController as? ARController {
            
//            if !topVC.call.outgoing{
//                topVC.sendText(string: Constants.ChannelMessages.leftCall)
//            }
//            if topVC.activeSessions.count == 1{
//                callInviteService(state: .callRejected) { (state) in
//                    if state {
//                    } else {
//                        UtilityClass.showAlert(title: "", message: "Something went wrong") { (act) in
//
//                        }
//                    }
//                }
//            }
//            if topVC.call.outgoing{
//                callInviteService(state: .callRejected) { (state) in
//                    if state {
//                    } else {
//                        UtilityClass.showAlert(title: "", message: "Something went wrong") { (act) in
//
//                        }
//                    }
//                }
//            }
//        }
       
       
    }
    
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        
        
        // Reset the timer (not required here but just for assurance
        //  self.resetTimer()
        
        guard let call = AppDelegate.shared.callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }
        
        status = .calling
        
        let callUpdate = CXCallUpdate()
        callUpdate.remoteHandle = action.handle
        callUpdate.hasVideo = true
        callUpdate.localizedCallerName = call.callName
        callUpdate.supportsDTMF = false
        provider.reportCall(with: action.callUUID, updated: callUpdate)
        
        action.fulfill()
    }
    
    
    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        
        guard let call = AppDelegate.shared.callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }
        print("HOLD THE CALL \(call.handle)")
        
        action.fulfill()
        
    }
    
    func nsdataToJSON(data: NSData) -> AnyObject? {
        do {
            return try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers) as AnyObject
            print(try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers) as AnyObject)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    func callInviteService(state: Constants.callInviteState, userCallId: String = "", completion:((Bool) -> ())?) {
//    func callInviteService(state: Constants.callInviteState, completion:((Bool) -> ())?) {
        
        if !AppGlobalManager.sharedInstance.isRejectedBy {
            completion!(true)
            return
        }
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        guard let user_call_id = AppGlobalManager.sharedInstance.user_call_id_Global else{
            return
        }
        
        var param = [String:Any]()
        param["senderId"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo
        
//        receiverIdArray = [String]()
//        receiverIdArray.append(AppGlobalManager.sharedInstance.incommingCall.last?.callFrom ?? "" )
        
        param["receiverId"] = AppGlobalManager.sharedInstance.incommingCall.last?.callFrom ?? ""
//        param["user_call_id"] = user_call_id
        if userCallId == "" {
          param["user_call_id"] = user_call_id
        } else {
          param["user_call_id"] = userCallId
        }
        param["room_name"] = AppGlobalManager.sharedInstance.incommingCall.last?.roomName
        param["device_id"] = device_id
        param["user_call_state"] = state.rawValue // "Call Accepted"
        param["device_type"] = "0"
        
        print("\n\n Param:  \(param) \n\n")
        
        
        if state == .callRejected {
            print("\n\n\n REJECTED :: \(user_call_id)\n\n\n")
        }
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.callInvite, parameters: param, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            
            switch response {
            case .success(let data):
                if state == .callRejected {
                    print("call invite rejected called")
                    AppGlobalManager.sharedInstance.incommingCall.removeAll()
                }
                 
                if completion != nil {
                    completion!(true)
                }
            case .failure( let error):
                print("error: ", error.localizedDescription)
                if completion != nil {
                    completion!(false)
                }
            }
        }
    }
    
}

//--------------------------------------------------------------------------------

//MARK:-  PUSHKIT DELEGATE Methdos

//--------------------------------------------------------------------------------


extension AppPushManager:PKPushRegistryDelegate {
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        print("Push registry update with credentials\(pushCredentials.token)")
        
//        UserDefaults.standard.set(pushCredentials.token, forKey: "TOKEN")

        if type == PKPushType.voIP {
            print(pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined())
            
            UserDefaults.standard.set(pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined(), forKey: "TOKEN")
            
            print(UserDefaults.standard.object(forKey: "TOKEN"))
            
            //            UserDefaults.standard.set("d45f7c51bb506c8bea6649eaffc496b230122084b3bcd3b5b1dbbaeaa8deb10b", forKey: "TOKEN")
            
            //            if let userData =  AppGlobalManager.sharedInstance.loggedInUser, let phoneNumber = userData.user?.mobileNo {
            //                APIHandler.registerDeviceToken(userID: "\(phoneNumber)", completion: nil)
            //            }
            
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        self.registerForPushKit()
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        
        NSLog("AGORA PUSH PAYLOAD %@", payload.dictionaryPayload)
        let scale = UIScreen.main.scale
        if let width = payload.dictionaryPayload["sender_width"] as? CGFloat {
            AppGlobalManager.sharedInstance.SenderWidth = width / scale
        }
        else if let width = payload.dictionaryPayload["sender_width"] as? String {
            AppGlobalManager.sharedInstance.SenderWidth = CGFloat(Double(width) ?? 0) / scale
        }
        if let height = payload.dictionaryPayload["sender_height"] as? CGFloat {
            AppGlobalManager.sharedInstance.SenderHeight = height / scale
        }
        else if let height = payload.dictionaryPayload["sender_height"] as? String {
            AppGlobalManager.sharedInstance.SenderHeight = CGFloat(Double(height) ?? 0) / scale
        }
//        AppGlobalManager.sharedInstance.SenderWidth = (payload.dictionaryPayload["sender_width"] as? CGFloat ?? 0.0 / scale)
//        AppGlobalManager.sharedInstance.SenderHeight = (payload.dictionaryPayload["sender_height"] as? CGFloat ?? 0.0 / scale)

        print("Push manager sender Height \(AppGlobalManager.sharedInstance.SenderHeight)")
        print("Push manager sender Width \(AppGlobalManager.sharedInstance.SenderWidth)")

        AppGlobalManager.sharedInstance.isRejectedBy = true
        let callUUID:UUID = UUID()
        print(callUUID)
        let callInfo = CallInfo(callFrom: (payload.dictionaryPayload["call_frm"] as? String) ?? "",
                                callerName: (payload.dictionaryPayload["caller_name"] as? String) ?? "",
                                callTo: "", roomName: (payload.dictionaryPayload["room_name"] as? String) ?? "",
                                callDate: (payload.dictionaryPayload["call_date"] as? String) ?? "",
                                callIdentifier:  (payload.dictionaryPayload["call_id"] as? String) ?? "",
                                chatId:(payload.dictionaryPayload["chat_id"] as? String) ?? "",
                                callerUserId: (payload.dictionaryPayload["caller_user_id"] as? String) ?? "",
                                callUUID: callUUID)

        AppGlobalManager.sharedInstance.user_call_id_Global = (payload.dictionaryPayload["user_call_id"]! as! Int)
        //        AppGlobalManager.sharedInstance.incommingCall = callInfo

        AppGlobalManager.sharedInstance.incommingCall.append(callInfo)
        print(callInfo)
        if status == .inCall || status == .receiveCalling || status == .calling {
            print("========= Already IN CALL =========")
//            (UIApplication.shared.delegate as! AppDelegate).pushManager.callInviteService(state: .callBusy) { (state) in
            (UIApplication.shared.delegate as! AppDelegate).pushManager.callInviteService(state: .callBusy, userCallId: "\(payload.dictionaryPayload["user_call_id"] as? Int ?? 0)" ) { (state) in
                if state {
                    if AppGlobalManager.sharedInstance.incommingCall.count != 0{
                    AppGlobalManager.sharedInstance.incommingCall.removeLast()
                    }
                }else {
                    UtilityClass.showAlert(title: "", message: "Something went wrong") { (act) in
                    }
                }
            }
//            self.reportIncomingCall(uuid: callUUID, handle:  callInfo.callFrom, callerName: callInfo.callerName, isVideo: false,completion:  { error in
//                self.provider.reportCall(with: callUUID, endedAt: nil, reason: .remoteEnded)
//                completion()
//            })
            let callUUID:UUID = UUID()
            let call = Call(uuid: callUUID, handle:(payload.dictionaryPayload["call_frm"] as? String)! , callName: (payload.dictionaryPayload["caller_name"] as? String)!, date: Date())
            
            CoreDataManager.saveCall(call: call, duration: 0)
            
            return
        }

        (UIApplication.shared.delegate as! AppDelegate).pushManager.callInviteService(state: .callRinging) { (state) in
            if state {
//                AppGlobalManager.sharedInstance.incommingCall.removeLast()
            }else {
                UtilityClass.showAlert(title: "", message: "Something went wrong") { (act) in
                }
            }
        }
        //        AppGlobalManager.sharedInstance.initcall?.user_call_id = payload.dictionaryPayload["user_call_id"] as? Int
        //        print(AppGlobalManager.sharedInstance.initcall?.user_call_id!)
        //        print("Paylod user call id %d", payload.dictionaryPayload["user_call_id"]!)
        guard callInfo.callDateToCurrentDate < 30 else {
            self.reportIncomingCall(uuid: callUUID, handle:  callInfo.callFrom, callerName: callInfo.callerName, isVideo: false,completion:  { error in
                self.provider.reportCall(with: callUUID, endedAt: nil, reason: .remoteEnded)
                completion()
            })
            showNotification(callName: callInfo.callerName)
            return
        }
        
        
        
        
        //        // THIS HAPPESN WHEN USER1 CALLS --> USER2 PRESS END CALL BUTTON BEFORE REMOTE INVITE RECV, NOW USER1 ENDS CURRENT CALL AND CALL AGAIN TO USER2.
        //        // IN THAT CASE OUR CALL LIFE CYCLE STATUS WOULD BE EITHER READY TO ACCEPT OR NEED TO REJECT. SO WHEN NEW INVITE RECV. IT DIRECLTY ACCEPT OR REJCT THE CAL
        //        // REJECT THE PREVIOS CALL IF NOTHING RECV.
        //        switch self.rtmCallLifeCycle {
        //
        //        case .readyToAcceptTheCall(let call, let action):
        //
        //            CoreDataManager.saveCall(call: call, duration: 0)
        //            AppDelegate.shared.callManager.remove(call: call)
        //            call.end()
        //            action.fail()
        //            self.rtmCallHandler.resetData()
        //        case .needToRejectTheCall(let call, let action):
        //            CoreDataManager.saveCall(call: call, duration: 0)
        //            AppDelegate.shared.callManager.remove(call: call)
        //            call.end()
        //            action.fail()
        //            self.rtmCallHandler.resetData()
        //
        //        @unknown default:
        //            break
        //        }
        //   if rtmCallHandler == nil {
        rtmCallHandler = RTMCallHandler(delegate: self)
        // } else {
        //     rtmCallHandler.updateCallKitDelegate(delegate: self)
        // }
        rtmCallHandler.callInfo = callInfo
        // self.addItemToPushStack(callID: self.rtmCallHandler.callInfo.callIdentifier)
        //RTM
        AppGlobalManager.sharedInstance.setupDataFromUserDefaultToCodableObject()
        //        if case  .logout = rtmCallHandler.loginStatus {
        //
        //        }
        //        rtmCallHandler.login(phoneNumber: "\(AppGlobalManager.sharedInstance.currentUserID ?? "")", completion: { (code) in
        //            print("RTM ==> APPDELEGATE LOGIN \(code.rawValue)")
        //            self.rtmCallHandler.sendLocalInvite(to: self.agoraSignalManager.callFrom) { (errorCode) in
        //                print("Local Invite Sent ==> \(errorCode.rawValue)")
        //            }
        //        })
        //TODO : TESTING
        self.status = .receiveCalling
        self.configureAudioSession()
        self.reportIncomingCall(uuid: callUUID, handle:  self.rtmCallHandler.callInfo.callFrom, callerName: self.rtmCallHandler.callInfo.callerName, isVideo: false,completion: { _  in
            self.resetTimer()
//            self.startTimerForIncomingCall()
        })
        
        //    DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
        //      completion()
        //   }
        self.completionPushHandler = completion
    }
    
    func showNotification(callName : String){
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert]) {
            (granted, error) in
            if granted {
                self.scheduleNotification(callName: callName)
            } else {
            }
        }
    }
    func scheduleNotification(callName: String) {
        
        let content = UNMutableNotificationContent()
        content.title = "You have missed call from \(callName)"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)
        
        // 4
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
    }
    
}

//--------------------------------------------------------------------------------

//MARK:-  RTM DELEGATE Methdos

//--------------------------------------------------------------------------------


extension AppPushManager:AgoraRtmCallDelegate {
    // CALLE GET INVITE
    func rtmCallKit(_ callKit: AgoraRtmCallKit, remoteInvitationReceived remoteInvitation: AgoraRtmRemoteInvitation) {
        DispatchQueue.main.async {
            print("INCOMING CALL REQUEST REC \(remoteInvitation.state.rawValue)")
            
            // If we are alrady in the call , we are directly refusing the invite
            if (self.status == .inCall || self.status == .receiveCalling || self.status == .calling) && self.rtmCallHandler.callInfo.callFrom != remoteInvitation.callerId {
                remoteInvitation.response = Constants.ChannelMessages.busy
                callKit.refuse(remoteInvitation) { [unowned self] (code) in
                    print("CALL REFUSE SENT for BUSY",code.rawValue)
                    
                }
                return
            }
            
            // Rest the timer.
            self.resetTimer()
            
            switch self.rtmCallLifeCycle {
                
            case .readyToAcceptTheCall(let call,let action):
                callKit.accept(remoteInvitation) { (code) in
                    if code == .ok {
                        //TODO:Need to check else condition
                        self.setupForAcceptTheCall(call: call, action: action)
                        self.status = .inCall
                        
                    }else if code == .alreadyEnd{
                        action.fail()
                        self.status = .ideal
                    } else {
                        action.fail()
                        self.status = .ideal
                    }
                    self.remoteInvite = nil
                    
                    self.rtmCallLifeCycle = .ideal
                }
            case .needToRejectTheCall(let call,let action):
                callKit.refuse(remoteInvitation) {[unowned self] (code) in
                    print("CALL REFUSE SENT")
                    
                    if code == .ok {
                        self.setupForRejectCall(call: call,action:action)
                    }else {
                        action.fail()
                        
                    }
                    self.rtmCallLifeCycle = .ideal
                    self.remoteInvite = nil
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        self.rtmCallHandler.logout()
                        
                    }
                }
                
            case .requestTimeOut:
                break
            case .ideal:
                self.remoteInvite = remoteInvitation
                break
                
            }
        }
        
        //        callKit.accept(remoteInvitation) { (code) in
        //            print("remoteInvitation \(code.rawValue) ")
        //
        //        }
        
    }
    
    func rtmCallKit(_ callKit: AgoraRtmCallKit, remoteInvitationCanceled remoteInvitation: AgoraRtmRemoteInvitation) {
        print("INCOMING CALL REQUEST CANCELS")
        rejectCallWithCallKit()
        // Rest the timer.
        self.resetTimer()
    }
    
    func rtmCallKit(_ callKit: AgoraRtmCallKit, remoteInvitationFailure remoteInvitation: AgoraRtmRemoteInvitation, errorCode: AgoraRtmRemoteInvitationErrorCode) {
        print("remoteInvitationFailure \(errorCode.rawValue)")
        rejectCallWithCallKit()
        // call status API
        callStatusMessage = "reject"
        self.callStatusAPI()
        // Rest the timer.
        self.resetTimer()
    }
    
    
    //CALLSTATUS
    
    func rtmCallKit(_ callKit: AgoraRtmCallKit, localInvitationFailure localInvitation: AgoraRtmLocalInvitation, errorCode: AgoraRtmLocalInvitationErrorCode) {
        print("localInvitationFailure: \(localInvitation.response ?? "")")
        (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast("localInvitationFailure", type: .fail)
    }
    
    func rtmCallKit(_ callKit: AgoraRtmCallKit, remoteInvitationAccepted remoteInvitation: AgoraRtmRemoteInvitation) {
        print("remoteInvitationAccepted ")
    }
    
}
