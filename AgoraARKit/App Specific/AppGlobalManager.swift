//
//  AppGlobalManager.swift
//  AgoraARKit
//
//  Created by Prashant on 29/04/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import Foundation
import UIKit

class AppGlobalManager:NSObject {
    
    
    static let sharedInstance = AppGlobalManager()
    
    var loggedInUser:LoginResponse?
    
    var syncLoginResponse : SyncResponse?
    
    var currentUserID:String? {
        return loggedInUser?.user?.mobileNo
    }
    
    var token:String? {
        return loggedInUser?.token
    }
    
    var agoraAppId : String?{
        return loggedInUser?.app_id
    }
    
    //changes in lock down

    var allowGroup_call : Int?{
        return syncLoginResponse?.company?.allow_groupcall
    }
    
    var allow_rec_full : Int?{
        return syncLoginResponse?.company?.allow_rec_full
    }
    
    var categoryArray : [category]?{
        return syncLoginResponse?.category
    }
    var tagsArray : [tags]?{
           return syncLoginResponse?.tags
       }
    var SenderHeight : CGFloat = UIScreen.main.bounds.size.height
    var SenderWidth : CGFloat = UIScreen.main.bounds.size.width
    
    var extraPaddingX : CGFloat = 0.0
    var extraPaddingY : CGFloat = 0.0
  
    var extraPaddingXTest : CGFloat = 0.0
    var extraPaddingYTest : CGFloat = 0.0
  
//    var incommingCall : CallInfo?
    var incommingCall : [CallInfo] = []

    /// isRejected by **Me(true)** or **Other(false)** default **Me**
    var isRejectedBy = true
    
    var is_guest:Bool? {
        return loggedInUser?.user?.is_guest
    }
    
    var chat_id  : String = ""
    
    var isGuestModeOn = false

    var linkURL : String = ""
    
    var user_call_id_Global : Int?
        
    var locationString : String = ""
    
    var latitude : Double = 0.0
    
    var longitude : Double = 0.0
    
    var uidArray : [String] = []
        
    var uidAudioArray : [String] = []
    
    var uidScreenShareArray : [String] = []
    
    var isScreenShareME : Bool = false
    
    /// For Chatting selected user
        var receiverUser: User?
         
    var aryChatData = [chatUsersModel]()
    
    func appendLog(string:String?) {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
         
        let filename = paths[0].appendingPathComponent("output.txt")
        
        //try! FileManager.default.removeItem(at: filename)
        let oldStringData = try? Data(contentsOf: filename)
        
        do {
            
            var oldString = oldStringData != nil ?  String(data:oldStringData!,encoding: .utf8) : ""
            let newString = (oldString ?? "") + "\r\n" + (string ?? "")
            try newString.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("Failed \(error)")
            // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
        }
    }
    
    
    func setupFlow() {
        setupDataFromUserDefaultToCodableObject()
        if self.loggedInUser != nil && self.loggedInUser?.user?.is_guest == false {
//        if self.loggedInUser != nil  {
            
            let vc = TabbarController.viewController()
            vc.selectedIndex = 3
            AppDelegate.shared.window?.rootViewController = vc

        } else {
            let vc = LoginVC.viewController()
            AppDelegate.shared.window?.rootViewController = vc
        }
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func setupDataFromUserDefaultToCodableObject (){
         if let userData =  UserDefaults.standard.object(forKey: "UserObject") as? Data, let userObject = CommonCodableHelper<LoginResponse>.codableObject(from: userData)?.output {
            AppGlobalManager.sharedInstance.loggedInUser = userObject

        }
    }
    
    func logoutAndClearAllCache() {
        UserDefaults.standard.removeObject(forKey: "UserObject")
        UserDefaults.standard.removeObject(forKey: "USER_PHONE")
        UserDefaults.standard.removeSuite(named: "TOKEN")
        UserDefaults.standard.removeSuite(named: "device_id")
        UserDefaults.standard.removeObject(forKey: "IS_GUEST")
        AppGlobalManager.sharedInstance.linkURL = ""
        AppGlobalManager.sharedInstance.incommingCall.removeAll()
        self.loggedInUser = nil
        SocketIOManager.shared.closeConnection()
        setupFlow()
        //development branch
        
    }
    func changeAspectRationAtReceiverSide() -> CGRect{
            let receiverWidth = UIScreen.main.bounds.size.width
            let receiverHeight = UIScreen.main.bounds.size.height
            
            var actualHeight : CGFloat = receiverHeight
            var actualWidth : CGFloat = receiverWidth
        
            if(AppGlobalManager.sharedInstance.SenderWidth != 0.0) {
                // myWidth = 375 // 414
                // newWidth = (768 / 1024) * 896 = 672
                let newWidth = (AppGlobalManager.sharedInstance.SenderWidth) / (AppGlobalManager.sharedInstance.SenderHeight) * receiverHeight
                // myHeight = 667 // 896
                // newHeight = (1024 / 768) * 414 = 552
                let newHeight = (AppGlobalManager.sharedInstance.SenderHeight) / (AppGlobalManager.sharedInstance.SenderWidth) * receiverWidth
                
                // 672 < 414
                if newWidth < receiverWidth {
                    //small to large
                  
//                    extraPaddingX = (receiverWidth - actualWidth) / 2
//                    print("Extrapadding X : \(extraPaddingX)")
                    
                    actualWidth = UIScreen.main.bounds.width
                    actualHeight = newHeight
                    print("height : \(actualHeight)")

                    extraPaddingY = (actualHeight - receiverHeight) / 2
                    print("Extrapadding Y : \(extraPaddingY)")
                }
                // 552 < 896
                if newHeight < receiverHeight {
                    
                    // large to small
                    
//                    extraPaddingY = (receiverHeight - actualHeight) / 2
//                    print("Extrapadding Y : \(extraPaddingY)")
                    actualHeight = UIScreen.main.bounds.height

                    actualWidth = newWidth
                    
                    extraPaddingX = (actualWidth - receiverWidth) / 2

                }
                
                
    //            if (AppGlobalManager.sharedInstance.SenderWidth < receiverWidth) {
    //                actualWidth = (AppGlobalManager.sharedInstance.SenderWidth) / (AppGlobalManager.sharedInstance.SenderHeight) * receiverHeight
    //
    //                print("Actual width : \(actualWidth)")
    //                extraPaddingX = (receiverWidth - actualWidth) / 2
    //                print("Extrapadding X : \(extraPaddingX)")
    //            } else {
    //
    //                actualHeight = (AppGlobalManager.sharedInstance.SenderHeight) / (AppGlobalManager.sharedInstance.SenderWidth) * receiverWidth
    //
    //                print("Actual height : \(actualHeight)")
    //                extraPaddingY = (receiverHeight - actualHeight) / 2
    //                print("Extrapadding Y : \(extraPaddingY)")
    //            }
            }
            return CGRect(x:0,y:0,width: actualWidth,height: actualHeight)
        }
}
