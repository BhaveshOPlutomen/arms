//
//  AppConstants.swift
//  AgoraARKit
//
//  Created by Prashant on 17/05/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    struct ChannelMessages {
        static let screenShare                                              =   "SCREEN_SHARE"
        static let screenShareME                                            =   "SCREEN_SHARE:ME"
        static let screenShareNonAR                                         =   "SCREEN_SHARE:NON_AR"
        static let allPoints                                                =   "all_points"

        
        static let drawingFinshed                                           =   "DRAWING_FINISHED"
        static let touchBegin                                               =   "TOUCH_BEGIN"
        static let touchMove                                                =   "TOUCH_MOVE"
        static let touchEnd                                                 =   "TOUCH_END"
        static let singleTap                                                =   "SINGLE_TAP"
        static let TapTextBox                                               =   "TAP_TEXTBOX"
        static let AddTextToTextBox                                         =   "ADDTEXTTOTEXTBOX"
        static let undo                                                     =   "R_UNDO"
        
        static let answerCall                                               =   "ANSWER_CALL"
        static let endCall                                                  =   "END_CALL"
        static let incomingCallShow                                         =   "INCOMING_CALL_SHOWED"
        static let busy                                                     =   "BUSY"
        static let noAnswer                                                 =   "NO_ANS"
        static let leftCall                                                 = "LEFT_CALL"

        static let whoIsPublisher                                           =   "WHO_IS_PUBLISHER"
        static let i_am_publisher                                           =   "I_AM_PUBLISHER:"
        static let user_joined                                              =   "USER_JOINED:"

        static let myCallDuration                                           =   "MY_CALL_DURATION:"
        
        static let chat                                                     =   "CHAT:"
        static let annotation                                               =   "ANNOTATION:"
        static let freezeMode                                              =  "freezeMode"
        
        // for android testing
        static let screensizeshareSenderFreezeMode                              = "SENDER_SCREENSIZE"
        static let screensizeshareReceiverFreezeMode                            = "RECE_SCREENSIZE"

        static let lineColor                                             = "Line_Color"

        static let recording                                                = "RECORDING"
        static let recordingThumb                                          = "RECORDING_THUMB"
        
        // line width
        static let lineSize                                                = "Line_SIZE"
        
        static let orientationPortrait = "orientation_portrait"

        static let orientationLandscape = "orientation_landscape"



        
    }
    
    
    
    struct SocketKeys {
        static let baseUrl = "https://arms.pluto-men.com:8890/"
        
        static let SocketLiveUrl = "https://app.thearms.io:8898/"
        
        static let authenticate = "authenticate"
        static let joinuser = "joinuser"
        static let message = "message"
        static let typing = "typing"
        static let unauthorized = "unauthorized"
        static let userconnected = "userconnected"
        static let userstatus = "userstatus"
        static let messageRead = "messageRead"
        static let messageReadAll = "messageReadAll"
        
        static let useronline = "useronline"
        
        static let messageDeliver = "messageDeliver"
        static let isOnline = "isOnline"
        static let userlive = "userlive"
        
        static let updateLatLong = "updateLatLong"
    }
    
    struct AppTheme {
        struct Fonts {
            enum ProximaNova:String {
                
                
                case FONT_REGULAR = "ProximaNova-Regular"
                case FONT_SemiBold = "ProximaNova-SemiBold"
                case FONT_BOLD = "ProximaNova-Bold"
            }
            
            static func font(type: ProximaNova,size:CGFloat) -> UIFont {
                return UIFont(name: type.rawValue, size: size)!
                
            }
        }
        
        struct Colors {
            enum ColorsOfApp:String,CaseIterable {
                case colorPrimaryDark                   = "#2f3740"
                case colorSecondaryGray                 = "#58636c"
                case colorLightGray                     = "#68737d"
                
                case green                              = "#5cd994"
                case redColor                           = "#F56868"
                case orangeColor                        = "#FFD800"
                
                var color:UIColor {
                    return UIColor(hexFromString: self.rawValue)
                }
            }
            
        }
        struct LineColors {
            enum ColorsOfApp:String,CaseIterable {
                case green            = "#1FAE5F"
                case red              = "#FF3B30"
                case orange           = "#FF9500"
                case white            = "#FFFFFF"
                case black            = "#000000"
                case blue             = "#007AFF"
                case gray             = "#8E8E93"
                case yellow           = "#FFCC00"
                case teal             = "#5AC8FA"
                case purple           = "#AF52DE"
                var color:UIColor {
                    return UIColor(hexFromString: self.rawValue)
                }
            }
        }
        
    }
    struct profileKeys{
        static let Profile = "Profile"
        static let Gallery = "Gallery"
        static let ChangePassword = "Change Password"
        static let GEOLocation = "GEO Location"
        static let ContactUs = "Contact us"
        static let Feedback = "Feedback"
        static let AboutUs = "About us"
        static let TermsAndConditions = "Terms & conditions"
        static let PrivacyPolicy = "Privacy Policy"
        static let Help = "Help"
    }
    
    var preferredFramesPerSecond = 10
    
    enum callInviteState: String {
        case callAccepted = "Call Accepted"
        case callRejected = "Call Rejected"
        case callMiscall = "Miss Call"
        case callRinging = "Call Ringing"
        case callBusy = "Call Busy"
    }
    
    enum channelJoinState : String{
        case ChannelJoinSendByReceiver = "ChannelJoinSendByReceiver"
        case ChannelJoinedBySender = "ChannelJoinedBySender"
    }
}

struct APIConstants {
    
    enum Environment  {
        case local
        case production
        case uat
    }
    
    
    // push changes
  private static let localServerURL =  "https://arms.pluto-men.com/api/"
    
    private static let productionURL =  "https://app.thearms.io/api/"
    
    private static let uatURL = "https://armsuat.pluto-men.com/api/"

//    private static let productionURL =  "https://armsapp.pluto-men.com/api/"

    
  //  private static let localServerURL =  "http://devarms.pluto-men.com/api/"
    //http://devarms.pluto-men.com/login/admin
    
//    http://armsapp.pluto-men.com/
     static let environment = Environment.local
    
    static var apiURL: String {
        if case  .local = environment {
            return APIConstants.localServerURL
        } else if case .uat = environment {
            return APIConstants.uatURL
        } else{
            return APIConstants.productionURL
        }
       
    }
    
//
    
    
    
    struct EndPoints {
       
        static let userLogin                                 =   "user/login"
        static let registerDeviceToken                       =   "registerDeviceToken"
        //devicetype
        static let userList                                  =   "userlist"
        static let callToUser                                =   "initCall"
        //device_type
        static let userLogout                                =   "user/logout"
        static let userSync                                  =   "sync_login"
        //changes in lock down
        //device_type
        static let storeCall                                =   "storeCall"
        //device_type
        static let callList                                =   "callList"
        //devidetype
        static let callStatus                             =   "callStatus"
        static let resetPass                            =  "user/reset/email"
        static let changePass                           = "user/changepassword"
        
        //devicetype
        static let callInvite                           = "callInvite"
        static let appSetting                           = "user/appsetting"

        static let joinSession                          = "joinSession"
        //device_type
        static let createSession                        = "createSession"
//        device_type
        static let recLinkJoin                          = "recLinkJoin"
        static let createGuest                          =   "createGuest"
        //device_type
        static let saveScreenShot                       =   "user_screenshot"
        //device_type
        static let screenshotList                       = "user_screenshot_list"
        //device_type
        static let userScreenshotDelete                 = "user_screenshot_delete"
        static let appfeedback                          = "feedback"
        //device_type
        static let stopRecordingThumb                   = "store_recording_thumb"
        //device_type
        static let deleteRecording                      = "delete_recording"
        //device_type
        static let saveVideoRecording                   = "recording_update"
        
        static let saveChatFile                          = "save_chat_file"
        static let chatList                             = "chatList"
        static let chatMessages                         = "chatMessages"
    }
    
    struct RequestKeys {
        
        ///
        
    }
    
    
}


extension Notification.Name {
    static let callInviteAccepted = Notification.Name("callInviteAccepted")
    static let callInviteRejected = Notification.Name("callInviteRejected")
    static let callInviteMiscall = Notification.Name("callInviteMiscall")
    static let callInviteRinging = Notification.Name("callInviteRinging")
    static let callInviteBusy = Notification.Name("callInviteBusy")
   // For Chatting
      static let notificationTyping = NSNotification.Name("chatMessageTyping")
      static let chatMessageReceived = NSNotification.Name("chatMessageReceived")
      static let chatUserOnline = NSNotification.Name("chatUserOnline")
      static let chatUserReadMessage = NSNotification.Name("chatUserReadMessage")
      static let chatUserReadAllMessage = NSNotification.Name("chatUserReadAllMessage")
      static let chatUserMessageDeliver = NSNotification.Name("chatUserMessageDeliver")
    static let chatUserIsOnline = NSNotification.Name("chatUserIsOnline")
    
    // For setting location
    static let locationPermission = NSNotification.Name("locationPermission")

}

