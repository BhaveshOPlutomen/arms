//
//  AppDelegate.swift
//  AgoraARKit
//
//  Created by Prashant on 15/04/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit
import CoreData
import PushKit
import CallKit
import Intents
import IQKeyboardManagerSwift
import UserNotifications
import AVKit
import Firebase
import NVActivityIndicatorView


enum AppStateMachine: Int {
    case calling
    case inCall
    case receiveCalling
    case refuseCall
    case endCall
    case callFail
    case ideal
}


// PushRecv -> Show Callkit -> case Accept --> status = readyToAcceptTheCall
// PushRecv -> Show Callkit -> case Reject --> status = needToRejectTheCall
// PushRecv -> Show Callkit -> no button press for 30 Seconds --> status = requestTimeOut



@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var status:AppStateMachine = .ideal
    var pushManager:AppPushManager!
    let callManager = CallManager()
    
    let geoCoder = CLGeocoder()
    
    private var rtmChannelManager:RTMChannelManager?
    
    var deviceOrientation = UIInterfaceOrientationMask.portrait

    //bhavesh changes
    var callingToName = String()
    
    class var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
//    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//        return .all
//       }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
//        fatalError()
        
                UserDefaults.standard.set("5cbe1c10637e4e9c820bdff3b62c433c",forKey: "AgoraAppId")
        //dev Appid
              KeyCenter.AppId = "3083bcc03f8644a2822da540dfb5e3c9"
        // live appid
//           KeyCenter.AppId = "e308842e423840498748e30b278ef87a"
        
        //dev authorization
        
        //it is created with the use of
        //        key：78446d122b864877ad9eb49c73e05f62
        //        secret：3cbde61e891d4a559f8d7d8b746a7a55
        //        converted key and sccret into bas 64 and added : in between

       KeyCenter.AuthorizationForRecording = "Basic Nzg0NDZkMTIyYjg2NDg3N2FkOWViNDljNzNlMDVmNjI6M2NiZGU2MWU4OTFkNGE1NTlmOGQ3ZDhiNzQ2YTdhNTU="
        
        //live authorization
        
        //it is created using this two
//        key：1eea3c52db9141ecb1eb00c3df0965bb
//        secret：7cb523cce79246d1a617c84941deb8b1
//        converted key and sccret into bas 64 and added : in between
        
//       KeyCenter.AuthorizationForRecording = "Basic MWVlYTNjNTJkYjkxNDFlY2IxZWIwMGMzZGYwOTY1YmI6N2NiNTIzY2NlNzkyNDZkMWE2MTdjODQ5NDFkZWI4YjE="

        application.shortcutItems = [.callNowItem]
        pushManager = AppPushManager()
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        setupNavbarAndTabbar()
        IQKeyboardManager.shared.enable = true
        
        //        AppGlobalManager.sharedInstance.setupFlow()
        
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert]) { (success, error) in
            print("Notification permission granted \(success) with error \(String(describing: error))")
        }
        
        let device_id = UIDevice.current.identifierForVendor?.uuidString
        UserDefaults.standard.set(device_id, forKey: "device_id")
        
        AppGlobalManager.sharedInstance.setupDataFromUserDefaultToCodableObject()

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
//        LocationHelper.shared.locationManagerDelegate = self
//        LocationHelper.shared.startUpdatingLocation()
   
        
        if AppGlobalManager.sharedInstance.loggedInUser != nil {
            if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false {
                
            }else {
                SocketIOManager.shared.establishConnection()
            }
        }
        
        return true
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//        SocketIOManager.shared.closeConnection()
        SocketIOManager.shared.offline()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
//        SocketIOManager.shared.establishConnection()
        SocketIOManager.shared.online()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        var bgTask = UIBackgroundTaskIdentifier.init(rawValue: 0)
        
        bgTask = application.beginBackgroundTask(withName:"MyBackgroundTask", expirationHandler: {() -> Void in
            // Do something to stop our background task or the app will be killed
            application.endBackgroundTask(bgTask)
            bgTask = UIBackgroundTaskIdentifier.invalid
        })
        
        if let topVC = UIApplication.shared.topMostViewController as? ARController {
            
            topVC.Endcall()
            _ = topVC.selfSession.calls.map{$0.end()}
            _ =  topVC.selfSession.calls.map{$0.generateCallLog(context: self.persistentContainer.viewContext, duration: $0.duration)}
        }
        
        //        if self.status == .inCall {
        //
        //
        //
        //            if let topVC = UIApplication.shared.topMostViewController as? ARController {
        //                //    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {// this line for just testing
        //
        ////                rtmChannelManager?.sendString(string: Constants.ChannelMessages.endCall, completion: { (errorCode) in
        ////                           print("RTM MESSAGE SENT WITH \(errorCode.rawValue)")
        ////                       })
        //                topVC.Endcall()
        //                topVC.sendText(string: Constants.ChannelMessages.endCall)
        //                topVC.storeCallApi()
        //                _ =  topVC.selfSession.calls.map{$0.generateCallLog(context: self.persistentContainer.viewContext, duration: $0.duration)}
        //
        ////
        ////
        ////                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {[ weak self] in
        ////                    // guard let `self` = self else {return}
        ////                    topVC.leaveChannelSaveRecord { [weak self] in
        ////                        //                self.call.end()
        ////                        //
        ////                        //                AppDelegate.shared.callManager.endCall(call: self.call)
        ////
        ////                        _ = topVC.selfSession.calls.map{$0.end()}
        ////                        _ = topVC.selfSession.calls.map{AppDelegate.shared.callManager.endCall(call:$0)}
        ////                        //   AppDelegate.shared.callManager.endCall(call: self.selfSession.call)
        ////
        ////
        ////                        topVC.stopAgoraAndDeallocateResources()
        ////
        ////                    }
        ////                }
        //                //    UIApplication.shared.endBackgroundTask(self.backgroundTask)
        //                //   self.backgroundTask = .invalid
        //
        //
        //            }
        //
        //
        //        }
        
        self.saveContext()
        
    }
 
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            let url = userActivity.webpageURL!
            //handle url
            print("URL from web %@",url)
            HandleUniversalLink.sharedInstance.universalLinkCreateSession(url: url.absoluteString)
            return true
        }
        
        guard let interaction = userActivity.interaction else {
            return false
        }
        // if status is ideal then only move
        guard status == .ideal else {
            return false
        }
        
        var phoneNumber: String?
        if let callIntent = interaction.intent as? INStartVideoCallIntent {
            phoneNumber = callIntent.contacts?.first?.personHandle?.value
        } else if let callIntent = interaction.intent as? INStartAudioCallIntent {
            phoneNumber = callIntent.contacts?.first?.personHandle?.value
        }
        
        // Remove special characters if any and white space
        phoneNumber = phoneNumber?.filter{$0.unicodeScalars.allSatisfy{$0.isASCII}}
        phoneNumber?.removeAll(where: {$0.isNewline || $0.isWhitespace})
        
        _ = showCallViewController(phoneNumber: phoneNumber)
        
        return true
        
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        // if status is ideal then only move
        guard status == .ideal else {
            completionHandler(false)
            return
        }
        
        if shortcutItem == .callNowItem {
            //  let vc = ViewController.viewController()
            completionHandler(showCallViewController(phoneNumber: nil))
            
        }
        
        completionHandler(false)
        
        
    }

    private func showCallViewController(phoneNumber:String?) -> Bool {
        AppGlobalManager.sharedInstance.setupDataFromUserDefaultToCodableObject()
        
        
        if AppGlobalManager.sharedInstance.loggedInUser != nil {
            
            let tabbarVC = TabbarController.viewController()
            tabbarVC.selectedIndex = 3


            self.window?.rootViewController = tabbarVC
            
            //bhavesh changes
            
            if let phonenumber = phoneNumber{
                if let nav = tabbarVC.viewControllers?.first(where: {($0 as? UINavigationController)?.topViewController is CallLogsVC}) as? UINavigationController,  let vc = nav.topViewController as? CallLogsVC {
                    //                    vc.callCameFromAppdelegate(callerID: phonenumber, callerName: phonenumber)
                    //                    tabbarVC.selectedViewController = nav
                    tabbarVC.selectedIndex = 0
                    
                    self.callAPIToFetchContacts(vc: vc, nv: nav, phoneNumber: phonenumber)
                    return true
                }
            }
            //            {
            //                if let nav = tabbarVC.viewControllers?.first(where: {($0 as? UINavigationController)?.topViewController is CallLogsVC}) as? UINavigationController,  let vc = nav.topViewController as? CallLogsVC {
            //                    vc.callCameFromAppdelegate(callerID: phonenumber, callerName: phonenumber)
            //                    tabbarVC.selectedViewController = nav
            //                    return true
            //                }
            //            }
            
            
        }
        return false
    }
    
    //bhavesh changes
    
    var filteredCompanyContact = [User]()
    private func callAPIToFetchContacts(vc: CallLogsVC, nv: UINavigationController, phoneNumber:String) {
        
        guard let userID = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else {
            return
        }
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        //  let parameters = ["userid":userID]
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.userList + "/" + "\(userID)?" + "device_id=" + "\(device_id)" + "&" + "device_type=" + "0"  , parameters: nil, token: AppGlobalManager.sharedInstance.token, method: .GET) { (result) in
            
            DispatchQueue.main.async {
                
                switch result {
                case .success( let data):
                    let object = CommonCodableHelper<[User]>.codableObject(from: data)
                    
                    guard let status = object?.statusCode,status == 1 else {
                        //                        self.showToast(object?.message ?? "Something went wrong !! Please try again later", type: .fail)
                        //                        _ = UIAlertAction(title: "Ok", style: .default) { (action) in
                        //                            AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                        //                        }
                        return
                    }
                    
                    self.filteredCompanyContact = (object?.output ?? []).sorted()
                    
                    let filter = self.filteredCompanyContact.filter { (user) -> Bool in
                        if user.mobileNo ?? "" == phoneNumber {
                            
                            return true
                        }
                        return false
                    }.first
                    
                    AppDelegate.shared.callingToName = filter?.name ?? phoneNumber
                    vc.callCameFromAppdelegate(callerID: phoneNumber, callerName: filter?.name ?? phoneNumber)
                    
                case .failure( _):
                    print("Something went wrong !!")
                }
            }
        }
    }
    
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "AgoraARKit")
        
        //        let description = NSPersistentStoreDescription()
        //        description.shouldMigrateStoreAutomatically = true
        //        description.shouldInferMappingModelAutomatically = true
        //        container.persistentStoreDescriptions = [description]
        //
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
}
extension AppDelegate: LocationManagerDelegate {

    func getLocation(location: CLLocation) {
        AppGlobalManager.sharedInstance.latitude = location.coordinate.latitude
        AppGlobalManager.sharedInstance.longitude = location.coordinate.longitude
              
        geoCoder.reverseGeocodeLocation(location) { placemarks, _ in
            if let place = placemarks?.first {
                let currentCityName : String = place.locality ?? ""
                let streetName : String = place.thoroughfare ?? ""
                let nameofplace : String = place.name ?? ""
                let administrativeArea : String = place.administrativeArea ?? ""
                let postalCode : String = place.postalCode ?? ""
                
                let combineString = ("\(currentCityName),\(streetName),\(nameofplace),\(administrativeArea),\(postalCode)")
                
                AppGlobalManager.sharedInstance.locationString = combineString
            }
        }
    }
}

//--------------------------------------------------------------------------------

//MARK:-  CXProvider Delegate Methdos

//--------------------------------------------------------------------------------






extension AppDelegate {

    func setupNavbarAndTabbar() {
        
        
        if #available(iOS 13.0, *) {
            let coloredAppearance = UINavigationBarAppearance()
            coloredAppearance.configureWithOpaqueBackground()
            coloredAppearance.backgroundColor = UIColor(named: "PrimaryDark")
            coloredAppearance.titleTextAttributes = [NSAttributedString.Key.font:Constants.AppTheme.Fonts.font(type: .FONT_BOLD, size: 19) ,NSAttributedString.Key.foregroundColor:UIColor.white]
            coloredAppearance.largeTitleTextAttributes = [NSAttributedString.Key.font:Constants.AppTheme.Fonts.font(type: .FONT_BOLD, size: 34) ,NSAttributedString.Key.foregroundColor:UIColor.white]
            UINavigationBar.appearance().standardAppearance = coloredAppearance
            UINavigationBar.appearance().scrollEdgeAppearance = coloredAppearance
            
            
            UITabBar.appearance().tintColor = .white
            UITabBar.appearance().barTintColor = UIColor(named: "PrimaryDark")
            UITabBar.appearance().isOpaque = false
            
            
            UITabBar.appearance().backgroundImage = UIImage.init(color: UIColor(named: "PrimaryDark")!, size: CGSize(width: 1, height: 1))
        } else {
            
            // Fallback on earlier versions
            
            
            
            UINavigationBar.appearance().isOpaque = true
            UINavigationBar.appearance().isTranslucent = false
            UINavigationBar.appearance().barTintColor = UIColor(named: "PrimaryDark")
            UINavigationBar.appearance().tintColor = UIColor.white
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font:Constants.AppTheme.Fonts.font(type: .FONT_BOLD, size: 19) ,NSAttributedString.Key.foregroundColor:UIColor.white]
            
            UINavigationBar.appearance().largeTitleTextAttributes   = [NSAttributedString.Key.font:Constants.AppTheme.Fonts.font(type: .FONT_BOLD, size: 34) ,NSAttributedString.Key.foregroundColor:UIColor.white]
            
            
            
            UITabBar.appearance().tintColor = .white
            UITabBar.appearance().barTintColor = UIColor(named: "PrimaryDark")
            UITabBar.appearance().isOpaque = true
            //   UITabBar.appearance().isTranslucent = false
            
            UITabBar.appearance().backgroundImage = UIImage.init(color: UIColor(named: "PrimaryDark")!, size: CGSize(width: 1, height: 1))
        }
        
    }
    

}



extension UIApplicationShortcutItem {
    
    static var callNowItem : UIApplicationShortcutItem {
        let item = UIMutableApplicationShortcutItem(type: "Make a call", localizedTitle: "Make a call")
        item.icon = UIApplicationShortcutIcon(type: .contact)
        
        return item
    }
    
    
    static func == (lhs: UIApplicationShortcutItem, rhs: UIApplicationShortcutItem) -> Bool{
        return lhs.hashValue == rhs.hashValue
        
    }
    
}

//--------------------------------------------------------------------------------

//MARK:-  UNUserNotificationCenterDelegate Methdos

//--------------------------------------------------------------------------------


extension AppDelegate: UNUserNotificationCenterDelegate {
    //MARK: - UNUserNotificationCenterDelegate methods
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("\n\ntoken :: \(token) \n\n")
        
        UserDefaults.standard.set(token, forKey: "apnsToken")
        
        //
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("\n\n willPresent :: \(notification.request.content.userInfo) \n\n")
            print("response from notificaition %@",notification)
        
            
            let channel_join_state = notification.request.content.userInfo["channel_join_state"] as? String ?? ""
            
            if channel_join_state == Constants.channelJoinState.ChannelJoinSendByReceiver.rawValue{
                if(self.status == .ideal){
                    //code sender
                    HandleUniversalLink.sharedInstance.callJoinSessionFromAPP_API(sessionId: notification.request.content.userInfo["room_name"] as? String ?? "", mobileNumber: notification.request.content.userInfo["call_frm"] as? String ?? "")
                    
                }else if(self.status == .inCall){
                    
                }
            }
            else if channel_join_state == Constants.channelJoinState.ChannelJoinedBySender.rawValue{
//                if(self.status == .ideal){

                    let user_call_id = notification.request.content.userInfo["user_call_id"] as? String ?? ""
                    
                    AppGlobalManager.sharedInstance.user_call_id_Global = Int(user_call_id)
                                
                let senderWidth = notification.request.content.userInfo["sender_width"] as? String
               let senderheight = notification.request.content.userInfo["sender_height"] as? String

                if let doubleValuewidth = Double(senderWidth ?? "") {
                    AppGlobalManager.sharedInstance.SenderWidth = CGFloat(doubleValuewidth)
                }
                
                if let doubleValueheight = Double(senderheight ?? "") {
                    AppGlobalManager.sharedInstance.SenderHeight = CGFloat(doubleValueheight)
                }
                print(AppGlobalManager.sharedInstance.SenderHeight)
                print(AppGlobalManager.sharedInstance.SenderWidth)
                
                    let callUUID:UUID = UUID()
                    
                    let callInfo = CallInfo(callFrom: notification.request.content.userInfo["call_frm"] as? String ?? "",
                                            callerName: "",
                                            callTo: "", roomName: notification.request.content.userInfo["room_name"] as? String ?? "",
                                            callDate: "\(Date())",
                        callIdentifier: "", chatId : notification.request.content.userInfo["chat_id"] as? String ?? "",
                        callUUID: callUUID)
                    
                    AppGlobalManager.sharedInstance.incommingCall.append(callInfo)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute:  {
                        if UserDefaults.standard.string(forKey: "USER_PHONE") != nil {
                            
                            self.status = .inCall
                            
                            let roomname =  notification.request.content.userInfo["room_name"] as? String ?? ""
                            
                            let callUUID:UUID = UUID()
                            
                            let call = Call(uuid: callUUID, outgoing: false, handle: notification.request.content.userInfo["call_frm"] as? String ?? "" , callName: notification.request.content.userInfo["caller_name"] as? String ?? "Guest", date: Date())

                        
                            let vc = ARController.viewController()
                            vc.modalPresentationStyle = .fullScreen
                            vc.call = call
                            
                            vc.roomName = roomname
                            //                            AppDelegate.shared.window?.rootViewController?.present(vc, animated: true, completion: nil)
                            
                            if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
                            
                                let vcTab = TabbarController.viewController()
                                vcTab.selectedIndex = 3
                                (vcTab.children.first as? UINavigationController)?.viewControllers[0] = vc
                                vcTab.tabBar.isHidden = true
                                (vcTab.children.first as? UINavigationController)?.navigationBar.isHidden = true
                                vcTab.navigationController?.navigationBar.isHidden = true
                                AppDelegate.shared.window?.rootViewController = vcTab
                                
                                SocketIOManager.shared.closeConnection()
                                SocketIOManager.shared.isSocketOn = false
                                SocketIOManager.shared.establishConnection()
                                                                
                            }else{
                                    AppDelegate.shared.window?.rootViewController?.present(vc, animated: true, completion: nil)
                            }

                        } else {
                            let vc = LoginVC.viewController()
                            AppDelegate.shared.window?.rootViewController = vc
                            
                        }
                    })

            }

        completionHandler([.badge, .sound])

    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("response from notificaition %@",response)
        print("\(response.notification.request.content.userInfo)")
        
        //  chat notification
        let senderId = response.notification.request.content.userInfo["google.c.sender.id"] as? String ?? ""
        let msgId = response.notification.request.content.userInfo["gcm.message_id"] as? String ?? ""
        let msgData = (response.notification.request.content.userInfo["aps"] as? [String:Any] ?? [:])["alert"] as? [String:Any] ?? [:]
        let chatId = response.notification.request.content.userInfo["chat_id"] as? String ?? ""
        let name = response.notification.request.content.userInfo["name"] as? String ?? ""
        let id = response.notification.request.content.userInfo["id"] as? String ?? ""
        let mobileNo = response.notification.request.content.userInfo["mobile_no"] as? String ?? ""
        let type = response.notification.request.content.userInfo["type"] as? String ?? ""
        let email = response.notification.request.content.userInfo["email"] as? String ?? ""
        
        if type.lowercased() == "chat" {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                let tabbarVC = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as! TabbarController
                tabbarVC.selectedIndex = 3
                if let chatUserListVC = ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as! TabbarController).children[3].children.first as? ChatUsersListViewController {
                    //      chatUserListVC.chatHistory()
                    //      DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    //        let currentObj = chatUserListVC.aryFilterChatData.filter{$0.message?.sender == senderId}
                    //      }
                    
                    var param = [String:Any]()
                    param["from"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
                    param["to"] = mobileNo
                    chatUserListVC.socketEmiter(key: Constants.SocketKeys.isOnline, param: param)
                           
                    let intId = Int(id) ?? 0
                    if let chatVC = ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.children[3] as? UINavigationController)?.visibleViewController as? ChatViewController //?.receiverMobileNumber
                    {
                        
                        if chatVC.receiverMobileNumber == mobileNo {
                            chatVC.chatUserHistory(chatId: chatId, pageNo: 1,isFromNotification: true)
                        }
                        else {
                            chatVC.chatId = chatId
                            chatVC.receiverMobileNumber = mobileNo
                            chatVC.receiverName = name
                            chatVC.receiverId = id
                            chatVC.receiverUser = User(id: intId, name: name, email: email, is_guest: false, mobileNo: mobileNo, department: "", company: nil, chatId: chatId)
                            chatUserListVC.receiverMobileNumber = mobileNo
                            chatUserListVC.receiverId = id
                            chatUserListVC.receiverName = name
                            chatUserListVC.receiverUser = User(id: intId, name: name, email: email, is_guest: false, mobileNo: mobileNo, department: "", company: nil, chatId: chatId)
                            AppGlobalManager.sharedInstance.receiverUser = User(id: intId, name: name, email: email, is_guest: false, mobileNo: mobileNo, department: "", company: nil, chatId: chatId)
                            chatVC.isBackFromChatScreen = { (state) in
                                chatUserListVC.receiverMobileNumber = ""
                                chatUserListVC.receiverId = ""
                                chatUserListVC.receiverName = ""
                                chatUserListVC.receiverUser = nil
                                chatUserListVC.tableView?.reloadData()
                            }
                            chatVC.viewDidLoad()
                            chatVC.viewWillAppear(true)
                        }
                    }
                    else {
                        let chat = ChatViewController.viewController()
                        chat.chatId = chatId
                        chat.receiverMobileNumber = mobileNo
                        chat.receiverName = name
                        chat.receiverId = id
                        chat.receiverUser = User(id: intId, name: name, email: email, is_guest: false, mobileNo: mobileNo, department: "", company: nil, chatId: chatId)
                        chatUserListVC.receiverMobileNumber = mobileNo
                        chatUserListVC.receiverId = id
                        chatUserListVC.receiverName = name
                        chatUserListVC.receiverUser = User(id: intId, name: name, email: email, is_guest: false, mobileNo: mobileNo, department: "", company: nil, chatId: chatId)
                        AppGlobalManager.sharedInstance.receiverUser = User(id: intId, name: name, email: email, is_guest: false, mobileNo: mobileNo, department: "", company: nil, chatId: chatId)
                        chat.isBackFromChatScreen = { (state) in
                            chatUserListVC.receiverMobileNumber = ""
                            chatUserListVC.receiverId = ""
                            chatUserListVC.receiverName = ""
                            chatUserListVC.receiverUser = nil
                            chatUserListVC.tableView?.reloadData()
                        }
                        chatUserListVC.navigationController?.pushViewController(chat, animated: true)
                    }
                }
            }
        }
        
        let channel_join_state = response.notification.request.content.userInfo["channel_join_state"] as? String ?? ""
        
        if channel_join_state == Constants.channelJoinState.ChannelJoinSendByReceiver.rawValue{
            if(self.status == .ideal){
                let activityData = ActivityData()
                NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
                
                HandleUniversalLink.sharedInstance.callJoinSessionFromAPP_API(sessionId: response.notification.request.content.userInfo["room_name"] as? String ?? "", mobileNumber: response.notification.request.content.userInfo["call_frm"] as? String ?? "")
                
            }else if(self.status == .inCall){
                
            }
        }
        else if channel_join_state == Constants.channelJoinState.ChannelJoinedBySender.rawValue{
            //            if(self.status == .ideal){
            let user_call_id = response.notification.request.content.userInfo["user_call_id"] as? String ?? ""
            
            AppGlobalManager.sharedInstance.user_call_id_Global = Int(user_call_id)
            
            AppGlobalManager.sharedInstance.SenderWidth = response.notification.request.content.userInfo["sender_width"] as? CGFloat ?? 0.0
            AppGlobalManager.sharedInstance.SenderHeight = response.notification.request.content.userInfo["sender_height"] as? CGFloat ?? 0.0

            print(response.notification.request.content.userInfo["sender_width"] as? CGFloat ?? 0.0)
            print(response.notification.request.content.userInfo["sender_height"] as? CGFloat ?? 0.0)

            let callUUID:UUID = UUID()
            
            let callInfo = CallInfo(callFrom: response.notification.request.content.userInfo["call_frm"] as? String ?? "",
                                    callerName: "",
                                    callTo: "", roomName: response.notification.request.content.userInfo["room_name"] as? String ?? "",
                                    callDate: "\(Date())",
                callIdentifier: "", chatId : response.notification.request.content.userInfo["chat_id"] as? String ?? "",
                callUUID: callUUID)
            
            AppGlobalManager.sharedInstance.incommingCall.append(callInfo)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute:  {
                if UserDefaults.standard.string(forKey: "USER_PHONE") != nil {
                    
                    self.status = .inCall
                    
                    let roomname =  response.notification.request.content.userInfo["room_name"] as? String ?? ""
                    
                    let callUUID:UUID = UUID()
                    
                    let call = Call(uuid: callUUID, outgoing: false, handle: response.notification.request.content.userInfo["call_frm"] as? String ?? "" , callName: response.notification.request.content.userInfo["caller_name"] as? String ?? "Guest", date: Date())
                    
                    let vc = ARController.viewController()
                    vc.modalPresentationStyle = .fullScreen
                    vc.call = call
                    vc.roomName = roomname
                    AppDelegate.shared.window?.rootViewController?.present(vc, animated: true, completion: nil)
                    
                } else {
                    let vc = LoginVC.viewController()
                    AppDelegate.shared.window?.rootViewController = vc
                }
            })
        }
    }
        
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            
            //               print("\n\n didReceive :: \(response.notification.request.content) \n\n")
            print("User info %@",userInfo)
            
            let call = CallInfo(callFrom: userInfo["call_frm"] as? String ?? "", callerName: userInfo["caller_name"] as? String ?? "", callTo: "", roomName: userInfo["room_name"] as? String ?? "", callDate: userInfo["call_date"] as? String ?? "", callIdentifier: "", callUUID: UUID())

            let callState = userInfo["user_call_state"] as? String ?? ""
            
            if AppGlobalManager.sharedInstance.incommingCall.first?.callFrom != call.callFrom {
                completionHandler(.newData)
                return
            }
                switch UIApplication.shared.applicationState {
                case .background, .inactive:
                    if userInfo["log_in_oth_dvc"] as? String ?? "" != "" {
                        UtilityClass.showAlert(title: "ARMS", message: userInfo["log_in_oth_dvc"] as? String ?? "logged in another device") { (act) in
                            AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                        }
                        completionHandler(.newData)
                        return
                    }                                             
                    var bgTask = UIBackgroundTaskIdentifier.init(rawValue: 0)
                    
                    bgTask = application.beginBackgroundTask(withName:"MyBackgroundTask1", expirationHandler: {() -> Void in
                        // Do something to stop our background task or the app will be killed
                        application.endBackgroundTask(bgTask)
                        bgTask = UIBackgroundTaskIdentifier.invalid
                    })
                    if callState == Constants.callInviteState.callAccepted.rawValue {
                        NotificationCenter.default.post(name: .callInviteAccepted, object: nil, userInfo: userInfo)
                    } else if callState == Constants.callInviteState.callRejected.rawValue {
                        AppGlobalManager.sharedInstance.isRejectedBy = false
                        status = .ideal
                        pushManager.status = .ideal
                        let uuid = AppGlobalManager.sharedInstance.incommingCall.last?.callUUID ?? UUID()
                        pushManager.provider.reportCall(with: uuid, endedAt: nil, reason: .remoteEnded)
                        let callUUID:UUID = UUID()
                        let call = Call(uuid: callUUID, handle:userInfo["call_frm"] as? String ?? "" , callName: userInfo["caller_name"] as? String ?? "", date: Date())
                        CoreDataManager.saveCall(call: call, duration: 0)
                        NotificationCenter.default.post(name: .callInviteRejected, object: nil, userInfo: userInfo)
                    } else if callState == Constants.callInviteState.callMiscall.rawValue {
                        AppGlobalManager.sharedInstance.isRejectedBy = false
                        NotificationCenter.default.post(name: .callInviteMiscall, object: nil, userInfo: userInfo)
                    } else if callState == Constants.callInviteState.callRinging.rawValue{
                        AppGlobalManager.sharedInstance.isRejectedBy = false
                        NotificationCenter.default.post(name: .callInviteRinging, object: nil, userInfo: userInfo)
                    }  else if callState == Constants.callInviteState.callBusy.rawValue{
                        AppGlobalManager.sharedInstance.isRejectedBy = false
                        NotificationCenter.default.post(name: .callInviteBusy, object: nil, userInfo: userInfo)
                    }
                    
                    break
                // background
                case .active:
                    if userInfo["log_in_oth_dvc"] as? String ?? "" != "" {
                        UtilityClass.showAlert(title: "ARMS", message: userInfo["log_in_oth_dvc"] as? String ?? "logged in another device") { (act) in
                            AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                        }
                        completionHandler(.newData)
                        return
                    }
                    if callState == Constants.callInviteState.callAccepted.rawValue {
                        NotificationCenter.default.post(name: .callInviteAccepted, object: nil, userInfo: userInfo)
                    } else if callState == Constants.callInviteState.callRejected.rawValue {
                        AppGlobalManager.sharedInstance.isRejectedBy = false
                        status = .ideal
                        pushManager.status = .ideal
                        let uuid = AppGlobalManager.sharedInstance.incommingCall.last?.callUUID ?? UUID()
                        pushManager.provider.reportCall(with: uuid, endedAt: nil, reason: .remoteEnded)
                        //            let callUUID:UUID = UUID()
                        //            let call = Call(uuid: callUUID, handle:userInfo["call_frm"] as? String ?? "" , callName: userInfo["caller_name"] as? String ?? "", date: Date())
                        //            CoreDataManager.saveCall(call: call, duration: 0)
                        NotificationCenter.default.post(name: .callInviteRejected, object: nil, userInfo: userInfo)
                    } else if callState == Constants.callInviteState.callMiscall.rawValue {
                        AppGlobalManager.sharedInstance.isRejectedBy = false
                        NotificationCenter.default.post(name: .callInviteMiscall, object: nil, userInfo: userInfo)
                    } else if callState == Constants.callInviteState.callRinging.rawValue{
                        AppGlobalManager.sharedInstance.isRejectedBy = false
                        NotificationCenter.default.post(name: .callInviteRinging, object: nil, userInfo: userInfo)
                    }
                    else if callState == Constants.callInviteState.callBusy.rawValue{
                        AppGlobalManager.sharedInstance.isRejectedBy = false
                        NotificationCenter.default.post(name: .callInviteBusy, object: nil, userInfo: userInfo)
                    }
                    
                    break
                // foreground
                default:
                    break
                }
                
            completionHandler(.newData)
        }
        
        
        func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
            print("Firebase registration token: \(fcmToken)")
            
            let dataDict:[String: String] = ["token": fcmToken]
            
            UserDefaults.standard.set(fcmToken, forKey: "fcmToken")
            NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
            // TODO: If necessary send token to application server.
            // Note: This callback is fired at each app startup and whenever a new token is generated.
        }
}

