//
//  LocationHelper.swift
//  AgoraARKit
//
//  Created by Hetali on 09/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import CoreLocation

@objc protocol LocationManagerDelegate {
    @objc optional func getLocation(location: CLLocation)
}

class LocationHelper: NSObject, CLLocationManagerDelegate {

    weak var locationManagerDelegate: LocationManagerDelegate?
    var isLocationfetched: Bool = false
    
    let geoCoder = CLGeocoder()

    var lastKnownLocation: CLLocation? {
        get {
            let latitude = UserDefaults.standard.double(forKey: "Latitude")
            let longitude = UserDefaults.standard.double(forKey: "Longitude")

            if latitude.isZero || longitude.isZero {
                return nil
            }
            return CLLocation(latitude: latitude, longitude: longitude)
        }
        set {
            UserDefaults.standard.set(newValue?.coordinate.latitude ?? 0, forKey: "Latitude")
            UserDefaults.standard.set(newValue?.coordinate.longitude ?? 0, forKey: "Longitude")
            UserDefaults.standard.synchronize()
        }
    }

    struct SharedInstance {
        static let instance = LocationHelper()
    }

    class var shared: LocationHelper {
        return SharedInstance.instance
    }

    enum Request {
        case requestWhenInUseAuthorization
        case requestAlwaysAuthorization
    }

    var clLocationManager = CLLocationManager()

    func setAccuracy(clLocationAccuracy: CLLocationAccuracy) {
        clLocationManager.desiredAccuracy = clLocationAccuracy
    }

    var isLocationEnable: Bool = false {
        didSet {
            if !isLocationEnable {
                lastKnownLocation = nil
            }
        }
    }
    func startUpdatingLocation() {
        isLocationfetched = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
               print("NotDetermined")
                clLocationManager.delegate = self
                clLocationManager.requestWhenInUseAuthorization()
                clLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                clLocationManager.startUpdatingLocation()
                isLocationEnable = false
               let permission: [String:Bool] = ["permission": false]
               NotificationCenter.default.post(name: .locationPermission, object: permission)
            case .restricted, .denied:
               print("Restricted")
                showLocationAccessAlert()
                isLocationEnable = false
               let permission: [String:Bool] = ["permission": false]
               NotificationCenter.default.post(name: .locationPermission, object: permission)
            case .authorizedAlways, .authorizedWhenInUse:
               print("AuthorizedWhenInUse")
                self.clLocationManager.delegate = self
                self.clLocationManager.startUpdatingLocation()
                isLocationEnable = true
               let permission: [String:Bool] = ["permission": true]
               NotificationCenter.default.post(name: .locationPermission, object: permission)
            default:
                print("Invalid AuthorizationStatus")
            }
        } else {
            isLocationEnable = false
            showLocationAccessAlert()
        }
    }
    func showLocationAccessAlert() {
            let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings. Go to Settings -> Plutomen ARMS -> Location -> Allow Always", preferredStyle: UIAlertController.Style.alert)
//            let okAction = UIAlertAction(title: "settings", style: .default, handler: {(cAlertAction) in
//                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
//            })
            let cancelAction = UIAlertAction(title: "cancel", style: UIAlertAction.Style.cancel)
            alertController.addAction(cancelAction)
//            alertController.addAction(okAction)
            let appdelegate = UIApplication.shared.delegate as? AppDelegate
            appdelegate?.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }

        func stopUpdatingLocation() {
            self.clLocationManager.stopUpdatingLocation()
        }

        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            if !isLocationfetched {
                isLocationfetched = true
                clLocationManager.startMonitoringSignificantLocationChanges()
            }
            let userLocation = locations[0] as CLLocation
            self.lastKnownLocation = userLocation
            if let delegate = self.locationManagerDelegate {
                delegate.getLocation!(location: userLocation)
            }
        }

        func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            if (status == CLAuthorizationStatus.denied) {
                // The user denied authorization
                print("Denied")
                isLocationEnable = false
                let permission: [String:Bool] = ["permission": false]
                NotificationCenter.default.post(name: .locationPermission, object: permission)
            } else if (status == CLAuthorizationStatus.authorizedWhenInUse) {
                // The user accepted authorization
                print("AuthorizedWhenInUse")
                self.clLocationManager.delegate = self
                self.clLocationManager.startUpdatingLocation()
                isLocationEnable = true
                let permission: [String:Bool] = ["permission": true]
                NotificationCenter.default.post(name: .locationPermission, object: permission)
            } else if (status == CLAuthorizationStatus.authorizedAlways) {
                print("AuthorizedAlways")
                isLocationEnable = true
                self.clLocationManager.delegate = self
                self.clLocationManager.startUpdatingLocation()
                let permission: [String:Bool] = ["permission": true]
                NotificationCenter.default.post(name: .locationPermission, object: permission)
            } else if (status == CLAuthorizationStatus.notDetermined) {
                print("NotDetermined")
                let permission: [String:Bool] = ["permission": false]
                NotificationCenter.default.post(name: .locationPermission, object: permission)
                
            } else if (status == CLAuthorizationStatus.restricted) {
                print("Restricted")
                let permission: [String:Bool] = ["permission": false]
                NotificationCenter.default.post(name: .locationPermission, object: permission)
                isLocationEnable = false
            } else if (status == CLAuthorizationStatus.authorized) {
                print("Authorized")
                self.clLocationManager.delegate = self
                self.clLocationManager.startUpdatingLocation()
                isLocationEnable = true
                let permission: [String:Bool] = ["permission": true]
                NotificationCenter.default.post(name: .locationPermission, object: permission)
            }
        }

        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("\n error description for location updation:- \(error.localizedDescription)")
        }
    
}
