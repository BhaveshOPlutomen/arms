//
//  CodableHelper.swift
//  AgoraARKit
//
//  Created by Prashant on 08/08/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit

class CommonResponse<T:Codable> :Codable {
    var status:String?
    var statusCode:Int?
    var message:String?
    var messageCode:String?
    var output:T?
}



final class CommonCodableHelper<T:Codable>: NSObject {
    
    
    
    class func codableObject(from json:[String:Any]) -> CommonResponse<T>? {
        do {
            
            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            
            let object =  try JSONDecoder().decode(CommonResponse<T>.self , from: data)
            return object
            
        } catch {
            print("Exception in parsing json to codable \(error)")
        }
        
        return nil
    }
    
    
    class func codableObject(from data:Data) -> CommonResponse<T>? {
        do {
            
            let object =  try JSONDecoder().decode(CommonResponse<T>.self , from: data)
            return object
            
        } catch {
            print("Exception in parsing json to codable \(error)")
        }
        
        return nil
    }
    
    
    
}

struct TTTT:Codable {
    var test:Int?
}

struct Test {
    
    init() {
        
    
        
    }
    
    func test () {
        
        let test = CommonCodableHelper<TTTT>.codableObject(from: ["test":123])
        
    }
    
}

