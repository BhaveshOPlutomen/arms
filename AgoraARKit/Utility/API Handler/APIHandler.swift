//
//  APIHandler.swift
//  AgoraARKit
//
//  Created by Prashant on 29/04/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class APIHandler:NSObject {
    
    enum Method:String {
        case GET = "GET"
        case POST = "POST"
    }
    
    static let header: [String: String] = ["Authorization": "Bearer \(AppGlobalManager.sharedInstance.token ?? "")"]
    
    class func callAPI(endPoint:String,parameters:[String:Any]?,token:String? = nil,method:Method,completion:((Result<Data, Error>) -> ())?) {
        
        
        if UtilityClass.checkInternetConnection(completionHandler: { (act) in
          callAPI(endPoint: endPoint, parameters: parameters, token: token, method: method, completion: completion)
//            callAPI(endPoint: endPoint, parameters: parameters, method: method, completion: completion)
        }) {
          return
        }
//        let urlString = endPoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

        let urlstring = APIConstants.apiURL + endPoint
        
        let stringtest = urlstring.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        //previously
//        let url = URL(string: APIConstants.apiURL)!.appendingPathComponent(endPoint)

//        let url = URL(string: APIConstants.apiURL)!.appendingPathComponent(urlString!)
        
        let url = URL(string: stringtest)
        
        print(url)
        
        var request = URLRequest(url: url!)
        request.httpMethod = method.rawValue
        
        if let parameters = parameters {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        if let token = token  {
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        
        let date = Date()

        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            print(Date().timeIntervalSince(date))

            if let error = error {
                completion?(.failure(error))
                return
            }
            
            if let data = data {
                completion?(.success(data))
                return
            }
            
        }.resume()
        
    }
    //MARK: - Mutipart Method
        
        class func postDataWithImage(parameter dictParams: [String:Any], image: UIImage?, imageUrl: URL?, videoUrl: URL?, fileUrl: URL?, fileType: FileType, fileParamName: String, imageName: String, nsURL: String, ext: String, completion: @escaping (_ result: Any, _ success: Bool) -> Void) {
            
            let urlstring = APIConstants.apiURL + nsURL
            let url = urlstring
            
    //        let stringtest = urlstring.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    //        let tempURL = URL(string: stringtest)

            
            if UtilityClass.checkInternetConnection(completionHandler: { (act) in
                postDataWithImage(parameter: dictParams, image: image, imageUrl: imageUrl, videoUrl: videoUrl, fileUrl: fileUrl, fileType: fileType, fileParamName: fileParamName, imageName: imageName, nsURL: nsURL, ext: ext, completion: completion)
            }) {
                return
            }
            
            AF.upload(multipartFormData: { (multipartFormData) in
                
                if fileType == .image {
                    
                    if image != nil {
                        
                        if let imageData = image?.jpegData(compressionQuality: 0.5) {
                            //                 multipartFormData.append(imageData, withName: fileParamName, fileName: "\(imageName).jpg", mimeType: "image/jpg")
                            let fNm = String.random(length: 10)
                            multipartFormData.append(imageData, withName: fileParamName, fileName: "\(fNm).\(ext)", mimeType: "image/\(ext == "" ? "jpg" : "\(ext)")" )
                        }
                        //                        for ind in 0..<(image ?? []).count {
                        //                            if let imageData = image?[ind].jpegData(compressionQuality: 0.5) {
                        //                                //                 multipartFormData.append(imageData, withName: fileParamName, fileName: "\(imageName).jpg", mimeType: "image/jpg")
                        //                                multipartFormData.append(imageData, withName: fileParamName, fileName: "\(imageName)\(ind).\(ext)", mimeType: "image/\(ext == "" ? "jpg" : "\(ext)")" )
                        //                            }
                        //                        }
                    }
                    else {
                        multipartFormData.append(imageUrl!, withName: fileParamName, fileName: "\(imageName).\(ext)", mimeType: "application/\(ext)")
                    }
                    
                }
                else if fileType == .file {
                    multipartFormData.append(fileUrl!, withName: fileParamName, fileName: "\(imageName).\(ext)", mimeType: "application/\(ext)")
                }
                else if fileType == .video {
                    multipartFormData.append(videoUrl!, withName: fileParamName, fileName: "\(imageName).\(ext)", mimeType: "video/\(ext)")
                }
                
                
                for (key, value) in dictParams {
                    if JSONSerialization.isValidJSONObject(value) {
                        let array = value as! [String]
                        
                        for string in array {
                            if let stringData = string.data(using: .utf8) {
                                multipartFormData.append(stringData, withName: key+"[]")
                            }
                        }
                    } else {
                        multipartFormData.append(String(describing: value).data(using: .utf8)!, withName: key)
                    }
                }
            }, to: url, usingThreshold: 10 * 1024 * 1024, method: .post, headers: HTTPHeaders(header))
            .uploadProgress(closure: { (progress) in
                            //Current upload progress of file
                            print("Upload Progress: \(progress.fractionCompleted)")
                            completion(progress, true)
                        })
            .response { response in
                
                switch response.result {
                case .success(let dt) :
                    do {
                        let res = try JSONSerialization.jsonObject(with: dt!, options: .mutableLeaves)
                        debugPrint(res)
                        completion(res, true)
                    }catch {
                        debugPrint(response.error?.localizedDescription as Any)
                        completion(response.error?.localizedDescription as Any, false)
                    }
                    
                case .failure(_):
                    debugPrint(response.error?.localizedDescription as Any)
                    completion(response.error?.localizedDescription as Any, false)
                }
                
            }
            
            //            print("Headers",HTTPHeaders(self.header))
            //                     switch encodingResult
            //                     {
            //
            //
            //                     case .success(let upload, _, _):
            //     //                    print(upload.responseString)
            //
            //                         upload.responseString(completionHandler: { (response) in
            //                             print(response)
            //                         })
            //
            //
            //                         request =  upload.responseJSON {
            //                             response in
            //
            //                             if let json = response.result.value {
            //
            //                                 if (json as AnyObject).object(forKey:("status")) as! Bool == false {
            //
            //                                     let resJson = JSON(json)
            //                                     if let result = resJson.dictionaryObject, let errormsg = result["error"] as? String, errormsg == "Invalid API key " {
            //                                         Appdel.ForceLogout()
            //                                     }
            //                                     completion(resJson, false)
            //
            //                                     LoaderClass.hideActivityIndicator()
            //                                     UtilityClass.hideHUD()
            //                                 }
            //                                 else {
            //                                     let resJson = JSON(json)
            //                                     completion(resJson, true)
            //
            //                                    LoaderClass.hideActivityIndicator()
            //                                     UtilityClass.hideHUD()
            //                                 }
            //                             }
            //                             else {
            //
            //                                 LoaderClass.hideActivityIndicator()
            //                                 UtilityClass.hideHUD()
            //                                 if let error = response.result.error {
            //                                     print("Error = \(error.localizedDescription)")
            //                                     guard let controller = UIApplication.topViewController() else{
            //                                         return
            //                                     }
            //                                     controller.showAlert(message: error.localizedDescription)
            //                                 }
            //
            //                             }
            //
            //
            //                             LoaderClass.hideActivityIndicator()
            //                             UtilityClass.hideHUD()
            //
            //                         }
            //                     case .failure( _):
            //                         print("failure")
            //                         break
            //                     }
            //                 }
            
        }
        

    
}


extension APIHandler {
    
    class func registerDeviceToken(userID:String,completion:((Result<Data, Error>) -> ())?) {
        let parameters = [
            "phone":userID,
            "token":UserDefaults.standard.object(forKey: "TOKEN") ?? "",
            "device_type":"0"
            
        ]
        
        
        callAPI(endPoint: APIConstants.EndPoints.registerDeviceToken, parameters: parameters,token:AppGlobalManager.sharedInstance.loggedInUser?.token , method: .POST, completion: completion)
        
    }
    
    
}

extension String {

    static func random(length: Int = 20) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""

        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
}
