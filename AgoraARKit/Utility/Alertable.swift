//
//  Alertable.swift
//  IMMS_SourcePro
//
//  Created by Prashant on 28/12/18.
//  Copyright © 2018 Prashant. All rights reserved.
//

import Foundation

import UIKit

protocol Alertable {}

extension Alertable where Self : UIViewController {
    func showAlert(withMessage message: String,title:String = "ARMS") {
        
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let actionOK = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            alertController.addAction(actionOK)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //----------------------------------------------------------------
    
    func showAlert(withMessage message: String,title:String = "ARMS", customActions actions: [UIAlertAction]) {
        
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            for (_, action) in actions.enumerated() {
                alertController.addAction(action)
            }
            self.present(alertController, animated: true, completion: nil)
        }
    }
}


