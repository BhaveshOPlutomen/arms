//
//  AgoraCallKitManager.swift
//  AgoraARKit
//
//  Created by Prashant on 06/11/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import Foundation
import AgoraRtmKit
import UIKit

//--------------------------------------------------------------------------------

//MARK:-  RTMCallHandler Methdos

//--------------------------------------------------------------------------------



struct CallInfo {
    var callFrom = ""
    var callerName = ""
    var callTo = ""
    var roomName = ""
    var callDate = ""
    var callIdentifier = ""
    var chatId = ""
    var callerUserId = ""
    var callUUID:UUID = UUID()
    
    var date:Date? {
        let dateFormatter  = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.date(from: callDate)
    }
    
    var callDateToCurrentDate:Int {
        let startDate =  date ?? Date()
        let endDate = Date()
        let difference = Calendar.current.dateComponents([.second], from: startDate, to: endDate)
        let duration = difference.second
        return duration ?? -1
    }
}



class RTMCallHandler:RTMManager {
   
  var callInfo:CallInfo = CallInfo()
  var localInvite:AgoraRtmLocalInvitation? = nil
//  var remoteInvite:AgoraRtmRemoteInvitation? = nil
   
  private override init() {
    super.init()
  }
   
  private var rtmCallKit : AgoraRtmCallKit? {
    return kit?.rtmCallKit
  }
   
  init(delegate:AgoraRtmCallDelegate,rtmDelegate:AgoraRtmDelegate? = nil) {
    super.init()
     
    if let delegateRTM = rtmDelegate {
      super.updateRTMKitDelegate(delegate: delegateRTM)
    }
    kit?.getRtmCall()?.callDelegate = delegate
     
    //TEMP
    kit?.agoraRtmDelegate = self
     
  }
   
  func updateCallKitDelegate(delegate:AgoraRtmCallDelegate) {
    kit?.rtmCallKit?.callDelegate = delegate
  }
   
  func sendLocalInvite(to user:String,content:String,completion:@escaping(AgoraRtmLocalInvitationSendBlock)) -> AgoraRtmLocalInvitation {
    
     
    self.localInvite = AgoraRtmLocalInvitation(calleeId: user)
    self.localInvite?.content = content
    rtmCallKit?.send(localInvite!, completion: completion)
    return self.localInvite!
  }
   
  //--------------------------------------------------------------------------------
  func cancelLocalInvite(invite:AgoraRtmLocalInvitation,completion:@escaping(AgoraRtmLocalInvitationCancelBlock)) {
     
    if UtilityClass.checkInternetConnection(completionHandler: { (act) in
      self.cancelLocalInvite(invite: invite, completion: completion)
    }) {
      return
    }
     
    rtmCallKit?.cancel(invite, completion: completion)
  }
   
  //--------------------------------------------------------------------------------
  func acceptRemoteInvite(remoteInvite:AgoraRtmRemoteInvitation?,completion:@escaping(AgoraRtmRemoteInvitationAcceptBlock)) {
    if let remoteInvite = remoteInvite {
      
      if UtilityClass.checkInternetConnection(completionHandler: { (act) in
        self.acceptRemoteInvite(remoteInvite: remoteInvite, completion: completion)
      }) {
        return
      }
       
      rtmCallKit?.accept(remoteInvite, completion: completion)
    } else {
      completion(.invalidAugment)
    }
  }
   
  //--------------------------------------------------------------------------------
   
  func refuseRemoteInvite(remoteInvite:AgoraRtmRemoteInvitation?,completion:@escaping(AgoraRtmRemoteInvitationRefuseBlock)) {
    if let remoteInvite = remoteInvite {
       
      if UtilityClass.checkInternetConnection(completionHandler: { (act) in
        self.refuseRemoteInvite(remoteInvite: remoteInvite, completion: completion)
      }) {
        return
      }
       
      rtmCallKit?.refuse(remoteInvite, completion: completion)
    } else {
      completion(.invalidAugment)
    }
  }
   
  //--------------------------------------------------------------------------------
  func sendPeerMessage(peer:String,message:String) {
     
     
    if UtilityClass.checkInternetConnection(completionHandler: { (act) in
      self.sendPeerMessage(peer: peer, message: message)
    }) {
      return
    }
     
    kit?.send(message.rtmMessage, toPeer: peer, completion: { (bloack) in
      print("sendPeerMessage \(bloack.rawValue)")
    })
  }
   
  //--------------------------------------------------------------------------------
  func resetData() {
    self.callInfo = CallInfo()
    // self.remoteInvite = nil
    self.localInvite = nil
  }
   
  //--------------------------------------------------------------------------------
 
  deinit {
    print("RTM CALLHANDLER DEINIT")
  }
   
}







extension RTMCallHandler : AgoraRtmDelegate {
    func rtmKit(_ kit: AgoraRtmKit, messageReceived message: AgoraRtmMessage, fromPeer peerId: String) {
        print("MEESSAGE RECV \(message)")
    }
    func rtmKit(_ kit: AgoraRtmKit, connectionStateChanged state: AgoraRtmConnectionState, reason: AgoraRtmConnectionChangeReason) {
        print("RTM connection state changed %@",state.rawValue)
        print("RTM connection state reason %@", reason.rawValue)
    }
}
//--------------------------------------------------------------------------------

//MARK:-  RTMChannelManager Methdos

//--------------------------------------------------------------------------------


class RTMChannelManager:RTMManager {
  var currentChannel:String = ""
  var rtmChannel: AgoraRtmChannel?
   var messageListener:((AgoraRtmMessage) -> Void)? = nil
   
  override init() {
    super.init()
    kit?.getRtmCall()?.callDelegate = self
  }
   
  func joinChannel(string:String,completion:@escaping((AgoraRtmJoinChannelBlock))) {
     
    if UtilityClass.checkInternetConnection(completionHandler: { (act) in
      self.joinChannel(string: string, completion: completion)
    }) {
      return
    }
     
    rtmChannel = kit?.createChannel(withId: string, delegate: self)
    rtmChannel?.join(completion: completion)
  }
   
  func leaveChannel(completion:@escaping((AgoraRtmLeaveChannelBlock))) {
     
    if UtilityClass.checkInternetConnection(completionHandler: { (act) in
      self.leaveChannel(completion: completion)
    }) {
      return
    }
     
    rtmChannel?.leave(completion: completion)
  }
   
  public func sendString(string:String,completion:@escaping((AgoraRtmSendChannelMessageBlock))) {
     
    if UtilityClass.checkInternetConnection(completionHandler: { (act) in
      self.sendString(string: string, completion: completion)
    }) {
      return
    }
     
    rtmChannel?.send(string.rtmMessage, completion: completion)
   }
   
}

extension RTMChannelManager:AgoraRtmChannelDelegate,AgoraRtmDelegate,AgoraRtmCallDelegate {
    func channel(_ channel: AgoraRtmChannel, messageReceived message: AgoraRtmMessage, from member: AgoraRtmMember) {
        messageListener?(message)
    }
    
    func channel(_ channel: AgoraRtmChannel, memberJoined member: AgoraRtmMember) {
        print("Member Joined")
    }
    
    func rtmKit(_ kit: AgoraRtmKit, messageReceived message: AgoraRtmMessage, fromPeer peerId: String) {
        print("MESSAGE PEER \(peerId)")
    }
    
    func rtmCallKit(_ callKit: AgoraRtmCallKit, remoteInvitationReceived remoteInvitation: AgoraRtmRemoteInvitation) {
        print("remoteInvitationReceived")
        remoteInvitation.response = Constants.ChannelMessages.busy
        callKit.refuse(remoteInvitation) { [unowned self] (code) in
            print("CALL REFUSE SENT for BUSY",code.rawValue)
            
        }
        return
    }
    
    
        
}

extension String {
    var rtmMessage :  AgoraRtmMessage {
        return AgoraRtmMessage(text: self)
    }
}

//--------------------------------------------------------------------------------

//MARK:-  RTM Manager Methdos

//--------------------------------------------------------------------------------


enum LoginStatus {
    case login(phoneNumber:String)
    case logout
}

class RTMManager:NSObject {
   
    fileprivate let kit = AgoraRtmKit(appId: KeyCenter.AppId, delegate: nil)
    
//   fileprivate let kit = AgoraRtmKit(appId: UserDefaults.standard.string(forKey: "AgoraAppId")!, delegate: nil)
    var loginStatus = LoginStatus.logout
    
    var rtmLoginCounter : Int = 0

    //--------------------------------------------------------------------------------

    
    override init() {
        super.init()
    }
    
    //--------------------------------------------------------------------------------

    
    func updateRTMKitDelegate(delegate:AgoraRtmDelegate) {
        kit?.agoraRtmDelegate = delegate

    }
    
    //--------------------------------------------------------------------------------

    
  public func login(phoneNumber:String,completion:@escaping( (AgoraRtmLoginErrorCode) -> Void) ) {
      if UtilityClass.checkInternetConnection(completionHandler: { (act) in
        self.login(phoneNumber: phoneNumber, completion: completion)
      }) {
        return
      }
        print("login(phoneNumber: ==> \(phoneNumber)")
        kit?.login(byToken: nil, user: phoneNumber, completion: {[unowned self] (errorCode) in
          print("rtmlogin counter", self.rtmLoginCounter)
          print("AGORA RTM LOGIN ==> \(errorCode.rawValue)")
          if errorCode.rawValue == 9 {
              if self.rtmLoginCounter <= 5{
  //                if self.rtmLoginCounter == 0{
                  self.rtmLoginCounter = self.rtmLoginCounter + 1
//                  print("rtmlogin counter", self.rtmLoginCounter)
//                  print("After error login(phoneNumber: ==> \(phoneNumber)")
  //                completion(AgoraRtmLoginErrorCode(rawValue: 1001)!)
                  self.login(phoneNumber: phoneNumber, completion: completion)
              }else{
                  self.rtmLoginCounter = 0
//                  print("RTM login counter reached to 5")
//                  print("rtmlogin counter", self.rtmLoginCounter)
                  completion(AgoraRtmLoginErrorCode(rawValue: 1001)!)
              }
            return
          }else if(errorCode.rawValue == 8){
              self.logout()
//              print("agora RTM error code 8 : ALREADY login")
              self.login(phoneNumber: phoneNumber, completion: completion)
          }
  //        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//          print("AgoraRtmLoginErrorCode:: \(errorCode.rawValue)")
          if (errorCode == .ok || errorCode == .alreadyLogin) {
            self.loginStatus = .login(phoneNumber: phoneNumber)
          }else {
            self.loginStatus = .logout
          }
          completion(errorCode)
        })
      }
    
    public func logout(completion:(() -> (Void))? = nil ) {
        if UtilityClass.checkInternetConnection(completionHandler: { (act) in
          self.logout(completion: completion)
        }) {
          return
        }
        
        kit?.logout(completion: { (errorCode) in
            print("Logout RTM \(errorCode.rawValue)")
            completion?()
        })
        
    }
        
    
    //--------------------------------------------------------------------------------

    
    func checkUserOnlineStatus(users:[String],completion:@escaping(AgoraRtmQueryPeersOnlineBlock)) {
        if UtilityClass.checkInternetConnection(completionHandler: { (act) in
          self.checkUserOnlineStatus(users: users, completion: completion)
        }) {
          return
        }
        
         kit?.queryPeersOnlineStatus(users, completion: completion)
     }
    
    
}
