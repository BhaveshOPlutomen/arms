//
//  UtilityClass.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 03/06/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class UtilityClass: NSObject {
    
    typealias completion = ((UIAlertAction) -> Void)?
    
    class func showAlert(title: String?, message: String?, completion: completion) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: completion)
        alert.addAction(ok)
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    class func checkInternetConnection(completionHandler: completion) -> Bool {
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Retry", style: .default, handler: completionHandler)
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            
            alert.addAction(ok)
            alert.addAction(cancel)
            
            (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(alert, animated: true, completion: nil)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            return true
            
        }
        return false
    }
    class func cornerWithBorder(vw: UIView, size: CGFloat, border: CGFloat, color: UIColor) {
        vw.layoutIfNeeded()
        vw.layer.cornerRadius = size
        vw.layer.borderWidth = border
        vw.layer.borderColor = color.cgColor
        vw.layer.masksToBounds = true
    }
    class func showAlertWithCancel(title: String?, message: String?, completion: completion) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: completion)
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(ok)
        alert.addAction(cancel)
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    class func corner(vw: UIView, size: CGFloat) {
        vw.layoutIfNeeded()
        vw.layer.cornerRadius = size
        vw.layer.masksToBounds = true
    }
    class func gredientColor(vw: UIView) {
        vw.layoutIfNeeded()
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = vw.bounds
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        vw.layer.sublayers?.removeAll()
        vw.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    // Header date for gallery vc and chat vc
    class func convertToStringForHeader(date:Date, isNeeded: Bool = true) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")// TimeZone.current
        
        let strNewDate = dateFormatter.string(from: date)
        if isNeeded {
            if Calendar.current.isDateInToday(date) {
                return "Today"
            }
            else if Calendar.current.isDateInYesterday(date) {
                return "Yesterday"
            }
        }
        return strNewDate
    }
    
    // Convert UTC Date String Which comes from api To Current Date String (Gallery VC and ChatGroupDetails VC)
    class func ConvertUTCDateStringToCurrentDateString(date:String) -> String {
        if date == "" {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        if let date = dateFormatter.date(from: date) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            return dateFormatter.string(from: date)
        }
        return ""
    }
    
    // Convert To AM/PM inside chat and groupchat cell
    class func convertToTime(date:String) -> String {
        if date == "" {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: date) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "h:mm a"
            
            return dateFormatter.string(from: date)
        }
        return ""
    }
    
    // Convert Date string to custom format output (Chat and Group chat)
    class func convertStringToDate(currentString:String, formatOutput: String) -> Date {
        if currentString == "" {
            return Date()
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        guard let dt = dateFormatter.date(from: currentString) else { return Date()}
        let str = dateFormatter.string(from: dt)
        let dat = dateFormatter.date(from: str)
        guard let asdf = dat?.toLocalTime() else { return Date()}
        dateFormatter.dateFormat = formatOutput //"yyyy-MM-dd"
        let strNew = dateFormatter.string(from: asdf)
        guard let dateNew = dateFormatter.date(from: strNew) else { return Date()}
        return dateNew
    }
    
    // Convert Date String to Date Format
    class func convertToDate(strDate:String, format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.calendar = NSCalendar.current
        
        let strNewDate = dateFormatter.date(from: strDate)
        return strNewDate ?? Date()
    }
    
    // Convert Date to String (For Socket manager)
    class func convertToString(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        dateFormatter.calendar = NSCalendar.current
        
        let strNewDate = dateFormatter.string(from: date)
        return strNewDate
    }
    
    // ----------------------------------------------------
    
    
    
    class func getCurrentDateToday() -> String{
        let today = Date()
        let formatter3 = DateFormatter()
        formatter3.dateFormat = "yyyy-MM-dd HH:mm:ss"
        print("Local date %@",formatter3.string(from: today))
        let GMTdate = localToUTC(date: formatter3.string(from: today))
        
        return GMTdate
    }
       
    class func localToUTC(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        print("local to UTC date %@",dateFormatter.string(from: dt!))
        
        return dateFormatter.string(from: dt!)
    }

    class func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat
        
        return dateFormatter.string(from: dt!)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

