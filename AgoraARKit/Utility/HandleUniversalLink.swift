//
//  HandleUniversalLink.swift
//  AgoraARKit
//
//  Created by Hetali on 14/07/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import Foundation
import NVActivityIndicatorView
import CoreLocation
import ARKit

class HandleUniversalLink: NSObject {
    
    static let sharedInstance = HandleUniversalLink()
    
    let geoCoder = CLGeocoder()

    var status:AppStateMachine  {
           get {
               return AppDelegate.shared.status
           } set {
               AppDelegate.shared.status = newValue
           }
       }
       
    func universalLinkCreateSession(url: String){
//        print("Handle universal link URL %@",url)
        
        AppGlobalManager.sharedInstance.linkURL = url
        
        if UserDefaults.standard.string(forKey: "USER_PHONE") != nil {
            
            let urlArray = url.components(separatedBy: "/")
            
            if let urlLastValue = urlArray.last{
                
                if urlArray.contains("app") {
                    if(self.status == .ideal){
                        
                        let activityData = ActivityData()
                        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
                        
                        HandleUniversalLink.sharedInstance.callreceiverLinkJoin(sessionId: urlLastValue)
                    }else if(self.status == .inCall){
                        
                    }
                }else{
                    if(self.status == .ideal){
                        callJoinSessionFromWebAPI(sessionId: urlLastValue)
                    }else if(self.status == .inCall){
                        
                    }
                }
//                print(urlLastValue)
            }
        }else{
//            AppGlobalManager.sharedInstance.isGuestModeOn = true
            let vc = LoginVC.viewController()
            AppDelegate.shared.window?.rootViewController = vc
        }

    }
    
    func callJoinSessionFromWebAPI(sessionId : String){
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        let roomname = "room_name_\(sessionId)"
        
        guard let mobileNumber = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo else{
            return
        }
        var isAR : Bool = false
        if ARWorldTrackingConfiguration.isSupported {
            isAR = true
        }
        let requestParameters = ["channel_key": sessionId,
                                 "room_name" : roomname,
                                 "join_by" : mobileNumber,
                                 "device_type" : "2",
                                 "location" : AppGlobalManager.sharedInstance.locationString,
                                 "latitude" : AppGlobalManager.sharedInstance.latitude,
                                 "longitude" : AppGlobalManager.sharedInstance.longitude,
                                 "is_ar_device" : isAR,
                                 "sender_width" : UIScreen.main.bounds.size.width,
                                 "sender_height" : UIScreen.main.bounds.size.height
            ] as [String : Any]
        
        print("join session params %@",requestParameters)
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.joinSession, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                                        
                    let userResponse =  CommonCodableHelper<JoinSessionResponse>.codableObject(from: data)
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast(userResponse?.message ?? "Unable to join session. Please try after sometime", type: .fail)

                        return
                    }
                    if isAR{
                        let callUUID:UUID = UUID()
                        let callInfo = CallInfo(callFrom: (userResponse?.output?.user?.mobileNo)!, callerName: "", callTo: "", roomName: roomname, callDate: "\(Date())", callIdentifier: "", chatId: userResponse?.output?.chat_id ?? "",callUUID: callUUID)
                    
                        AppGlobalManager.sharedInstance.incommingCall.append(callInfo)
                        
                        let user_call_id = userResponse?.output?.user_call_id
                        
                        AppGlobalManager.sharedInstance.user_call_id_Global = user_call_id
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute:  {
                            
                            if UserDefaults.standard.string(forKey: "USER_PHONE") != nil {
                                
                                self.status = .inCall
                                
                                let callUUID:UUID = UUID()
                                
                                let call = Call(uuid: callUUID, outgoing: true, handle: userResponse?.output?.user?.mobileNo ?? roomname, callName: userResponse?.output?.user?.name ?? "Guest", date: Date())
                                
                                let vc = ARController.viewController()
                                vc.modalPresentationStyle = .fullScreen
                                vc.call = call
                                
                                vc.roomName = roomname
                                
                                if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
                                    let vcTab = TabbarController.viewController()
                                    vcTab.selectedIndex = 3
                                    (vcTab.children.first as? UINavigationController)?.viewControllers[0] = vc
                                    vcTab.tabBar.isHidden = true
                                    (vcTab.children.first as? UINavigationController)?.navigationBar.isHidden = true
                                    vcTab.navigationController?.navigationBar.isHidden = true
                                    AppDelegate.shared.window?.rootViewController = vcTab
                                    
                                    SocketIOManager.shared.closeConnection()
                                    SocketIOManager.shared.isSocketOn = false
                                    SocketIOManager.shared.establishConnection()
                                }else{
                                    AppDelegate.shared.window?.rootViewController?.present(vc, animated: true, completion: nil)
                                }
                                
                            } else {
                                let vc = LoginVC.viewController()
                                AppDelegate.shared.window?.rootViewController = vc
                                //                             AppGlobalManager.sharedInstance.isGuestModeOn = true
                            }
                        })
                    }else{
                        UtilityClass.showAlert(title: "", message: "Your device does not support Augmented reality feature so you can not join this code.") { (act) in

                        }

                    }
                   
                }
                
            case .failure( let error):
                print("error",error)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
    }
    func callJoinSessionFromAPP_API(sessionId : String,mobileNumber : String){
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        let roomname = "room_name_\(sessionId)"
        
        let requestParameters = ["channel_key": sessionId,
                                 "room_name" : roomname,
                                 "join_by" : mobileNumber,
                                 "device_type" : "0",
                                 "location" : AppGlobalManager.sharedInstance.locationString,
                                 "latitude" : AppGlobalManager.sharedInstance.latitude,
                                 "longitude" : AppGlobalManager.sharedInstance.longitude,
                                 "sender_width" : UIScreen.main.bounds.size.width,
                                 "sender_height" : UIScreen.main.bounds.size.height
            ] as [String : Any]
        
//        print("join session params %@",requestParameters)
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.joinSession, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    
                    let userResponse =  CommonCodableHelper<JoinSessionResponse>.codableObject(from: data)
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast(userResponse?.message ?? "Unable to join session. Please try after sometime", type: .fail)
                        
                        return
                    }
                    
                    let callUUID:UUID = UUID()
                    
                    let callInfo = CallInfo(callFrom: (userResponse?.output?.user?.mobileNo)!,
                                            callerName: "",
                                            callTo: "", roomName: roomname,
                                            callDate: "\(Date())",
                        callIdentifier: "",
                        chatId: (userResponse?.output?.chat_id)!,
                        callUUID: callUUID)
                    AppGlobalManager.sharedInstance.incommingCall.append(callInfo)
                    
                    let user_call_id = userResponse?.output?.user_call_id
                    
                    AppGlobalManager.sharedInstance.user_call_id_Global = user_call_id
                           

                    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute:  {
                        
                        if UserDefaults.standard.string(forKey: "USER_PHONE") != nil{
                            
                            self.status = .inCall

                            let callUUID:UUID = UUID()
                            
                            let call = Call(uuid: callUUID, outgoing: true, handle: userResponse?.output?.user?.mobileNo ?? roomname, callName: userResponse?.output?.user?.name ?? "Guest", date: Date())
//
//                            var chatUserListVC: ChatUsersListViewController?
//
//                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//                            chatUserListVC = storyboard.instantiateViewController(withIdentifier: "ChatUsersListViewController") as? ChatUsersListViewController
//
//                            let vc = ARController.viewController()
//                            vc.modalPresentationStyle = .fullScreen
//                            vc.call = call
//                            vc.roomName = roomname
//                            AppDelegate.shared.window?.rootViewController?.present(vc, animated: true, completion: nil)
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)

                            let chatUserListVC = storyboard.instantiateViewController(withIdentifier: "ChatUsersListViewController") as? ChatUsersListViewController

                            let vc = ARController.viewController()
                            vc.modalPresentationStyle = .fullScreen
                            vc.call = call
                            vc.roomName = roomname
                            AppDelegate.shared.window?.rootViewController?.present(vc, animated: true, completion: nil)
                            if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
                                SocketIOManager.shared.closeConnection()
                                SocketIOManager.shared.isSocketOn = false
                                SocketIOManager.shared.establishConnection()
                            }
                            
//                            let vcTab = TabbarController.viewController()
//                            vcTab.selectedIndex = 3
//                            (vcTab.children.first as? UINavigationController)?.viewControllers[0] = vc
//                            vcTab.tabBar.isHidden = true
//                            AppDelegate.shared.window?.rootViewController = vcTab
//
                            
                        } else {
                            let vc = LoginVC.viewController()
                            AppDelegate.shared.window?.rootViewController = vc
//                             AppGlobalManager.sharedInstance.isGuestModeOn = true
                        }
                    })
                }
                
            case .failure( let error):
                print("error",error)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
    }
    
    func callreceiverLinkJoin(sessionId : String){
          
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        guard let mobileNumber = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo else{
            return
        }
        
        let requestParameters = ["channel_key": sessionId,
                                 "device_id" : device_id,
                                 "join_by" : mobileNumber,
                                 "device_type" : "0"
            ] as [String : Any]
        
        print("recLinkJoin params %@",requestParameters)
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.recLinkJoin, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                
                    
                    let userResponse =  CommonCodableHelper<RecLinkJoinResponse>.codableObject(from: data)
                    
                    let sessCreatedByWeb = userResponse?.output?.sessCreatedByWeb
                    if(sessCreatedByWeb ?? false){
//                        LocationHelper.shared.locationManagerDelegate = self
//                        LocationHelper.shared.startUpdatingLocation()
                        
//                        if(LocationHelper.shared.isLocationEnable){
                            self.callJoinSessionFromWebAPI(sessionId: sessionId)
//                            print("rec link join but from web")
//                        }
                    }
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast(userResponse?.message ?? "Unable to join session. Please try after sometime", type: .fail)
                        return
                    }
//                    print("recLinkJoin return success")
                }
                
            case .failure( let error):
                print("error",error)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
    }
    
}

extension HandleUniversalLink: LocationManagerDelegate {

    func getLocation(location: CLLocation) {
        AppGlobalManager.sharedInstance.latitude = location.coordinate.latitude
        AppGlobalManager.sharedInstance.longitude = location.coordinate.longitude
        
        geoCoder.reverseGeocodeLocation(location) { placemarks, _ in
            if let place = placemarks?.first {
                let currentCityName : String = place.locality ?? ""
                let streetName : String = place.thoroughfare ?? ""
                let nameofplace : String = place.name ?? ""
                let administrativeArea : String = place.administrativeArea ?? ""
                let postalCode : String = place.postalCode ?? ""
                let combineString = ("\(currentCityName),\(streetName),\(nameofplace),\(administrativeArea),\(postalCode)")
                
                AppGlobalManager.sharedInstance.locationString = combineString
            }
        }

    }
}
