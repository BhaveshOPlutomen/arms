//
//  NameInitalLabel.swift
//  AgoraARKit
//
//  Created by Prashant on 22/05/2019.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit

class NameInitalLabel: UILabel {

    override func awakeFromNib() {
        super.awakeFromNib()

        
    }
    
   
   
    func setText(fullname:String?) {
//        let authorName = fullname?.components(separatedBy: " ")  ?? []
//        var stringNeed =  authorName.count > 1 ? "" : String(fullname?.prefix(1) ?? "").uppercased()
//
//        for string in authorName where authorName.count > 1 && stringNeed.count <= 2   {
//
//            stringNeed = String(stringNeed.uppercased() + String(string.first!) + " ").uppercased()
//
//        }
        let formatter = PersonNameComponentsFormatter()
        
        guard let personNameComponents = formatter.personNameComponents(from: fullname ?? "") else {
            return 
        }
        
  //      return personNameComponents.initials

        self.text = personNameComponents.initials
    }
    
}

import Foundation

extension PersonNameComponents {
    var fullName: String {
        return [givenName, middleName, familyName].compactMap{ $0 }.joined(separator: " ")
    }
    
    var fullNameWithSuffix: String {
        return [givenName, middleName, familyName, nameSuffix].compactMap{ $0 }.joined(separator: " ")
    }
    
    var initials: String {
        let firstName = givenName?.first ?? Character(" ")
        let lastName = familyName?.first ?? Character(" ")
        let firstnameCapital = firstName.uppercased()
        let lastNameCapital = lastName.uppercased()
        return "\(firstnameCapital)\(lastNameCapital)".trimmingCharacters(in: .whitespaces)
    }
    
    // Note: If You need first, middle, last
    /*
     var initials: String {
     let firstName = givenName?.first ?? Character(" ")
     let middleName = middleName?.first ?? Character(" ")
     let lastName = familyName?.first ?? Character(" ")
     return "\(firstName)\(middleName)\(lastName)".trimmingCharacters(in: .whitespaces)
     }
     */
}
