//
//  RecordingHelper.swift
//  AgoraARKit
//
//  Created by Hetali on 14/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class RecordingHelper: NSObject {
    static let sharedInstance = RecordingHelper()
    
    let acquireURL = "https://api.agora.io/v1/apps/\(KeyCenter.AppId)/cloud_recording/acquire"
    
    var startRecordingURL : String = ""
    
    var queryURL : String = ""
    
    var stopURL : String = ""
    
    var resourceId : String = ""
    
    var sid : String = ""
    
    var cnmeStop : String = ""
    
    func acquireSeriveRecording(roomname: String,uid : String){
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        let clientreqDict : NSDictionary = [ "resourceExpiredHour" : 24]
        
        let requestParameters = ["cname": roomname,
                                 "uid" : "55555",
                                 "clientRequest" : clientreqDict
            ] as [String : Any]
        
        print("acquireSeriveRecording params %@",requestParameters)
        
        var request = URLRequest(url: URL(string: acquireURL)!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: requestParameters, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(KeyCenter.AuthorizationForRecording, forHTTPHeaderField: "Authorization")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                print(json)
                if let resourceId = json["resourceId"] as? String {
//                    print("========================\(resourceId)")
                    self.startRecordingURL = "https://api.agora.io/v1/apps/\(KeyCenter.AppId)/cloud_recording/resourceid/\(resourceId)/mode/mix/start"
                    self.StartRecording(roomname: roomname, uid: uid)
                }else{
                    let message = json["message"] as? String
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute:  {

                    (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast("\(message ?? "Unable to start recording")", type: .fail)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    })
                }
            } catch {
                print("error")
            }
        })
        
        task.resume()
    }
    
    func StartRecording(roomname: String,uid : String){
        
        let recordingFileConfig  = [ "avFileType" : ["hls"]]
        
        let transcodingConfig  = ["height" :  640,
                                  "width" : 480,
                                  "bitrate" : 500,
                                  "fps" : 15,
                                  "mixedVideoLayout" : 1
            //                                  "backgroundColor" : "#FF0000"
            ]as [String : Any]
        
        let storageConfig = ["accessKey" : "AKIA5GZGIBABLNISRK4A",
                             "region" : 14,
                             "vendor" : 1,
                             "secretKey" : "m9LCFJxVyi0tg14U/Je0qhQ0PqX3Kc+BF0CBiJGg",
                             "bucket" : "plutomen-web",
                             "fileNamePrefix" : ["VideoRecording","Recording"]
            ]
            as [String : Any]
        
        var  recordingConfig : [String : Any] = [:]
        
        if(AppGlobalManager.sharedInstance.isScreenShareME){
            recordingConfig  = ["maxIdleTime" : 30,
                                "streamTypes" : 2,
                                "channelType" : 0,
                                "videoStreamType" : 0,
                                "subscribeVideoUids" : AppGlobalManager.sharedInstance.uidScreenShareArray,
                                "subscribeAudioUids" : AppGlobalManager.sharedInstance.uidAudioArray,
                                "subscribeUidGroup" : 0,
                                "transcodingConfig" : transcodingConfig
                ]as [String : Any]
        }else{
            recordingConfig  = ["maxIdleTime" : 30,
                                "streamTypes" : 2,
                                "channelType" : 0,
                                "videoStreamType" : 0,
                                "subscribeVideoUids" : AppGlobalManager.sharedInstance.uidArray,
                                "subscribeAudioUids" : AppGlobalManager.sharedInstance.uidAudioArray,
                                "subscribeUidGroup" : 0,
                                "transcodingConfig" : transcodingConfig
                ]as [String : Any]
        }
        
        let clientreqDict  = [ "recordingConfig" : recordingConfig,
                               "storageConfig" : storageConfig,
                               "recordingFileConfig" : recordingFileConfig
        ]
        
        let requestParameters = ["cname": roomname,
                                 "uid" : "55555",
                                 "clientRequest" : clientreqDict
            ] as [String : Any]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: requestParameters, options: [])
        let decoded = String(data: jsonData, encoding: .utf8)!
        print(decoded)
        
        let postData = decoded.data(using: .utf8)
        
        print("start recording params %@",requestParameters)
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": KeyCenter.AuthorizationForRecording]
        
        var request = URLRequest(url: URL(string: startRecordingURL)!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData //jsonData
        
        //        request.httpBody = try? JSONSerialization.data(withJSONObject: requestParameters, options: [])
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                print(json)
                if let sid = json["sid"] as? String {
                    print("========================\(sid)")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute:  {
                        
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()

                        (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast("Recording Started", type: .success)
                    })
                    
                    let resourceId1 = json["resourceId"] as? String
                    
                    self.resourceId = resourceId1!
                    self.sid = sid
                    self.cnmeStop = roomname
                    
//                    self.queryURL = "https://api.agora.io/v1/apps/\(KeyCenter.AppId)/cloud_recording/resourceid/\(self.resourceId)/sid/\(sid)/mode/mix/query"
                    
//                    print(self.queryURL)
                }
                else{
                    let message = json["message"] as? String
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute:  {

                    (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast("\(message ?? "Unable to start recording")", type: .fail)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    })
                }
            } catch {
                print("error")
            }
        })
        
        task.resume()
        
    }
   
//    func stopRECORDING(completionBlock: () -> Void)
//    {
//        self.stopURL = "https://api.agora.io/v1/apps/\(KeyCenter.AppId)/cloud_recording/resourceid/\(resourceId)/sid/\(sid)/mode/mix/stop"
//        print(self.stopURL)
//        self.stopRecording()
//
//    }
    func stopRECORDING(completion:@escaping((String) -> Void)){
        
    let activityData = ActivityData()
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
          
//    func stopRECORDING(completionBlock: () -> String){
        
        self.stopURL = "https://api.agora.io/v1/apps/\(KeyCenter.AppId)/cloud_recording/resourceid/\(resourceId)/sid/\(sid)/mode/mix/stop"
        
        let emptyDictionary = [String: String]()
        
        let requestParameters = ["cname": self.cnmeStop,
                                 "uid" : "55555",
                                 "clientRequest" : emptyDictionary
            ] as [String : Any]
        
        
        let jsonData = try! JSONSerialization.data(withJSONObject: requestParameters, options: [])
        let decoded = String(data: jsonData, encoding: .utf8)!
        print(decoded)
        
        let postData = decoded.data(using: .utf8)
        
        print("STOP recording params %@",requestParameters)
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization": KeyCenter.AuthorizationForRecording]
        
        var request = URLRequest(url: URL(string: stopURL)!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData //jsonData
        
        //        request.httpBody = try? JSONSerialization.data(withJSONObject: requestParameters, options: [])
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print(response!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
                print(json)

                if let serverResponse = json["serverResponse"] as? NSDictionary {
                    if let fileList = serverResponse["fileList"] as? String{
                        if !fileList.isEmpty{
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute:  {

                            (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast("Recording Stopped", type: .success)
                            
                            })
                            
                            completion(fileList)

                        }
                    }
                }
                else{
                    let message = json["message"] as? String
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute:  {
                        
                        (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast("\(message ?? "Unable to stop recording there is some error sercer side")", type: .fail)
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        
                        completion("Failure")
                    })
                }
            } catch {
                print("error")
            }
        })
        task.resume()
    }
    
//    func StoreRecordingThumb(recordingPath : String,duration:String){
//
//        let activityData = ActivityData()
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
//
//        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
//            return
//        }
//        guard let user_call_id = AppGlobalManager.sharedInstance.user_call_id_Global else{
//            return
//        }
//
//        let requestParameters = [ "device_id" : "\(device_id)",
//                                  "recording_path" : "\(recordingPath)",
//                                  "user_call_id" : "\(user_call_id)",
//                                  "duration": "\(duration)"] as [String : Any]
//
//        print("store Thumb api parameters : \(requestParameters)")
//
//        APIHandler.callAPI(endPoint: APIConstants.EndPoints.stopRecordingThumb, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
//
//            switch response {
//            case .success(let data):
//                DispatchQueue.main.async {
//                    let userResponse =  CommonCodableHelper<saveThumbResponse>.codableObject(from: data)
//
//                    guard let status = userResponse?.statusCode,status == 1 else {
//                        (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast(userResponse?.message ?? "Unable to stop recording. Please try after sometime", type: .fail)
//                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//                        return
//                    }
//                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//
//
//                    (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast(userResponse?.message ?? "Stored", type: .fail)
//
//                    let saveSS = SaveSSVC.viewController()
//
//                    saveSS.mediaType = .saveVideo
//                    saveSS.thumb_url = userResponse?.output?.thumb_path
//                    saveSS.recording_id = userResponse?.output?.id
//
//                    present(saveSS, animated: true, completion: nil)
//
//                }
//
//            case .failure( let error):
//                print("error",error)
//            }
//        }
//    }
    
    func queryAPICall(){
        
        let url = URL(string: self.queryURL)
        guard let requestUrl = url else { fatalError() }
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(KeyCenter.AuthorizationForRecording, forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let error = error {
                print("Error took place \(error)")
                return
            }
            if let response = response as? HTTPURLResponse {
                print("Response HTTP Status code: \(response.statusCode)")
            }
            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                print("Response data string:\n \(dataString)")
            }
            guard let json = (try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                print("Not containing JSON")
                return
            }
            
//            print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!gotten json response dictionary is \n \(json)")
            
            if let serverResponse = json["serverResponse"] as? NSDictionary {
                if let status = serverResponse["status"] as? String{
                    print(status)
                    let statusInt = Int(status) ?? 0
                    if(statusInt == 5){
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute:  {

                        (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast("Recording is working perfectly", type: .success)
                        })
                    }
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute:  {

                (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast("Recording is Not working Properly Please try again", type: .fail)
                })
            }

        }
        task.resume()
    }
}


