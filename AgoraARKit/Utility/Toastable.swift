////
////  Toastable.swift
////  AgoraARKit
////
////  Created by Prashant on 02/07/19.
////  Copyright © 2019 Prashant. All rights reserved.
////
//
//
import SwiftToast
import UIKit

enum ToastType {
    case success
    case fail
    case info
    case chat
    
    var color : UIColor {
        switch self {
        case .success :
            return Constants.AppTheme.Colors.ColorsOfApp.green.color
        case .fail:
            return  Constants.AppTheme.Colors.ColorsOfApp.redColor.color
        case .info:
            return Constants.AppTheme.Colors.ColorsOfApp.colorPrimaryDark.color
        case .chat:
            return Constants.AppTheme.Colors.ColorsOfApp.colorSecondaryGray.color
        }
    }
}
protocol Toastable:SwiftToastDelegate {
    func showToast(_ message: String,type:ToastType)
    func showToastBottom(_ message: String,type:ToastType)
    func showToast2(message:String,type:ToastType)
}

extension Toastable where Self : UIViewController {



    func showToast(_ message: String,type:ToastType) {

        DispatchQueue.main.async { [weak self] in
            
            guard let self = self else {return}
            
            // let type = ToastType.success
            
            let color = type.color
            let toast = SwiftToast(text: message, textAlignment: .center, image: nil, backgroundColor:color , textColor: .white, font: Constants.AppTheme.Fonts.font(type: .FONT_SemiBold, size: 13), duration: 2.3, minimumHeight: 40, statusBarStyle: .lightContent, aboveStatusBar: true, isUserInteractionEnabled: true, style: SwiftToastStyle.navigationBar)
            
            self.present(toast, animated: true)
          
        }
    }
    
    func showToastForChat(_ message: String,type:ToastType) {

        DispatchQueue.main.async { [weak self] in
            
            guard let self = self else {return}
            
            // let type = ToastType.success
            
            let color = type.color
            let toast = SwiftToast(text: message, textAlignment: .center, image: nil, backgroundColor:color , textColor: .white, font: Constants.AppTheme.Fonts.font(type: .FONT_SemiBold, size: 16), duration: 3.0, minimumHeight: 40, statusBarStyle: .lightContent, aboveStatusBar: true, isUserInteractionEnabled: true, style: SwiftToastStyle.navigationBar)
            
            self.present(toast, animated: true)
          
        }
    }
    func showToastBottom(_ message: String,type:ToastType) {
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {return}

            // let type = ToastType.success
            let color = type.color
            let toast = SwiftToast(text: message, textAlignment: .center, image: nil, backgroundColor:color , textColor: .white, font: Constants.AppTheme.Fonts.font(type: .FONT_SemiBold, size: 13), duration: 2.3, minimumHeight: 40, statusBarStyle: .lightContent, aboveStatusBar: true, isUserInteractionEnabled: true, style: SwiftToastStyle.navigationBar)
            toast.style = .bottomToTop
            self.present(toast, animated: true)
            
        }
    }

    func swiftToastDidTouchUpInside(_ swiftToast: SwiftToastProtocol) {
        
        self.dismissSwiftToast(true)
        
    }


    func showToast2(message:String,type:ToastType) { 
//        let color = type == .success ? Constants.AppTheme.Colors.ColorsOfApp.green.color : Constants.AppTheme.Colors.ColorsOfApp.redColor.color
//        let toast = SwiftToast(text: message, textAlignment: .center, image: nil, backgroundColor:color , textColor: .white, font: Constants.AppTheme.Fonts.font(type: .FONT_REGULAR, size: 12), duration: 4.0, minimumHeight: 50, statusBarStyle: .lightContent, aboveStatusBar: false, isUserInteractionEnabled: false, target: nil, style: SwiftToastStyle.statusBar)
//
//        present(toast, animated: true)
        DispatchQueue.main.async { [weak self] in
                
                guard let self = self else {return}
                
                // let type = ToastType.success
                
                let color = type.color
            let toast = SwiftToast(text: message, textAlignment: .center, image: nil, backgroundColor:color , textColor: .white, font: Constants.AppTheme.Fonts.font(type: .FONT_SemiBold, size: 13), duration: 4.0, minimumHeight: 40, statusBarStyle: .lightContent, aboveStatusBar: true, isUserInteractionEnabled: true, style: SwiftToastStyle.navigationBar)
                
                self.present(toast, animated: true)
              
            }
    }


}
