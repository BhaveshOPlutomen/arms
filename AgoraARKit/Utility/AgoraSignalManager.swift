//
//  AgoraSignalManager.swift
//  AgoraARKit
//
//  Created by Prashant on 17/05/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

/*

import Foundation
import AgoraSigKit
import AgoraRtcEngineKit


enum SignalStatus {
    case logout
    case login(userID:UInt32)
    case channelJoined(channelName:String)
    case channelLeave
    case userJoined(uid:UInt32)
    
  /*  case calling
    case inCall
    case receiveCalling
    case refuseCall
    case endCall
    case callFail*/
    
    case messageReceived(string:String)
    case userStatus(userID:String,isOnline:Bool)
    
    
}




class AgoraSignalManager:NSObject  {
    
    private var agoraSignal:AgoraAPI!
    
    var isUserLoggedIn = false
    var myPhoneNumber:String!
    var callFrom = ""
    var callerName = ""
    var callTo = ""
    var roomName = ""
    var callDate = ""
    
    var date:Date? {
        let dateFormatter  = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.date(from: callDate)
    }
    
    var callDateToCurrentDate:Int {
        let startDate =  date ?? Date()
        let endDate = Date()
        let difference = Calendar.current.dateComponents([.second], from: startDate, to: endDate)
        let duration = difference.second
        return duration ?? -1
    }
    
   // var callUUID:UUID = UUID()

    var currentUserID:UInt32 = 0
    var currentChannel:String = ""
    
     var signalStatusReceived:((SignalStatus) -> Void)?
    
    private var status = SignalStatus.logout {
        didSet {
            signalStatusReceived?(status)
        }
    }
    
    
    public override init () {
        super.init()
        agoraSignal = AgoraAPI.getInstanceWithoutMedia(KeyCenter.AppId)
        initCallBack()
    }
    
    deinit {
        
        print("Deinit Signal manager")
        
        self.destroyAgora()
    }
    
    func initCallBack() {
        agoraSignal.onLoginSuccess = {[weak self] (uid,fd) in
            print("Login success \(uid)")
            self?.status = .login(userID: uid)
            self?.isUserLoggedIn = true
        }
        
        agoraSignal.onLogout = { [weak self]  code in
            
            self?.isUserLoggedIn = false

        }
        
        
        agoraSignal.onChannelJoined = {[weak self] (channelName) in
            print("Channel Join success \(channelName)")
            self?.status = .channelJoined(channelName: channelName ?? "")

        }
        
        agoraSignal.onChannelLeaved = {[weak self] (string,code) in
            print("Channel \(string) leaved with \(code.rawValue)")
            self?.status = .channelLeave

        }
        
        agoraSignal.onChannelUserJoined = {[weak self] (string,userID) in
            print("Channel Joined \(string)")
            self?.status = .userJoined(uid: userID)
    //        self?.status = .channelLeave
            
        }
        
        agoraSignal.onMessageChannelReceive = {[weak self] (channel,account,userid,message)  in
            print("Message  RECV  \(message) FROM \(account)")
            
            // Check if current user has send the string to channel
            if account != AppGlobalManager.sharedInstance.currentUserID {
                self?.status = .messageReceived(string: message ?? "")
            }
        }
        
        
        agoraSignal.onMessageInstantReceive = { [weak self] (account,userid,message)  in
            print("INSTANT MESSAGE RECV. \(userid) FROM \(account) message \(message)")
            
            // Check if current user has send the string to channel
            if userid != (self?.agoraSignal.uid ?? 0) {
                
                
             /*   do {
                    if let data = message?.data(using: .utf8) {
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
                        
                        self?.status = .messageReceived(string: (json?["MSG"] as? String) ?? "")

                    }
                } catch {
                    print(" EXCEPTION IN JSON CREATE")
                }
                */
                self?.status = .messageReceived(string: message ?? "")
            }
            
            
        }
        
        agoraSignal.onQueryUserStatusResult = {[weak self] str1,str2 in
            print(str1,str2)
            self?.status = .userStatus(userID: str1 ?? "", isOnline: str2 == "1" ? true : false )
        }
        
        agoraSignal.onInvokeRet = {[weak self] str1,str2,str3 in
            
            print(str1,str2,str3)
            
            
        }
        agoraSignal.onLog = { [weak self] (string) in


            //TESTING
          //  print("LOG \(string)")

        //    AppGlobalManager.sharedInstance.appendLog(string: string)



        }
        
    }
    
    public func login(phoneNumber:String) {
        agoraSignal.logout()
        self.myPhoneNumber = phoneNumber
        
        agoraSignal.login(KeyCenter.AppId, account: self.myPhoneNumber, token: "_no_need_token", uid: currentUserID, deviceID: "")
    }
    
    public func joinChannel(channelName:String) {
        self.currentChannel = channelName
        agoraSignal.channelJoin(channelName)
        
    }
    
    public func leaveChannel() {
        
        agoraSignal.channelLeave(self.currentChannel)
      //  agoraSignal.destroy()
        //signalStatusReceived = nil
    }
    
    public func sendString(string:String) {
        agoraSignal.messageChannelSend(self.currentChannel, msg: string, msgID: "")
    }
    
    public func sendInstantString(string:String,account:String) {
        
        agoraSignal.messageInstantSend(account, uid: 0, msg: string, msgID: "")

      /*  let json = [ "MSG" : string,
            "account":account,
            "time_interval_milliseconds":Date().timeIntervalSince1970 * 1000
        
            ] as [String : Any]
        
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            let stringNEW = String(data:data,encoding: .utf8)
            
            agoraSignal.messageInstantSend(account, uid: 0, msg: stringNEW, msgID: "")

        } catch {
            print(" EXCEPTION IN JSON CREATE")
        }
        
        */
        
    }
    
    func userStatus(userID:String) {
        agoraSignal.queryUserStatus(userID)
        
//        let json = ["account":"\(userID)"]
//        if let data = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), let string = String(data:data,encoding:.utf8) {
//
//        agoraSignal.invoke("io.agora.signal.user_query_user_status", req: string, callID: "")
        
    }
    
    
    
    
  //  public func inviteUser(userID:UInt32) {
        //agoraSignal.channelInviteUser(self.currentChannel, account: userID, uid: userID)
   // }
    
//    func callTo(phoneName: String) {
//        callTo = phoneName
//        callFrom = self.myPhoneNumber
//        status = .calling
//        joinChannel(channelName: phoneName)
//      //  agoraSignal.channelInviteUser(phoneName, account: phoneName, uid: 0)
//    }
//
//    func sendString(string:String) {
//        agoraSignal.sendS
//    }
    
    
    func destroyAgora() {
        agoraSignal.destroy()
    }
    
}


*/
