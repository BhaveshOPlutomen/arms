//
//  CallLogcell.swift
//  AgoraARKit
//
//  Created by Prashant on 16/05/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit

class CallLogcell: UITableViewCell {
    
    @IBOutlet weak var imgCallIndicator: UIImageView!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCallType: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func formatDate(date:Date?) -> (date:String?,time:String?)? {
        guard let date = date else {
            return nil
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: date)
        guard  let yourDate = formatter.date(from: myString) else{
            return nil
        }
        formatter.dateFormat = "dd-MMM-yyyy"
        let myStringafd = formatter.string(from: yourDate)

        formatter.dateFormat = "hh:mm a"
        let myStringTime = formatter.string(from: yourDate)
        
        return (myStringafd,myStringTime)
        
    }

}
