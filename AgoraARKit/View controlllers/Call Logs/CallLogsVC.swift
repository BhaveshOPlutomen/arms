//
//  CallLogsVC.swift
//  AgoraARKit
//
//  Created by Prashant on 15/05/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit
import CoreData
import NVActivityIndicatorView
import CoreLocation
import ARKit

class CallLogsVC: BaseViewController,Toastable,Alertable {

    //MARK:- Outlets
    
    @IBOutlet weak var tblCallLogs: UITableView!
    var callPhoneNumber:String = ""

    var callLogArray = [CallLogResponse]()
    
    let geoCoder = CLGeocoder()

    //MARK:- Var
    
  //  var callLogs = [CallLog]()
    fileprivate lazy var fetchedResultsController: NSFetchedResultsController<CallLog> = {
        // Create Fetch Request
        let fetchRequest: NSFetchRequest<CallLog> = CallLog.fetchRequest()
        
        // Configure Fetch Request
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        
        let predicate = NSPredicate(format: "userID == %d", AppGlobalManager.sharedInstance.loggedInUser?.user?.id ?? 0)
        fetchRequest.predicate = predicate
        
        
        // Create Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: AppDelegate.shared.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        
        // Configure Fetched Results Controller
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()
    //--------------------------------------------------------------------------------
    
    //MARK:- Memory Managment Methods
    
    //--------------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Abstract Methdos
    
    //--------------------------------------------------------------------------------
    
    class func viewController () -> CallLogsVC {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CallLogsVC") as! CallLogsVC
    }
    
    
    //MARK:-  Custom Methdos
    
    //--------------------------------------------------------------------------------
    
    private func setupTableView() {
        
        //   self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tblCallLogs.dataSource = self
        self.tblCallLogs.delegate = self
        self.tblCallLogs.tableFooterView = UIView()
        self.tblCallLogs.estimatedRowHeight = 84
        self.tblCallLogs.rowHeight = UITableView.automaticDimension
        
    }
    
    //--------------------------------------------------------------------------------
    
    private func fetchCallLogs() {
      
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print("Something went wrong with fetch result controller \(error)")
        }
        DispatchQueue.main.async {
            self.tblCallLogs.reloadData()

        }
        
    }
    private func getCallLogsAPI()
    {
        guard let userID = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else {
            return
        }
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
                return
        }
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
      //  let parameters = ["userid":userID]
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.callList + "/" + "\(userID)?" + "device_id=" + "\(device_id)" + "&" + "device_type=" + "0" , parameters: nil, token: AppGlobalManager.sharedInstance.token, method: .GET) { (result) in

            
            DispatchQueue.main.async {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                switch result {
                case .success( let data):
                    let object = CommonCodableHelper<[CallLogResponse]>.codableObject(from: data)
                    
                    guard let status = object?.statusCode,status == 1 else {
                        //                        self.showToast(object?.message ?? "Something went wrong !! Please try again later", type: .fail)
//                        let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
////                            AppGlobalManager.sharedInstance.logoutAndClearAllCache()
//                        }
                        self.showToast(object?.message ?? "Something went wrong !! Please try again later", type: .success)
//                        self.showAlert(withMessage: object?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                        
                        return
                    }
                
                    self.callLogArray = object?.output ?? []
                    
                    self.tblCallLogs.reloadData()
                    
                case .failure( _):
                    self.showToast("Something went wrong !!", type: .fail)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }
                
            }
            
        }
        
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- ViewLifeCycle Methods
    
    //--------------------------------------------------------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        fetchCallLogs()
        
        let screenRect = UIScreen.main.bounds
        let screenWidth = screenRect.width
        let screenHeight = screenRect.height
        
        print(UIScreen.main.scale)
        
        print(screenWidth)
        print(screenHeight)

//        getCallLogsAPI()
    }
    
    //--------------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tblCallLogs.reloadData()
    }
    
    //--------------------------------------------------------------------------------
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    public func callCameFromAppdelegate( callerID : String, callerName : String)
    {
        self.callPhoneNumber = callerID

        let roomName = "room_name_\(self.callPhoneNumber)"

        self.showCallStatusVC(callerID, roomName: roomName, contactName: callerName)
        
    }


}

extension CallLogsVC:NSFetchedResultsControllerDelegate {
//    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
//        if type == .insert {
//            self.tblCallLogs.insertRows(at: [IndexPath(item: 0, section: 0)], with: .fade)
//        }
//    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        //    self.tblCallLogs.beginUpdates()
        //     DispatchQueue.main.async {
        
        
        if type == .insert {
            //  self.tblCallLogs.reloadData()
            self.tblCallLogs.insertRows(at: [newIndexPath!], with: .automatic)
        } else if type == .delete && indexPath != nil {
            self.tblCallLogs.deleteRows(at: [indexPath!], with: .fade)
        }
        //      self.tblCallLogs.endUpdates()
    }
    
    //}
    
   
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tblCallLogs.beginUpdates()
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tblCallLogs.endUpdates()
    }
}


extension CallLogsVC:UITableViewDataSource,UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {

       
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        
        if (fetchedResultsController.fetchedObjects ?? []).count == 0 {

            showNoRecordFoundLabel(tableView)
            return 0

        }
        tableView.backgroundView = UIView()


        return (fetchedResultsController.fetchedObjects ?? []).count
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CallLogcell
        
        let contact = fetchedResultsController.object(at: indexPath)
        cell.lblPhoneNumber.text = contact.callName ??  contact.handle
        let dateString = cell.formatDate(date: contact.date)
        cell.lblDate.text = dateString?.date
        cell.lblTime.text = dateString?.time
       // cell.imgCallIndicator.image =  contact.outgoing ?  #imageLiteral(resourceName: "outgoing-call")  : #imageLiteral(resourceName: "incoming-call (1)")
        
        
        if contact.duration == 0 {
            cell.lblCallType.text = "Missed Call"
           // cell.lblDate.textColor = UIColor(named: "Red")
            cell.lblCallType.textColor = UIColor(named: "Red")
            cell.lblDuration.isHidden = true
            cell.lblDuration.text = ""
            cell.imgCallIndicator.image =  contact.outgoing ?  #imageLiteral(resourceName: "red-out")  : #imageLiteral(resourceName: "red-in")

        } else {
            cell.lblCallType.text = contact.outgoing ? "Outgoing Call" : "Incoming Call"
            cell.lblCallType.textColor = UIColor(named: "Green")

            cell.lblDuration.isHidden = false
            cell.lblDuration.text =  Int(contact.duration).asTimeString()

            cell.imgCallIndicator.image =  contact.outgoing ?  #imageLiteral(resourceName: "green-out")  : #imageLiteral(resourceName: "green-in")
        
        }
        cell.setNeedsDisplay()

        cell.layoutIfNeeded()
        return cell
    }
    
    //--------------------------------------------------------------------------------
    
    
    fileprivate func showNoRecordFoundLabel(_ tableView: UITableView) {
        let noRecordFound: UILabel   = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width  , height: tableView.frame.height))
        noRecordFound.text = "No Call logs found."
        noRecordFound.font = Constants.AppTheme.Fonts.font(type: .FONT_REGULAR, size: 16)
        //    noDataLabel.titleLabel?.textColor   = UIColor.red.withAlphaComponent(0.8)
        noRecordFound.textColor = UIColor(named: "LightGray")
        noRecordFound.numberOfLines = 0
        noRecordFound.textAlignment = .center
        noRecordFound.lineBreakMode = .byWordWrapping
        
        tableView.backgroundView  = noRecordFound
    }
    
  
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let contextualActionDelete = UIContextualAction(style: .destructive, title: "Delete") {[unowned self] (action, view,finshed) in
            finshed(true)

            let object = self.fetchedResultsController.object(at: indexPath)
            
            if !object.isDeleted {
                AppDelegate.shared.persistentContainer.viewContext.delete(object)
                AppDelegate.shared.saveContext()
            }
        
            
        }
        contextualActionDelete.backgroundColor = UIColor(named: "Red")
        contextualActionDelete.image = #imageLiteral(resourceName: "ic_close")

     
        
        let swipeActionCofig = UISwipeActionsConfiguration(actions: [contextualActionDelete])
        
        return swipeActionCofig
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
               
        if UtilityClass.checkInternetConnection(completionHandler: { (act) in
        }) {
          return
        }
//        LocationHelper.shared.locationManagerDelegate = self
//        LocationHelper.shared.startUpdatingLocation()
//
//        if(LocationHelper.shared.isLocationEnable){
//        if ARWorldTrackingConfiguration.isSupported {
            
            let object = self.fetchedResultsController.object(at: indexPath)
            self.callPhoneNumber = object.handle!
            self.showCallStatusVC(object.handle!, roomName: "room_name_\(object.handle!)", contactName: object.callName!)
//        } else {
//            let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
//            }
//            self.showAlert(withMessage: "Your device does not supports Augmented reality feature.", title: "", customActions: [yesAction])
//        }
           
//        }
   
    }
    

     fileprivate func showCallStatusVC(_ phoneNumber: String,roomName:String,contactName:String) {
           let callStatusVC = CallStatusViewController.viewController()
           callStatusVC.delegate = self
           callStatusVC.roomName = roomName
           callStatusVC.callerID = phoneNumber
           callStatusVC.callName = contactName
           self.present(callStatusVC, animated: true, completion: nil)
       }
   
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//        let transform = CGAffineTransform.init(translationX: 0, y: -40)
//        cell.alpha = 0.5
//        cell.transform = transform
//        
//        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
//            cell.transform = CGAffineTransform.identity
//            cell.alpha = 1.0
//
//        }, completion: nil)
//        
//        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .preferredFramesPerSecond60, animations: {
//            cell.transform = CGAffineTransform.identity
//            cell.alpha = 1.0
//
//        }, completion: nil)
       
    }
        
       
    
}
extension CallLogsVC: LocationManagerDelegate {

    func getLocation(location: CLLocation) {
        AppGlobalManager.sharedInstance.latitude = location.coordinate.latitude
        AppGlobalManager.sharedInstance.longitude = location.coordinate.longitude
        
        geoCoder.reverseGeocodeLocation(location) { placemarks, _ in
            if let place = placemarks?.first {
                let currentCityName : String = place.locality ?? ""
                let streetName : String = place.thoroughfare ?? ""
                let nameofplace : String = place.name ?? ""
                let administrativeArea : String = place.administrativeArea ?? ""
                let postalCode : String = place.postalCode ?? ""
                let combineString = ("\(currentCityName),\(streetName),\(nameofplace),\(administrativeArea),\(postalCode)")
                
                AppGlobalManager.sharedInstance.locationString = combineString
            }
        }

    }

}
extension CallLogsVC: CallStatusDelegate {
    func callDisconntedByReciver(call:Call) {
        CoreDataManager.saveCall(call: call, duration: 0)
        AppDelegate.shared.status = .ideal
        call.end()
        AppDelegate.shared.callManager.remove(call: call) // important , endCallCXAction == CALLS =>   func provider(_ provider: CXProvider, perform action: CXEndCallAction)
        AppDelegate.shared.callManager.endCallCXAction(call: call, completion: nil)
    }
    
    func callAccpted(call:Call) {
        DispatchQueue.main.async {
        //    let call = AppDelegate.shared.callManager.startCall(handle: call.handle/* AppGlobalManager.sharedInstance.currentUserID ?? ""*/, videoEnable: true)
            
            let vc = ARController.viewController()
            vc.modalPresentationStyle = .fullScreen
            vc.roomName =  "room_name_\(self.callPhoneNumber)" //self.txtRoomID.text ?? ""
            print(self.callPhoneNumber)
            vc.call = call
            self.present(vc, animated: true, completion: nil)
            
            
        }
    }
    
    func callDisconnectedByCaller(call:Call) {
        CoreDataManager.saveCall(call: call, duration: 0)
        AppDelegate.shared.status = .ideal
        call.end()
        AppDelegate.shared.callManager.remove(call: call) // important , endCallCXAction == CALLS =>   func provider(_ provider: CXProvider, perform action: CXEndCallAction)
        
        AppDelegate.shared.callManager.endCallCXAction(call: call, completion: nil)

    }
    
    
}


