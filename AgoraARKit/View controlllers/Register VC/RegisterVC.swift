////
////  RegisterVC.swift
////  AgoraARKit
////
////  Created by Prashant on 29/04/19.
////  Copyright © 2019 Prashant. All rights reserved.
////
//
//import UIKit
//
//class RegisterVC: BaseViewController {
//
//    //MARK:- Outlets
//
//    @IBOutlet weak var txtPhoneNumber: UITextField!
//
//    //MARK:- Var
//
//
//
//    //--------------------------------------------------------------------------------
//
//    //MARK:- Memory Managment Methods
//
//    //--------------------------------------------------------------------------------
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//    }
//
//    //--------------------------------------------------------------------------------
//
//    //MARK:- Abstract Methdos
//
//    //--------------------------------------------------------------------------------
//
//    class func viewController () -> RegisterVC {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        return storyboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
//    }
//
//
//    //--------------------------------------------------------------------------------
//
//    //MARK:-  Action Methdos
//
//    //--------------------------------------------------------------------------------
//
//   //segueToCallVC
//    @IBAction func btnRegisterTapped(_ sender: Any) {
//
//        if let text = txtPhoneNumber.text?.trimmingCharacters(in: .whitespaces),!text.isEmpty {
//
//            let parameters = [
//                "phone":text,
//                "token":UserDefaults.standard.object(forKey: "TOKEN") ?? "",
//                "device_type":"0"
//
//            ]
//
//            APIHandler.callAPI(endPoint: "storeData", parameters: parameters, method: .POST) {[unowned self] (response) in
//                switch response {
//                case .success(let data):
//                    DispatchQueue.main.async {
//                        print(String(data: data, encoding: .utf8) ?? "")
//
//                        UserDefaults.standard.set(text, forKey: "USER_PHONE")
////                        AppGlobalManager.sharedInstance.currentUserID = text
//
//                        let vc = TabbarController.viewController()
//                        AppDelegate.shared.window?.rootViewController = vc
//
//                        AppDelegate.shared.agoraSignalManager.login(phoneNumber: text)
//
//
//                    }
//
//                case .failure( let error):
//                    print("error",error)
//                }
//            }
//
//
//        }
//
//    }
//
//    //--------------------------------------------------------------------------------
//
//    //MARK:- ViewLifeCycle Methods
//
//    //--------------------------------------------------------------------------------
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//    }
//
//    //--------------------------------------------------------------------------------
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//    }
//
//    //--------------------------------------------------------------------------------
//
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//    }
//
//
//
//
//}
