//
//  ChatGroupViewController.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 3/19/21.
//  Copyright © 2021 Prashant. All rights reserved.
//

import UIKit
import AVKit
import AgoraRtmKit
import SocketIO
import NVActivityIndicatorView
import SwiftToast
import MobileCoreServices
import IQKeyboardManagerSwift


class ChatGroupViewController: BaseViewController, Alertable, Toastable, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIDocumentMenuDelegate,UIDocumentPickerDelegate, UITextViewDelegate {
    
    // MARK:- Outlets UILabel
    @IBOutlet weak var lblTyping: UILabel!
    @IBOutlet weak var lblChatStatus: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblinitials: NameInitalLabel!
    
    // MARK:- Outlets UIView
    @IBOutlet weak var inputContainView: UIView!
    @IBOutlet weak var viewTextFieldContainer: UIView!
    @IBOutlet weak var customNavigationBarView: UIView!
    @IBOutlet var viewBackground: UIView!
    @IBOutlet weak var viewMoveUp: UIView!
    @IBOutlet weak var chatLabelView: UIView!
    
    // MARK:- Outlets Constraints
    @IBOutlet weak var constraintHeightOfCustomNavigationBar: NSLayoutConstraint! // 64
    @IBOutlet weak var inputViewBottom: NSLayoutConstraint!
    @IBOutlet weak var cons_ViewMove_bottom: NSLayoutConstraint!
    
    // MARK:- Outlets UITableView
    @IBOutlet weak var inputTextField: UITextView!
    @IBOutlet weak var tableView: UITableView?
    
    // MARK:- Outlets Others
    @IBOutlet weak var progressView: UIProgressView!
   
    // MARK:- Variables
    private var list:[Message] = []
    var type: ChatType = .peer("unknow")
    var rtmChannel: AgoraRtmChannel?
    var receiverMobileNumber = String()
    var receiverName = String()
    var receiverId = String()// "8585285852" // "8989896565" //"9879252952" // 8585285852
    var senderumber = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "" // "9879252952"
    var tempCount: Int = 3
    var timer = Timer()
    var receiverUser: User?
    var sendChatMessage:((String) -> ())? = nil
    var isBackFromChatScreen: ((Bool) -> Void)?
    var strStatus = String()
    
    var chatId = String()
    private let refreshControl = UIRefreshControl()
    
    var aryData = [chatHistoryDatum]()
    var aryDataItem : [[Date: [chatHistoryDatum]]]?
    var model : chatGroupModel?
    
    var strFileName = String()
    
    let socket = SocketIOManager.shared.socket // (UIApplication.shared.delegate as! AppDelegate).manager.defaultSocket // SocketIOManager.shared.socket //
    private var isSocketOn = false
    
    //For Pagination
    var isDataLoading:Bool=false
    var pageNo:Int=1
    var limit:Int=20
    var offset:Int=0 //pageNo*limit
    
    var isFromOnCall : Bool = false
    
    lazy var imagePicker = UIImagePickerController()
    var selectedImages = [UIImage]()
    let syncConc = DispatchQueue(label:"con",attributes:.concurrent)
    
    let anotherQueue = DispatchQueue(label: "com.plutomen.arms", qos: .utility)
    
    let messageTextViewMaxHeight: CGFloat = 100
    private var isOversized = false {
        didSet {
            
            guard oldValue != isOversized else {
                return
            }
            
            inputTextField.reloadInputViews()
            
            inputTextField.isScrollEnabled = isOversized
            inputTextField.setNeedsUpdateConstraints()
            if isOversized == true {
                viewBackground.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            }
        }
    }
    var placeHolderText = "Type your message here"
    
    var isFromGroupChat = false
    var aryMembers = [Member]()
    
    //--------------------------------------------------------------------------------
    //MARK:- Abstract Methdos
    //--------------------------------------------------------------------------------
    class func viewController () -> ChatGroupViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ChatGroupViewController") as! ChatGroupViewController
    }
    
    
    
    //--------------------------------------------------------------------------------
    //MARK:-  View LifeCycle  Methdos
    //--------------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.tableView?.isHidden = true
        lblChatStatus.text = ""
        
        IQKeyboardManager.shared.disabledToolbarClasses.append(ChatGroupViewController.self)
        inputTextField.keyboardDistanceFromTextField = -5
        chatLabelView.isHidden = true
        // IQKeyboardManager.shared.enable = false
        
        viewBackground.backgroundColor = UIColor.init(named: "PrimaryDark")
        
        self.aryDataItem = []
        self.aryData = []
        addKeyboardObserver()
        updateViews()
        inputTextField.delegate = self
        inputTextField.isScrollEnabled = false
        inputTextField.text = placeHolderText
        inputTextField.textColor = UIColor.white
        
        lblName.text = self.receiverName
        lblinitials.setText(fullname: self.receiverName)
        lblinitials.clipsToBounds = true
        lblinitials.layer.cornerRadius = 25.0
        
        lblTyping.isHidden = true
        self.title = receiverName
        
        progressView.isHidden = true
        
        chatUserHistory(chatId: chatId, pageNo: 1)
        
        if isFromOnCall {
            self.chatLabelView.isHidden = false
            self.customNavigationBarView.isHidden = true
        }
        else if(AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false){
            self.chatLabelView.isHidden = false
            self.customNavigationBarView.isHidden = true
        }
        else {
            self.chatLabelView.isHidden = false
            self.customNavigationBarView.isHidden = false
            if self.strStatus != "" {
                print(self.strStatus)
                self.lblChatStatus.isHidden = false
                self.lblChatStatus.text = "Last seen : \(self.strStatus)"
                self.lblChatStatus.font = Constants.AppTheme.Fonts.font(type: .FONT_REGULAR, size: 13)
            }
        }
        
//        constraintHeightOfCustomNavigationBar.constant = topbarHeight
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        NotificationCenter.default.removeObserver(self, name: .chatUserOnline, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userOnlineStatus(_:)), name: .chatUserOnline, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: .chatUserIsOnline, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userIsOnline(_:)), name: .chatUserIsOnline, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: .notificationTyping, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.fire(_:)), name: .notificationTyping, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: .chatMessageReceived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceivedMessage(_:)), name: .chatMessageReceived, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: .chatUserReadMessage, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReadMessage(_:)), name: .chatUserReadMessage, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: .chatUserReadAllMessage, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReadAllMessage(_:)), name: .chatUserReadAllMessage, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: .chatUserMessageDeliver, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationChatUserMessageDeliver(_:)), name: .chatUserMessageDeliver, object: nil)
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        
        inputTextField.resignFirstResponder()
        NotificationCenter.default.removeObserver(self, name: .notificationTyping, object: nil)
        NotificationCenter.default.removeObserver(self, name: .chatMessageReceived, object: nil)
        NotificationCenter.default.removeObserver(self, name: .chatUserOnline, object: nil)
        NotificationCenter.default.removeObserver(self, name: .chatUserIsOnline, object: nil)
        NotificationCenter.default.removeObserver(self, name: .chatUserReadAllMessage, object: nil)
        NotificationCenter.default.removeObserver(self, name: .chatUserMessageDeliver, object: nil)
    }
    override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            let statusBar = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.safeAreaInsets.top ?? UIApplication.shared.statusBarFrame.size.height
            let totalHeight = statusBar + self.topbarHeight
            
            if totalHeight <= 64 {
                self.constraintHeightOfCustomNavigationBar.constant = 64
            } else {
                self.constraintHeightOfCustomNavigationBar.constant = totalHeight // self.topbarHeight
            }
            
            print("Height :: \(self.constraintHeightOfCustomNavigationBar.constant)")
        }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //--------------------------------------------------------------------------------
    //MARK:-  Action Methdos
    //--------------------------------------------------------------------------------
    @IBAction func btnSendTapped(_ sender: Any) {
        
        if inputTextField.text.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 || inputTextField.text == placeHolderText {
            view.endEditing(true)
            inputTextField.text = nil
            inputTextField.text = placeHolderText
            isOversized = false
            return
        }
        if pressedReturnToSendText(self.inputTextField.text) {
            inputTextField.text = nil
            inputTextField.text = placeHolderText
            
        } else {
            view.endEditing(true)
            inputTextField.text = nil
            inputTextField.text = placeHolderText
        }
        isOversized = false
        inputTextField.selectedRange = NSMakeRange(0, 0)
    }
    
    @IBAction func btnGroupDetailsAction(_ sender: UIButton) {
        
        if model != nil {
            if let next = self.storyboard?.instantiateViewController(withIdentifier: "ChatGroupDetailsViewController") as? ChatGroupDetailsViewController {
                next.model = model
                next.navigationController?.isNavigationBarHidden = true
                self.navigationController?.pushViewController(next, animated: true)
            }
        }
    }
    
    @IBAction func textChangeEventCall(_ sender: UITextField) {
        sendTypingOnSocket()
    }
    
    @IBAction func btnAttachmentAction(_ sender: Any) {
        
        if UtilityClass.checkInternetConnection(completionHandler: { (act) in
            self.btnAttachmentAction(self)
        }) {
            return
        }
        
        attachments()
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        isBackFromChatScreen?(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //--------------------------------------------------------------------------------
    //MARK:-  Custom  Methdos
    //--------------------------------------------------------------------------------
    func messageRecv(text:String,fromUser userName:String,userID:String) {
        appendMessage(userID: userID, userName: userName, content: text)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.inputTextField.resignFirstResponder()
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                let height = keyboardSize.height
                self.viewMoveUp.frame.origin.y += height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                let height = keyboardSize.height
                self.viewMoveUp.frame.origin.y -= height
            }
        }
    }
    
    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    func addrefreshControl() {
        
        if #available(iOS 10.0, *) {
            tableView?.refreshControl = refreshControl
        } else {
            tableView?.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshContactData), for: .valueChanged)
    }
    
    @objc private func refreshContactData() {
        //        refreshControl.beginRefreshing()
        self.aryDataItem = []
        pageNo = 1
        chatUserHistory(chatId: chatId, pageNo: pageNo)
        
        //        refreshControl.endRefreshing()
    }
    
    
    // MARK:- Socket Methods
    func socketEmiter(key: String, param: Any) {
        self.socket.emit(key, with: [param])
        print("KEY :: \(key)\nPARAM ::\(param)")
    }
    
    func sendMessageOnSocket(message: String) {
        //        {from :  sender ni user id , to : receiver no mobile number , message : user message }
        //      8585285852
        
        if socket.status == .connected {
            var param = [String:Any]()
            param["from"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
            param["to"] = receiverMobileNumber
            param["sender_device_type"] = "0"
            param["message"] = message
            param["sender_name"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? ""
            param["sender_user_id"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.id ?? ""
            //        param["attach"] = ["file_ext": "png", "file_name": "tick-green.png", "file_path": "https://plutomen-web.s3.ap-south-1.amazonaws.com/images/originals/tick-green.png", "file_size": "1.93 KB", "file_thumbnail_path": "htt s://plutomen-web.s3.ap-south-1.amazonaws.com/images/originals/tick-green_thumb.png"]
            
            if isFromGroupChat {
                param["to"] = receiverMobileNumber
                param["group"] = true
                param["group_name"] = self.receiverName
            }
            
            self.socketEmiter(key: Constants.SocketKeys.message, param: param)
            
        }
        else {
            UtilityClass.showAlertWithCancel(title: "", message: "Connection issue, please try again") { (act) in
                if AppGlobalManager.sharedInstance.loggedInUser != nil {
                    SocketIOManager.shared.establishConnection()
                }
                
                var param = [String:Any]()
                param["from"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
                param["to"] = self.receiverMobileNumber
                param["sender_device_type"] = "0"
                param["message"] = message
                param["sender_name"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? ""
                param["sender_user_id"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.id ?? ""
                //        param["attach"] = ["file_ext": "png", "file_name": "tick-green.png", "file_path": "https://plutomen-web.s3.ap-south-1.amazonaws.com/images/originals/tick-green.png", "file_size": "1.93 KB", "file_thumbnail_path": "https://plutomen-web.s3.ap-south-1.amazonaws.com/images/originals/tick-green_thumb.png"]
                
                if self.isFromGroupChat {
                    param["to"] = self.receiverMobileNumber
                    param["group"] = true
                    param["group_name"] = self.receiverName
                }
                self.socketEmiter(key: Constants.SocketKeys.message, param: param)
            }
        }
    }
    
    func sendTypingOnSocket() {
        //        {from :  sender ni user id , to : receiver no mobile number}
        //      8585285852
        var param = [String:Any]()
        param["from"] = senderumber
        param["to"] = receiverMobileNumber
        param["group"] = true
        param["typing_name"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? ""
        self.socketEmiter(key: Constants.SocketKeys.typing, param: param)
    }
    
    // MARK:- Notification Center Methods
    @objc func userOnlineStatus(_ notification: Notification) {
        
        //        let dict = notification.userInfo as? [String:Any]
        guard let dict = notification.userInfo as? [String:Any] else { return }
        
        let num = dict["mobile"] as? String ?? ""
        let status = dict["status"] as? String ?? ""
        let at = dict["at"] as? String ?? ""
        
        if num == receiverMobileNumber {
            DispatchQueue.main.async {
                if self.isFromOnCall {
                    self.lblChatStatus.isHidden = false
                    self.lblChatStatus.text = "Chat"
                }else{
                    self.lblChatStatus.isHidden = false
                    //                self.customNavigationBarView.lblStatus.isHidden = false
                    self.lblChatStatus.text = status
                }
                
                //                self.customNavigationBarView.status = status
                //                print(self.strStatus)
                
                if status == "offline" {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.strStatus == "" || self.strStatus == "offline" {
                            
                            let newDate = self.convertToTimeToStatus(updatedDate: at)
                            self.lblChatStatus.isHidden = false
                            self.lblChatStatus.text = "Last seen : \(newDate)"
                            self.lblChatStatus.font = Constants.AppTheme.Fonts.font(type: .FONT_REGULAR, size: 13)
                            
                        }
                    }
                }
            }
        }
    }
    
    @objc func userIsOnline(_ notification: Notification) {
        
        //        let dict = notification.userInfo as? [String:Any]
        guard let dict = notification.userInfo as? [String:Any] else { return }
        
        let from = dict["from"] as? String ?? ""
        print(receiverMobileNumber)
        if self.strStatus == "" {
            if receiverMobileNumber == from{
                let isOnline = dict["isOnline"] as? Bool ?? false
                if(isOnline){
                    self.lblChatStatus.isHidden = false
                    self.lblChatStatus.text = "Online"
                }
            }
        }
        //        let to = dict["to"] as? String ?? ""
    }
    @objc func fire(_ notification: Notification) {
        print("FIRE!!!")
        
        //        Socket typing :: [{
        //            from = 9898989815;
        //            group = 1;
        //            to = 6052fc18fb0b327edb12ce52;
        //            "typing_name" = dixit;
        //        }]
        let dict = notification.object as? [String:Any]
        
        self.tempCount = 3
        self.timer.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(self.typingFire(sender:)), userInfo: dict, repeats: true)
    }
    
    @objc func typingFire(sender: Timer) {
        
        DispatchQueue.main.async { [self] in
            
            let dict = sender.userInfo as? [String:Any] ?? [:]
            let otherUser = dict["typing_name"] as? String ?? ""
            
            self.lblTyping.isHidden = false
            self.lblChatStatus.isHidden = true
            self.lblTyping.text = "\(otherUser) typing..."
            //            self.customNavigationBarView.lblStatus.isHidden = false
            //            self.customNavigationBarView.lblStatus.text = "typing..."
            
            self.tempCount -= 1
            if self.tempCount == 0 {
                self.timer.invalidate()
                self.lblTyping.isHidden = true
                lblChatStatus.isHidden = false
                self.lblTyping.text = ""
                //                self.lblTyping.text = "tap here for group info"
                //                self.customNavigationBarView.lblStatus.isHidden = true
            }
        }
        
    }
    
    @objc func notificationReceivedMessage(_ notification: Notification) {
        anotherQueue.async {
            guard let from = notification.userInfo as? [String:Any] else { return }
            var attachDict: [String:Any]?
            if let at = from["attach"] as? [String:Any] {
                attachDict = at
            }
            else if let at = from["attach"] as? [[String:Any]] {
                attachDict = at.first ?? [:]
            }
            let atch = chatHistoryAttach(fileExt: attachDict?["file_ext"] as? String, fileName: attachDict?["file_name"] as? String, filePath: attachDict?["file_path"] as? String, fileSize: attachDict?["file_size"] as? String, fileThumbnailPath: attachDict?["file_thumbnail_path"] as? String)
            if from["sender"] as? String ?? "" == self.receiverMobileNumber || from["chat_id"] as? String ?? "" == self.receiverMobileNumber {
                var param = [String:Any]()
                param["__v"] = from["__v"]
                param["_id"] = from["_id"]
                param["chat_id"] = from["chat_id"]
                param["createdAt"] = from["createdAt"]
                param["message"] = from["message"]
                param["read"] = 1
                param["sender"] = from["sender"]
                param["updatedAt"] = from["updatedAt"]
                param["attach"] = attachDict
                // Bhavesh - 12 Nov 2020
                self.socketEmiter(key: Constants.SocketKeys.messageRead, param: param)
                self.aryData.append(chatHistoryDatum(v: from["__v"] as? Int ?? 0, id: from["_id"] as? String ?? "", chatId: from["chat_id"] as? String ?? "", createdAt: from["createdAt"] as? String ?? "", message: from["message"] as? String ?? "", sender: from["sender"] as? String ?? "", updatedAt: from["updatedAt"] as? String ?? "", read: from["read"] as? Bool ?? false, deliver: from["deliver"] as? Bool ?? false, attach: atch, sortedDate: UtilityClass.convertStringToDate(currentString: from["createdAt"] as? String ?? "", formatOutput: "yyyy-MM-dd"), senderName: from["sender_name"] as? String ?? "", senderUserId: from["sender_user_id"] as? String ?? ""))
                //      self.aryDataItem = []
                //      setupData(res: aryData)
            }
            else if from["sender"] as? String ?? "" == self.senderumber {
                // Bhavesh - 12 Nov 2020
                self.aryData.append(chatHistoryDatum(v: from["__v"] as? Int ?? 0, id: from["_id"] as? String ?? "", chatId: from["chat_id"] as? String ?? "", createdAt: from["createdAt"] as? String ?? "", message: from["message"] as? String ?? "", sender: from["sender"] as? String ?? "", updatedAt: from["updatedAt"] as? String ?? "", read: from["read"] as? Bool ?? false, deliver: from["deliver"] as? Bool ?? false, attach: atch, sortedDate: UtilityClass.convertStringToDate(currentString: from["createdAt"] as? String ?? "", formatOutput: "yyyy-MM-dd"), senderName: from["sender_name"] as? String ?? "", senderUserId: from["sender_user_id"] as? String ?? ""))
                //      self.aryDataItem = []
                //      setupData(res: aryData)
            }
            self.aryDataItem = []
            self.setupData(res: self.aryData, isFromAPI: false)
            
        }
    }
    
    @objc func notificationReadMessage(_ notification: Notification) {
        //      {
        //        "__v" = 0;
        //        "_id" = 5f8944c971138d02943fb548;
        //        "chat_id" = 5f80488335f66749f13af7f2;
        //        createdAt = "2020-10-16 06:59:21";
        //        message = hiy;
        //        read = 1;
        //        sender = 9666377788;
        //      }
        anotherQueue.async {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                guard let from = notification.object as? [String:Any] else { return }
                self.aryData = self.aryData.map({ (item) -> chatHistoryDatum in
                    var currentItem = item
                    if currentItem.id == from["_id"] as? String ?? "" {
                        //        currentItem.v = from["__v"] as? Int ?? 0
                        //        currentItem.id = from["_id"] as? String ?? ""
                        //        currentItem.chatId = from["chat_id"] as? String ?? ""
                        //        currentItem.createdAt = from["createdAt"] as? String ?? ""
                        //        currentItem.message = from["message"] as? String ?? ""
                        currentItem.read = from["read"] as? Bool ?? false
                        //        currentItem.sender = from["sender"] as? String ?? ""
                        return currentItem
                    }
                    return currentItem
                })
                self.aryDataItem = []
                //      self.setupData(res: self.aryData)
                var ary = self.aryData
                for (i,item) in ary.enumerated() {
                    let convertedDate = UtilityClass.convertStringToDate(currentString: item.createdAt ?? "", formatOutput: "yyyy-MM-dd")
                    ary[i].sortedDate = convertedDate
                }
                let dateSorting = ary.sorted { (_$0, _$1) -> Bool in
                    let d1 = UtilityClass.convertStringToDate(currentString: _$0.createdAt ?? "", formatOutput: "yyyy-MM-dd HH:mm:ss")
                    let d2 = UtilityClass.convertStringToDate(currentString: _$1.createdAt ?? "", formatOutput: "yyyy-MM-dd HH:mm:ss")
                    if d1 < d2 {
                        return true
                    }
                    return false
                }
                ary = dateSorting
                let groupData = Dictionary(grouping: ary) { (gallery) -> Date in
                    return gallery.sortedDate ?? Date()
                }
                _ = groupData.forEach { (current) in
                    let dict: [Date:[chatHistoryDatum]] = [current.key:current.value]
                    self.aryDataItem?.append(dict)
                }
                let sortedAry = self.aryDataItem?.sorted(by: { (_$0, _$1) -> Bool in
                    if (_$0.keys.first ?? Date()) < (_$1.keys.first ?? Date()) {
                        return true
                    }
                    return false
                })
                self.aryDataItem = sortedAry
                //    OperationQueue.main.addOperation {
                DispatchQueue.main.async{
                    self.tableView?.indexPathsForVisibleRows?.forEach {
                        if self.aryDataItem?.count == 0 {
                            return
                        }
                        else if (self.aryDataItem?.count ?? 0) <= $0.section { //||
                            return
                        }
                        else if self.aryDataItem?[$0.section].values.count == 0 {
                            return
                        }
                        else if (self.aryDataItem?[$0.section].values.first?.count ?? 0) <= $0.row {
                            return
                        }
                        var current = self.aryDataItem?[$0.section].values.first?[$0.row]
                        current?.read = true
                        current?.deliver = true
                        let name = current?.sender ?? "" == "\(AppGlobalManager.sharedInstance.loggedInUser?.user?.id ?? 0)" ? AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? "" : self.receiverName
                        let number = current?.sender ?? "" == "\(AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "")" ? AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "" : self.receiverMobileNumber
                        let msg = Message(userId: number, userName: name, text: current?.message ?? "", read: current?.read ?? false)
                        let type: CellType = number == (AppGlobalManager.sharedInstance.currentUserID ?? "") ? .right : .left
                        if current?.message == "" {
                            if (current?.attach?.fileExt ?? "").lowercased() == "jpg" || (current?.attach?.fileExt ?? "").lowercased() == "jpeg" || (current?.attach?.fileExt ?? "").lowercased() == "png" || (current?.attach?.fileExt ?? "").lowercased() == "gif" || (current?.attach?.fileExt ?? "").lowercased() == "svg" || (current?.attach?.fileExt ?? "").lowercased() == "heic" || (current?.attach?.fileExt ?? "").lowercased() == "heif" {
                                let msgs = Message(userId: number, userName: name, text: current?.attach?.fileSize ?? "", read: current?.read ?? false)
                                if let cell = self.tableView?.cellForRow(at: $0) as? MessageCellForImage {
                                    cell.currentObj = current
                                    cell.receiverName = self.receiverName
                                    cell.update(type: type, message: msgs, time: UtilityClass.convertToTime(date: current?.createdAt ?? ""), imageURL: current?.attach?.fileThumbnailPath ?? "")
                                    cell.backgroundColor = .clear
                                }
                            }
                            if (current?.attach?.fileExt ?? "").lowercased() == "mov" || (current?.attach?.fileExt ?? "").lowercased() == "mp4" || (current?.attach?.fileExt ?? "").lowercased() == "avi" || (current?.attach?.fileExt ?? "").lowercased() == "mkv" || (current?.attach?.fileExt ?? "").lowercased() == "webm" || (current?.attach?.fileExt ?? "").lowercased() == "3gp" {
                                // For Video Cell Setup
                                let msgs = Message(userId: number, userName: name, text: current?.attach?.fileSize ?? "", read: current?.read ?? false)
                                if let cell = self.tableView?.cellForRow(at: $0) as? MessageCellForVideo {
                                    cell.currentObj = current
                                    cell.receiverName = self.receiverName
                                    cell.update(type: type, message: msgs, time: UtilityClass.convertToTime(date: current?.createdAt ?? ""), imageURL: current?.attach?.fileThumbnailPath ?? "")
                                    cell.backgroundColor = .clear
                                }
                            }
                            else {
                                let msgs = Message(userId: number, userName: name, text: current?.attach?.fileName ?? "", read: current?.read ?? false)
                                if let cell = self.tableView?.cellForRow(at: $0) as? MessageCellForDocument {
                                    cell.currentObj = current
                                    cell.receiverName = self.receiverName
                                    cell.update(type: type, message: msgs, fileName: current?.attach?.fileName, time: UtilityClass.convertToTime(date: current?.createdAt ?? ""), imageURL: current?.attach?.filePath ?? "", fileType: current?.attach?.fileExt ?? "")
                                    cell.backgroundColor = .clear
                                }
                            }
                        }
                        else {
                            if let cell = self.tableView?.cellForRow(at: $0) as? MessageCell {
                                cell.currentObj = current
                                cell.receiverName = self.receiverName
                                cell.update(type: type, message: msg, time: UtilityClass.convertToTime(date: current?.createdAt ?? ""))
                                cell.backgroundColor = .clear
                                //                cell.configure()
                            }
                        }
                    }
                }
                
            })
        }
    }
    
    @objc func notificationReadAllMessage(_ notification: Notification) {
        //    DispatchQueue.global().sync {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            guard let from = notification.object as? [String:Any] else { return }
            print("notificationReadAllMessage :: \(from)")
            var isLoadingNeed = false
            self.aryData = self.aryData.map({ (item) -> chatHistoryDatum in
                var currentItem = item
                if currentItem.read == false {
                    isLoadingNeed = true
                }
                
                currentItem.read = true //from["read"] as? Int ?? 0
                currentItem.deliver = true
                
                return currentItem
            })
            if isLoadingNeed {
                self.aryDataItem = []
                self.setupData(res: self.aryData, isFromAPI: false)
            }
        })
        
    }
    
    @objc func notificationChatUserMessageDeliver(_ notification: Notification) {
        //    DispatchQueue.global().sync {
        anotherQueue.async {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                guard let from = notification.object as? [String:Any] else { return }
                var isLoadingNeed = false
                print("notificationChatUserMessageDeliver :: \(from)")
                self.aryData = self.aryData.map({ (item) -> chatHistoryDatum in
                    var currentItem = item
                    if currentItem.id == from["_id"] as? String ?? "" {
                        if currentItem.deliver == false {
                            isLoadingNeed = true
                        }
                        
                        currentItem.read = from["read"] as? Bool ?? false
                        currentItem.deliver = true
                        
                    }
                    return currentItem
                })
                if isLoadingNeed == false {
                    return
                }
                self.aryDataItem = []
                var ary = self.aryData
                for (i,item) in ary.enumerated() {
                    let convertedDate = UtilityClass.convertStringToDate(currentString: item.createdAt ?? "", formatOutput: "yyyy-MM-dd")
                    ary[i].sortedDate = convertedDate
                }
                let dateSorting = ary.sorted { (_$0, _$1) -> Bool in
                    let d1 = UtilityClass.convertStringToDate(currentString: _$0.createdAt ?? "", formatOutput: "yyyy-MM-dd HH:mm:ss")
                    let d2 = UtilityClass.convertStringToDate(currentString: _$1.createdAt ?? "", formatOutput: "yyyy-MM-dd HH:mm:ss")
                    if d1 < d2 {
                        return true
                    }
                    return false
                }
                ary = dateSorting
                let groupData = Dictionary(grouping: ary) { (gallery) -> Date in
                    return gallery.sortedDate ?? Date()
                }
                _ = groupData.forEach { (current) in
                    let dict: [Date:[chatHistoryDatum]] = [current.key:current.value]
                    self.aryDataItem?.append(dict)
                }
                let sortedAry = self.aryDataItem?.sorted(by: { (_$0, _$1) -> Bool in
                    if (_$0.keys.first ?? Date()) < (_$1.keys.first ?? Date()) {
                        return true
                    }
                    return false
                })
                self.aryDataItem = sortedAry
                DispatchQueue.main.async {
                    self.tableView?.indexPathsForVisibleRows?.forEach {
                        if self.aryDataItem?.count == 0 {
                            return
                        }
                        else if (self.aryDataItem?.count ?? 0) <= $0.section { //||
                            return
                        }
                        else if self.aryDataItem?[$0.section].values.count == 0 {
                            return
                        }
                        else if (self.aryDataItem?[$0.section].values.first?.count ?? 0) <= $0.row {
                            return
                        }
                        var current = self.aryDataItem?[$0.section].values.first?[$0.row]
                        current?.deliver = true
                        let name = current?.sender ?? "" == "\(AppGlobalManager.sharedInstance.loggedInUser?.user?.id ?? 0)" ? AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? "" : self.receiverName
                        let number = current?.sender ?? "" == "\(AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "")" ? AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "" : self.receiverMobileNumber
                        let msg = Message(userId: number, userName: name, text: current?.message ?? "", read: current?.read ?? false)
                        let type: CellType = number == (AppGlobalManager.sharedInstance.currentUserID ?? "") ? .right : .left
                        if current?.message == "" {
                            if (current?.attach?.fileExt ?? "").lowercased() == "jpg" || (current?.attach?.fileExt ?? "").lowercased() == "jpeg" || (current?.attach?.fileExt ?? "").lowercased() == "png" || (current?.attach?.fileExt ?? "").lowercased() == "gif" || (current?.attach?.fileExt ?? "").lowercased() == "svg" || (current?.attach?.fileExt ?? "").lowercased() == "heic" || (current?.attach?.fileExt ?? "").lowercased() == "heif" {
                                let msgs = Message(userId: number, userName: name, text: current?.attach?.fileSize ?? "", read: current?.read ?? false)
                                if let cell = self.tableView?.cellForRow(at: $0) as? MessageCellForImage {
                                    cell.currentObj = current
                                    cell.receiverName = self.receiverName
                                    cell.update(type: type, message: msgs, time: UtilityClass.convertToTime(date: current?.createdAt ?? ""), imageURL: current?.attach?.fileThumbnailPath ?? "")
                                    cell.backgroundColor = .clear
                                }
                            }
                            if (current?.attach?.fileExt ?? "").lowercased() == "mov" || (current?.attach?.fileExt ?? "").lowercased() == "mp4" || (current?.attach?.fileExt ?? "").lowercased() == "avi" || (current?.attach?.fileExt ?? "").lowercased() == "mkv" || (current?.attach?.fileExt ?? "").lowercased() == "webm" || (current?.attach?.fileExt ?? "").lowercased() == "3gp" {
                                // For Video Cell Setup
                                let msgs = Message(userId: number, userName: name, text: current?.attach?.fileSize ?? "", read: current?.read ?? false)
                                if let cell = self.tableView?.cellForRow(at: $0) as? MessageCellForVideo {
                                    cell.currentObj = current
                                    cell.receiverName = self.receiverName
                                    cell.update(type: type, message: msgs, time: UtilityClass.convertToTime(date: current?.createdAt ?? ""), imageURL: current?.attach?.fileThumbnailPath ?? "")
                                    cell.backgroundColor = .clear
                                }
                            }
                            else {
                                let msgs = Message(userId: number, userName: name, text: current?.attach?.fileName ?? "", read: current?.read ?? false)
                                if let cell = self.tableView?.cellForRow(at: $0) as? MessageCellForDocument {
                                    cell.currentObj = current
                                    cell.receiverName = self.receiverName
                                    cell.update(type: type, message: msgs, fileName: current?.attach?.fileName, time: UtilityClass.convertToTime(date: current?.createdAt ?? ""), imageURL: current?.attach?.filePath ?? "", fileType: current?.attach?.fileExt ?? "")
                                    cell.backgroundColor = .clear
                                }
                            }
                        }
                        else {
                            if let cell = self.tableView?.cellForRow(at: $0) as? MessageCell {
                                cell.currentObj = current
                                cell.receiverName = self.receiverName
                                cell.update(type: type, message: msg, time: UtilityClass.convertToTime(date: current?.createdAt ?? ""))
                                cell.backgroundColor = .clear
                                //                cell.configure()
                            }
                        }
                    }
                }
                
            })
        }
        //    }
    }
    
   
    
    // MARK:- UITextView Delegate Methods
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.text.contains(placeHolderText) {
            textView.text = textView.text.replacingOccurrences(of: placeHolderText, with: "")
        }
        
        sendTypingOnSocket()
        
        if textView.contentSize.height >= messageTextViewMaxHeight {
            isOversized = true
        } else {
            isOversized = false
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeHolderText {
            textView.text = nil
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.textColor = UIColor.white
        }
    }
    
    // MARK:- Documnets and Image Picker Methods
    func attachments() {
        inputTextField.resignFirstResponder()
        let actionSheet = UIAlertController(title: "ARMS", message: "Choose one", preferredStyle: .actionSheet)
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
            actionSheet.popoverPresentationController!.sourceView = self.view
            actionSheet.popoverPresentationController!.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        }
        
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.imagePicker.stopVideoCapture()
                self.imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeJPEG as String, kUTTypeGIF as String, kUTTypePNG as String, kUTTypeICO as String, kUTTypeLivePhoto as String]
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Video", style: .default, handler: { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = false
                self.imagePicker.mediaTypes = [kUTTypeMovie as String, kUTTypeAVIMovie as String, kUTTypeVideo as String, kUTTypeMPEG4 as String]
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Document", style: .default, handler: { (act) in
            
            let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeRTFD), String(kUTTypeFlatRTFD), String(kUTTypeTXNTextAndMultimediaData), "com.microsoft.word.doc","org.openxmlformats.wordprocessingml.document"], in: .import)
            
            //            let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
            importMenu.delegate = self
            importMenu.modalPresentationStyle = .formSheet
            self.present(importMenu, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (act) in
            
        }))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let videoUrl = info[.mediaURL] as? URL{
            //            print("\(videoUrl as URL?)")
            let ext = videoUrl.pathExtension // png
            
            if ext.lowercased() == "mov" {
                webserviceForSaveFile(fileType: .video, image: nil, imageUrl: nil, videoUrl: videoUrl, fileUrl: nil, ext: ext)
                { (progress) in
                    if let prg = progress as? Progress {
                        print("Video Uploding : \(prg.fractionCompleted)")
                        self.progressView.isHidden = false
                        self.progressView.progress = Float(prg.fractionCompleted)
                        if prg.fractionCompleted == 1 {
                            self.progressView.isHidden = true
                        }
                    }
                }
            }
            else {
                webserviceForSaveFile(fileType: .image, image: nil, imageUrl: videoUrl, videoUrl: nil, fileUrl: nil, ext: ext)
                { (progress) in
                    if let prg = progress as? Progress {
                        print("Video Uploding : \(prg.fractionCompleted)")
                        self.progressView.isHidden = false
                        self.progressView.progress = Float(prg.fractionCompleted)
                        if prg.fractionCompleted == 1 {
                            self.progressView.isHidden = true
                        }
                    }
                }
            }
            
        }
        else if let imageURL = info[.imageURL] as? URL{
            //            print("\(imageURL as URL?)")
            //            imageURL.lastPathComponent // C323EC31-B5EB-4DBD-B488-9CC6F2E73512.png
            let ext = imageURL.pathExtension // png
            webserviceForSaveFile(fileType: .image, image: nil, imageUrl: imageURL, videoUrl: nil, fileUrl: nil, ext: ext)
            { (progress) in
                if let prg = progress as? Progress {
                    print("Video Uploding : \(prg.fractionCompleted)")
                    self.progressView.isHidden = false
                    self.progressView.progress = Float(prg.fractionCompleted)
                    if prg.fractionCompleted == 1 {
                        self.progressView.isHidden = true
                    }
                }
            }
        }
        else if let imageURL = info[.phAsset] as? URL{
            //            print("\(imageURL as URL?)")
            let ext = imageURL.pathExtension // png
            webserviceForSaveFile(fileType: .image, image: nil, imageUrl: imageURL, videoUrl: nil, fileUrl: nil, ext: ext)
            { (progress) in
                if let prg = progress as? Progress {
                    print("Video Uploding : \(prg.fractionCompleted)")
                    self.progressView.isHidden = false
                    self.progressView.progress = Float(prg.fractionCompleted)
                    if prg.fractionCompleted == 1 {
                        self.progressView.isHidden = true
                    }
                }
            }
        }
        else if var image = info[.originalImage] as? UIImage {
            //            print(image)
            image = image.upOrientationImage() ?? image
            
            selectedImages.append(image)
            
            switch image.sd_imageFormat {
            case .GIF:
                webserviceForSaveFile(fileType: .image, image: image, imageUrl: nil, videoUrl: nil, fileUrl: nil, ext: "gif")
                { (progress) in
                    if let prg = progress as? Progress {
                        print("Video Uploding : \(prg.fractionCompleted)")
                        self.progressView.isHidden = false
                        self.progressView.progress = Float(prg.fractionCompleted)
                        if prg.fractionCompleted == 1 {
                            self.progressView.isHidden = true
                        }
                    }
                }
            case .JPEG:
                webserviceForSaveFile(fileType: .image, image: image, imageUrl: nil, videoUrl: nil, fileUrl: nil, ext: "jpeg")
                { (progress) in
                    if let prg = progress as? Progress {
                        print("Video Uploding : \(prg.fractionCompleted)")
                        self.progressView.isHidden = false
                        self.progressView.progress = Float(prg.fractionCompleted)
                        if prg.fractionCompleted == 1 {
                            self.progressView.isHidden = true
                        }
                    }
                }
            case .PNG:
                webserviceForSaveFile(fileType: .image, image: image, imageUrl: nil, videoUrl: nil, fileUrl: nil, ext: "png")
                { (progress) in
                    if let prg = progress as? Progress {
                        print("Video Uploding : \(prg.fractionCompleted)")
                        self.progressView.isHidden = false
                        self.progressView.progress = Float(prg.fractionCompleted)
                        if prg.fractionCompleted == 1 {
                            self.progressView.isHidden = true
                        }
                    }
                }
            case .SVG:
                webserviceForSaveFile(fileType: .image, image: image, imageUrl: nil, videoUrl: nil, fileUrl: nil, ext: "svg")
                { (progress) in
                    if let prg = progress as? Progress {
                        print("Video Uploding : \(prg.fractionCompleted)")
                        self.progressView.isHidden = false
                        self.progressView.progress = Float(prg.fractionCompleted)
                        if prg.fractionCompleted == 1 {
                            self.progressView.isHidden = true
                        }
                    }
                }
            case .HEIC:
                webserviceForSaveFile(fileType: .image, image: image, imageUrl: nil, videoUrl: nil, fileUrl: nil, ext: "heic")
                { (progress) in
                    if let prg = progress as? Progress {
                        print("Video Uploding : \(prg.fractionCompleted)")
                        self.progressView.isHidden = false
                        self.progressView.progress = Float(prg.fractionCompleted)
                        if prg.fractionCompleted == 1 {
                            self.progressView.isHidden = true
                        }
                    }
                }
            case .HEIF:
                webserviceForSaveFile(fileType: .image, image: image, imageUrl: nil, videoUrl: nil, fileUrl: nil, ext: "heif")
                { (progress) in
                    if let prg = progress as? Progress {
                        print("Video Uploding : \(prg.fractionCompleted)")
                        self.progressView.isHidden = false
                        self.progressView.progress = Float(prg.fractionCompleted)
                        if prg.fractionCompleted == 1 {
                            self.progressView.isHidden = true
                        }
                    }
                }
            default:
                webserviceForSaveFile(fileType: .image, image: image, imageUrl: nil, videoUrl: nil, fileUrl: nil, ext: "jpeg")
                { (progress) in
                    if let prg = progress as? Progress {
                        print("Video Uploding : \(prg.fractionCompleted)")
                        self.progressView.isHidden = false
                        self.progressView.progress = Float(prg.fractionCompleted)
                        if prg.fractionCompleted == 1 {
                            self.progressView.isHidden = true
                        }
                    }
                }
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        webserviceForSaveFile(fileType: .file, image: nil, imageUrl: nil, videoUrl: nil, fileUrl: myURL, ext: myURL.pathExtension)
        { (progress) in
            if let prg = progress as? Progress {
                print("Video Uploding : \(prg.fractionCompleted)")
                self.progressView.isHidden = false
                self.progressView.progress = Float(prg.fractionCompleted)
                if prg.fractionCompleted == 1 {
                    self.progressView.isHidden = true
                }
            }
        }
    }
    
    public func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        //        print("view was cancelled")
        
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- Date Formatter Methods
    func convertToTimeToStatus(updatedDate: String) -> String {
        if updatedDate == "" {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // "yyyy-MM-dd HH:mm:ss"//"yyyy-MM-dd'T'HH:mm:ssZ"
        
        
        
        let timeStamp = self.UTCToLocal(UTCDateString: updatedDate)
        
        let date = dateFormatter.date(from: timeStamp)
        
        if Calendar.current.isDateInToday(date ?? Date()) {
            dateFormatter.dateFormat = "h:mm a"
            let timeStamp2 = dateFormatter.string(from: date ?? Date())
            return "Today \(timeStamp2)"
        }
        else if Calendar.current.isDateInYesterday(date ?? Date()) {
            dateFormatter.dateFormat = "h:mm a"
            let timeStamp2 = dateFormatter.string(from: date ?? Date())
            return "Yesterday \(timeStamp2)"
        }
        return timeStamp
    }
    
    func UTCToLocal(UTCDateString: String) -> String {
        
        if UTCDateString == "" {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Input Format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let UTCDate = dateFormatter.date(from: UTCDateString)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // Output Format
        dateFormatter.timeZone = TimeZone.current
        let UTCToCurrentFormat = dateFormatter.string(from: UTCDate ?? Date())
        //        print("Local Date %@",UTCToCurrentFormat)
        return UTCToCurrentFormat
    }
    
    // --------------------------------------------
    // MARK:- Webservice For Save File
    // --------------------------------------------
    
    func webserviceForSaveFile(fileType: FileType, image: UIImage?, imageUrl: URL?, videoUrl: URL?, fileUrl: URL?, ext: String, completion: @escaping (_ result: Any) -> Void) {
        
        //        let activityData = ActivityData()
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else { return }
        guard let user_id = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else {  return }
        var parameters = [String : Any]()
        
        var fileName = imageUrl?.lastPathComponent.components(separatedBy: ".").first ?? ""
        
        if fileName == "" {
            fileName = videoUrl?.lastPathComponent.components(separatedBy: ".").first ?? ""
            if fileName == "" {
                fileName = fileUrl?.lastPathComponent.components(separatedBy: ".").first ?? ""
            } else {
                fileName = Date().toString(dateFormat: "yyyy:MM:dd_HH:mm:ss")
            }
        }
        else {
            fileName = Date().toString(dateFormat: "yyyy:MM:dd_HH:mm:ss")
        }
        strFileName = fileName
        
        parameters["device_id"] = device_id
        parameters["user_id"] = "\(user_id)"
        //        parameters["session_id"] = ""
        //        parameters["user_call_id"] = ""
        //        parameters["tempnamepath"] = ""
        parameters["receiver_id"] = "\(receiverUser?.id ?? 0)"
        parameters["device_type"] = "0"
        
        //        selectedImages.first!
        APIHandler.postDataWithImage(parameter: parameters, image: image, imageUrl: imageUrl, videoUrl: videoUrl, fileUrl: fileUrl, fileType: fileType, fileParamName: "uploadfiledata[]", imageName: strFileName, nsURL: APIConstants.EndPoints.saveChatFile, ext: ext) { (response, state) in
            
            self.selectedImages.removeAll()
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if state {
                if let res = response as? [String: Any] {
                    if let aryOutput = res["output"] as? [[String: Any]] {
                        
                        var param = [String: Any]()
                        param["from"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
                        param["to"] = self.receiverMobileNumber
                        param["message"] = ""
                        param["sender_name"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? ""
                        param["sender_user_id"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.id ?? ""
                        
                        for item in aryOutput {
                            param["attach"] = item
                            if self.socket.status == .connected {
                                
                                
                                if self.isFromGroupChat {
                                    param["group_name"] = self.receiverName
                                    param["group"] = true
                                    self.socketEmiter(key: Constants.SocketKeys.message, param: param)
                                }else {
                                    self.socketEmiter(key: Constants.SocketKeys.message, param: param)
                                }
                                
                            }
                            else {
                                //                            UtilityClass.showAlertWithCancel(title: "", message: "Connection issue, please try again") { (act) in
                                self.socket.connect()
                                
                                if self.isFromGroupChat {
                                    param["group_name"] = self.receiverName
                                    param["group"] = true
                                    self.socketEmiter(key: Constants.SocketKeys.message, param: param)
                                }else {
                                    self.socketEmiter(key: Constants.SocketKeys.message, param: param)
                                }
                            }
                        }
                        
                    }
                }
                else if let progress = response as? Progress {
                    completion(progress)
                }
            } else {
                if ((response as? String) != nil) {
                    self.showToast(response as? String ?? "Something went wrong !! Please try again later", type: .fail)
                }
            }
        }
    }
    
    func generateBoundary() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createDataBody(withParameters params: Parameters?, media: [Media]?, boundary: String) -> Data {
        
        let lineBreak = "\r\n"
        var body = Data()
        
        if let parameters = params {
            for (key, value) in parameters {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)")
                body.append("\("\(value as? String)" + lineBreak)")
            }
        }
        
        if let media = media {
            for photo in media {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(photo.key)\"; filename=\"\(photo.filename)\"\(lineBreak)")
                body.append("Content-Type: \(photo.mimeType + lineBreak + lineBreak)")
                body.append(photo.data)
                body.append(lineBreak)
            }
        }
        
        body.append("--\(boundary)--\(lineBreak)")
        
        return body
    }
    
    
    //--------------------------------------------------------------------------------
    //MARK:- Webservice  Methdos
    //--------------------------------------------------------------------------------
    func chatUserHistory(chatId: String, pageNo: Int) {
        
        //        https://arms.pluto-men.com/api/chatMessages/5f80488335f66749f13af7f2?page=1
        
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        
        var pg = "&page=\(pageNo)"
        if pageNo == 1 {
            pg = ""
        }
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.chatMessages + "/" + "\(chatId)" + "?" + "device_id=" + "\(device_id)\(pg)" + "&" + "device_type=" + "0", parameters: nil, token: AppGlobalManager.sharedInstance.token, method: .GET) { (result) in
            
            DispatchQueue.main.async {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                switch result {
                case .success( let data):
                    
                    do {
                        let res = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        print(res)
                    } catch {
                        //                        print(error.localizedDescription)
                    }
                    
                    do {
                        let object = try JSONDecoder().decode(chatHistoryMain.self, from: data)
                        //                    let res = try JSONDecoder().decode(chatUserListMain.self, from: data)
                        
                        if let status = object.statusCode,status == 1 {
                            self.list = []
                            
                            let res = object.output?.data
                            if pageNo == 1 {
                                self.aryData = object.output?.data ?? []
                                self.model = object.output?.group
                            }
                            else {
                                for item in res ?? [] {
                                    self.aryData.append(item)
                                }
                            }
                            
                            //                            self.aryDataItem = []
                            self.setupData(res: res!, isFromAPI: true)
                            
                            res?.forEach({ (item) in
                                
                                let name = item.sender ?? "" == "\(AppGlobalManager.sharedInstance.loggedInUser?.user?.id ?? 0)" ? AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? "" : self.receiverName
                                let number = item.sender ?? "" == "\(AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "")" ? AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "" : self.receiverMobileNumber
                                
                                let msg = Message(userId: number, userName: name, text: item.message ?? "", read: item.read ?? false)
                                self.list.append(msg)
                            })
                            
                            
                            //                            self.tableView?.reloadData()
                            return
                        }
                    }
                    catch {
                        print(error.localizedDescription)
                    }
                case .failure(let error):
                    self.showToast(error.localizedDescription, type: .fail)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }
            }
        }
    }
    
    func setupData(res: [chatHistoryDatum],isFromAPI: Bool) {
        //         self.aryDataItem = []
        
        DispatchQueue.global(qos: .background).async {
            //DispatchQueue.global().async {
            var ary = res
            for (i,item) in ary.enumerated() {
                //                let convertedDate = self.convertToDate(date: item.createdAt ?? "", formatOutput: "yyyy-MM-dd")
                
                let convertedDate = UtilityClass.convertStringToDate(currentString: item.createdAt ?? "", formatOutput: "yyyy-MM-dd")//?.toLocalTime()
                //                print("Converted Date From: \("2021-02-04 22:54:39") To: \(convertedDate)")
                ary[i].sortedDate = convertedDate
            }
            
            let dateSorting = ary.sorted { (_$0, _$1) -> Bool in
                
                let d1 = UtilityClass.convertStringToDate(currentString: _$0.createdAt ?? "", formatOutput: "yyyy-MM-dd HH:mm:ss")
                let d2 = UtilityClass.convertStringToDate(currentString: _$1.createdAt ?? "", formatOutput: "yyyy-MM-dd HH:mm:ss")
                
                if d1 < d2 {
                    return true
                }
                return false
            }
            
            ary = dateSorting
            
            let groupData = Dictionary(grouping: ary) { (gallery) -> Date in
                return gallery.sortedDate ?? Date()
            }
            
            _ = groupData.forEach { (current) in
                let dict: [Date:[chatHistoryDatum]] = [current.key:current.value]
                self.aryDataItem?.append(dict)
            }
            
            let sortedAry = self.aryDataItem?.sorted(by: { (_$0, _$1) -> Bool in
                if (_$0.keys.first ?? Date()) < (_$1.keys.first ?? Date()) {
                    return true
                }
                return false
            })
            
            self.aryDataItem = sortedAry
            
            //        for i in 0..<res.count {
            //            let from = res[i]
            //            if (from.read ?? 0) == 0 {
            //                var param = [String:Any]()
            //                if from.sender != AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "" {
            //                    param["__v"] = from.v
            //                    param["_id"] = from.id
            //                    param["chat_id"] = from.chatId
            //                    param["createdAt"] = from.createdAt
            //                    param["message"] = from.message
            //                    param["read"] = 1
            //                    param["sender"] = from.sender
            //                    param["updatedAt"] = from.updatedAt
            //                    self.socketEmiter(key: Constants.SocketKeys.messageRead, param: param)
            //                }
            //            }
            //        }
            
            if isFromAPI {
                let readZero = res.filter{($0.read ?? false) == false}
                if readZero.count != 0 {
                    let anotherUserMessage = readZero.last?.sender ?? "" != AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
                    if anotherUserMessage {
                        var param = [String:Any]()
                        param["chat_id"] = self.chatId
                        param["from"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
                        param["to"] = self.receiverMobileNumber
                        self.socketEmiter(key: Constants.SocketKeys.messageReadAll, param: param)
                    }
                }
            }
            
            //        DispatchQueue(label: "background").async {
            //            let end = IndexPath(row: (self.aryDataItem?.last?.values.first?.count ?? 1) - 1, section: (self.aryDataItem?.count ?? 0) - 1)
            DispatchQueue.main.async { [unowned self] in
                self.tableView?.reloadData()
                if self.pageNo == 1 {
                    self.tableView?.tableViewScrollToBottom(animated: false)
                    //                    self.tableView?.scrollToRow(at: end, at: .bottom, animated: false)
                }
                
                //                self.tableView?.reloadData()
            }
            //        }
            
            //        let end = IndexPath(row: (self.aryDataItem?.last?.values.first?.count ?? 1) - 1, section: (self.aryDataItem?.count ?? 0) - 1)
            //        DispatchQueue.main.async { [unowned self] in
            //            self.tableView?.reloadData()
            //            if self.pageNo == 1 {
            //                self.tableView?.scrollToRow(at: end, at: .bottom, animated: true)
            //            }
            //        }
        }
    }
    
}

private extension ChatGroupViewController {
    func updateViews() {
        
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 34
        tableView?.contentInset = .init(top: 20, left: 0, bottom: 0, right: 0)
        
        let nibName = UINib(nibName: "HeaderInChatScreen", bundle: nil)
        self.tableView?.register(nibName, forHeaderFooterViewReuseIdentifier: "HeaderInChatScreen")
        
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "chatbackgroundPNG")
        imageView.backgroundColor = UIColor.lightGray
        
        imageView.contentMode = .scaleAspectFill
        
        viewTextFieldContainer.layer.borderColor = UIColor.white.cgColor // Constants.AppTheme.Colors.ColorsOfApp.green.color.cgColor
        viewTextFieldContainer.layer.borderWidth = 2
        viewTextFieldContainer.layer.cornerRadius = 16
        
        //        let placeHolderText = inputTextField.placeholder ?? "Please enter message"
        //        inputTextField.attributedPlaceholder = NSAttributedString(string: placeHolderText,
        //                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        UtilityClass.cornerWithBorder(vw: viewTextFieldContainer, size: viewTextFieldContainer.frame.size.height / 2, border: 1, color: UIColor.white)
        
        
        //        let blurEffect = UIBlurEffect(style: .extraLight)
        //        let blurView = UIVisualEffectView(effect: blurEffect)
        //        blurView.frame = tableView.bounds
        //        imageView.addSubview(blurView)
        
        
        //    tableView.backgroundView = imageView
    }
    
    func pressedReturnToSendText(_ text: String?) -> Bool {
        guard let text = text?.trimmingCharacters(in: .whitespaces), text.count > 0 else {
            return false
        }
        
        if socket.status == .connected {
            //            self.sendChatMessage?(text)
            
            appendMessage(userID: AppGlobalManager.sharedInstance.currentUserID ?? "0", userName: AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? "0" , content: text)
            
            sendMessageOnSocket(message: text)
        }
        else {
            UtilityClass.showAlertWithCancel(title: "", message: "Connection issue, please try again") { (act) in
                if AppGlobalManager.sharedInstance.loggedInUser != nil {
                    SocketIOManager.shared.establishConnection()
                }
                //                self.sendChatMessage?(text)
                self.appendMessage(userID: AppGlobalManager.sharedInstance.currentUserID ?? "0", userName: AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? "0" , content: text)
                self.sendMessageOnSocket(message: text)
            }
        }
        
        //        let dt = convertToStringForHeader(date: Date(), format: "yyyy-MM-dd HH:mm:ss")
        //        aryData.append(chatHistoryDatum(v: 0, id: "", chatId: "", createdAt: dt, message: text, sender: AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "0", updatedAt: dt, read: 0, sortedDate: Date()))
        //        setupData(res: aryData)
        
        return true
    }
    
    func appendMessage(userID: String,userName:String, content: String) {
        let msg = Message(userId: userID, userName: userName, text: content, read: false)
        list.append(msg)
        //        if list.count > 300 {
        //            list.removeFirst()
        //        }
        
        //        let end = IndexPath(row: list.count - 1, section: 0)
        //        DispatchQueue.main.async { [unowned self] in
        //            self.tableView.reloadData()
        //            self.tableView.scrollToRow(at: end, at: .bottom, animated: true)
        //            let systemSoundID: SystemSoundID = 1115
        //            AudioServicesPlaySystemSound (systemSoundID)
        //        }
        
    }
    
    func addKeyboardObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardFrameWillChange(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func keyboardFrameWillChange(notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let endKeyboardFrameValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
              let durationValue = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber else {
            return
        }
        
        let endKeyboardFrame = endKeyboardFrameValue.cgRectValue
        let duration = durationValue.doubleValue
        
        let isShowing: Bool = endKeyboardFrame.maxY > UIScreen.main.bounds.height ? false : true
        
        UIView.animate(withDuration: duration) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            
            if isShowing {
                let offsetY = strongSelf.inputContainView.frame.maxY - endKeyboardFrame.minY
                guard offsetY > 0 else {
                    return
                }
                strongSelf.cons_ViewMove_bottom.constant = CGFloat(offsetY)
                //                                strongSelf.tableView?.contentInset = .init(top: offsetY, left: 0, bottom: 0, right: 0)
                strongSelf.tableView?.tableViewScrollToBottom(animated: true)
                //                strongSelf.tableView?.contentInset = .init(top: offsetY, left: 0, bottom: 0, right: 0)
                //  strongSelf.customNavigationBarView.frame = CGRect(x: 0, y: 0, width: self?.view.frame.width ?? 100, height: 60)
                //     strongSelf.inputViewBottom.constant = -offsetY
            } else {
                //   strongSelf.inputViewBottom.constant = 0
                strongSelf.cons_ViewMove_bottom.constant = 0
                //                strongSelf.tableView?.contentInset = .init(top: 20, left: 0, bottom: 0, right: 0)
                //  strongSelf.customNavigationBarView.frame = CGRect(x: 0, y: 0, width: self?.view.frame.width ?? 100, height: 60)
                
            }
            strongSelf.view.layoutIfNeeded()
        }
    }
}

// MARK: Tableview Methods
extension ChatGroupViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.aryDataItem?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.aryDataItem?[section].values.first?.count ?? 0
        
        //        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.aryDataItem?.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.init(named: "PrimaryDark")
            return cell
        }
        else if (self.aryDataItem?.count ?? 0) <= indexPath.section  { //|| (self.aryDataItem?.count ?? 0) == indexPath.section {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.init(named: "PrimaryDark")
            return cell
        }
        else if self.aryDataItem?[indexPath.section].values.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.init(named: "PrimaryDark")
            return cell
        }
        else if (self.aryDataItem?[indexPath.section].values.first?.count ?? 0) <= indexPath.row { //|| (self.aryDataItem?[indexPath.section].values.first?.count ?? 0) == indexPath.row {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.init(named: "PrimaryDark")
            return cell
        }
        
        
        let current = self.aryDataItem?[indexPath.section].values.first?[indexPath.row]
        
        //        print(current)
        
        let name = current?.sender ?? "" == "\(AppGlobalManager.sharedInstance.loggedInUser?.user?.id ?? 0)" ? AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? "" : self.receiverName
        let number = current?.sender ?? "" == "\(AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "")" ? AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "" : self.receiverMobileNumber
        
        let msg = Message(userId: number, userName: name, text: current?.message ?? "", read: current?.read ?? false)
        let type: CellType = number == (AppGlobalManager.sharedInstance.currentUserID ?? "") ? .right : .left
        
        if current?.message == "" {
            
            if (current?.attach?.fileExt ?? "").lowercased() == "jpg" || (current?.attach?.fileExt ?? "").lowercased() == "jpeg" || (current?.attach?.fileExt ?? "").lowercased() == "png" || (current?.attach?.fileExt ?? "").lowercased() == "gif" || (current?.attach?.fileExt ?? "").lowercased() == "svg" || (current?.attach?.fileExt ?? "").lowercased() == "heic" || (current?.attach?.fileExt ?? "").lowercased() == "heif" {
                
                let msgs = Message(userId: number, userName: name, text: current?.attach?.fileSize ?? "", read: current?.read ?? false)
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCellForImage", for: indexPath) as! MessageCellForImage
                cell.isFromGroupChat = true
                cell.currentObj = current
                cell.receiverName = receiverName
                
                cell.update(type: type, message: msgs, time: UtilityClass.convertToTime(date: current?.createdAt ?? ""), imageURL: current?.attach?.fileThumbnailPath ?? "")
                cell.backgroundColor = .clear
                cell.leftUserLabel.isHidden = false
                cell.leftUserLabel.text = current?.senderName
                return cell
            }
            if (current?.attach?.fileExt ?? "").lowercased() == "mov" || (current?.attach?.fileExt ?? "").lowercased() == "mp4" || (current?.attach?.fileExt ?? "").lowercased() == "avi" || (current?.attach?.fileExt ?? "").lowercased() == "mkv" || (current?.attach?.fileExt ?? "").lowercased() == "webm" || (current?.attach?.fileExt ?? "").lowercased() == "3gp" {
                // For Video Cell Setup
                
                let msgs = Message(userId: number, userName: name, text: current?.attach?.fileSize ?? "", read: current?.read ?? false)
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCellForVideo", for: indexPath) as! MessageCellForVideo
                cell.isFromGroupChat = true
                cell.currentObj = current
                cell.receiverName = receiverName
                cell.update(type: type, message: msgs, time: UtilityClass.convertToTime(date: current?.createdAt ?? ""), imageURL: current?.attach?.fileThumbnailPath ?? "")
                cell.backgroundColor = .clear
                cell.leftUserLabel.isHidden = false
                cell.leftUserLabel.text = current?.senderName
                return cell
            }
            else {
                
                let msgs = Message(userId: number, userName: name, text: current?.attach?.fileName ?? "", read: current?.read ?? false)
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCellForDocument", for: indexPath) as! MessageCellForDocument
                cell.isFromGroupChat = true
                //                cell.update(type: type, message: msgs, time: convertToTime(date: current?.createdAt ?? ""), imageURL: current?.attach?.filePath ?? "", fileType: (current?.attach?.fileExt ?? ""))
                
                cell.currentObj = current
                cell.receiverName = receiverName
                cell.update(type: type, message: msgs, fileName: current?.attach?.fileName, time: UtilityClass.convertToTime(date: current?.createdAt ?? ""), imageURL: current?.attach?.filePath ?? "", fileType: current?.attach?.fileExt ?? "")
                cell.backgroundColor = .clear
                cell.leftUserLabel.isHidden = false
                cell.leftUserLabel.text = current?.senderName
                return cell
            }
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
            cell.isFromGroupChat = true
            cell.currentObj = current
            cell.receiverName = receiverName
            
            cell.update(type: type, message: msg, time: UtilityClass.convertToTime(date: current?.createdAt ?? ""))
            cell.leftUserLabel.isHidden = false
            cell.leftUserLabel.text = current?.senderName
            
            cell.backgroundColor = .clear
            return cell
        }
        
        
        
        //        let msg = list[indexPath.row]
        //        let type: CellType = msg.userId == (AppGlobalManager.sharedInstance.currentUserID ?? "") ? .right : .left
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        //        cell.update(type: type, message: msg)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        FileDetailsForChatViewController
        
        if self.aryDataItem?.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.init(named: "PrimaryDark")
        }
        else if (self.aryDataItem?.count ?? 0) <= indexPath.section  { //|| (self.aryDataItem?.count ?? 0) == indexPath.section {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.init(named: "PrimaryDark")
        }
        else if self.aryDataItem?[indexPath.section].values.count == 0 {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.init(named: "PrimaryDark")
        }
        else if (self.aryDataItem?[indexPath.section].values.first?.count ?? 0) <= indexPath.row { //|| (self.aryDataItem?[indexPath.section].values.first?.count ?? 0) == indexPath.row {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
            cell.backgroundColor = UIColor.init(named: "PrimaryDark")
        }
        
        
        let current = self.aryDataItem?[indexPath.section].values.first?[indexPath.row]
        
        if current?.message == "" {
            self.view.endEditing(true)
            self.cons_ViewMove_bottom.constant = 0
            inputTextField.text = nil
            inputTextField.text = placeHolderText
            isOversized = false
            
            if (current?.attach?.fileExt ?? "").lowercased() == "jpg" || (current?.attach?.fileExt ?? "").lowercased() == "jpeg" || (current?.attach?.fileExt ?? "").lowercased() == "png" || (current?.attach?.fileExt ?? "").lowercased() == "gif" || (current?.attach?.fileExt ?? "").lowercased() == "svg" || (current?.attach?.fileExt ?? "").lowercased() == "heic" || (current?.attach?.fileExt ?? "").lowercased() == "heif" {
                
                let next = self.storyboard?.instantiateViewController(withIdentifier: "FileDetailsForChatViewController") as! FileDetailsForChatViewController
                next.fileTypes = .image
                next.strUrl = current?.attach?.filePath ?? ""
                self.present(next, animated: true, completion: nil)
            }
            else if (current?.attach?.fileExt ?? "").lowercased() == "mov" || (current?.attach?.fileExt ?? "").lowercased() == "mp4" || (current?.attach?.fileExt ?? "").lowercased() == "avi" || (current?.attach?.fileExt ?? "").lowercased() == "mkv" || (current?.attach?.fileExt ?? "").lowercased() == "webm" || (current?.attach?.fileExt ?? "").lowercased() == "3gp" {
                
                let next = self.storyboard?.instantiateViewController(withIdentifier: "FileDetailsForChatViewController") as! FileDetailsForChatViewController
                next.fileTypes = .video
                next.strUrl = current?.attach?.filePath ?? ""
                self.present(next, animated: true, completion: nil)
            }
            else {
                let next = self.storyboard?.instantiateViewController(withIdentifier: "FileDetailsForChatViewController") as! FileDetailsForChatViewController
                next.fileTypes = .file
                next.strUrl = current?.attach?.filePath ?? ""
                self.present(next, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if (self.aryDataItem?.count ?? 0) > section {
            let zxc = (self.aryDataItem?[section].keys.first)! // self.ary[indexPath.section].first?.sortedDate ?? Date()
            let str = UtilityClass.convertToStringForHeader(date: zxc)
            if str == "" {
                return nil
            }
            let headerView = self.tableView?.dequeueReusableHeaderFooterView(withIdentifier: "HeaderInChatScreen" ) as! HeaderInChatScreen
            headerView.setupData(text: str)
            return headerView
            
        }
        return nil
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
}

extension ChatGroupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if pressedReturnToSendText(textField.text) {
            textField.text = nil
        } else {
            view.endEditing(true)
        }
        self.resignFirstResponder()
        return true
    }
    
    
}

extension ChatGroupViewController {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        isDataLoading = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    }
    //Pagination
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if ((tableView?.contentOffset.y ?? 0) + (tableView?.frame.size.height ?? 0)) >= (tableView?.contentSize.height ?? 0)
        {
            //            refreshContactData()
        }
        else if (tableView?.contentOffset.y ?? 0) <= 0 {
            if !isDataLoading{
                isDataLoading = true
                self.pageNo = self.pageNo+1
                chatUserHistory(chatId: chatId, pageNo: self.pageNo)
                
            }
        }
    }
}



