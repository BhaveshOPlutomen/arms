//
//  ChatUsersListViewController.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 23/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import SocketIO
import NVActivityIndicatorView
import SwiftToast
import AVKit




class ChatUsersListViewController: BaseViewController, Alertable, Toastable {
    
    @IBOutlet weak var tableView: UITableView?
    
    @IBOutlet weak var parentView: UIView!
    
    @IBOutlet weak var segmentCtrl: UISegmentedControl!
    @IBOutlet weak var tableViewGroup: UITableView!
    var aryChatData = [chatUsersModel]()
    var aryFilterChatData = [chatUsersModel]()
    var aryGroupsChatData = [chatGroupModel]()
    var aryFilterGroupsChatData = [chatGroupModel]()
    
    private let refreshControlForGroupChat = UIRefreshControl()
    
    var isFromGroupChat = false
    var isSearchOn = false
    var searchController = UISearchController()
    
    let socket = SocketIOManager.shared.socket
    var tempCount: Int = 3
    var timer = Timer()
    var receiverMobileNumber = String()
    var receiverId = String()
    var receiverName = String()
    var receiverUser: User?
    private let refreshControl = UIRefreshControl()
    
    // Guest Mode
    var roomName:String!
    var call:Call!
    
    override func loadView() {
        super.loadView()
        if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
          parentView.isHidden = true
          DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.parentView.isHidden = false
          }
        } else {
          self.parentView.isHidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        let rightButtonItem = UIBarButtonItem.init(
        //        title: "Title",
        //        style: .done,
        //        target: self,
        //        action: #selector(self.rightButtonAction(sender:)))
        
//        self.title = "Chat"
//        let rightButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "setting"), style: .plain, target: self, action: #selector(self.rightButtonAction(sender:)))
//        self.navigationItem.rightBarButtonItem = rightButtonItem
        
//        chatHistory()

        self.addrefreshControl()
        setupSearchbar()
        segmentCtrl.isHidden = true
        tableView?.isHidden = false
        tableViewGroup.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
       
         self.chatHistory()
       
    }
    
    deinit {
        print("\n\n\n \t ChatUserListVC DEINIT \n\n\n")
    }
    
    @objc func rightButtonAction(sender: UIBarButtonItem) {
        
        let chat = SettingsListViewController.viewController()
        self.navigationController?.pushViewController(chat, animated: true)
    }
    
    func addrefreshControl(){
        
        if #available(iOS 10.0, *) {
            tableView?.refreshControl = refreshControl
            tableViewGroup?.refreshControl = refreshControlForGroupChat
        } else {
            tableView?.addSubview(refreshControl)
            tableViewGroup?.addSubview(refreshControlForGroupChat)
        }
        refreshControl.addTarget(self, action: #selector(refreshContactData(_:)), for: .valueChanged)
        refreshControlForGroupChat.addTarget(self, action: #selector(refreshContactDataGroupChat(_:)), for: .valueChanged)
        
    }
    @objc private func refreshContactData(_ sender: Any) {
        refreshControl.beginRefreshing()
        chatHistory()
        refreshControl.endRefreshing()
    }
    @objc private func refreshContactDataGroupChat(_ sender: Any) {
          refreshControlForGroupChat.beginRefreshing()
          chatHistory()
          refreshControlForGroupChat.endRefreshing()
      }
    
    class func viewController () -> ChatUsersListViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ChatUsersListViewController") as! ChatUsersListViewController
    }
    
    private func setupSearchbar() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Contacts"
        //      searchController.searchBar.barTintColor  = UIColor(named: "LightGray")
        searchController.searchBar.tintColor = .white
        //   searchController.searchBar.isTranslucent = false
        let attributes =  [NSAttributedString.Key.foregroundColor: UIColor.white]
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = attributes
        
        if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
            if let backgroundview = textfield.subviews.first {
                
                // Background color
                backgroundview.backgroundColor =  #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)  //UIColor(hexFromString: "#2F3740", alpha: 1.0)
                
                // Rounded corner
                backgroundview.layer.cornerRadius = 14;
                backgroundview.clipsToBounds = true;
            }
        }
        navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController = searchController
        self.definesPresentationContext = true
    }
    @IBAction func segmentCtrlAction(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        
        case 0:
            NSLog("Chat selected")
            //show popular view
            tableView?.isHidden = false
            tableViewGroup.isHidden = true
            isFromGroupChat = false
        case 1:
            NSLog("Group selected")
            //show history view
            tableView?.isHidden = true
            tableViewGroup.isHidden = false
            isFromGroupChat = true
        default:
            break;
        }
    }
    func socketEmiter(key: String, param: Any) {
        
        if self.socket.status == .connected {
            self.socket.emit(key, with: [param])
            print("KEY :: \(key)\nPARAM ::\(param)")

        } else {
            socket.connect()
//            socketEmiter(key: key, param: param)
            
            UtilityClass.showAlertWithCancel(title: "", message: "Connection issue, please try again") { (act) in
                self.socketEmiter(key: key, param: param)
            }
        }
    }
    
    
    func socketOnMethods() {
        onSocketUnauthorized()
        onSocketUserConnected()
        onSocketMessage()
        onSocketTyping()
        onSocketisOnline()
        onSocketuserLive()
        onSocketUserstatus()
        onSocketMessageRead()
        onSocketMessageReadAll()
        onSocketMessageDeliver()
    }
    
    func socketDisconnect() {
        
        //        {status : "offline", mobile:state.mobile ,at : "2020-09-25 17:02:02"}
        
        var statusParam = [String:Any]()
        statusParam["status"] = "offline"
        statusParam["mobile"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
        statusParam["at"] = UtilityClass.convertToString(date: Date())
        statusParam["device_type"] = "0"
        self.socketEmiter(key: Constants.SocketKeys.userstatus, param: statusParam)
        
        socket.off(Constants.SocketKeys.unauthorized)
        socket.off(Constants.SocketKeys.userconnected)
        socket.off(Constants.SocketKeys.message)
        socket.off(Constants.SocketKeys.typing)
        socket.off(Constants.SocketKeys.userstatus)
        socket.off(Constants.SocketKeys.messageRead)
        socket.off(Constants.SocketKeys.messageReadAll)
        socket.off(Constants.SocketKeys.messageDeliver)
    }
    
    func onSocketUnauthorized() {
        socket.on(Constants.SocketKeys.unauthorized) { (data, ack) in
            print("Socket unauthorized :: \(data)")
            //            [{
            //                data =     {
            //                    code = "invalid_token";
            //                    message = "invalid token datatype";
            //                    type = UnauthorizedError;
            //                };
            //                inner =     {
            //                    message = "invalid token datatype";
            //                };
            //                message = "invalid token datatype";
            //            }]
        }
    }
    
    func onSocketUserConnected() {
        socket.on(Constants.SocketKeys.userconnected) { (data, ack) in
            print("Socket userconnected :: \(data)")
            //            [1234567890]
//            self.showToast("\(data)", type: .info)
        }
    }
    
    func onSocketUserstatus() {
        socket.on(Constants.SocketKeys.userstatus) { (data, ack) in
            print("Socket Userstatus :: \(data)")
//            [{
//                at = "10/12/2020, 3:19:45 PM";
//                mobile = 8989896565;
//                status = offline;
//            }]
            //            self.showToast("\(data)", type: .info)
            
            let status = (data as! [[String:Any]]).first?["status"] as? String ?? ""
            let mobile = (data as! [[String:Any]]).first?["mobile"] as? String ?? ""
            
            if status == "offline" {
//                let offlineTime = UtilityClass.UTCToLocal(date: (data as! [[String:Any]]).first!["at"] as! String, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "yyyy-MM-dd HH:mm:ss")
                let offlineTime = self.convertToTimeToStatus(updatedDate: (data as! [[String:Any]]).first!["at"] as? String ?? "")//(date: (data as! [[String:Any]]).first!["at"] as! String, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "yyyy-MM-dd HH:mm:ss")
                // UtilityClass.convertToString(strDate: (data as! [[String:Any]]).first!["at"] as! String, format: "yyyy-MM-dd HH:mm:ss")
                
                self.aryFilterChatData = self.aryFilterChatData.map({ (item) -> chatUsersModel in
                    
                    var current = item
                    if current.mobileNo == mobile {
                        current.lastSeen = offlineTime
                    }
                    return current
                })
                print(offlineTime)
            }
            else {
                self.aryFilterChatData = self.aryFilterChatData.map({ (item) -> chatUsersModel in
                    
                    var current = item
                    if current.mobileNo == mobile {
                        current.lastSeen = ""
                    }
                    return current
                })
            }
            
            if let dict = data.first as? [String:Any] {
                NotificationCenter.default.post(name: .chatUserOnline, object: nil, userInfo: dict)
                
                let notification = Notification(name: .chatUserOnline)
                NotificationQueue.default.enqueue(notification, postingStyle: .whenIdle, coalesceMask: .none, forModes: nil)

            }
        }
    }
    func onSocketisOnline(){
        socket.on(Constants.SocketKeys.isOnline) { (data, ack) in
            print("isOnline received")
            print(data)
            if let res = data as? [[String:Any]] {
                guard let item = res.first else { return }
                let from = item["from"] as? String ?? ""
 
                var param = [String:Any]()
                param["from"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
                param["to"] = from
                param["isOnline"] = true
                self.socketEmiter(key: Constants.SocketKeys.userlive, param: param)
            }
        }
    }
    func onSocketuserLive(){
        socket.on(Constants.SocketKeys.userlive) { (data, ack) in
            print("userlive received")
            print(data)
            if let res = data as? [[String:Any]] {
                guard let item = res.first else { return }
                
                let isOnline = item["isOnline"] as? Bool ?? false
                if(isOnline){
                    if let dict = data.first as? [String:Any] {
                        NotificationCenter.default.post(name: .chatUserIsOnline, object: nil, userInfo: dict)
                    }
                }
            }
        }
    }
    func onSocketMessage(){
        socket.on(Constants.SocketKeys.message) { (data, ack) in
            print("Socket message :: \(data)")
            
            let receiverUserId: String = self.receiverMobileNumber //"\(AppGlobalManager.sharedInstance.receiverUser?.mobileNo ?? "")"
            if let curretntArray = (data as? [[String:Any]]) {
                for (_,item) in curretntArray.enumerated() {
                    var currentItem = item
                    currentItem["createdAt"] = self.convertToTime(updatedDate: item["createdAt"] as? String ?? "").1
                    let messageFrom = "\(item["group"] ?? "")"
                    let attach = item["attach"] as? [String:Any]
                    
                    
                    
                    if messageFrom == "1" {
                        if (item["sender"] as? String ?? "") == (AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "") {
                            NotificationCenter.default.post(name: .chatMessageReceived, object: nil, userInfo: currentItem)
                            let systemSoundID: SystemSoundID = 1009
                            AudioServicesPlaySystemSound (systemSoundID)
                            self.unreadCountForGroupChat(dataItem: item, isNeedCount: false)
                        }
                        else if item["chat_id"] as? String ?? "" == receiverUserId {
                            //                        var deliveryItem = item
                            
                            if (UIApplication.shared.topMostViewController as? ChatGroupViewController) != nil {
                                
                                NotificationCenter.default.post(name: .chatMessageReceived, object: nil, userInfo: currentItem)
                                let systemSoundID: SystemSoundID = 1009
                                AudioServicesPlaySystemSound (systemSoundID)
                            } else {
                                if let arVC = self.navigationController?.visibleViewController as? ARController {
                                    arVC.viewGreenOnChat.isHidden = false
                                }
                                let senderName = item["sender_name"] as? String ?? ""
                                let message = item["message"] as? String ?? ""
                                let gtoupName = "\(item["group_name"] as? String ?? "")"
                                
                                if attach != nil {
                                    let file_ext = attach?["file_ext"] as? String ?? ""
                                    
                                    if (file_ext).lowercased() == "jpg" || (file_ext ).lowercased() == "jpeg" || (file_ext).lowercased() == "png" || (file_ext).lowercased() == "gif" || (file_ext).lowercased() == "svg" || (file_ext).lowercased() == "heic" || (file_ext).lowercased() == "heif" {
                                        
                                        let finalMessage = "\(gtoupName) \n\(senderName) : Image"
                                        self.showToastForChat("\(finalMessage)", type: .chat)
                                    }
                                    else if (file_ext).lowercased() == "mov" || (file_ext).lowercased() == "mp4" || (file_ext).lowercased() == "avi" || (file_ext).lowercased() == "mkv" || (file_ext).lowercased() == "webm" || (file_ext).lowercased() == "3gp" {
                                        
                                        let finalMessage = "\(gtoupName) \n\(senderName) : Video"
                                        self.showToastForChat("\(finalMessage)", type: .chat)
                                    }
                                    else {
                                        let finalMessage = "\(gtoupName) \n\(senderName) : Document"
                                        self.showToastForChat("\(finalMessage)", type: .chat)
                                    }
                                    
                                    //                                let finalMessage = "\(gtoupName) \n\(senderName) : \(message)"
                                    //                                self.showToastForChat("\(finalMessage)", type: .chat)
                                } else {
                                    let finalMessage = "\(gtoupName) \n\(senderName) : \(message)"
                                    self.showToastForChat("\(finalMessage)", type: .chat)
                                }
                            }
                        }
                        else {
                            
                            if (((self.tabBarController)?.selectedViewController?.children.first as? ChatUsersListViewController) != nil){
                                if self.isFromGroupChat == false {
                                    let senderName = (item["sender_name"] as? String ?? "")
                                    let message = (item["message"] as? String ?? "")
                                    self.showToastForChat("\(senderName) : \(message)", type: .chat)
                                }
                            }
                            else {
                                let senderName = item["sender_name"] as? String ?? ""
                                let message = item["message"] as? String ?? ""
                                let gtoupName = "\(item["group_name"] as? String ?? "")"
                                
                                let finalMessage = "\(gtoupName) \n\(senderName) : \(message)"
                                
                                self.showToastForChat("\(finalMessage)", type: .chat)
                            }
                        }
                        
                        self.unreadCountForGroupChat(dataItem: currentItem)
                        return
                    }
                    
                    
                    if item["sender"] as? String ?? "" == receiverUserId {
                        if let arVC = self.navigationController?.visibleViewController as? ARController {
                            arVC.viewGreenOnChat.isHidden = false
                        }
                        var deliveryItem = item
                        deliveryItem["deliver"] = true
                        currentItem["deliver"] = true
                        if (UIApplication.shared.topMostViewController as? ChatViewController) != nil {
                            deliveryItem["read"] = true
                            currentItem["read"] = true
                        }
                  
                        self.socketEmiter(key: Constants.SocketKeys.messageDeliver, param: deliveryItem)
                        NotificationCenter.default.post(name: .chatMessageReceived, object: nil, userInfo: currentItem)
                        let systemSoundID: SystemSoundID = 1009
                        AudioServicesPlaySystemSound (systemSoundID)
                    }
                    else if (item["sender"] as? String ?? "") == (AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "") {
                        //                currentItem["deliver"] = true
                        NotificationCenter.default.post(name: .chatMessageReceived, object: nil, userInfo: currentItem)
                        let systemSoundID: SystemSoundID = 1009
                        AudioServicesPlaySystemSound (systemSoundID)
                        self.unreadCount(dataItem: item, isNeedCount: false)
                    }
                    else {
                        // Bhavesh - 12 Nov 2020
                        var deliveryItem = item
                        deliveryItem["deliver"] = true
                        currentItem["deliver"] = true
                        //            deliveryItem["deliver"] = true // "1"
                        //            currentItem["deliver"] = true // "1"
                        self.socketEmiter(key: Constants.SocketKeys.messageDeliver, param: deliveryItem)
                        if (((self.tabBarController)?.selectedViewController?.children.first as? ChatUsersListViewController) != nil){
                            if self.isFromGroupChat {
                                let senderName = (item["sender_name"] as? String ?? "")
                                let message = (item["message"] as? String ?? "")
                                self.showToastForChat("\(senderName) : \(message)", type: .chat)
                            }
                        }else{
                            let senderName = (item["sender_name"] as? String ?? "")
                            let message = (item["message"] as? String ?? "")
                            
                            self.showToastForChat("\(senderName) : \(message)", type: .chat)
                        }
                        self.unreadCount(dataItem: item)
                    }
                    
                    // ---------------------------------------------
                }
            }
        }
    }
//    {
//        socket.on(Constants.SocketKeys.message) { (data, ack) in
//            print("Socket message :: \(data)")
//
//
////            self.convertToTime(updatedDate: (data as? [[String:Any]])!.first!["createdAt"] as? String ?? "").1
//
//            let receiverUserId: String = self.receiverMobileNumber //"\(AppGlobalManager.sharedInstance.receiverUser?.mobileNo ?? "")"
//            if let curretntArray = (data as? [[String:Any]]) {
//
//                for (_,item) in curretntArray.enumerated() {
//
//                    var currentItem = item
//                    currentItem["createdAt"] = self.convertToTime(updatedDate: item["createdAt"] as? String ?? "").1
//
//                    // ---------------------------------------------
//                    // Bhavesh - 12 Nov 2020
//                    // ---------------------------------------------
//                    if (item["sender"] as? String ?? "") != (AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "") {
//                        var deliveryItem = item
////                        deliveryItem["deliver"] = "1"
////                        currentItem["deliver"] = "1"
//                        deliveryItem["deliver"] = true
//                        currentItem["deliver"] = true
//                        deliveryItem["read"] = true
//                        currentItem["read"] = true
//                        self.socketEmiter(key: Constants.SocketKeys.messageDeliver, param: deliveryItem)
//                    }
//                    // ---------------------------------------------
//
//                    if item["sender"] as? String ?? "" == receiverUserId {
//                        NotificationCenter.default.post(name: .chatMessageReceived, object: nil, userInfo: currentItem)
//                        let systemSoundID: SystemSoundID = 1009
//                        AudioServicesPlaySystemSound (systemSoundID)
//                    }
//                    else if (item["sender"] as? String ?? "") == (AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "") {
//                        NotificationCenter.default.post(name: .chatMessageReceived, object: nil, userInfo: currentItem)
//                        let systemSoundID: SystemSoundID = 1009
//                        AudioServicesPlaySystemSound (systemSoundID)
//                        self.unreadCount(dataItem: item, isNeedCount: false)
//                    }
//                    else {
//                        // Bhavesh - 12 Nov 2020
//                        if (((self.tabBarController)?.selectedViewController?.children.first as? ChatUsersListViewController) != nil){
//                        }else{
//                            let senderName = (item["sender_name"] as? String ?? "")
//                            let message = (item["message"] as? String ?? "")
//
//                            self.showToastForChat("\(senderName) : \(message)", type: .chat)
//                        }
//                        if (item["sender"] as? String ?? "") != (AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "") {
//                            var deliveryItem = item
//    //                        deliveryItem["deliver"] = "1"
//    //                        currentItem["deliver"] = "1"
//                            deliveryItem["deliver"] = true
//                            currentItem["deliver"] = true
//                            deliveryItem["read"] = false
//                            currentItem["read"] = false
//                            self.socketEmiter(key: Constants.SocketKeys.messageDeliver, param: deliveryItem)
//                        }
//
//                            self.unreadCount(dataItem: item)
//                    }
//                }
//
//            }
//        }
//    }
    
    func onSocketTyping() {
        socket.on(Constants.SocketKeys.typing) { (data, ack) in
            print("Socket typing :: \(data) \nACK :: \(ack)\n")
            //            [{
            //                from = 9879252952;
            //                to = 8585285852;
            //            }]
//            Socket typing :: [{
//                from = 1133552244;
//                group = 1;
//                message = 1;
//                to = 604c9db9517f512e087cc572;
//                "typing_name" = Alexa;
//            }]
            
            if let curretntArray = (data as? [[String:Any]]) {
                
                let isGroup = curretntArray.first?["group"] as? Int ?? 0
                print("Group :: \(isGroup)")
                
                if isGroup == 1 {
                    let to = curretntArray.first?["to"] as? String ?? ""
                    let from = curretntArray.first?["from"] as? String ?? ""
                    if to == self.receiverMobileNumber && from != AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "" {
                        self.timer.invalidate()
                        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.fire(timer:)), userInfo: curretntArray.first, repeats: false)
                    }
                    return
                }
                
                let from = curretntArray.filter{$0["from"] as? String ?? "" == AppGlobalManager.sharedInstance.receiverUser?.mobileNo ?? ""}
                if from.count != 0 {
                    //                    self.lblTyping.isHidden = false
                    //                    self.lblTyping.text = "\(self.receiverUser?.name ?? "") typing..."
//                    self.tempCount = 3
                    
                    self.timer.invalidate()
                    self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.fire(timer:)), userInfo: from.first, repeats: false)
                }
            }
        }
    }
    
    func onSocketMessageRead() {
        socket.on(Constants.SocketKeys.messageRead) { (data, ack) in
            print("Socket MessageRead :: \(data)")
//            [{
//                "__v" = 0;
//                "_id" = 5f8944c971138d02943fb548;
//                "chat_id" = 5f80488335f66749f13af7f2;
//                createdAt = "2020-10-16 06:59:21";
//                message = hiy;
//                read = 1;
//                sender = 9666377788;
//            }]
                        
            if let res = data as? [[String:Any]] {
                guard let item = res.first else { return }
                
                if item["sender"] as? String ?? "" == (AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "") {
                    NotificationCenter.default.post(name: .chatUserReadMessage, object: res.first)
                }
            }
        }
    }
    
    func onSocketMessageReadAll() {
        socket.on(Constants.SocketKeys.messageReadAll) { (data, ack) in
            print("Socket MessageReadAll :: \(data)")
            
//            [{
//                "chat_id" = 5f7d9876ed216c17b280aa42;
//                from = 8585285852;
//                to = 9879252952;
//            }]
 
            if let res = data as? [[String:Any]] {
                guard let item = res.first else { return }

                if item["from"] as? String ?? "" != (AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "") {
                    NotificationCenter.default.post(name: .chatUserReadAllMessage, object: res.first)
                }
            }
        }
    }
    
    func onSocketMessageDeliver() {

        socket.on(Constants.SocketKeys.messageDeliver) { (data, ack) in
            print("Socket MessageDeliver :: \(data)")
            
//                [{
//                    "__v" = 0;
//                    "_id" = 5fb632a08398f211c122a08a;
//                    "chat_id" = 5f7d9876ed216c17b280aa42;
//                    createdAt = "2020-11-19T08:53:52.038Z";
//                    deliver = 1;
//                    message = hiii;
//                    read = 0;
//                    sender = 8585285852;
//                    updatedAt = "2020-11-19T08:53:52.038Z";
//                }]
 
            if let res = data as? [[String:Any]] {
                guard let item = res.first else { return }

                if item["sender"] as? String ?? "" == (AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? "") {
                    NotificationCenter.default.post(name: .chatUserMessageDeliver, object: res.first)
                }
            }
        }
    }
    
    
    @objc func fire(timer:Timer) {
        
        NotificationCenter.default.post(name: .notificationTyping, object: timer.userInfo as? [String:Any]) // addObserver(self, selector: #selector(self.fire), name: .notificationTyping, object: nil)
        
//        self.tempCount -= 1
//        if self.tempCount == 0 {
            self.timer.invalidate()
//        }
    }
    
    func chatHistory() {
        
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.chatList + "?" + "device_id=" + "\(device_id)" + "&" + "device_type=" + "0"  , parameters: nil, token: AppGlobalManager.sharedInstance.token, method: .GET) { (result) in
            
            DispatchQueue.main.async {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                switch result {
                case .success( let data):
                    
                    do {
                        let res = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
//                        print(res)
                    } catch {
                        print(error.localizedDescription)
                    }
                    
                    do {
                        let object = try JSONDecoder().decode(chatUserListMain.self, from: data)
                        //                    let res = try JSONDecoder().decode(chatUserListMain.self, from: data)
                        
                        if let status = object.statusCode,status == 1 {
                            
                            self.aryChatData = object.output?.chats ?? []
                            
                            self.aryGroupsChatData = object.output?.groups ?? []
                            self.aryFilterGroupsChatData = object.output?.groups ?? []
                            
                            if self.aryGroupsChatData.count != 0 {
                                self.segmentCtrl.isHidden = false
                            }
                            
                            AppGlobalManager.sharedInstance.aryChatData = self.aryChatData
                            var output = [chatUsersModel]()
                            var outputForGroupChat = [chatGroupModel]()
                            
                            for i in 0..<self.aryChatData.count {
                                
                                //                                if self.aryChatData[i].message != nil {
                                //                                    output.insert(self.aryChatData[i], at: 0)
                                //                                } else {
                                output.append(self.aryChatData[i])
                                //                                }
                            }
                            
                            var aryMsg = [chatUsersModel]()
                            
                            for i in 0..<self.aryChatData.count {
                                
                                if self.aryChatData[i].message != nil {
                                    let item = self.aryChatData[i]
                                    aryMsg.insert(item, at: 0)
                                }
                            }
                            for i in 0..<self.aryChatData.count {
                                
                                if self.aryChatData[i].message != nil {
                                    let item = self.aryChatData[i]
                                    output = output.filter{$0.mobileNo ?? "" != item.mobileNo ?? ""}
                                }
                            }
                            
                            aryMsg = aryMsg.sorted { (_$0, _$1) -> Bool in
                                //                                2020-10-10 13:17:17
                                
                                let item = UtilityClass.convertToDate(strDate: _$0.message?.createdAt ?? "", format: "yyyy-MM-dd HH:mm:ss")
                                let item2 = UtilityClass.convertToDate(strDate: _$1.message?.createdAt ?? "", format: "yyyy-MM-dd HH:mm:ss")
                                
                                if item < item2 {
                                    return true
                                }
                                return false
                            }
                            
                            for i in 0..<aryMsg.count {
                                output.insert(aryMsg[i], at: 0)
                            }
                            
                            // Group Chat Sorting
                            if self.aryGroupsChatData.count != 0 {
                                var aryGroupMsg = [chatGroupModel]()
                                
                                for i in 0..<self.aryGroupsChatData.count {
                                    outputForGroupChat.append(self.aryGroupsChatData[i])
                                }
                                
                                for i in 0..<self.aryGroupsChatData.count {
                                    
                                    if self.aryGroupsChatData[i].message != nil {
                                        let item = self.aryGroupsChatData[i]
                                        aryGroupMsg.insert(item, at: 0)
                                    }
                                }
                                for i in 0..<self.aryGroupsChatData.count {
                                    
                                    if self.aryGroupsChatData[i].message != nil {
                                        let item = self.aryGroupsChatData[i]
                                        outputForGroupChat = outputForGroupChat.filter{$0.groupId ?? "" != item.groupId ?? ""}
                                    }
                                }
                                
                                aryGroupMsg = aryGroupMsg.sorted { (_$0, _$1) -> Bool in
                                    
                                    let item = UtilityClass.convertToDate(strDate: _$0.message?.createdAt ?? "", format: "yyyy-MM-dd HH:mm:ss")
                                    let item2 = UtilityClass.convertToDate(strDate: _$1.message?.createdAt ?? "", format: "yyyy-MM-dd HH:mm:ss")
                                    
                                    if item < item2 {
                                        return true
                                    }
                                    return false
                                }
                                
                                for i in 0..<aryGroupMsg.count {
                                    outputForGroupChat.insert(aryGroupMsg[i], at: 0)
                                }
                                
                                self.aryGroupsChatData = outputForGroupChat
                                self.aryFilterGroupsChatData = outputForGroupChat
                            }
                            
                            self.aryChatData = output
                            self.aryFilterChatData = output
                            self.tableView?.reloadData()
                            self.tableViewGroup.reloadData()
                            return
                        }
                    }
                    catch {
                        print(error.localizedDescription)
                    }
                    
                case .failure(let error):
                    self.showToast(error.localizedDescription, type: .fail)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }
            }
        }
    }
    
}

extension ChatUsersListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewGroup {
            return aryFilterGroupsChatData.count
        }
        return aryFilterChatData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewGroup {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsCellForGroup", for: indexPath) as! ContactsCell
            cell.selectionStyle = .none
            let currentData = aryFilterGroupsChatData[indexPath.row]
            cell.chatMessageCounterView.isHidden = true
            cell.lblNoOfMsg.layer.cornerRadius = cell.lblNoOfMsg.frame.height / 2
            cell.lblNoOfMsg.layer.masksToBounds = true
            cell.lblName.text = currentData.groupName
            cell.lblNameIntials.setText(fullname: currentData.groupName)
            cell.lblPhoneNumber.text = "\(currentData.message?.senderName ?? "") : \(currentData.message?.message ?? "")"
            let showDate = convertToTimeToStatusTest(updatedDate: currentData.message?.createdAt ?? "")
            if showDate == "" {
                cell.lblDate.text = ""
            }
            else {
                let stringDate = UtilityClass.convertToDate(strDate: showDate, format: "yyyy-MM-dd HH:mm:ss")
                DispatchQueue.main.asyncAfter(wallDeadline: .now() + 1) {
                    cell.lblDate.text = "\(self.timeInterval(timeAgo: stringDate))"
                }
            }
        
            if let file_ext = currentData.message?.attach?.fileExt {
                if (file_ext).lowercased() == "jpg" || (file_ext ).lowercased() == "jpeg" || (file_ext).lowercased() == "png" || (file_ext).lowercased() == "gif" || (file_ext).lowercased() == "svg" || (file_ext).lowercased() == "heic" || (file_ext).lowercased() == "heif" {
                    cell.lblPhoneNumber.text = "\(currentData.message?.senderName ?? "") : Image"
                }
                
                else if (file_ext).lowercased() == "mov" || (file_ext).lowercased() == "mp4" || (file_ext).lowercased() == "avi" || (file_ext).lowercased() == "mkv" || (file_ext).lowercased() == "webm" || (file_ext).lowercased() == "3gp" {
                    
                    cell.lblPhoneNumber.text = "\(currentData.message?.senderName ?? "") : Video"
                    
                }else{
                    cell.lblPhoneNumber.text = "\(currentData.message?.senderName ?? "") : File"
                }
            }
            if currentData.unreadCount ?? 0 != 0 {
                cell.chatMessageCounterView.isHidden = false
                cell.lblNoOfMsg.text = "\(currentData.unreadCount ?? 0)"
                cell.lblMessages.text = "Message"
                if (currentData.unreadCount ?? 0) > 1 {
                    cell.lblMessages.text = "Messages"
                }
            }
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath) as! ContactsCell
            let currentData = aryFilterChatData[indexPath.row]
            
            cell.chatMessageCounterView.isHidden = true
            cell.lblNoOfMsg.layer.cornerRadius = cell.lblNoOfMsg.frame.height / 2
            cell.lblNoOfMsg.layer.masksToBounds = true
            cell.lblName.text = currentData.name
            cell.lblNameIntials.setText(fullname: currentData.name)  // "\(currentData.name?.components(separatedBy: " ").first?.uppercased() ?? "")\(currentData.name?.components(separatedBy: " ").last?.uppercased() ?? "")"
            
            let showDate = convertToTimeToStatusTest(updatedDate: currentData.message?.createdAt ?? "")
            let stringDate = UtilityClass.convertToDate(strDate: showDate, format: "yyyy-MM-dd HH:mm:ss")
            cell.lblPhoneNumber.text = "\(currentData.message?.message ?? "")"
            //        timeInterval(timeAgo: stringDate as Date)
            cell.lblDate.text = "\(timeInterval(timeAgo: stringDate as Date))"
            //
            //        cell.lblPhoneNumber.text = currentData.message?.message ?? ""
            
            //        DispatchQueue.main.asyncAfter(wallDeadline: .now() + 1) {
            //                    cell.lblDate.text = "\(self.timeInterval(timeAgo: stringDate as Date))"
            //        }
            
            if let file_ext = currentData.message?.attach?.fileExt {
                if (file_ext).lowercased() == "jpg" || (file_ext ).lowercased() == "jpeg" || (file_ext).lowercased() == "png" || (file_ext).lowercased() == "gif" || (file_ext).lowercased() == "svg" || (file_ext).lowercased() == "heic" || (file_ext).lowercased() == "heif" {
                    print(file_ext)
                    cell.lblPhoneNumber.text = "Image"
                }
                else if (file_ext).lowercased() == "mov" || (file_ext).lowercased() == "mp4" || (file_ext).lowercased() == "avi" || (file_ext).lowercased() == "mkv" || (file_ext).lowercased() == "webm" || (file_ext).lowercased() == "3gp" {
                    cell.lblPhoneNumber.text = "Video"
                }else{
                    cell.lblPhoneNumber.text = "Document"
                }
            }
            
            if currentData.unreadCount ?? 0 != 0 {
                cell.chatMessageCounterView.isHidden = false
                cell.lblNoOfMsg.text = "\(currentData.unreadCount ?? 0)"
                cell.lblMessages.text = "Message"
                if (currentData.unreadCount ?? 0) > 1 {
                    cell.lblMessages.text = "Messages"
                }
            }
            return cell
        }
        
    }
    
   func timeInterval(timeAgo:Date) -> String
          {
            let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: timeAgo, to: Date())
              if let year = interval.year, year > 0 {
                  return year == 1 ? "\(year)" + " " + "year ago" : "\(year)" + " " + "years ago"
              } else if let month = interval.month, month > 0 {
                  return month == 1 ? "\(month)" + " " + "month ago" : "\(month)" + " " + "months ago"
              } else if let day = interval.day, day > 0 {
                let stringDate = self.convertToStringforShowingIncell(date: timeAgo)
                return stringDate
//                  return day == 1 ? "\(day)" + " " + "day ago" : "\(day)" + " " + "days ago"
              }else if Calendar.current.isDateInYesterday(timeAgo){
                    return "Yesterday"
              }
              else if let hour = interval.hour, hour > 0 {
                let stringDate = self.convertToStringFromDateTOTime(date: timeAgo)
                return stringDate
//                  return hour == 1 ? "\(hour)" + " " + "hour ago" : "\(hour)" + " " + "hours ago"
              }else if let minute = interval.minute, minute > 0 {
                let stringDate = self.convertToStringFromDateTOTime(date: timeAgo)
                return stringDate
//                  return minute == 1 ? "\(minute)" + " " + "minute ago" : "\(minute)" + " " + "minutes ago"
              }else if let second = interval.second, second > 0 {
//                  return second == 1 ? "\(second)" + " " + "second ago" : "\(second)" + " " + "seconds ago"
//                return "Just Now"
                let stringDate = self.convertToStringFromDateTOTime(date: timeAgo)
                return stringDate
              } else {
                  return ""
              }
          }
    func convertToStringforShowingIncell(date:Date) -> String {
              let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = "dd-MMM-yyyy"
              dateFormatter.calendar = NSCalendar.current
              dateFormatter.timeZone = TimeZone(abbreviation: "UTC")// TimeZone.current
              let strNewDate = dateFormatter.string(from: date)
              return strNewDate
          }
    func convertToStringFromDateTOTime(date:Date) -> String {
              let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = "hh:mm a"
              dateFormatter.calendar = NSCalendar.current
//              dateFormatter.timeZone = TimeZone(abbreviation: "UTC")// TimeZone.current
              let strNewDate = dateFormatter.string(from: date)
              return strNewDate
          }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewGroup {
            let currentData = aryFilterGroupsChatData[indexPath.row]
            let chat = ChatGroupViewController.viewController()
            chat.aryMembers = currentData.members ?? []
            isFromGroupChat = true
            chat.chatId = currentData.id ?? ""
            chat.isFromGroupChat = true
            chat.receiverMobileNumber = currentData.id ?? ""
            chat.receiverName = currentData.groupName ?? ""
            chat.model = currentData
            receiverMobileNumber = currentData.id ?? ""
            receiverId = currentData.id ?? ""
            receiverName = currentData.groupName ?? ""
            chat.isBackFromChatScreen = { (state) in
                self.receiverMobileNumber = ""
                self.receiverId = ""
                self.receiverName = ""
                self.receiverUser = nil
            }
            tableView.reloadData()
            tableViewGroup.reloadData()
            self.navigationController?.pushViewController(chat, animated: true)
            return
            
        }else{
            let currentData = aryFilterChatData[indexPath.row]
            
            //Trial code for online user
            
            var param = [String:Any]()
            param["from"] = AppGlobalManager.sharedInstance.loggedInUser?.user?.mobileNo ?? ""
            param["to"] = currentData.mobileNo ?? ""
            self.socketEmiter(key: Constants.SocketKeys.isOnline, param: param)
            
            let chat = ChatViewController.viewController()
            
            chat.chatId = currentData.message?.chatId ?? ""
            chat.strStatus = convertToTimeToStatus(updatedDate: currentData.lastSeen ?? "") // currentData.lastSeen ?? ""
    //        print("Last Seen : *************",currentData.lastSeen!)
            print("Last Seen : *************",chat.strStatus)

            chat.receiverMobileNumber = currentData.mobileNo ?? ""
            chat.receiverName = currentData.name ?? ""
            chat.receiverId = "\(currentData.id ?? 0)"
            chat.receiverUser = User(id: currentData.id, name: currentData.name ?? "", email: currentData.email ?? "", is_guest: false, mobileNo: currentData.mobileNo ?? "", department: currentData.department ?? "", company: nil, chatId: currentData.message?.chatId ?? "")
            
            receiverMobileNumber = currentData.mobileNo ?? ""
            receiverId = "\(currentData.id ?? 0)"
            receiverName = currentData.name ?? ""
            receiverUser = User(id: currentData.id, name: currentData.name ?? "", email: currentData.email ?? "", is_guest: false, mobileNo: currentData.mobileNo ?? "", department: currentData.department ?? "", company: nil, chatId: currentData.message?.chatId ?? "")
            
            AppGlobalManager.sharedInstance.receiverUser = User(id: currentData.id, name: currentData.name ?? "", email: currentData.email ?? "", is_guest: false, mobileNo: currentData.mobileNo ?? "", department: currentData.department ?? "", company: nil, chatId: currentData.message?.chatId ?? "")
            
            chat.isBackFromChatScreen = { (state) in
                
                self.receiverMobileNumber = ""
                self.receiverId = ""
                self.receiverName = ""
                self.receiverUser = nil
            }
            
            aryFilterChatData[indexPath.row].unreadCount = 0
            tableView.reloadData()
            self.navigationController?.pushViewController(chat, animated: true)
        }
        }
       
    
    
}

extension ChatUsersListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBarText = searchController.searchBar.text
        guard let searchText = searchBarText, !searchText.isEmpty else {
            //   filteredPhoneNumbers = phoneNumbers
            
            if isFromGroupChat {
                aryFilterGroupsChatData = aryGroupsChatData
                self.tableViewGroup?.reloadData()
                return
            }
            aryFilterChatData = aryChatData
            self.tableView?.reloadData()
            return
        }
        if isFromGroupChat {
            aryFilterGroupsChatData = aryGroupsChatData.filter{(($0.groupName ?? "").lowercased().contains(searchText.lowercased()))}.sorted(by: {$0.groupName ?? "" > $1.groupName ?? ""})
            self.tableViewGroup?.reloadData()
            return
        }
        
        aryFilterChatData = aryChatData.filter{(($0.name ?? "").lowercased().contains(searchText.lowercased())) || (($0.mobileNo ?? "").lowercased().contains(searchText.lowercased()))}.sorted(by: {$0.name ?? "" > $1.name ?? ""})
        
        self.tableView?.reloadData()
    }
    
    func unreadCount(dataItem: [String: Any], isNeedCount: Bool = true) {
        
        let mobileNumber = dataItem["sender"] as? String ?? ""
        
        aryFilterChatData = aryFilterChatData.map { (item) -> chatUsersModel in
            var newItem = item
            if newItem.mobileNo == mobileNumber {
                var count = newItem.unreadCount ?? 0
                if isNeedCount {
                    count += 1
                    newItem.unreadCount = count
                }
                newItem.message?.message = dataItem["message"] as? String ?? ""
                if newItem.message?.message == "" {
                    
                    let attach = dataItem["attach"] as? [String:Any]
                    var fileSize = String()
                    if let sz = attach?["file_size"] as? String {
                        fileSize = sz
                    }
                    else if let sz = attach?["file_size"] as? Int {
                        fileSize = "\(sz)"
                    }
                    else if let sz = attach?["file_size"] as? Double {
                        fileSize = "\(sz)"
                    }
                    
                    newItem.message?.attach = chatHistoryAttach(fileExt: attach?["file_ext"] as? String ?? "", fileName: attach?["file_name"] as? String ?? "", filePath: attach?["file_path"] as? String ?? "", fileSize: fileSize, fileThumbnailPath: attach?["file_thumbnail_path"] as? String ?? "")
                }
                else {
                    newItem.message?.attach = nil
                }
//                newItem.message?.createdAt = convertToTime(updatedDate: dataItem["createdAt"] as? String ?? "").1 //dataItem["createdAt"] as? String ?? ""
            }
            return newItem
        }
        
        for i in 0..<aryFilterChatData.count {
            
            if aryFilterChatData[i].mobileNo ?? "" == mobileNumber {
                let item = aryFilterChatData.remove(at: i)
                aryFilterChatData.insert(item, at: 0)
                break
            }
        }
        
        tableView?.reloadData()
    }
    func unreadCountForGroupChat(dataItem: [String: Any], isNeedCount: Bool = true) {
            let mobileNumber = dataItem["sender"] as? String ?? ""
            let chatId = dataItem["chat_id"] as? String ?? ""
            aryFilterGroupsChatData = aryFilterGroupsChatData.map { (item) -> chatGroupModel in
                var newItem = item
                if newItem.id == chatId {
//                    var count = newItem.unreadCount ?? 0
//                    if isNeedCount {
//                        count += 1
//                        newItem.unreadCount = count
//                    }
                    newItem.message?.message = dataItem["message"] as? String ?? ""
                    if newItem.message?.message == "" {

                        let attach = dataItem["attach"] as? [String:Any]
                        var fileSize = String()
                        if let sz = attach?["file_size"] as? String {
                            fileSize = sz
                        }
                        else if let sz = attach?["file_size"] as? Int {
                            fileSize = "\(sz)"
                        }
                        else if let sz = attach?["file_size"] as? Double {
                            fileSize = "\(sz)"
                        }
                        newItem.message?.attach = chatHistoryAttach(fileExt: attach?["file_ext"] as? String ?? "", fileName: attach?["file_name"] as? String ?? "", filePath: attach?["file_path"] as? String ?? "", fileSize: fileSize, fileThumbnailPath: attach?["file_thumbnail_path"] as? String ?? "")
                    }
                    else {
                        newItem.message?.attach = nil
                    }
                    newItem.message?.createdAt = convertToTime(updatedDate: dataItem["createdAt"] as? String ?? "").1 //dataItem["createdAt"] as? String ?? ""
                }
                return newItem
            }

            for i in 0..<aryFilterGroupsChatData.count {
                if aryFilterGroupsChatData[i].id ?? "" == chatId {
                    let item = aryFilterGroupsChatData.remove(at: i)
                    aryFilterGroupsChatData.insert(item, at: 0)
                    break
                }
            }
            tableViewGroup?.reloadData()
        }
    func convertToTime(updatedDate: String) -> (Date, String) {
            // message cell date and time show
            if updatedDate == "" {
                return (Date(), "")
            }
           
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" // "yyyy-MM-dd HH:mm:ss"//"yyyy-MM-dd'T'HH:mm:ssZ"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            guard let date = dateFormatter.date(from: updatedDate) else {
                return (Date(), "")
            }// create date from string

            // change to a readable time format and change to local time zone
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC") // TimeZone.current
            let timeStamp = dateFormatter.string(from: date)
            return (date, timeStamp)
        }
    func convertToTimeToStatus(updatedDate: String) -> String {
        if updatedDate == "" {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // "yyyy-MM-dd HH:mm:ss"//"yyyy-MM-dd'T'HH:mm:ssZ"
        
        let timeStamp = self.UTCToLocal(UTCDateString: updatedDate)
        
        let date = dateFormatter.date(from: timeStamp)
        
        if Calendar.current.isDateInToday(date ?? Date()) {
            dateFormatter.dateFormat = "h:mm a"
            let timeStamp2 = dateFormatter.string(from: date ?? Date())
            return "Today \(timeStamp2)"
        }
        else if Calendar.current.isDateInYesterday(date ?? Date()) {
            dateFormatter.dateFormat = "h:mm a"
            let timeStamp2 = dateFormatter.string(from: date ?? Date())
            return "Yesterday \(timeStamp2)"
        }
        return timeStamp
    }
    func convertToTimeToStatusTest(updatedDate: String) -> String {
        if updatedDate == "" {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // "yyyy-MM-dd HH:mm:ss"//"yyyy-MM-dd'T'HH:mm:ssZ"
        
        let timeStamp = self.UTCToLocal(UTCDateString: updatedDate)
        
        return timeStamp
    }
    func UTCToLocal(UTCDateString: String) -> String {
        
        if UTCDateString == "" {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Input Format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let UTCDate = dateFormatter.date(from: UTCDateString)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // Output Format
        dateFormatter.timeZone = TimeZone.current
        let UTCToCurrentFormat = dateFormatter.string(from: UTCDate ?? Date())
        print("Local Date %@",UTCToCurrentFormat)
        return UTCToCurrentFormat
    }
}



