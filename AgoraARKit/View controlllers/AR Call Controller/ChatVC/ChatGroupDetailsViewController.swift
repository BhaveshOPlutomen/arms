//
//  ChatGroupDetailsViewController.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 3/25/21.
//  Copyright © 2021 Prashant. All rights reserved.
//

import UIKit

class ChatGroupDetailsViewController: BaseViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblGroupSortName: NameInitalLabel!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lblNumberOfMember: UILabel!
    
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblChatDiscussGroup: UILabel!
    
    @IBOutlet weak var lblMebmersAboveTableView: UILabel!
    
    
    
    // MARK: - Variables
    var model : chatGroupModel?
    
    
    // MARK: - View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupData()
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - Custom Methods
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupData() {
        lblGroupSortName.setText(fullname: model?.groupName)
        lblGroupName.text = model?.groupName
        lblNumberOfMember.text = "\(model?.members?.count ?? 0) members"
        lblCreatedDate.text = "Created at " + UtilityClass.ConvertUTCDateStringToCurrentDateString(date: model?.createdAt ?? "")
        lblDescription.text = model?.groupDes ?? ""
        lblChatDiscussGroup.text = model?.message?.message ?? ""
        lblMebmersAboveTableView.text = "\(model?.members?.count ?? 0) members"
    }
    
//    func ConvertUTCDateStringToCurrentDateString(date:String) -> String {
//        if date == "" {
//            return ""
//        }
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//
//        if let date = dateFormatter.date(from: date) {
//            dateFormatter.timeZone = TimeZone.current
//            dateFormatter.dateFormat = "dd MMM yyyy"
//
//            return dateFormatter.string(from: date)
//        }
//        return ""
//
//        //        let dateFormatter = DateFormatter()
//        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        //        dateFormatter.calendar = NSCalendar.current
//        //        dateFormatter.timeZone = TimeZone.current
//        //
//        //        let dt = dateFormatter.date(from: date)
//        //        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//        //        dateFormatter.dateFormat = "h:mm a"
//        //
//        //        let strNewDate = dateFormatter.string(from: dt!)
//        //        return strNewDate
//    }


}
// MARK: - TableView Methods



extension ChatGroupDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return model?.members?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath) as! ContactsCell
        let currentData = model?.members?[indexPath.row]
        
        cell.chatMessageCounterView.isHidden = true
        cell.lblNoOfMsg.layer.cornerRadius = cell.lblNoOfMsg.frame.height / 2
        cell.lblNoOfMsg.layer.masksToBounds = true
        cell.lblName.text = currentData?.name
        cell.lblNameIntials.setText(fullname: currentData?.name)
        cell.lblDate.text = "\(currentData?.mobileNo ?? "")"
        
        return cell
    }
    
    
}
