//
//  FileDetailsForChatViewController.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 28/10/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit
import WebKit
import Photos

class FileDetailsForChatViewController: UIViewController, WKNavigationDelegate,Toastable {

    // MARK: - Outlets
    @IBOutlet weak var viewFile: UIView!
    @IBOutlet weak var btnDownload: UIButton!
    
    // MARK: - Variables
    var fileTypes: FileType!
    var strUrl = String()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnDownload.isHidden = false
        btnDownload.clipsToBounds = true
        btnDownload.layer.cornerRadius = 8
        
        let currentFileURL = URL(string:strUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        switch fileTypes {
            case .image:
                print("image")
                openImageAction(url: currentFileURL!)
            case .video:
                print("video")
                playVideoAction(url: currentFileURL!)
            case .file:
                print("file")
                openFilesAction(url: currentFileURL!)
//                btnDownload.isHidden = true
            case .none:
                print("none")
        }        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        UtilityClass.corner(vw: viewFile, size: 10)
        viewFile.clipsToBounds = true
    }
//        let rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(self.handleRotate(gesture:)))
//        viewFile.addGestureRecognizer(rotateGesture)
//        let pinchGesture = UIRotationGestureRecognizer(target: self, action: #selector(self.handlePinch(gesture:)))
//        viewFile.addGestureRecognizer(pinchGesture)
      
    // Rotate action
//    @objc func handleRotate(gesture: UIRotationGestureRecognizer) {
//        if gesture.state == UIGestureRecognizer.State.changed {
//            let transform = CGAffineTransform(rotationAngle: gesture.rotation)
//            viewFile.transform = transform
//        }
//    }
    
    // MARK: - Custom Methods
    
    func openImageAction(url: URL) {
        let img = UIImageView(frame: viewFile.bounds)
        img.sd_setImage(with: url, completed: nil)
        img.contentMode = .scaleAspectFit
        img.clipsToBounds = true
        viewFile.addSubview(img)
    }
    
    func playVideoAction(url: URL) {
        
        let videoURL = url // URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
        let player = AVPlayer(url: videoURL)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.viewFile.bounds
        self.viewFile.layer.addSublayer(playerLayer)
        player.play()
    }

    func openFilesAction(url: URL) {
        let webview = WKWebView(frame: self.viewFile.bounds)
        view.addSubview(webview)
        webview.navigationDelegate = self
        webview.load(URLRequest(url: url))
        webview.clipsToBounds = true
        self.viewFile.addSubview(webview)
//        webview.load(URLRequest(url: URL(fileURLWithPath: path)))//URL(string: "http://") for web URL
    }
    
    @IBAction func btnDownloadClick(_ sender: Any) {
     
        switch fileTypes {
              case .image:
                print("image")
                //                guard let image = imgTemp.image else { return }
                //                UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                saveToGallery()
              case .video:
                print("video")
                saveToGallery()
              case .file:
                print("file")
                saveToFileManage()
//                savePDF()
              case .none:
                print("none")
            }
       
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if error != nil {
            // we got back an error!
            self.showToast("Image save error", type: .fail)
        } else {
            self.showToast("Image saved to photoes", type: .success)
        }
    }
    func saveToGallery() {
        // Get the current authorization state.
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
          // Access has been granted.
          print("Access has been granted.")
          saveVideoToGallery()
        }
        else if (status == PHAuthorizationStatus.denied) {
          // Access has been denied.
          print("Access has been denied.")
          displayPermissionDenieAlert()
        }
        else if (status == PHAuthorizationStatus.notDetermined) {
          // Access has not been determined.
          PHPhotoLibrary.requestAuthorization({ (newStatus) in
            if (newStatus == PHAuthorizationStatus.authorized) {
              self.saveVideoToGallery()
            }
            else {
              print("Denine")
              self.displayPermissionDenieAlert()
            }
          })
        }
        else if (status == PHAuthorizationStatus.restricted) {
          // Restricted access - normally won't happen.
          print("DeniRestricted access - normally won't happen.")
          displayPermissionDenieAlert()
        }
      }
      func saveVideoToGallery() {
    //    let videoImageUrl = "http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4"
        let currentFileURL = URL(string:strUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        DispatchQueue.global(qos: .background).async {
          if let url = currentFileURL,
            let urlData = NSData(contentsOf: url) {
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
            let filePath="\(documentsPath)/\(url.lastPathComponent.replacingOccurrences(of: ".\(url.pathExtension)", with: "")).\(url.pathExtension)"
            print("filePath = \(filePath)")
            DispatchQueue.main.async {
              urlData.write(toFile: filePath, atomically: true)
              PHPhotoLibrary.shared().performChanges({
                if self.fileTypes == .video {
                  PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                }
                else if self.fileTypes == .image {
                  PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: URL(fileURLWithPath: filePath))
                }
              }) { completed, error in
                if completed {
                  if self.fileTypes == .video {
                    print("Video is saved!")
                    self.showToast("Video saved to photoes", type: .success)
                  }
                  else if self.fileTypes == .image {
                    print("Image is saved!")
                    self.showToast("Image saved to photoes", type: .success)
                  }
                }
              }
            }
          }
        }
      }
    
    let documentInteractionController = UIDocumentInteractionController()
      
    
      func saveToFileManage() {
        let currentFileURL = URL(string:strUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        guard let url = currentFileURL else { return }
                URLSession.shared.dataTask(with: url) { data, response, error in
                    guard let data = data, error == nil else { return }
                    let tmpURL = FileManager.default.temporaryDirectory
                        .appendingPathComponent(response?.suggestedFilename ?? "fileName.csv")
                    do {
                        try data.write(to: tmpURL)
                        DispatchQueue.main.async {
                            self.share(url: tmpURL)
                        }
                    } catch {
                        print(error)
                    }

                }.resume()
        
        
      }
    
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
     
        documentInteractionController.presentOptionsMenu(from: view.frame, in: view, animated: true)
    }
    
      func displayPermissionDenieAlert() {
        UtilityClass.showAlert(title: "ARMS", message: "Please allow photo permission from settings to this applocation") { (act) in
          print("OK")
        }
      }
    
    @available(iOS 11.0, *)
    func savePDF() {
        
        if #available(iOS 14.0, *) {
            let barButtonItem = UIBarButtonItem(systemItem: .action)
            let currentFileURL = URL(string:strUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            let doc = UIDocumentInteractionController(url: currentFileURL!)
            doc.presentOptionsMenu(from: barButtonItem, animated: true)
        } else {
            // Fallback on earlier versions
        }
        
        
        
//        guard let documentData = document.dataRepresentation() else { return }
//        let activityController = UIActivityViewController(activityItems: [documentData], applicationActivities: nil)
//        self.present(activityController, animated: true, completion: nil)
    }

}


extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}
