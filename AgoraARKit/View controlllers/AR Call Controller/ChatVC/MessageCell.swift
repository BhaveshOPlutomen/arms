//
//  MessageCell.swift
//  Agora-Rtm-Tutorial
//
//  Created by CavanSu on 2019/1/17.
//  Copyright © 2019 Agora. All rights reserved.
//

import UIKit

enum CellType {
    case left, right
}

class MessageCell: UITableViewCell {
    @IBOutlet weak var rightContentLabel: UILabel!
    @IBOutlet weak var rightChatBubble: UIImageView!
    @IBOutlet weak var rightContentBgView: UIView!
    @IBOutlet weak var imgReadUnReadStatus: UIImageView!
    @IBOutlet weak var rightTime: UILabel!
    @IBOutlet weak var leftUserName: NameInitalLabel!
    @IBOutlet weak var lblRightUserName: NameInitalLabel!
    

    @IBOutlet weak var leftUserLabel: UILabel!
    @IBOutlet weak var leftContentLabel: UILabel!
    
    @IBOutlet weak var leftContentBgView: UIView!
    @IBOutlet weak var leftChatBubble: UIImageView!
    @IBOutlet weak var leftTime: UILabel!
    
    var isFromGroupChat = false
    
    override func awakeFromNib() {
        rightContentBgView.layer.cornerRadius = 5
        
        leftContentBgView.layer.cornerRadius = 5
        
        self.leftContentLabel.sizeToFit()
        self.rightContentLabel.sizeToFit()
    }

    var currentObj : chatHistoryDatum?   // Bhavesh - 12 Nov 2020
    
    private var type: CellType = .right {
        didSet {
            let rightHidden = type == .left ? true : false
            
            rightContentLabel.isHidden = rightHidden
        
            leftUserLabel.isHidden = !rightHidden
            if isFromGroupChat {
                leftUserLabel.isHidden = false
            }
            leftContentLabel.isHidden = !rightHidden
            
            rightContentBgView.isHidden = rightHidden
            
            leftContentBgView.isHidden = !rightHidden
            
            lblRightUserName.isHidden = rightHidden
            leftUserName.isHidden = !rightHidden
        }
    }
    
    var receiverName = String()
    
    private var user: String? {
        didSet {
            switch type {
            case .left:
                leftUserLabel.text = "" //user
                if isFromGroupChat {
                                    leftUserLabel.text = currentObj?.senderName
                                }
                leftUserName.setText(fullname: receiverName)
            case .right:
                
//                print(receiverName)
//                print(AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? "")
                
                lblRightUserName.setText(fullname: AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? "")
            }
        }
    }
    
    private var content: String? {
        didSet {
            switch type {
            case .left:
                leftContentLabel.text = content
            case .right:
                rightContentLabel.text = content
            }
        }
    }
    
    func update(type: CellType, message: Message, time: String) {
        self.type = type
        self.user = message.userName
        self.content = message.text
        
        if type == .left {
            leftTime.text = time
        } else {
            rightTime.text = time
        }
        
        self.leftContentLabel.sizeToFit()
        self.rightContentLabel.sizeToFit()
        
        
        // Bhavesh - 12 Nov 2020
        if (currentObj?.deliver ?? false) == true {
            self.imgReadUnReadStatus.image = message.read == false ? #imageLiteral(resourceName: "reach-tick") : #imageLiteral(resourceName: "read-tick")
        } else {
            if message.read == true {
                self.imgReadUnReadStatus.image = #imageLiteral(resourceName: "read-tick")
            } else {
                self.imgReadUnReadStatus.image = UIImage(named: "send-tick")
            }            
        }
        
        setupBubbleImage()
    }
    
    func setupBubbleImage() {
        switch type {
        case .left:
            let bubbleImage = UIImage(named: "bubble_received")
            
            self.leftChatBubble.image = bubbleImage?.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),resizingMode: .stretch)
                .withRenderingMode(.alwaysTemplate)
                
            self.leftChatBubble.tintColor = UIColor.init(red: 59/255, green: 64/255, blue: 70/255, alpha: 1.0) // init(named: "SeondaryDark") //  Constants.AppTheme.Colors.ColorsOfApp.colorSecondaryGray.color // Constants.AppTheme.Colors.ColorsOfApp.green.color
            
        case .right:
            let bubbleImage = UIImage(named: "bubble_sent")
               self.rightChatBubble.image = bubbleImage?.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),resizingMode: .stretch)
                   .withRenderingMode(.alwaysTemplate)
            
            self.rightChatBubble.tintColor = UIColor.init(red: 83/255, green: 172/255, blue: 103/255, alpha: 1.0) // Constants.AppTheme.Colors.ColorsOfApp.green.color
        }
    }
}
