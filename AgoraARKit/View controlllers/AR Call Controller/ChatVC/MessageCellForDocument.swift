//
//  MessageCellForDocument.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 24/10/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation

class MessageCellForDocument: UITableViewCell {

    @IBOutlet weak var rightContentLabel: UILabel!
    @IBOutlet weak var rightChatBubble: UIImageView!
    @IBOutlet weak var rightContentBgView: UIView!
    @IBOutlet weak var imgReadUnReadStatus: UIImageView!
    @IBOutlet weak var rightTime: UILabel!
    @IBOutlet weak var rightImgItem: UIImageView!
    @IBOutlet weak var lblFileName: UILabel!
    

    @IBOutlet weak var leftUserLabel: UILabel!
    @IBOutlet weak var leftContentLabel: UILabel!
    
    @IBOutlet weak var leftContentBgView: UIView!
    @IBOutlet weak var leftChatBubble: UIImageView!
    @IBOutlet weak var leftTime: UILabel!
    @IBOutlet weak var leftImgItem: UIImageView!
    
    @IBOutlet weak var leftUserName: NameInitalLabel!
    @IBOutlet weak var rightUserName: NameInitalLabel!
    
    var currentObj : chatHistoryDatum?   // Bhavesh - 12 Nov 2020
    var receiverName = String()
    
    var isFromGroupChat = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        rightContentBgView.layer.cornerRadius = 5
        leftContentBgView.layer.cornerRadius = 5
        self.leftContentLabel.sizeToFit()
        self.rightContentLabel.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    private var type: CellType = .right {
        didSet {
            let rightHidden = type == .left ? true : false
            
            rightContentLabel.isHidden = true //rightHidden
            leftUserLabel.isHidden = !rightHidden
            if isFromGroupChat {
                leftUserLabel.isHidden = false
            }
            leftContentLabel.isHidden = !rightHidden
            rightContentBgView.isHidden = rightHidden
            leftContentBgView.isHidden = !rightHidden
            
            rightUserName.isHidden = rightHidden
            leftUserName.isHidden = !rightHidden
        }
    }
    
    private var user: String? {
        didSet {
            switch type {
            case .left:
                leftUserLabel.text = " " //user
                if isFromGroupChat {
                                    leftUserLabel.text = currentObj?.senderName
                                }
                leftUserName.setText(fullname: receiverName)
            case .right:
                rightUserName.setText(fullname: AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? "")
            }
        }
    }
    
    private var content: String? {
        
        didSet {
            switch type {
            case .left:  leftContentLabel.text = content
            case .right: rightContentLabel.text = content
            }
        }
    }
    
    func update(type: CellType, message: Message, fileName: String?, time: String, imageURL: String, fileType: String) {
        self.type = type
        self.user = message.userName
        self.content = message.text
        
        
        self.leftContentLabel.sizeToFit()
        self.rightContentLabel.sizeToFit()
        
        // Bhavesh - 12 Nov 2020
        if (currentObj?.deliver ?? false) == true {
            self.imgReadUnReadStatus.image = message.read == false ? #imageLiteral(resourceName: "reach-tick") : #imageLiteral(resourceName: "read-tick")
        } else {
            if message.read == true {
                self.imgReadUnReadStatus.image = #imageLiteral(resourceName: "read-tick")
            } else {
                self.imgReadUnReadStatus.image = UIImage(named: "send-tick")
            }
        }

        setupBubbleImage()
        
        
        
        if type == .left {
            leftTime.text = time
            leftImgItem.sd_imageTransition = .flipFromBottom
            leftImgItem.sd_imageIndicator = SDWebImageActivityIndicator.gray
            
            leftImgItem.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "placeholder"))
            if fileType.lowercased() == "pdf" {
                leftImgItem.image = UIImage(named: "pdf")
            }
            else if fileType.lowercased() == "xls" || fileType.lowercased() == "xlsx" {
                leftImgItem.image = UIImage(named: "xls")
            }
            else if fileType.lowercased() == "ppt" || fileType.lowercased() == "pptx" {
                leftImgItem.image = UIImage(named: "ppt")
            }
            else if fileType.lowercased() == "doc" || fileType.lowercased() == "docx" {
                leftImgItem.image = UIImage(named: "doc")
            }
            else if fileType.lowercased() == "txt" {
                leftImgItem.image = UIImage(named: "txt")
            }
        } else {
            lblFileName.text = fileName ?? ""
            rightTime.text = time
            leftImgItem.sd_imageTransition = .flipFromBottom
            rightImgItem.sd_imageIndicator = SDWebImageActivityIndicator.gray
            rightImgItem.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "placeholder"))
            
            rightImgItem.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "placeholder"))
            if fileType == "pdf" {
                rightImgItem.image = UIImage(named: "pdf")
            }
            else if fileType.lowercased() == "xls" {
                rightImgItem.image = UIImage(named: "xls")
            }
            else if fileType.lowercased() == "ppt" {
                rightImgItem.image = UIImage(named: "ppt")
            }
            else if fileType.lowercased() == "doc" || fileType.lowercased() == "docx" {
                rightImgItem.image = UIImage(named: "doc")
            }
            else if fileType.lowercased() == "txt" {
                rightImgItem.image = UIImage(named: "txt")
            }
        }
       

    }
    
    func setupBubbleImage() {
        switch type {
        case .left:
            let bubbleImage = UIImage(named: "bubble_received")
            
            self.leftChatBubble.image = bubbleImage?.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),resizingMode: .stretch)
                .withRenderingMode(.alwaysTemplate)
                
            self.leftChatBubble.tintColor =  UIColor.init(red: 59/255, green: 64/255, blue: 70/255, alpha: 1.0)// Constants.AppTheme.Colors.ColorsOfApp.colorLightGray.color // Constants.AppTheme.Colors.ColorsOfApp.green.color
            
        case .right:
            let bubbleImage = UIImage(named: "bubble_sent")
               self.rightChatBubble.image = bubbleImage?.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),resizingMode: .stretch)
                   .withRenderingMode(.alwaysTemplate)
            
            self.rightChatBubble.tintColor = UIColor.init(red: 83/255, green: 172/255, blue: 103/255, alpha: 1.0) // Constants.AppTheme.Colors.ColorsOfApp.green.color
        }
    }
    
    
}
