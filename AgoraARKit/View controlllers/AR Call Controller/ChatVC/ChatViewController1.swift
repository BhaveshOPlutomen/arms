//
//  ChatViewController.swift
//  Agora-Rtm-Tutorial
//
//  Created by CavanSu on 2019/1/17.
//  Copyright © 2019 Agora. All rights reserved.
//

import UIKit
import AVKit
import AgoraRtmKit


class ChatViewController1: UIViewController, Alertable {
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var inputViewBottom: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputContainView: UIView!
    @IBOutlet weak var viewTextFieldContainer: UIView!
    
    private var list:[Message] = []
    var type: ChatType = .peer("unknow")
    var rtmChannel: AgoraRtmChannel?
    
    
     var sendChatMessage:((String) -> ())? = nil
    

    
    
     
    //--------------------------------------------------------------------------------
    
    //MARK:- Abstract Methdos
    
    //--------------------------------------------------------------------------------
    
    class func viewController () -> ChatViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Custom  Methdos
    
    //--------------------------------------------------------------------------------

    func messageRecv(text:String,fromUser userName:String,userID:String) {
        appendMessage(userID: userID, userName: userName, content: text)
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Action Methdos
    
    //--------------------------------------------------------------------------------

    @IBAction func btnSendTapped(_ sender: Any) {
        if pressedReturnToSendText(self.inputTextField.text) {
            inputTextField.text = nil
            
        } else {
            view.endEditing(true)
        }
               
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  View LifeCycle  Methdos
    
    //--------------------------------------------------------------------------------

    
    
    override func viewDidLoad() {
       addKeyboardObserver()
        updateViews()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

private extension ChatViewController1 {
    func updateViews() {
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 55
        tableView.contentInset = .init(top: 20, left: 0, bottom: 0, right: 0)
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "chatbackgroundPNG")
        imageView.backgroundColor = UIColor.lightGray

        imageView.contentMode = .scaleAspectFill

        viewTextFieldContainer.layer.borderColor = Constants.AppTheme.Colors.ColorsOfApp.green.color.cgColor
        viewTextFieldContainer.layer.borderWidth = 2
        viewTextFieldContainer.layer.cornerRadius = 16

        
//        let blurEffect = UIBlurEffect(style: .extraLight)
//        let blurView = UIVisualEffectView(effect: blurEffect)
//        blurView.frame = tableView.bounds
//        imageView.addSubview(blurView)

        
    //    tableView.backgroundView = imageView
    }
    
    func pressedReturnToSendText(_ text: String?) -> Bool {
        guard let text = text?.trimmingCharacters(in: .whitespaces), text.count > 0 else {
            return false
        }
        self.sendChatMessage?(text)
        
        appendMessage(userID: AppGlobalManager.sharedInstance.currentUserID ?? "0", userName: AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? "0" , content: text)

        return true
    }
    
    func appendMessage(userID: String,userName:String, content: String) {
        let msg = Message(userId: userID, userName: userName, text: content, read: false)
        list.append(msg)
        if list.count > 300 {
            list.removeFirst()
        }
        let end = IndexPath(row: list.count - 1, section: 0)
        DispatchQueue.main.async { [unowned self] in
            self.tableView.reloadData()
            self.tableView.scrollToRow(at: end, at: .bottom, animated: true)
            let systemSoundID: SystemSoundID = 1115
            AudioServicesPlaySystemSound (systemSoundID)

        }
    }
    
    
    func addKeyboardObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardFrameWillChange(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func keyboardFrameWillChange(notification: NSNotification) {
        guard let userInfo = notification.userInfo,
            let endKeyboardFrameValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let durationValue = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber else {
                return
        }
        
        let endKeyboardFrame = endKeyboardFrameValue.cgRectValue
        let duration = durationValue.doubleValue
        
        let isShowing: Bool = endKeyboardFrame.maxY > UIScreen.main.bounds.height ? false : true
        
        UIView.animate(withDuration: duration) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            if isShowing {
                let offsetY = strongSelf.inputContainView.frame.maxY - endKeyboardFrame.minY
                guard offsetY > 0 else {
                    return
                }
                strongSelf.tableView.contentInset = .init(top: offsetY, left: 0, bottom: 0, right: 0)
           //     strongSelf.inputViewBottom.constant = -offsetY
            } else {
             //   strongSelf.inputViewBottom.constant = 0
                strongSelf.tableView.contentInset = .init(top: 20, left: 0, bottom: 0, right: 0)

            }
            strongSelf.view.layoutIfNeeded()
        }
    }
}

extension ChatViewController1: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let msg = list[indexPath.row]
        let type: CellType = msg.userId == (AppGlobalManager.sharedInstance.currentUserID ?? "") ? .right : .left
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        cell.update(type: type, message: msg, time: "")
        cell.backgroundColor = .clear

        return cell
    }
}

extension ChatViewController1: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if pressedReturnToSendText(textField.text) {
            textField.text = nil
            
        } else {
            view.endEditing(true)
        }
        
        return true
    }
}
