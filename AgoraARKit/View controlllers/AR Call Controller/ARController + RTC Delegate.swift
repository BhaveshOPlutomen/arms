//
//  ARController + RTC Delegate.swift
//  AgoraARKit
//
//  Created by Prashant on 03/06/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import Foundation
import AgoraRtcKit
import ARKit

//--------------------------------------------------------------------------------

//MARK:-   Agora Room Delegate Methdos

//--------------------------------------------------------------------------------

extension ARController:AgoraRtcEngineDelegate {
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        print("Channel JOIN BY \(uid) ")
        
        if uid != selfSession.userID {
            //   let userSession = videoSession(ofUid: Int64(uid))
            
            
            AppGlobalManager.sharedInstance.uidAudioArray.append(String(uid))
            
            AppGlobalManager.sharedInstance.uidScreenShareArray = []
            //            AppGlobalManager.sharedInstance.uidScreenShareArray.append(String(uid))
            AppGlobalManager.sharedInstance.uidScreenShareArray.append(String(self.selfSession.userID))
            
            let arSession = self.activeSessions.findOrInsertSession(userID: Int64(uid))
            
            
            let scale = UIScreen.main.scale

                let messageTRY = "{\(Int(round(self.sceneView.frame.width))),\(Int(round(self.sceneView.frame.height)))}"
                let message = "\(Constants.ChannelMessages.screensizeshareSenderFreezeMode)-\(messageTRY)"
                let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
                print("Sender message joining didJoinedOfUid : \(string)")

                self.sendText(string: string)
           
            // Check if only tw0o user in the channel (self and other), and current user is not sharing the screen. assign full sesion to other user (who must sharing the screen)
            // If there is more than two users send Who IS Publisher message and resolve full screen on handleMessage Screen
            // If Current user is sharing the screen then send user_joined message with handle. only person who share the screen can invite other user for group
            // so selfSession.calls will have new call object when user invited user for group call
            
            
            if activeSessions.count > 1 && !self.selfSession.isSharingSession {
                self.sendText(string:Constants.ChannelMessages.whoIsPublisher)
                print("Check who is publisher message sent")
                
            } else if activeSessions.count > 1 && self.selfSession.isSharingSession {
                
//                self.activeUserNames = [String]()
                self.arrayUserNames = [[String:Any]]()
               
                for call in selfSession.calls {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [self] in
                        self.sendText(string: Constants.ChannelMessages.user_joined + call.handle + "-" + call.callName)
                        print("\n -------------------------------------")
                        print(selfSession.userID)
                        print(call.callName)
                        print(call.handle)
                        
                        self.DictActiveUsernames["UserName"] = call.callName
                        self.DictActiveUsernames["MobileNumber"] = call.handle
                        self.DictActiveUsernames["uid"] = "202020"
                        if self.arrayUserNames.filter({$0["MobileNumber"] as? String ?? "" == call.handle}).count == 0 {
                            self.arrayUserNames.append(self.DictActiveUsernames)
                        }
//                        self.arrayUserNames.append(self.DictActiveUsernames)
                        print("\n &&&&&&&&&&& diduserjoined Active user name array : %@",self.arrayUserNames)

                        self.colUserName.reloadData()
                        self.view.setNeedsLayout()
                    }
                }
            }  else {
                print("Check fullsession")
             
                fullSession = self.activeSessions.first(where: {$0.userID == Int64(uid)})?.videoSession
                rtcEngine.setupRemoteVideo(arSession.videoSession!.canvas) // Forcefully unwrap videoSession because we have call findOrInsertSession which create if not exits
                
            }
            
            //            if videoSessions.count > 1 && !self.selfSession.isSharingSession {
            //                self.sendText(string: Constants.ChannelMessages.whoIsPublisher)
            //            } else {
            //                //FIXME: Un Comment it
            //
            //                fullSession = videoSession(ofUid: Int64(uid))
            //                rtcEngine.setupRemoteVideo(userSession.canvas)
            //
            //            }
            //            resolveScreenSharing(userId:uid)
        }
        
    }
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstLocalVideoFrameWith size: CGSize, elapsed: Int) {
        print("##########################\n\n *** firstLocalVideoFrameWith ***\n Size :: \(size) \n elapsed :: \(elapsed) \n\n#########################")

    }
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoFrameOfUid uid: UInt, size: CGSize, elapsed: Int) {
        print("##########################\n\n *** firstRemoteVideoFrameOfUid ***\n Size :: \(size) \n uid :: \(uid) \n\n#########################")

    }
   
    func rtcEngine(_ engine: AgoraRtcEngineKit, videoSizeChangedOfUid uid: UInt, size: CGSize, rotation: Int) {
            print("##########################\n\n *** videoSizeChangedOfUid *** \n UID :: \(uid) \n Size :: \(size) \n Rotation :: \(rotation) \n\n#########################")
        }
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int) {
        print("Channel JOIN BY \(uid) Current USER \(uid) \(UIDevice.current.name)")
        
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        
        print("offline user %d", uid)
        
        if let sessionIndex = self.activeSessions.firstIndex(where: {$0.userID == Int64(uid)}) {
            let session =  self.activeSessions.remove(at: sessionIndex)
            
            session.videoSession?.hostingView.removeFromSuperview()
            if session.isFullSession && session.videoSession == fullSession {
                fullSession = nil
            }
        }
        if arrayUserNames.count == 0 {
            if reason == .quit{
//                unfortunatelyStopCall()
//                self.status = .ideal
            }
        }
        
//            arrayUserNames = arrayUserNames.filter{$0["uid"] as? String != String(uid)}
//            print(arrayUserNames)
//                self.colUserName.reloadData()
//                self.view.setNeedsLayout()
//                if arrayUserNames.count == 0 {
//                    if reason == .quit{
//                        unfortunatelyStopCall()
//                    }
//                }
//        
        
        //        var indexToDelete: Int?
        //        for (index, session) in videoSessions.enumerated() {
        //            if session.uid == Int64(uid) {
        //                indexToDelete = index
        //            }
        //        }
        //
        //        if let indexToDelete = indexToDelete {
        //            let deletedSession = videoSessions.remove(at: indexToDelete)
        //            deletedSession.hostingView.removeFromSuperview()
        //
        //            if deletedSession == fullSession {
        //                fullSession = nil
        //            }
        //        }
    }
    
    
    
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didLeaveChannelWith stats: AgoraChannelStats) {
        print("Leave chaannel \(stats.duration)")
        
        print("%@ active session count", self.activeSessions.count)
        // Save to local database
        // CoreDataManager.saveCall(call: call,duration:stats.duration)
        _ = self.selfSession.calls.map{CoreDataManager.saveCall(call: $0, duration: stats.duration)}
        
        
        // CoreDataManager.saveCall(call: self.selfSession.call, duration: stats.duration)
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didRegisteredLocalUser userAccount: String, withUid uid: UInt) {
        //right now not in use
        //        print("\n\n didRegisteredLocalUser userAccount :: \(userAccount) and uid :: \(uid) \n\n")
        
    }
    func rtcEngine(_ engine: AgoraRtcEngineKit, didUpdatedUserInfo userInfo: AgoraUserInfo, withUid uid: UInt) {
        //        print("\n\n didUpdatedUserInfo userInfo :: \(userInfo.userAccount ?? "") and uid :: \(userInfo.uid) \n\n")
        
        // right now not in use
        
        timer?.invalidate()
        timer = nil
        if let alertVC = self.presentedViewController as? UIAlertController {
            alertVC.dismiss(animated: true, completion: nil)
        }
        DictActiveUsernames["UserName"] = userInfo.userAccount ?? ""
        DictActiveUsernames["uid"] = userInfo.uid
        arrayUserNames.append(DictActiveUsernames)
        print(arrayUserNames)
        self.colUserName.reloadData()
        self.view.setNeedsLayout()
        
    }
    func rtcEngine(_ engine: AgoraRtcEngineKit, networkQuality uid: UInt, txQuality: AgoraNetworkQuality, rxQuality: AgoraNetworkQuality) {
        
        
            // print("Network Quality : \(txQuality.rawValue) , rxQuality : \(rxQuality.rawValue), UID :\(uid)")
        
        guard uid == 0 else {
            return
        }
        DispatchQueue.main.async {
            //  self.lblNetworkPing.isHidden = false
            
            switch txQuality {
                
            case AgoraNetworkQuality.excellent:
                self.signalStrenghtView.level = .excellent
                self.signalStrenghtView.color = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
                self.lblNetwork.text = "Excellent Network"
                
            case AgoraNetworkQuality.good:
                self.signalStrenghtView.level = .veryGood
                self.signalStrenghtView.color = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
                self.lblNetwork.text = "Very Good Network"
                
            case AgoraNetworkQuality.poor:
                self.signalStrenghtView.level = .good
                self.signalStrenghtView.color = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
                self.lblNetwork.text = "Good Network"
                
            case AgoraNetworkQuality.bad:
                self.signalStrenghtView.level = .low
                self.signalStrenghtView.color = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
                self.lblNetwork.text = "Low Network"
                
            case AgoraNetworkQuality.vBad:
                self.signalStrenghtView.level = .veryLow
                self.signalStrenghtView.color = #colorLiteral(red: 1, green: 0.1490196078, blue: 0, alpha: 1)
                self.lblNetwork.text = "Very Low Network"
                if self.networkTimerCount == 30 {
                    self.networkTimerCount = 0
                    self.ShowLowNetworkAlert()
                }
                self.networkTimerCount += 1
                
            case AgoraNetworkQuality.down:
                self.signalStrenghtView.level = .noSignal
                self.signalStrenghtView.color =  #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
                self.lblNetwork.text = "No Signal"
                if self.networkTimerCount == 30 {
                    self.networkTimerCount = 0
                    self.ShowLowNetworkAlert()
                }
                self.networkTimerCount += 1
                
            default :
                self.signalStrenghtView.level = .veryLow
                self.signalStrenghtView.color = #colorLiteral(red: 0.02352941176, green: 0.2588235294, blue: 0.3450980392, alpha: 1)
                
            }
        }
        
    }
   
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, lastmileQuality quality: AgoraNetworkQuality) {
        print("lastmileQuality : \(quality.rawValue) ")

    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, reportRtcStats stats: AgoraChannelStats) {
        self.call.duration = stats.duration
        
        let message = "\(Constants.ChannelMessages.myCallDuration)\(stats.duration)"
        let string =  MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
        self.sendText(string: string)
        
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didLocalPublishFallbackToAudioOnly isFallbackOrRecover: Bool) {
        if (isFallbackOrRecover){
//            self.showAlert(withMessage: "fallback method called to audio")
            print("fallback to audio")
        }else{
//            self.showAlert(withMessage: "fallback method called to video")
            print("Video is started")
        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didRemoteSubscribeFallbackToAudioOnly isFallbackOrRecover: Bool, byUid uid: UInt) {
        if (isFallbackOrRecover){
//            self.showAlert(withMessage: "fallback method called to audio")
            print("fallback to audio")
            print(uid)
        }else{
//            self.showAlert(withMessage: "fallback method called to video")
            print("Video is started")
            print(uid)
        }
    }
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        print("error: \(errorCode.rawValue)")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurWarning warningCode: AgoraWarningCode) {
        print("warning: \(warningCode.rawValue)")
    }
    /// Occurs when the connection between the SDK and the server is interrupted.
    func rtcEngineConnectionDidInterrupted(_ engine: AgoraRtcEngineKit) {
        //        self.showAlert(withMessage: "Connection Interrupted")
//        self.showToast("Connection Interrupted", type: .fail)
    }
    
    /// Occurs when the SDK cannot reconnect to Agora’s edge server 10 seconds after its connection to the server is interrupted.
    func rtcEngineConnectionDidLost(_ engine: AgoraRtcEngineKit) {
        //        self.showAlert(withMessage: "Connection Lost")
//        self.showToast("Internet Connection Lost", type: .fail)
        
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, localVideoStats stats: AgoraRtcLocalVideoStats) {
        //        print("local video stats frame count %@",stats.encoderOutputFrameRate)
        //        print("local video stats %@",stats.qualityAdaptIndication.rawValue)
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, connectionChangedTo state: AgoraConnectionStateType, reason: AgoraConnectionChangedReason) {
        switch state {
            
            
        case .connecting:
//            showToast("Connecting to Server ", type: .info)
//            showToast("", type: .info)
            break
        case .connected:
//            showToast("Connected with Server ", type: .info)
//            showToast("", type: .info)
            break
        case .reconnecting:
            if self.networkTimerCount == 40 {
                self.connectionLostCount = 0
                showToast("Connection lost, reconnecting to server ... ", type: .fail)
            }
            self.connectionLostCount += 1
            
        case .failed:
            if self.networkTimerCount == 40 {
                self.connectionLostCount = 0
                showToast("Failed to connect with Server ", type: .fail)
            }
            self.connectionLostCount += 1
            
        default:
            break
            
        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, networkTypeChangedTo type: AgoraNetworkType) {
        switch type {
            
        case .unknown:
            break
        case .disconnected:
            self.showToast("Network disconnected", type: .fail)
            
        case .LAN:
            break
        case .WIFI:
            self.showToastBottom("Connected with WIFI", type: .success)
            
        case .mobile2G:
            self.showToastBottom("Connected with Mobile network (2G)", type: .success)
            
        case .mobile3G:
            self.showToastBottom("Connected with Mobile network (3G)", type: .success)
            
        case .mobile4G:
            self.showToastBottom("Connected with Mobile network (4G)", type: .success)
            
        }
    }
    
    
    
}


