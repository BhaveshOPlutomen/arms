//
//  AnnotaionListViewController.swift
//  AgoraARKit
//
//  Created by Prashant on 26/11/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit

class AnnotaionListViewController: BaseViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK:- Var
    
    var closeAction: (() -> ())? = nil
    var annotationSelected: ((ARController.AnnotaionType) -> ())? = nil

    //--------------------------------------------------------------------------------
    
    //MARK:- Memory Managment Methods
    
    //--------------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Abstract Methdos
    
    //--------------------------------------------------------------------------------
    
    class func viewController () -> AnnotaionListViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AnnotaionListViewController") as! AnnotaionListViewController
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Action Methdos
    
    //--------------------------------------------------------------------------------

    @IBAction func closeTapped(_ sender: Any) {
        self.closeAction?()
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Custom Methdos
    
    //--------------------------------------------------------------------------------

    private func setupCollectionView() {
        self.collectionView.register(UINib(nibName: "AnnotaionCell", bundle: nil), forCellWithReuseIdentifier: "AnnotaionCell")
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }
    
    
    //--------------------------------------------------------------------------------
    
    //MARK:- ViewLifeCycle Methods
    
    //--------------------------------------------------------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }
    
    //--------------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //--------------------------------------------------------------------------------
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    

}

extension AnnotaionListViewController: UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return   ARController.AnnotaionType.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnotaionCell", for: indexPath) as! AnnotaionCell
        cell.imgAnnotation.image = ARController.AnnotaionType.allCases[indexPath.row].image
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        print("Will display cell")
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedAnnotaion = ARController.AnnotaionType.allCases[indexPath.item]
        self.annotationSelected?(selectedAnnotaion)
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        self.view.layoutIfNeeded()
        let numberOfItems:CGFloat = 5.0
        let space:CGFloat = 10
        let itemSpace = ((numberOfItems - 1) * space)
        let edgeinsetSpace = 2 * space
        let width = collectionView.frame.width  -  itemSpace  - edgeinsetSpace
        let size = width  / numberOfItems
        return CGSize(width:size,height:50)
    }
    
    
}
