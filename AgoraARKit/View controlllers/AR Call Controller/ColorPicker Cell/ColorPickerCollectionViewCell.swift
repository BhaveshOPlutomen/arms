//
//  ColorPickerCollectionViewCell.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 18/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit

class ColorPickerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewColor: UIButton!
    
}
