//
//  ARHelper.swift
//  AgoraARKit
//
//  Created by Hetali on 29/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

// store screenshot API
//store call API
//Sync API
//ContactSelectedDelegate
//CallStatusDelegate
//UITextFieldDelegate for adding custom textview
//Video Display Methdos
//Drawing methods

import Foundation
import NVActivityIndicatorView
import ARKit

extension ARController{
 
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Store Call service
    
    //--------------------------------------------------------------------------------
    
    func storeCallApi(){
        
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        guard let user_call_id = AppGlobalManager.sharedInstance.user_call_id_Global else{
            return
        }
        guard let user_id = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else{
            return
        }
        var join_time : String = ""
        var leave_time : String = ""
        if isJoinChannel{
            join_time = UtilityClass.getCurrentDateToday()
            leave_time = ""
        }else{
            leave_time = UtilityClass.getCurrentDateToday()
            join_time = ""
        }
        let requestParameters = [
            "user_call_id": user_call_id,
            "device_id" : device_id,
            "device_type" : "0",
            "room_name" : self.roomName!,
            "user_id" : user_id,
            "join_time" : join_time,
            "leave_time" : leave_time,
            "total_time" : call.duration,
            "agora_user_id" : self.selfSession.userID,
            "location" : AppGlobalManager.sharedInstance.locationString,
            "latitude" : AppGlobalManager.sharedInstance.latitude,
            "longitude" : AppGlobalManager.sharedInstance.longitude] as [String : Any]
        
        print("store call api parameters : \(requestParameters)")
        
        print("\n\n\n \(#function) :: \(user_call_id)\n\n\n")
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.storeCall, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    let userResponse =  CommonCodableHelper<GeneralResponse>.codableObject(from: data)
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        self.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                        return
                    }
                                    
                }
                
            case .failure( let error):
                print("error",error)
            }
            
        }
        
    }
    //--------------------------------------------------------------------------------
       
       //MARK:-  Sync API call 
       
       //--------------------------------------------------------------------------------
       
     func SyncAPI(){
            let activityData = ActivityData()
    //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            guard let userID = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else{
                return
            }
            guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
                return
            }
            let requestParameters = ["user_id" : userID,
                                     "device_type" : "0",
                                     "device_id" : device_id,
                                     "device_token" : UserDefaults.standard.object(forKey: "TOKEN") ?? "",
                                     "invite_token":  UserDefaults.standard.object(forKey: "fcmToken") ?? ""
                                        ]
            
            print(requestParameters)
            
            APIHandler.callAPI(endPoint: APIConstants.EndPoints.userSync, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { [weak self](response) in
                switch response{
                case .success(let data):
                    DispatchQueue.main.async {
    //                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        let userResponse =  CommonCodableHelper<SyncResponse>.codableObject(from: data)
                        
                        if let userData =  data as? Data, let userObject = CommonCodableHelper<SyncResponse>.codableObject(from: userData)?.output {
                            AppGlobalManager.sharedInstance.syncLoginResponse = userObject
                        }
                        
                        guard let status = userResponse?.statusCode,status == 1 else {
                            //                        self!.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                                if let messageCode = userResponse?.messageCode{
                                if messageCode == "log_in_oth_dvc"{
                                    let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                        AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                    }
                                    
                                    self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                                }else if messageCode == "val_fail"{
                                    let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                        AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                    }
                                    
                                    self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                                }else if messageCode == "comp_nt_act" || messageCode == "usr_acnt_dsbl" {
                                    let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                        AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                    }
                                    
                                    self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                                    
                                    
                                }else if messageCode == "rec_acnt_dsbl" || messageCode == "rec_comp_nt_act"{
                                    let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    }
                                    self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                                }
                            }
                            return
                        }
                        let syncCalloutput = CommonCodableHelper<SyncResponse>.codableObject(from: data)?.output
                        
                        //                    let categoryOutput = userResponse?.output?.category
                        
                        AppGlobalManager.sharedInstance.syncLoginResponse = syncCalloutput
                        
                    }
                    
                case .failure( let error):
                    print("error",error)
    //                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }
            }
        }
}
extension ARController:ContactSelectedDelegate,CallStatusDelegate {
    func contactSelected(contact:String,name:String) {
        // print("Contact Selected \(contact.phoneNumber)")
        isContactVCOpen = false
        var phoneNumber = contact
        phoneNumber = phoneNumber.filter{$0.unicodeScalars.allSatisfy{$0.isASCII}}         // Remove special characters if any and white space
        phoneNumber.removeAll(where: {$0.isNewline || $0.isWhitespace})
        
        showCallStatusVC(callerID: phoneNumber,name:name)
        
        
    }
    
    fileprivate func showCallStatusVC(callerID:String,name:String) {
        let callStatusVC = CallStatusViewController.viewController()
        callStatusVC.roomName = self.roomName
        callStatusVC.isForGroupCall = true
        callStatusVC.delegate = self
        callStatusVC.callName = name
        callStatusVC.callerID = callerID
        self.present(callStatusVC, animated: true, completion: nil)
    }
        
    
    func callDisconntedByReciver(call:Call) {
        setupRTM()
        CoreDataManager.saveCall(call: call, duration: 0)
        
        call.end()
        AppDelegate.shared.callManager.remove(call: call) // important , endCallCXAction == CALLS =>   func provider(_ provider: CXProvider, perform action: CXEndCallAction)
        
        AppDelegate.shared.callManager.endCallCXAction(call: call, completion: nil)
        
    }
    
    func callAccpted(call:Call) {
        self.selfSession.calls.appendNewCall(call: call)
                
        setupRTM()
        // setupSignalingSDK()
//        self.showAlert(withMessage: "User has accpted group call request will join soon.")
        let message = "\(call.callName) has joined group call."
//        self.showAlert(withMessage: message)

        self.showToast(message, type: .success)
        
        print(self.selfSession.calls.map{$0.callName})
        
//        self.activeUserNames = self.selfSession.calls.map{$0.callName}
        //        self.colUserName.reloadData()
//        print("Active user name array : %@",self.activeUserNames)
        self.DictActiveUsernames["UserName"] = call.callName
        self.DictActiveUsernames["MobileNumber"] = call.handle
        self.DictActiveUsernames["uid"] = "202020"
//        self.arrayUserNames.append(self.DictActiveUsernames)
        if self.arrayUserNames.filter({$0["MobileNumber"] as? String ?? "" == call.handle}).count == 0 {
                    self.arrayUserNames.append(self.DictActiveUsernames)
                }
        print("\n &&&&&&&&&&& CallAccepted Active user name array : %@",self.arrayUserNames)
        self.colUserName.reloadData()
        self.view.setNeedsLayout()
      
//        canStartRecording = true
    }
    
    func callDisconnectedByCaller(call:Call) {
        CoreDataManager.saveCall(call: call, duration: 0)
        //  AppDelegate.shared.status = .ideal
        setupRTM()
        
        call.end()
        AppDelegate.shared.callManager.remove(call: call) // important , endCallCXAction == CALLS =>   func provider(_ provider: CXProvider, perform action: CXEndCallAction)
        
        AppDelegate.shared.callManager.endCallCXAction(call: call, completion: nil)
        
    }
}
//--------------------------------------------------------------------------------

//MARK:-  Video Display Methdos

//--------------------------------------------------------------------------------

extension ARController {
    func updateInterface(withAnimation animation: Bool) {
        if animation {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.updateInterface()
                self?.view.layoutIfNeeded()
            }
        } else {
            updateInterface()
        }
    }
    
    func updateInterface() {
        let videoSessions = self.activeSessions.compactMap({$0.videoSession})
        viewLayouter.layout(sessions: videoSessions, fullSession: fullSession, inContainer: viewToDisplayAR)
        setStreamType(forSessions: videoSessions, fullSession: fullSession)
    }
    
    //    func fetchSession(ofUid uid: Int64) -> VideoSession? {
    //        for session in videoSessions {
    //            if session.uid == uid {
    //                return session
    //            }
    //        }
    //
    //        return nil
    //    }
    //
    //    func videoSession(ofUid uid: Int64) -> VideoSession {
    //        if let fetchedSession = fetchSession(ofUid: uid) {
    //            return fetchedSession
    //        } else {
    //            let newSession = VideoSession(uid: uid)
    //            videoSessions.append(newSession)
    //            return newSession
    //        }
    //    }
}

//--------------------------------------------------------------------------------

//MARK:-  Drawing Methdos

//--------------------------------------------------------------------------------


extension ARController {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       
  
        guard let touchCount = event?.allTouches?.count, touchCount == 1 else  {
            return
        }
        
        print("\n touchesMoved Touch Count :: \(touchCount)\n ")
        guard let touchInView = touches.first?.location(in: sceneView),self.isDrawingEnable else {
            return
        }

        if isFreezeModeOn == true {
            
            if self.selfSession.isSharingSession {
                isTouchCancelCall = false
                guard let touchInView2 = touches.first?.location(in: imgViewScreenshot) else { return }
                touchBeginPoint = touchInView2
                tempSenderBufferLocal.append([touchInView2])
                tempSenderDrawLineColor.append([senderLineColor])
                var obj = Points2D()
                obj.points2D = tempSenderBufferLocal
                obj.lineColor = tempSenderDrawLineColor
                tempSenderBufferLocalScreenDrawing = obj
            }
            else {
                
                guard let touchInView2 = touches.first?.location(in: viewToDisplayAR) else { return }
               
                tempSenderBufferLocal.append([touchInView2])
                tempSenderDrawLineColor.append([senderLineColor])
                var obj = Points2D()
                obj.points2D = tempSenderBufferLocal
                obj.lineColor = tempSenderDrawLineColor
                tempSenderBufferLocalScreenDrawing = obj
                //Keep Update Gloabl object for on screen drawing (Image render)
                bezierPath.removeAllPoints()
                screenDrawingLastPoint = touches.first?.location(in: imgViewScreenshot) ?? CGPoint(x: 150, y: 150)
                let message = "\(Constants.ChannelMessages.touchBegin)-\(NSCoder.string(for: normalizedCGPoint(point: touchInView2)))"
                let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                self.sendText(string: string)
            }
            return
        }
        
        //-----------------------------------------------
        if self.selfSession.isSharingSession {
            
            print("TOUCH BEGIN ====== \(touchInView)")
            // hold onto touch location for projection
            touchPoint = touchInView
            
            // begin a new stroke
            let stroke = Stroke()
            print("Touch")
            if let anchor = makeAnchor(at:touchPoint) {
                stroke.anchor = anchor
                stroke.points.append(SCNVector3Zero)
                stroke.touchStart = touchPoint
                stroke.lineWidth = strokeSize.rawValue
                print(strokeSize.rawValue)
                stroke.color = senderLineColor
                self.selfSession.strokes.append(stroke)
                self.selfSession.arUndoManager.addUndoableObject(stroke)
                sceneView.session.add(anchor: anchor)
            }
            
        }  else {
            
            // Add Point to removePoints Array
            //     self.remotePoints.append(touchInView)
            
            //Keep Update Gloabl object for on screen drawing (Image render)
            
            if(self.isFromNonARDevice){
                self.showToast("Caller's device is not  AR Supported. You can draw only in freeze mode.", type: .fail)
            }else{
                bezierPath.removeAllPoints()
                screenDrawingLastPoint = touches.first?.location(in: viewToDisplayAR) ?? CGPoint(x: 150, y: 150)
                
                let message = "\(Constants.ChannelMessages.touchBegin)-\(NSCoder.string(for: normalizedCGPoint(point: touchInView)))"
                //FIXME: =======  Changed Here ======
                //    self.sendText(string: message)
                
                let string =  MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                self.sendText(string: string)
            }
         
            
        }
        
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let touchCount = event?.allTouches?.count, touchCount == 1 else  {
            return
        }

        guard let touchInView = touches.first?.location(in: sceneView),self.isDrawingEnable else {
            return
        }
        
        if isFreezeModeOn == true {
            if self.selfSession.isSharingSession {
                guard let touchInView2 = touches.first?.location(in: imgViewScreenshot) else { return }
                var obj = Points2D()
                guard var lastLine = tempSenderBufferLocal.popLast() else { return }
                lastLine.append(touchInView2)
                tempSenderBufferLocal.append(lastLine)
                guard var lastLineColor = tempSenderDrawLineColor.popLast() else { return }
                lastLineColor.append(senderLineColor)
                tempSenderDrawLineColor.append(lastLineColor)
                
                obj.points2D = tempSenderBufferLocal
                obj.lineColor = tempSenderDrawLineColor
                tempSenderBufferLocalScreenDrawing = obj
                drawImage()
            }
            else {
                
                guard let touchInView2 = touches.first?.location(in: viewToDisplayAR) else { return }
               
                // Keeping buffer in local until
                bufferLocalScreenDrawing.append(touchInView2)
                // Send 10 Objects and empty buffer
                if bufferLocalScreenDrawing.count == 10 {
                    do {
                        let dictRequest = [Constants.ChannelMessages.allPoints:self.bufferLocalScreenDrawing.map{NSCoder.string(for: normalizedCGPoint(point: $0))}]
                        let data = try JSONSerialization.data(withJSONObject: dictRequest, options: .prettyPrinted)
                        //FIXME: ======= Changed Here ======
                        //self.sendData(data: data)
                        if let message = String(data:data,encoding: .utf8) {
                            let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                            self.sendText(string: string)
                            print("Message Sent allpoints %@",string)
                        }
                    } catch {
                        bufferLocalScreenDrawing.removeAll()
                        print("Error\(error)")
                    }
                    bufferLocalScreenDrawing.removeAll()
                }
                
                autoreleasepool {
                    drawLine(fromPoint: screenDrawingLastPoint, toPoint: touches.first?.location(in: imgViewScreenshot) ?? CGPoint(x: 150, y: 150))
                }
                
                // Add Point to removePoints Array
                //  self.remotePoints.append(touchInView)
                //Keep Update Gloabl object for on screen drawing (Image render)
                screenDrawingLastPoint = touchInView2 // touches.first?.location(in: imgViewScreenshot) ?? CGPoint(x: 150, y: 150)
                isPanningForScreenDrawing = true
            }
            return
        }
        
        //-------------------------------------------------
        // hold onto touch location for projection
        
        if self.selfSession.isSharingSession && touchPoint != .zero {
            touchPoint = touchInView
            
        } else {
            if(self.isFromNonARDevice){
                self.showToast("Caller's device is not  AR Supported. You can draw only in freeze mode.", type: .fail)
            }else{
                let message = "\(Constants.ChannelMessages.touchMove)-\(NSCoder.string(for: normalizedCGPoint(point: touchInView)))"
                //   self.sendText(string: message)
                
                // Keeping buffer in local until
                bufferLocalScreenDrawing.append(touchInView)
                
                // Send 10 Objects and empty buffer
                if bufferLocalScreenDrawing.count == 10 {
                    do  {
                        let dictRequest = [Constants.ChannelMessages.allPoints:self.bufferLocalScreenDrawing.map{NSCoder.string(for: normalizedCGPoint(point: $0))}]
                        let data = try JSONSerialization.data(withJSONObject: dictRequest, options: .prettyPrinted)
                        
                        //FIXME: =======  Changed Here ======
                        //self.sendData(data: data)
                        if let message = String(data:data,encoding: .utf8) {
                            let string =  MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                            self.sendText(string: string)
                            print("Message allpoints %@",string)
                            
                        }
                        
                        
                    } catch {
                        bufferLocalScreenDrawing.removeAll()
                        
                        print("Error\(error)")
                    }
                    bufferLocalScreenDrawing.removeAll()
                }
                autoreleasepool {
                    drawLine(fromPoint: screenDrawingLastPoint, toPoint: touches.first?.location(in: viewToDisplayAR) ?? CGPoint(x: 150, y: 150))
                }
                
                // Add Point to removePoints Array
                //   self.remotePoints.append(touchInView)
                
                //Keep Update Gloabl object for on screen drawing (Image render)
                
                screenDrawingLastPoint = touches.first?.location(in: viewToDisplayAR) ?? CGPoint(x: 150, y: 150)
                isPanningForScreenDrawing = true
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Touch Ended")
        //--------------------------
        if isFreezeModeOn == true {
            if self.selfSession.isSharingSession {
                if isTouchCancelCall { return }

                var obj = Points2D()
                obj.points2D = tempSenderBufferLocal
                obj.lineColor = tempSenderDrawLineColor
                if self.selfSession.isSharingSession {
                    tempSenderBufferLocalScreenDrawing = obj
                    self.selfSession.arUndoManager.addUndoableObject(obj)
                }
            }
            else {
                
                // Send pending Objects and empty buffer
                if !bufferLocalScreenDrawing.isEmpty {
                    do {
                        let dictRequest = ["all_points":self.bufferLocalScreenDrawing.map{NSCoder.string(for: normalizedCGPoint(point: $0))}]
                        let data = try JSONSerialization.data(withJSONObject: dictRequest, options: .prettyPrinted)
                        // self.sendData(data: data)
                        if let message = String(data:data,encoding: .utf8) {
                            let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                            self.sendText(string: string)
                            print("Message %@",message)
                        }
                    } catch {
                        bufferLocalScreenDrawing.removeAll()
                        print("Error\(error)")
                    }
                    bufferLocalScreenDrawing.removeAll()
                }
                
                // Bhavesh changes 19-feb-2020 [ touchInView ]
                guard let touchInView2 = touches.first?.location(in: viewToDisplayAR) else { return }
                
//                guard let touchInView = touches.first?.location(in: sceneView) else {
//                    return
//                }
                touchPoint = touchInView2
                let message = "\(Constants.ChannelMessages.touchEnd)-\(NSCoder.string(for: normalizedCGPoint(point: touchPoint)))"
                //   self.sendText(string: message)
                let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                self.sendText(string: string)
                print("Message %@",message)
            }
            return
        }
        
        // ---------------------------------
        
        guard self.isDrawingEnable else {
            return
        }
        
        if self.selfSession.isSharingSession {
            touchPoint = CGPoint.zero
            self.selfSession.strokes.last?.resetMemory()
            print("hanleTouchEnd ====== ")
        } else {
            
            // Send pending Objects and empty buffer
            if !bufferLocalScreenDrawing.isEmpty {
                do  {
                    let dictRequest = ["all_points":self.bufferLocalScreenDrawing.map{NSCoder.string(for: normalizedCGPoint(point: $0))}]
                    let data = try JSONSerialization.data(withJSONObject: dictRequest, options: .prettyPrinted)
                    // self.sendData(data: data)
                    if let message = String(data:data,encoding: .utf8) {
                        let string =  MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                        self.sendText(string: string)
                        print("Message %@",message)
                    }
                    
                    
                } catch {
                    bufferLocalScreenDrawing.removeAll()
                    
                    print("Error\(error)")
                }
                bufferLocalScreenDrawing.removeAll()
            }
            
            guard let touchInView = touches.first?.location(in: sceneView) else {
                return
            }
            touchPoint = touchInView
            
            let message = "\(Constants.ChannelMessages.touchEnd)-\(NSCoder.string(for: normalizedCGPoint(point: touchPoint)))"
            //     self.sendText(string: message)
            let string =  MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
            self.sendText(string: string)
            print("Message %@",message)
        }
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Touch cancelled")
//        self.tempSenderBufferLocalScreenDrawing?.points2D?.popLast()
////        self.tempSenderBufferLocalScreenDrawing?.lineColor?.popLast()
//        self.tempSenderBufferLocal.popLast()
//        self.tempSenderDrawLineColor.popLast()
        if isFreezeModeOn {
            if self.selfSession.isSharingSession {
                isTouchCancelCall = true
                self.tempSenderBufferLocal = self.tempSenderBufferLocal.filter{$0 != [touchBeginPoint]}
                self.tempSenderBufferLocalScreenDrawing?.points2D = self.tempSenderBufferLocal
                if self.tempSenderBufferLocal.count != self.tempSenderDrawLineColor.count {
                    self.tempSenderDrawLineColor.popLast()
                    self.tempSenderBufferLocalScreenDrawing?.lineColor?.popLast()
                }
                //                var obj = Points2D()
                //                obj.points2D = tempSenderBufferLocal
                //                obj.lineColor = tempSenderDrawLineColor
                //                tempSenderBufferLocalScreenDrawing = obj
                print("cancle points : \(String(describing: self.tempSenderBufferLocalScreenDrawing?.points2D?.count))")
                print("cancle lineColor : \(String(describing: self.tempSenderBufferLocalScreenDrawing?.lineColor?.count))")
                print("selfSession.arUndoManager : ", self.selfSession.arUndoManager.count())
            }
        }
    }
    func drawLine(fromPoint:CGPoint,toPoint:CGPoint) {
        guard isPanningForScreenDrawing else {
            return
        }
        
        UIGraphicsBeginImageContext(self.viewToDisplayAR.bounds.size)
        
        let context = UIGraphicsGetCurrentContext()
        //        context?.setStrokeColor(UIColor.white.cgColor)
        context?.setStrokeColor(senderLineColor.cgColor)
        bezierPath.move(to: fromPoint)
        bezierPath.addLine(to: toPoint)
        bezierPath.lineWidth = 10
        bezierPath.stroke()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let imageView = UIImageView(image: image)
        imageView.tag = 1699
        self.viewToDisplayAR.viewWithTag(1699)?.removeFromSuperview()
        imageView.frame = self.viewToDisplayAR.bounds
        print("TEST image view width \(self.viewToDisplayAR.bounds.width)")
        print("TEST image view height \(self.viewToDisplayAR.bounds.height)")

        self.viewToDisplayAR.addSubview(imageView)
        
    }
    
//    func normalizedCGPoint(point:CGPoint) -> CGPoint {
//        print(point.x)
//        print(point.y)
//        print(sceneView.frame.width)
//        print(sceneView.frame.height)
//
//        let scale = UIScreen.main.scale
//
//        return CGPoint(x: (point.x * scale) / (sceneView.frame.width * scale), y: (point.y * scale) / (sceneView.frame.height * scale))
//
////        return CGPoint(x: point.x / sceneView.frame.width, y: point.y / sceneView.frame.height)
//    }
    func normalizedCGPoint(point:CGPoint) -> CGPoint {
        let scale = UIScreen.main.scale

            // Always call at recceiver side
//        print(point.x)
//        print(point.y)
      
//        print(sen_w_h.width)
//        print(sen_w_h.height)
//        if(isFreezeModeOn){
//            let persentageWidth = (point.x * scale) / (UIScreen.main.bounds.width * scale)
//            let persentageHeight = (point.y * scale) / (UIScreen.main.bounds.height * scale)
//
//            let finalX = sen_w_h.width * persentageWidth
//            let finalY = sen_w_h.height * persentageHeight
//            return CGPoint(x: finalX, y: finalY)
//
////            let persentageWidth = (point.x * scale) * sen_w_h.width
////            let persentageHeight = (point.y * scale) * sen_w_h.height
////
////            let finalX = persentageWidth /  (UIScreen.main.bounds.width * scale)
////            let finalY = persentageHeight / (UIScreen.main.bounds.height * scale)
////            return CGPoint(x: finalX, y: finalY)
//
//        }else{
        print(self.viewToDisplayAR.frame.width)
        print(self.viewToDisplayAR.frame.height)
        
        print(AppGlobalManager.sharedInstance.extraPaddingX)
        print(AppGlobalManager.sharedInstance.extraPaddingY)
//        if selfSession.isSharingSession == false {
//
//            // Current Device = 375, 667
//
//            // viewToDisplayAR : (-176.37722126086783, -454.4154764368658, 727.7544425217357, 1575.8309528737318)
//            // viewToDisplayAR : (-177.18567238074053, -456.1660425950969, 729.3713447614809, 1579.3320851901935)
//
//            // viewToDisplayAR : (-0.10838470472026529, -72.73468901395427, 375.21676940944053, 812.4693780279085)
//
//            //        Y = 1575  :  50                     X = 727  :  50
//            //            812   :   ?                         375  :   ?
//            //
//            //        (812 * 50)/ 1575 = 25.78            (375 * 50) / 727 = 25.79
//
////            var newX = point.x
////            var newY = point.y
////            print("test HEIGHT \(AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide().height)")
////            print("test width \(AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide().width)")
////            let testPaddingX = abs(AppGlobalManager.sharedInstance.extraPaddingX)
////             let testPaddingY = abs(AppGlobalManager.sharedInstance.extraPaddingY)
////
////            let testPaddingXRRR = abs(AppGlobalManager.sharedInstance.extraPaddingXTest)
////             let testPaddingYRRR = abs(AppGlobalManager.sharedInstance.extraPaddingYTest)
////
////            let newPaddingX = testPaddingXRRR - testPaddingX
////            let newpaddingY = testPaddingYRRR - testPaddingY
////
////
////            let finalPaddingX = (viewToDisplayAR.frame.width - AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide().width) / 2
////            let finalPaddingY = (viewToDisplayAR.frame.height - AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide().height) / 2
////
////            if (AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide().height < viewToDisplayAR.frame.height) && (AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide().width < viewToDisplayAR.frame.width) {
////                newY = (AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide().height * point.y) / viewToDisplayAR.frame.height
////                newX = (AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide().width * point.x) / viewToDisplayAR.frame.width
////
////                return CGPoint(x: ((newX + finalPaddingX)) / (viewToDisplayAR.frame.width), y: ((newY + finalPaddingY)) / (viewToDisplayAR.frame.height))
////
////            }
//
////            if AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide().width < viewToDisplayAR.frame.width {
////                newX = (AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide().width * point.x) / viewToDisplayAR.frame.width
////
////
////                return CGPoint(x: ((newX + finalPaddingX)) / (viewToDisplayAR.frame.width), y: ((newY + finalPaddingY)) / (viewToDisplayAR.frame.height))
////
////           }
////
//                return CGPoint(x: (newX) / (viewToDisplayAR.frame.width), y: (newY) / (viewToDisplayAR.frame.height))
//
//
//        }
        if(isFreezeModeOn){
            return CGPoint(x: ((point.x) * scale) / (viewToDisplayAR.frame.width * scale), y: ((point.y) * scale) / (viewToDisplayAR.frame.height * scale))

        }
        return CGPoint(x: ((point.x + AppGlobalManager.sharedInstance.extraPaddingX) * scale) / (viewToDisplayAR.frame.width * scale), y: ((point.y + AppGlobalManager.sharedInstance.extraPaddingY) * scale) / (viewToDisplayAR.frame.height * scale))

//        }
        
        
    }
    
    func draw3DStrokeFromRemotePoints(allPoints:[CGPoint],stroke:Stroke) {
        //  let stroke = Stroke()
        print("Touch")
        let position = allPoints.first ?? CGPoint.zero
        
        if let anchor = makeAnchor(at:position) {
            stroke.anchor = anchor
            stroke.points.append(SCNVector3Zero)
            stroke.touchStart = position
            stroke.lineWidth = receiverLineSize.rawValue
            stroke.mLineWidth = receiverLineSize.rawValue
            print(strokeSize.rawValue)
            let node = SCNNode()
            node.simdTransform = anchor.transform
            stroke.node = node
            
            self.sceneView.scene.rootNode.addChildNode(node)
        }
        
        for position in allPoints.dropFirst() {
            //    let  position =   NSCoder.cgPoint(for: positionStr)
            //  let point = CGPoint(x: position.x * self.sceneView.frame.width, y: position.y * sceneView.frame.height)
            
            guard  let _ = stroke.points.last, let strokeNode = stroke.node else {
                continue
            }
            let offset = unprojectedPosition(for: stroke, at: position)
            let newPoint = strokeNode.convertPosition(offset, from: sceneView.scene.rootNode)
            
            stroke.lineWidth = receiverLineSize.rawValue
            stroke.mLineWidth = receiverLineSize.rawValue
            print(receiverLineSize.rawValue)
            if (stroke.add(point: newPoint)) {
                //  pairingManager?.updateStroke(stroke)
                //   updateGeometry(remoteStroke)
            }
        }
        
        stroke.prepareLine()
        updateGeometry(stroke)
        
        stroke.resetMemory()
        
        // drawing finished send message
        
        self.sendText(string: Constants.ChannelMessages.drawingFinshed)
        
    }
    
    func drawImage(textAnnotationFromFreezeMode: Bool = false) {
        var img: UIImage = imgFreeze
        let allReceiverSession = activeSessions.filter{$0.userID != self.selfSession.userID}
        if tempSenderAryAnnotationData?.count != 0 {
            tempSenderAryAnnotationData?.forEach { (line) in
                //        imgViewScreenshot.image = imgViewScreenshot.image?.mergeImageWith(topImage: line.image ?? UIImage(), position: line.points ?? CGPoint())
                //                img = img.mergeImageWith(topImage: line.image ?? UIImage(), position: line.points ?? CGPoint())
                
                if let imageType = line.image?.imageAsset?.value(forKey: "_assetName") as? String {
                    if imageType == "TextBox_F" {
                        img = img.mergeImageWith(topImage: line.image ?? UIImage(), position: line.points ?? CGPoint(), textAnnotationFromFreezeMode: true)
                    }
                    else {
                        img = img.mergeImageWith(topImage: line.image ?? UIImage(), position: line.points ?? CGPoint(), textAnnotationFromFreezeMode: false)
                    }
                }
                else {
                    img = img.mergeImageWith(topImage: line.image ?? UIImage(), position: line.points ?? CGPoint(), textAnnotationFromFreezeMode: false)
                }
            }
        }
        if allReceiverSession.count != 0 {
            for (_,item) in allReceiverSession.enumerated() {
                if item.tempSenderAryAnnotationData?.count != 0 {
                    item.tempSenderAryAnnotationData?.forEach { (line) in
//                        img = img.mergeImageWith(topImage: line.image ?? UIImage(), position: line.points ?? CGPoint())
                        
                        if let imageType = line.image?.imageAsset?.value(forKey: "_assetName") as? String {
                            if imageType == "TextBox_F" {
                                img = img.mergeImageWith(topImage: line.image ?? UIImage(), position: line.points ?? CGPoint(), textAnnotationFromFreezeMode: true)
                            }
                            else {
                                img = img.mergeImageWith(topImage: line.image ?? UIImage(), position: line.points ?? CGPoint(), textAnnotationFromFreezeMode: false)
                            }
                        }
                        else {
                                img = img.mergeImageWith(topImage: line.image ?? UIImage(), position: convertPixelImage(touchPoints:line.points ?? CGPoint() ), textAnnotationFromFreezeMode: false)
                        }
                        
                        
//                        img = img.mergeImageWith(topImage: line.image ?? UIImage(), position: convertPixelImage(touchPoints:line.points ?? CGPoint() ), textAnnotationFromFreezeMode: textAnnotationFromFreezeMode)
                    }
                }
            }
        }
        //    imgViewScreenshot.image = drawLineOnImage(image: imgFreeze)
        DispatchQueue.main.async {
            self.imgViewScreenshot.image = self.drawLineOnImage(image: img, allReceiverSession: allReceiverSession)
        }
    }
    
    func drawLineOnImage(image: UIImage, allReceiverSession: [ARDrawingSession]?) -> UIImage {
//        UIGraphicsBeginImageContext(imgViewScreenshot.frame.size)
        UIGraphicsBeginImageContext(imgViewScreenshot.image!.size)
        
//        let w = AppGlobalManager.sharedInstance.WidthPoints ?? imgViewScreenshot.frame.size.width
//        let h = AppGlobalManager.sharedInstance.HeightPoints ?? imgViewScreenshot.frame.size.height
//        UIGraphicsBeginImageContext(CGSize(width: w, height: h))
        
        image.draw(at: CGPoint.zero)
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(5.0)
        //        context?.setStrokeColor(UIColor.white.cgColor)
        var myLineCount = 0
        
        if tempSenderBufferLocalScreenDrawing?.points2D?.count != 0 {
            tempSenderBufferLocalScreenDrawing?.points2D?.forEach { (line) in
                
                for (i, p) in line.enumerated() {
                    if i == 0 {
                        let currentColor = tempSenderBufferLocalScreenDrawing?.lineColor?[myLineCount].first ?? UIColor.white
                        context?.setStrokeColor(currentColor.cgColor)
                        context?.move(to: convertPixelImage(touchPoints: p))
                        myLineCount += 1
                    } else {
                        context?.addLine(to: convertPixelImage(touchPoints: p))
                    }
                }
                context?.strokePath()
            }
        }
        
        if allReceiverSession != nil {
            if allReceiverSession?.count != 0 {
                for (_,item) in (allReceiverSession ?? []).enumerated() {
                    var otherLineCount = 0
                    if item.tempSenderBufferLocalScreenDrawing?.points2D?.count != 0 {
                        item.tempSenderBufferLocalScreenDrawing?.points2D?.forEach { (line) in
                            let currentColor = item.tempSenderBufferLocalScreenDrawing?.lineColor?[otherLineCount].first ?? UIColor.white
                            
                            for (i, p) in line.enumerated() {
                                if i == 0 {
                                    context?.setStrokeColor(currentColor.cgColor)
                                    context?.move(to: convertPixelImage(touchPoints: p))
                                    otherLineCount += 1
                                } else {
                                    context?.addLine(to: convertPixelImage(touchPoints: p))
                                }
                            }
                            context?.strokePath()
                        }
                    }
                }
            }
        }
        
        context?.strokePath()
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resultImage ?? image
    }
    
    func convertPixelImage(touchPoints: CGPoint) -> CGPoint {
        let imageSize = imgViewScreenshot.image?.size
        let screenSize = self.view.frame.size
        let newX = (touchPoints.x * imageSize!.width) / screenSize.width
        let newY = (touchPoints.y * imageSize!.height) / screenSize.height
        return CGPoint(x: newX, y: newY)
    }
    //not in use
    func convertPixelImageReceiver(touchPoints: CGPoint) -> CGPoint {
        let imageSize = viewToDisplayAR.frame.size
        let screenSize = self.view.frame.size
        let newX = (touchPoints.x * imageSize.width) / screenSize.width
        let newY = (touchPoints.y * imageSize.height) / screenSize.height
        return CGPoint(x: newX, y: newY)
    }
}



//--------------------------------------------------------------------------------

//MARK:-  Screen Sharing Setup Methdos

//--------------------------------------------------------------------------------

extension ARController {
    //FIXME: For future use
    func resolveScreenSharing(userId:UInt) {
        let alertController = UIAlertController.init(title: "Choose Screen to share",
                                                     message: "Who want to share screen ?",
                                                     preferredStyle: UIAlertController.Style.alert)
        
        let me = UIAlertAction.init(title: "Share My Screen", style: UIAlertAction.Style.default, handler: { (action) in
            let message = "\(Constants.ChannelMessages.screenShareME)-\(NSCoder.string(for: CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)))"
            let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
            self.sendText(string: string)
            
//            self.sendText(string: "SCREEN_SHARE:ME")
            self.selfSession.isSharingSession = true
            self.isSharingMyScreen = true
            self.sceneView.isHidden = false
            self.displayLink?.invalidate()
            self.sceneView.delegate = nil
            self.sceneView.session.delegate = nil
            self.setupARView()
            self.call.outgoing = true
            self.toggleARViewForScreenSharingType()
            
            self.canStartRecording = true
            AppGlobalManager.sharedInstance.isScreenShareME = true
            
            self.convertViewBasedOnAspectRatio()

         
        })
        let his = UIAlertAction.init(title: "Share Other person screen", style: UIAlertAction.Style.default, handler: { [self] (action) in
            self.sendText(string: "SCREEN_SHARE:HIS")

            self.canStartRecording = true

            self.isSharingMyScreen = false
            self.sceneView.session.pause()
            self.sceneView.isHidden = true
            // sceneView.removeFromSuperview()
            self.displayLink?.invalidate()
             self.sceneView.delegate = nil
            self.toggleARViewForScreenSharingType()
            
//            let scale = UIScreen.main.scale

//            let messageTRY = "{\(self.viewToDisplayAR.frame.width),\(self.viewToDisplayAR.frame.height)}"
//            let message = "\(Constants.ChannelMessages.screensizeshareReceiverFreezeMode)-\(messageTRY)"
//            let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
//            self.sendText(string: string)
            
            if(isFromNonARDevice){
                let yesAction = UIAlertAction(title: "Ok", style: .default) { [self] (action) in
                   }
                   self.showAlert(withMessage: "Caller's device is not  AR Supported. You can draw only in freeze mode.", customActions: [yesAction])
            }
            //   self.setupViewForScreenShare(addSharingView: true,uid: userId)
            
        })
        
        alertController.addAction(me)
        alertController.addAction(his)
        
        self.present(alertController, animated: true) {
            // self.room = nil
            //self.setNeedsUpdateOfHomeIndicatorAutoHidden()
        }
    }
}

extension UIImage {
    func mergeImageWith(topImage: UIImage, position: CGPoint, textAnnotationFromFreezeMode: Bool = false) -> UIImage {
        let bottomImage = self
        UIGraphicsBeginImageContext(size)
        let areaSize = CGRect(x: 0, y: 0, width: bottomImage.size.width, height: bottomImage.size.height)
        bottomImage.draw(in: areaSize)
        if textAnnotationFromFreezeMode {
            topImage.draw(in: CGRect(x: position.x-50, y: position.y-80, width: 100, height: 80), blendMode: .normal, alpha: 1.0)
        } else {
            topImage.draw(in: CGRect(x: position.x-25, y: position.y-25, width: 50, height: 50), blendMode: .normal, alpha: 1.0)
        }
//        topImage.draw(in: CGRect(x: position.x-25, y: position.y-25, width: 50, height: 50), blendMode: .normal, alpha: 1.0)
        let mergedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return mergedImage
    }
    func convertImageToBase64String (img: UIImage) -> String {
        return img.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
    }
    func mergeImageWithForFreezeModeTextAnnotation(topImage: UIImage, position: CGPoint, sz: CGSize) -> UIImage {
            let bottomImage = self
            UIGraphicsBeginImageContext(size)
            let areaSize = CGRect(x: 0, y: 0, width: bottomImage.size.width, height: bottomImage.size.height)
            bottomImage.draw(in: areaSize)
            topImage.draw(in: CGRect(x: position.x-25, y: position.y-25, width: sz.width, height: sz.height), blendMode: .normal, alpha: 1.0)
            let mergedImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            return mergedImage
        }
}
extension UIView {

    func takeScreenshot() -> UIImage {

        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)

        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)

        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}

