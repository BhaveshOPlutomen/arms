//
//  AgoraUploader.swift
//  Agora-Screen-Sharing-iOS-Broadcast
//
//  Created by GongYuhua on 2017/1/16.
//  Copyright © 2017年 Agora. All rights reserved.
//

import Foundation
import CoreMedia
import ReplayKit
import AgoraRtcKit



class ARVideoSource: NSObject, AgoraVideoSourceProtocol {
    var consumer: AgoraVideoFrameConsumer?
    
    func shouldInitialize() -> Bool { return true }
    
    func shouldStart() { }
    
    func shouldStop() { }
    
    func shouldDispose() { }
    
    func bufferType() -> AgoraVideoBufferType {
        return .pixelBuffer
    }
    
    func sendBuffer(_ buffer: CVPixelBuffer, timestamp: TimeInterval) {
        let time = CMTime(seconds: timestamp, preferredTimescale: 1000)
        consumer?.consumePixelBuffer(buffer, withTimestamp: time, rotation: .rotationNone)
    }
}

/*
class DelegateHandler:NSObject, AgoraRtcEngineDelegate {
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        print(#function)

    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        print(#function)
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, streamUnpublishedWithUrl url: String) {
        print(#function)

    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurWarning warningCode: AgoraWarningCode) {
        print(#function)
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurStreamMessageErrorFromUid uid: UInt, streamId: Int, error: Int, missed: Int, cached: Int) {
        print(#function)
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, receiveStreamMessageFromUid uid: UInt, streamId: Int, data: Data) {
        print("STREAM MESSAGE RECIEED")
        print(String(data: data, encoding: .utf8) ?? "OPPS")
    }
    
    
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, streamPublishedWithUrl url: String, errorCode: AgoraErrorCode) {
        print("Published")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, streamInjectedStatusOfUrl url: String, uid: UInt, status: AgoraInjectStreamStatus) {
        print("Stream INJECTION")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        print("ERROR OCCURED",errorCode.rawValue)
    }
    
    
    
}*/

/*class AgoraUploader {
    
    static let delegateHandler = DelegateHandler()
    static var videoSource = ARVideoSource()
    static var dataStreamID:Int = -1
    
    private static let videoDimension : CGSize = {
        let screenSize = UIScreen.main.currentMode!.size
        var boundingSize = CGSize(width: 720, height: 1280)
        let mW = boundingSize.width / screenSize.width
        let mH = boundingSize.height / screenSize.height
        if( mH < mW ) {
            boundingSize.width = boundingSize.height / screenSize.height * screenSize.width
        }
        else if( mW < mH ) {
            boundingSize.height = boundingSize.width / screenSize.width * screenSize.height
        }
        return boundingSize
    }()
    
    static let sharedAgoraEngine: AgoraRtcEngineKit = {
        let kit = AgoraRtcEngineKit.sharedEngine(withAppId: KeyCenter.AppId, delegate: delegateHandler)
        kit.setChannelProfile(.liveBroadcasting)
        kit.setClientRole(.broadcaster)
        
        kit.enableVideo()
        kit.setVideoSource(videoSource)
        kit.enableDualStreamMode(true)
        kit.setDefaultAudioRouteToSpeakerphone(true)

      //  kit.setExternalVideoSource(true, useTexture: true, pushMode: true)
        let videoConfig = AgoraVideoEncoderConfiguration(size: videoDimension,
                                                         frameRate: .fps24,
                                                         bitrate: AgoraVideoBitrateStandard,
                                                         orientationMode: .adaptative)
        kit.setVideoEncoderConfiguration(videoConfig)
        
     //   AgoraAudioProcessing.registerAudioPreprocessing(kit)
      //  kit.setRecordingAudioFrameParametersWithSampleRate(44100, channel: 1, mode: .readWrite, samplesPerCall: 1024)
     //   kit.setParameters("{\"che.audio.external_device\":true}")

        
        kit.enableAudio()
      
//        kit.muteAllRemoteVideoStreams(true)
//        kit.muteAllRemoteAudioStreams(true)
        
        return kit
    }()
    
    static func startBroadcast(to channel: String) {
             
        let code =  sharedAgoraEngine.joinChannel(byToken: nil, channelId: channel, info: nil, uid: 0) { (string, intvalue, intValue2) in
            print("Test")
            print(intvalue)
        }

        
        print(code)
        
        sharedAgoraEngine.createDataStream(&dataStreamID, reliable: true, ordered: true)
        sharedAgoraEngine.setEnableSpeakerphone(true)
       
    }
    
    static func sendVideoBuffer(_ sampleBuffer: CMSampleBuffer) {
        guard let videoFrame = CMSampleBufferGetImageBuffer(sampleBuffer)
             else {
            return
        }
        
        var rotation : Int32 = 0
//        CMGetAttachment(sampleBuffer, key: <#T##CFString#>, attachmentModeOut: <#T##UnsafeMutablePointer<CMAttachmentMode>?#>)
//        if let orientationAttachment = CMGetAttachment(sampleBuffer, RPVideoSampleOrientationKey as CFString, nil) as? NSNumber {
//            if let orientation = CGImagePropertyOrientation(rawValue: orientationAttachment.uint32Value) {
//                switch orientation {
//                case .up:
//                    fallthrough
//                case .upMirrored:
//                    rotation = 0
//
//                case .down:
//                    fallthrough
//                case .downMirrored:
//                    rotation = 180
//
//                case .left:
//                    fallthrough
//                case .leftMirrored:
//                    rotation = 90
//
//                case .right:
//                    fallthrough
//                case .rightMirrored:
//                    rotation = 270
//                }
//            }
//        }
        
        let time = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
        
        let frame = AgoraVideoFrame()
        frame.format = 12
        frame.time = time
        frame.textureBuf = videoFrame
        frame.rotation = rotation

        sharedAgoraEngine.pushExternalVideoFrame(frame)
    }
    
    static func sendVideoBufferPixel(_ sampleBuffer: CVPixelBuffer,timeStemp:TimeInterval) {
     
        videoSource.sendBuffer(sampleBuffer, timestamp: timeStemp)
        
        
        
    }
    
    static func sendText(string:String) {
      
        sharedAgoraEngine.sendStreamMessage(dataStreamID, data: string.data(using: .utf8)!)
            
        
    }
    
    
    
    static func stopBroadcast() {
        sharedAgoraEngine.leaveChannel(nil)
    }
}
*/
