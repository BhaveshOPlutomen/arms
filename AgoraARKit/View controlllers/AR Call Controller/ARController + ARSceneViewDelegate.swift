// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import ARKit

extension ARController {
    enum AnnotaionType:String,CaseIterable {
        case anti_clockwise = "AntiClockwise"
        case clock_wise = "Clockwise_Green"
        case tick       = "Tick"
        case cross      = "cross"
        case up         = "UP_Green"
        case down       = "Down"
        case left       = "Left_Green"
        case right      = "Right"
        case inward     = "Inward_Green"
        case outward    = "Outward"
        case zero       = "0"
        case one        = "1"
        case two        = "2"
        case three      = "3"
        case four       = "4"
        case five       = "5"
        case six        = "6"
        case seven      = "7"
        case eight      = "8"
        case nine       = "9"
        case zero_nine  = "09"
        case on         = "ON"
        case off        = "OFF"
        case PlugIn     = "PlugIn"
        case Plugout    = "Plugout"
        case TextBox  = "TextBox"
//        case Rods_Rods = "Rods_Rods"
//        case Rods_Rods_test = "Rods_Rods_test"

        var image:UIImage? {
            return UIImage(named: self.rawValue)
        }
        var freezeModeImage: UIImage? {
          return UIImage(named: self.rawValue+"_F")
        }
        
        var index:Int? {
            return AnnotaionType.allCases.firstIndex(of: self)
        }
        
        func loadAnnotation() -> SCNNode? {
            switch self{
                
            case .anti_clockwise,
                 .clock_wise,
                 .tick,
                 .cross,
                 .up,
                 .down,
                 .left,
                 .right,
                 .inward,
                 .outward,
                 .zero,
                 .one,
                 .two,
                 .three,
                 .four,
                 .five,
                 .six,
                 .seven,
                 .eight,
                 .nine,
                 .TextBox:
//                 .Rods_Rods:
//                 .Rods_Rods_test:

                guard let scene =  SCNScene(named: "ARAssest.scnassets/Annotations/\(self.rawValue)/\(self.rawValue).scn") else {print("failed to load the scene"); return nil}
                
                return scene.rootNode
                
            case .zero_nine,
                 .on,
                 .off,
                 .PlugIn,
                 .Plugout:
                
                guard let scene =  SCNScene(named: "ARAssest.scnassets/Annotations/commons/commons.scn") else {print("failed to load the scene"); return nil}
                
                let node =   scene.rootNode.childNodes.first
                node?.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "ARAssest.scnassets/Annotations/commons/\(self.rawValue).jpg")
                return node
            }
        }
    }
}


extension ARController: ARSCNViewDelegate, ARSessionDelegate {
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        node.simdTransform = anchor.transform
        
        if let stroke = getStroke(for: anchor) {
            print ("did add: \(node.position)")
            print ("stroke first position: \(stroke.points[0])")
            stroke.node = node
            
            DispatchQueue.main.async {
                self.sliderZoom.isHidden = true
                self.updateGeometry(stroke)
            }
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if let stroke = getStroke(for: anchor) {
            //            print("Renderer did update node transform: \(node.transform) and anchorTranform: \(anchor.transform)")
            stroke.node = node
            
            //Added this line
            self.updateGeometry(stroke)
            //            if ((stateManager?.state == .HOST_CONNECTING || stateManager?.state == .SYNCED || stateManager?.state == .FINISHED)
            //                        && strokes.contains(stroke)) {
            //
            //                pairingManager?.updateStroke(stroke)
            //
            //                DispatchQueue.main.async {
            //                    self.updateGeometry(stroke)
            //                }
            //            }
        }
        
    }
    
    
    
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        if let stroke = getStroke(for: node) {
            
            if (self.selfSession.strokes.contains(stroke)) {
                if let index = self.selfSession.strokes.firstIndex(of: stroke) {
                    self.selfSession.strokes.remove(at: index)
                }
            } else {
                for session in self.activeSessions {
                    if let index = session.strokes.firstIndex(of: stroke) {
                        session.strokes.remove(at: index)
                    }
                }
            }
            stroke.cleanup()
            
            //            DispatchQueue.main.async {
            //                self.uiViewController?.undoButton.isHidden = self.shouldHideUndoButton()
            //                self.uiViewController?.clearAllButton.isHidden = self.shouldHideTrashButton()
            //                if (self.mode == .DRAW && self.strokes.count == 0) { self.uiViewController?.showDrawingPrompt() }
            //                UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, nil)
            //            }
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        if (touchPoint != .zero) {
            if let stroke = self.selfSession.strokes.last {
                DispatchQueue.main.async {
                    self.updateLine(for: stroke)
                }
            }
        }
        //        //ADDED TESTING PENDING
        //        if let remoteStroke =  remoteStroke, remoteStroke.updateAnimatedStroke() {
        //            DispatchQueue.main.async {
        //                self.updateGeometry(remoteStroke)
        //            }
        //        }
        
        
        //  DispatchQueue.main.async {
        //    self.btnUndo.isHidden = !self.selfSession.arUndoManager.hasObjectToUndo
        //     self.btnTrash.isHidden = !self.selfSession.arUndoManager.hasObjectToUndo
        
        
        //            let hitTest = self.sceneView.hitTest(self.view.center, types: [.featurePoint,.existingPlane,.estimatedVerticalPlane,.estimatedHorizontalPlane])
        //
        //            if let result = hitTest.last  {
        //                let position = result.worldTransform.columns.3
        //
        //
        //                let positionConverted = self.sceneView.pointOfView?.convertPosition(SCNVector3(position.x, position.y, position.z), from: self.sceneView.scene.rootNode)
        //
        //
        //                self.hitNode?.position.z = positionConverted?.z ?? -0.1
        //
        //
        //
        //            }
        // }
        
        
        
        
        
        //        for (_, stroke) in partnerStrokes {
        //            let needsUpdate = stroke.updateAnimatedStroke()
        //            if needsUpdate {
        //                DispatchQueue.main.async {
        //                    self.updateGeometry(stroke)
        //                }
        //            }
        //        }
        
    }
    
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        switch camera.trackingState {
        case .notAvailable:
            print("No tracking")
            if shouldShowTrackingIndicator() {
                enterTrackingState()
            }
            
        case .limited(let reason):
            print("Limited tracking")
            if shouldShowTrackingIndicator() {
                if reason == .relocalizing {
                    NSLog("Relocalizing...")
                    self.configureARSession(runOptions: [.resetTracking, .removeExistingAnchors])
                    
                    //                    // while relocalizing after interruption, only attempt for 5 seconds, then reset, and only when not paired
                    //                    if self.selfSessionstrokes.count > 0 && resumeFromInterruptionTimer == nil && pairingManager?.isPairingOrPaired == false {
                    //                        resumeFromInterruptionTimer = Timer(timeInterval: 5, repeats: false, block: { (timer) in
                    //                            NSLog("Resetting ARSession because relocalizing took too long")
                    //                            DispatchQueue.main.async {
                    //                                self.resumeFromInterruptionTimer?.invalidate()
                    //                                self.resumeFromInterruptionTimer = nil
                    //                                self.configureARSession(runOptions: [ARSession.RunOptions.resetTracking])
                    //                            }
                    //                        })
                    //                        RunLoop.main.add(resumeFromInterruptionTimer!, forMode: RunLoopMode.defaultRunLoopMode)
                    //                    } else { // if strokes.count == 0 {
                    //                        // only do the timer if user has drawn strokes
                    //                        self.configureARSession(runOptions: [.resetTracking, .removeExistingAnchors])
                    //                    }
                }
                else if reason == .excessiveMotion{
                    print("excessiveMotion.....")
                }else if reason == .insufficientFeatures{
                    print("insufficientFeatures...")
                }
                enterTrackingState()
            }
            
       
        case .normal:
            //            if (!hasInitialTracking) {
            //                hasInitialTracking = true
            //                Analytics.setUserProperty(AnalyticsKey.val(.value_true), forName: AnalyticsKey.val(.tracking_has_established))
            //            }
            if !shouldShowTrackingIndicator() {
                exitTrackingState()
            }
            //            if let pairingMgr = pairingManager, pairingMgr.isPairingOrPaired, shouldRetryAnchorResolve {
            //                pairingManager?.retryResolvingAnchor()
            //                pairingManager?.stopObservingLines()
            //            }
        }
    }
    
    /// Hold onto tracking mode exiting (unless it is already .TRACKING) enter .TRACKING and start animation
    func enterTrackingState() {
        print("ViewController: enterTrackingState")
        resetTouches()
        
//        self.showToastBottom("Looking ....", type: .info)
        
        
        //        trackingMessage = .looking
        //
        //        if let pairingMgr = pairingManager, pairingMgr.isPairingOrPaired == true, (mode == .DRAW || (mode == .TRACKING && modeBeforeTracking == .DRAW)) {
        //            trackingMessage = .anchorLost
        //        } else if trackingMessageTimer == nil {
        //            trackingMessage = .looking
        //
        //            trackingMessageTimer = Timer(timeInterval: 3, repeats: false, block: { (timer) in
        //                self.trackingMessage = .lookingEscalated
        //
        //                // need to set mode again to update tracking message
        //                self.mode = .TRACKING
        //
        //                self.trackingMessageTimer?.invalidate()
        //                self.trackingMessageTimer = nil
        //            })
        //            RunLoop.main.add(trackingMessageTimer!, forMode: RunLoopMode.defaultRunLoopMode)
        //        }
        //
        //        if mode != .TRACKING {
        //            print("Entering tracking with mode: \(mode)")
        //            modeBeforeTracking = mode
        //        }
        //        mode = .TRACKING
    }
    
    /// Clean up when returning to normal tracking
    func exitTrackingState() {
        print("ViewController: exitTrackingState")
        
//        self.showToastBottom("Tracking normal", type: .success)
        
        //        if resumeFromInterruptionTimer != nil { print("Relocalizing successful.") }
        //
        //        trackingMessageTimer?.invalidate()
        //        trackingMessageTimer = nil
        //
        //        resumeFromInterruptionTimer?.invalidate()
        //        resumeFromInterruptionTimer = nil
        //
        //        // Restore previous mode set in enterTrackingState and updated in mode changes
        //        if let previousMode = modeBeforeTracking {
        //            mode = previousMode
        //            modeBeforeTracking = nil
        //        }
    }
    
    /// In pair mode, only show tracking indicator in certain states
    func shouldShowTrackingIndicator()->Bool {
        let shouldShow = false
        if let trackingState = sceneView.session.currentFrame?.camera.trackingState {
            switch trackingState {
            case .limited:
                return true
                
                //                if let pairState = (UIApplication.shared.delegate as! AppDelegate).pairingState, (mode == .PAIR || modeBeforeTracking == .PAIR) {
                //                    shouldShow = StateManager.shouldShowTracking(for:pairState)
                //                } else {
                //                    shouldShow = true
            //                }
            default:
                break
            }
        }
        // when rejoining after background, continue to show tracking message even when no longer tracking until cloud anchor is re-resolved
        //if shouldRetryAnchorResolve { shouldShow = true }
        
        return shouldShow
    }
    
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        guard let arError = error as? ARError else { return }
        
        if !self.call.outgoing /*&& UIApplication.shared.applicationState != .background*/ {
            
            self.configureARSession(runOptions: [.resetTracking, .removeExistingAnchors])
            
            sceneView.delegate = self
            sceneView.preferredFramesPerSecond = Constants().preferredFramesPerSecond
            sceneView.contentScaleFactor = 1
//            sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints,
//                                         ARSCNDebugOptions.showWorldOrigin]
            
            let config = ARWorldTrackingConfiguration()
            sceneView.session.run(config, options: ARSession.RunOptions.removeExistingAnchors)
        }
        
        let nsError = error as NSError
        var sessionErrorMsg = "\(nsError.localizedDescription) \(nsError.localizedFailureReason ?? "")"
        if let recoveryOptions = nsError.localizedRecoveryOptions {
            for option in recoveryOptions {
                sessionErrorMsg.append("\(option).")
            }
        }
        
        let isRecoverable = (arError.code == .worldTrackingFailed)
        if isRecoverable {
            sessionErrorMsg += "\nYou can try resetting the session or quit the application."
        } else {
            sessionErrorMsg += "\nThis is an unrecoverable error that requires to quit the application."
        }
        
        if (arError.code == .cameraUnauthorized) {
            //  Analytics.logEvent(AnalyticsKey.val(.camera_permission_denied), parameters: nil)
            let alertController = UIAlertController(title: NSLocalizedString("error_resuming_session", comment: "Sorry something went wrong"), message: NSLocalizedString("error_camera_not_available", comment: "Sorry, something went wrong. Please try again."), preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("ok", comment: "OK"), style: .default) { (action) in
                alertController.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
        //        displayErrorMessage(title: "We're sorry!", message: sessionErrorMsg, allowRestart: isRecoverable)
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        //        if let _ = pairingManager?.garAnchor {
        //            shouldRetryAnchorResolve = true
        ////            sceneView.session.setWorldOrigin(relativeTransform: float4x4(SCNMatrix4Identity))
        //        }
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        NSLog("Session resuming after interruption")
        
        #if JOIN_GLOBAL_ROOM
        if pairingManager?.isPairingOrPaired == true {
            pairingManager?.resumeSession(fromDate: Date())
        }
        #else
        if let backgroundDate = UserDefaults.standard.object(forKey: DefaultsKeys.backgroundDate.rawValue) as? Date {
            if backgroundDate.addingTimeInterval(180).compare(Date()) == ComparisonResult.orderedAscending {
                // if it has been too long since last session, reset tracking and remove anchors
                configureARSession(runOptions: [.resetTracking, .removeExistingAnchors])
                //   self.pairCancelled()
            } else {
                //                if pairingManager?.isPairingOrPaired == true {
                //                    if let _ = pairingManager?.garAnchor {
                ////                        sceneView.session.setWorldOrigin(relativeTransform: garAnchor.transform)
                //                    }
                //
                //                    pairingManager?.resumeSession(fromDate: backgroundDate)
                //                }
            }
        }
        #endif
    }
    
    func sessionShouldAttemptRelocalization(_ session: ARSession) -> Bool {
        return true
    }
    
    
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
    
        guard let featurePoints = frame.rawFeaturePoints else { return }

    }
    
   
    func configureARSession(runOptions: ARSession.RunOptions = []) {
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal,.vertical]
        //        configuration.isAutoFocusEnabled = false
        
        #if DEBUG
//        sceneView.debugOptions = [ARSCNDebugOptions.showWorldOrigin]
        #endif
        
        // Run the view's session
        sceneView.session.run(configuration, options: runOptions)
        sceneView.session.delegate = self
        sceneView.preferredFramesPerSecond = Constants().preferredFramesPerSecond
        sceneView.contentScaleFactor = 1
//        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints,
//                                                ARSCNDebugOptions.showWorldOrigin]
        
    }
    
    func visualizeFeaturePointsIn(_ featurePointsArray: [vector_float3]){
           
           //1. Remove Any Existing Nodes
           sceneView.scene.rootNode.enumerateChildNodes { (featurePoint, _) in
               
               featurePoint.geometry = nil
               featurePoint.removeFromParentNode()
           }

    }

}
