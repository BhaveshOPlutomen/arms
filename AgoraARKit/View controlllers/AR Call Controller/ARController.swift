//
//  ARController.swift
//  AgoraARKit
//
//  Created by Prashant on 15/04/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit
import AgoraRtcKit
import ARKit
import NVActivityIndicatorView
import IQKeyboardManagerSwift


class ARController: BaseViewController,Alertable,Toastable,UIGestureRecognizerDelegate {
    
    //EXTRA
    var rec_w_h : CGSize = CGSize(width: 0, height: 0)
    var sen_w_h : CGSize = CGSize(width: 0, height: 0)

    //MARK:- MainView outlets

    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var sideStackView: UIStackView!
    @IBOutlet weak var viewToDisplayAR: UIView!
    
    //MARK:- Button Outlets

    @IBOutlet weak var btnTrash: UIButton!
    @IBOutlet weak var btnUndo: UIButton!
    @IBOutlet weak var btnMuteUnmute: UIButton!
    @IBOutlet weak var btnAnnotaion: UIButton!
    @IBOutlet weak var btnAddCall: UIButton!
    @IBOutlet weak var btn_feature_point: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnScreenShot: UIButton!
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var btnFlash: UIButton!
    
    @IBOutlet weak var btnMainLineWidth: UIButton!
    @IBOutlet weak var btnSmallLine: UIButton!
    @IBOutlet weak var btnMediumLine: UIButton!
    @IBOutlet weak var btnLargeLine: UIButton!
    @IBOutlet weak var btnFreezeMode: UIButton!
    @IBOutlet weak var btnLineColorChange: UIButton!
    @IBOutlet weak var btnAddText: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    
    //MARK:- View Outlets
    
    @IBOutlet weak var signalStrenghtView: SignalStrengthIndicator!
    @IBOutlet weak var view_featurePoints: UIView!
    @IBOutlet weak var view_timer: UIView!
    @IBOutlet weak var viewLineWidth: UIView!
    @IBOutlet weak var viewColorPicker: UIView!
    @IBOutlet weak var viewGreenOnChat: UIView!
    
    //MARK:- Imageview Outlets

    @IBOutlet weak var imgViewScreenshot: UIImageView!
    
    //MARK:- CollectionView Outlets

    @IBOutlet weak var collectionViewColorPicker: UICollectionView!
    @IBOutlet weak var colUserName: UICollectionView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    //MARK:- Textfield Outlets

    @IBOutlet weak var txtCustomText: UITextField!
    var toolBar = UIToolbar()
    
    //MARK:- Label Outlets

    @IBOutlet weak var lblNetwork: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
  
    //MARK:- Slider Outlets

    @IBOutlet weak var sliderZoom: UISlider!
    
    //MARK:- var SCNNode

    var selectedNode: SCNNode?
    var lastnode : SCNNode? = nil
    /// SCNNode floating in front of camera the distance drawing begins
    var hitNode: SCNNode?
    
    var tapLocation : CGPoint?
    
    var pinchGestureForFreezeMode = UIPinchGestureRecognizer()
    var panGestureForFreezeMode = UIPanGestureRecognizer()

    //MARK:- Text annotation variables

    var locationTextBox : CGPoint? = nil
    var locationTextBoxFreezeMode : CGPoint? = nil
    var TapTextBoxFreezeMode : CGPoint? = nil
    
    static let orientationPortrait                                     = "orientation_portrait"
    static let orientationLandscape                                    = "orientation_landscape"
    var myOrientation: NSLayoutConstraint.Axis = .vertical

    //MARK:- Testing receiver constraints
    var senderVideoSize = CGSize()
 
    
    @IBOutlet weak var viewtodisplayARWidth: NSLayoutConstraint!
    @IBOutlet weak var viewtodisplayARHeight: NSLayoutConstraint!

    @IBOutlet weak var imgScreenShotHeight: NSLayoutConstraint!
    @IBOutlet weak var imgScreenShotWidth: NSLayoutConstraint!

    var roomName:String!
    var call:Call! {
        didSet {
            if  selfSession != nil {
                selfSession.calls.appendNewCall(call: call)
            } else {
                selfSession = ARDrawingSession(userID: Int64(UInt.random(in: 1...15000)))
                selfSession.calls.appendNewCall(call: call)
            }
        }
    }
    var status:AppStateMachine  {
        get {
            return AppDelegate.shared.status
        } set {
            AppDelegate.shared.status = newValue
        }
    }
    var touchBeginPoint = CGPoint()
    var isTouchCancelCall : Bool = false
    
    var activeUserNames = [String]()
    
    var arrayUserNames = [[String:Any]]()
    var DictActiveUsernames = [String:Any]()
    
    var activeSessions = [ARDrawingSession]() // Newly added sessions
    lazy var selfSession: ARDrawingSession =  ARDrawingSession(userID: Int64(UInt.random(in: 1...15000))) // STEP1:  Current Active session , Current Session may be boradcaster or receiver, init on View DidLoad
    
    var rtcEngine: AgoraRtcEngineKit!
    //  private var agoraSignalManager:AgoraSignalManager? = AgoraSignalManager()
    private var rtmChannelManager:RTMChannelManager?
  
    /// Currently selected stroke size
    var strokeSize: Radius = .small
    
    var touchPoint: CGPoint = .zero
    
    var displayLink:CADisplayLink?
    
    /// array of strokes a user has drawn in current session
    // var strokes: [Stroke] = [Stroke]()
    /// Most situations we show the looking message, but when relocalizing and currently paired, show anchorLost type
    var trackingMessage: TrackingMessageType = .looking
    
    //    var audioBitrate : AgoraAudioSampleRateType = .type48000
    
    // remoteStroke is used to draw Line from point received by remote user i.e remotePoints
    var remoteStroke:Stroke? // UN - USED
    var remotePoints:[CGPoint] = []
    
    // Buffer local screen drawing is array which keeps data until limit and send to remote user
    var bufferLocalScreenDrawing:[CGPoint] = []
    
    //Local Drawing on Screen
    let bezierPath:UIBezierPath = {
        let   bezierPath = UIBezierPath()
        bezierPath.lineCapStyle = CGLineCap.round
        bezierPath.lineJoinStyle = CGLineJoin.round
        return bezierPath
    }()
    var screenDrawingLastPoint:CGPoint = .zero
    var isPanningForScreenDrawing = false
    
    var timer:Timer? = nil
    
    var networkTimer:Timer? = nil
        
    let viewLayouter = VideoViewLayouter()
    var fullSession: VideoSession? {
        didSet {
            if fullSession != oldValue && viewToDisplayAR != nil {
                updateInterface(withAnimation: true)
                
            }
        }
    }
    var videoSource = ARVideoSource()
    
    //   var currentUserID = UInt.random(in: 1...15000)
    
    //MARK:- Booleans

    //share my screen or share other's screen
    var isSharingMyScreen = false

    //mute unmute manage
    var isAudioMute:Bool = false
    
    //send jointime or leavetime in store call method
    var isJoinChannel:Bool = false
    
    var isContactVCOpen:Bool = false
    
    //for featured point show/hide
    var isSwitchOn : Bool = false
    
    // for recording
    var isRecordingStarted : Bool = false
    
    var canStartRecording : Bool = false
    
    var recordingSTART : Bool = false
    
    //for textbox annotation
    var isTextBoxAnnotation : Bool = false
    
    //for normal annotation
    var isAnnotationSelected : Bool = false
    
    //color picker is selected or not
    var isColorViewSelected : Bool = false
    
    var recordingOnCallEndSender : Bool = false
    
    var recordingOnCallEndReceiver : Bool = false
    
    var isLineWidthViewSelected : Bool = false
    
    var isStartingTextAnnotationonFreezmode = false

    var isFromNonARDevice : Bool = false
    
    var allColorAry: [UIColor] = Constants.AppTheme.LineColors.ColorsOfApp.allCases.map{$0.color}
    
    private lazy var stopwatch = Stopwatch(timeUpdated: { [weak self] timeInterval in
        guard let strongSelf = self else { return }
        strongSelf.lblTimer.text = strongSelf.timeString(from: timeInterval)
    })
    
    //*******************************************************
    // For Freeze Mode
    //*******************************************************
    var imgFreeze = UIImage()
    var isFreezeModeOn = false
    // Sender Side
    var tempSenderBufferLocal:[[CGPoint]] = []
    var tempSenderBufferLocalScreenDrawing: Undoable?
    var tempSenderAryAnnotationData:[Annotation2DData]? = []
    var tempSenderDrawLineColor:[[UIColor]] = []
    var senderLineColor = UIColor.white
    
    // Receiver Side
    var isFreezeModeOnFromOtherTemp = false
    var tempReceiverBufferLocalScreenDrawing:[[CGPoint]] = []
    var tempReceiverAryAnnotationData:[Annotation2DData]? = []
    var tempReceiverDrawLineColor:[[UIColor]] = []
    var receiverLineColor = UIColor.white
    var receiverLineSize = Radius.small

    
    var initialCenter = CGPoint()  // The initial center point of the view.

    struct CardInfo {
        
        enum CardState {
            case expanded
            case collapsed
        }
        enum CardType {
            case annotation
            case chat
        }
        var cardType:CardType
        
        var cardHandleHeight:CGFloat
        var cardState:CardState = .collapsed
        var nextState : CardState {
            return cardState == .expanded ?   .collapsed : .expanded
        }
        
        mutating func updateToNextState() {
            cardState = nextState
        }
    }
    
    var userInfoCallFrom : String!
    
    var userInforCallerName : String!
    
    class CardHandler {
        var cardInfo:CardInfo
        private(set) unowned var viewController : UIViewController
        var cardHeight:CGFloat {
            switch self.cardInfo.cardType {
            case .annotation:
                return UIDevice.current.userInterfaceIdiom == .pad  ? UIScreen.main.bounds.height * 0.4  : 300
            case .chat:
                return UIDevice.current.userInterfaceIdiom == .pad  ? UIScreen.main.bounds.height * 0.4  : 450
            }
        }
        
        var isExpanded:Bool {
            return cardInfo.cardState == .expanded
        }
        
        init (viewController:UIViewController,info:CardInfo) {
            self.viewController = viewController
            self.cardInfo = info
        }
    }
    
    lazy var annotationCard = CardHandler(viewController: AnnotaionListViewController.viewController(), info: CardInfo(cardType: .annotation, cardHandleHeight: 0))
    lazy var chatCard = CardHandler(viewController: ChatViewController.viewController(), info: CardInfo(cardType: .chat, cardHandleHeight: 0))
    
    // Animation
    var animations:[UIViewPropertyAnimator] = []
    // visual effects
    var visualEffectView: UIVisualEffectView!
    var isDrawingEnable = true
    
    var timerCount:Int = 0
    var networkTimerCount : Int = 30
    
    var connectionLostCount : Int = 40
    //--------------------------------------------------------------------------------
    
    //MARK:- Memory Managment Methods
    
    //--------------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Abstract Method
    
    //--------------------------------------------------------------------------------
    
    class func viewController () -> ARController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ARController") as! ARController
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- ViewLifeCycle Methods
    
    //--------------------------------------------------------------------------------
  

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.delegate = self
        sideStackView.addBackground(color: .black)

        sceneView.isMultipleTouchEnabled = false
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(sceneViewTapped(gesture:)))
        tapGesture.cancelsTouchesInView = true
        tapGesture.delaysTouchesBegan = true
        
        self.view.addGestureRecognizer(tapGesture)
        
        setupRTM()
        //loadAgoraKit()
//        setupARView()
        setupCardView()
        
        viewtodisplayARWidth.constant = UIScreen.main.bounds.width
        viewtodisplayARHeight.constant = UIScreen.main.bounds.height
        
        imgScreenShotWidth.constant = UIScreen.main.bounds.width
        imgScreenShotHeight.constant = UIScreen.main.bounds.height
        
        convertViewBasedOnAspectRatio()
        
        isTextBoxAnnotation = false
        self.sliderZoom.isHidden = true
        NotificationCenter.default.removeObserver(self, name: .callInviteRejected, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.callInviteRejectedAction(_:)), name: .callInviteRejected, object: nil)
        
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: nil) { [weak self] (notification) in
            self?.touchPoint = .zero
        }
        viewGreenOnChat.isHidden = true
        imgViewScreenshot.isUserInteractionEnabled = true
        viewToDisplayAR.isUserInteractionEnabled = true
    }
     override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let width = self.colUserName.collectionViewLayout.collectionViewContentSize.width
        widthConstraint.constant = width
        
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.parent?.navigationController?.navigationBar.isHidden = true
        self.navigationController?.parent?.navigationController?.isNavigationBarHidden = true
        
        viewGreenOnChat.layer.cornerRadius = viewGreenOnChat.frame.size.height / 2
        viewGreenOnChat.layer.masksToBounds = true
        
//        viewtodisplayARWidth.constant = UIScreen.main.bounds.width
//        viewtodisplayARHeight.constant = UIScreen.main.bounds.height
        
//        viewToDisplayAR.frame = AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide()
//        imgViewScreenshot.frame = AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide()

//        if selfSession.isSharingSession == true{
//            //sender
//
//            imgViewScreenshot.center = self.view.center
//            imgViewScreenshot.frame.size.height = UIScreen.main.bounds.height
//            imgViewScreenshot.frame.size.width = UIScreen.main.bounds.width
//            imgScreenShotWidth.constant = UIScreen.main.bounds.width
//            imgScreenShotHeight.constant = UIScreen.main.bounds.height
//        }else{
//            //receiver
//            let rect = AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide()
//            viewToDisplayAR.center = self.view.center
//            viewToDisplayAR.frame.size.height = rect.height
//            viewToDisplayAR.frame.size.width = rect.width
//            viewtodisplayARWidth.constant = rect.width
//            viewtodisplayARHeight.constant = rect.height
//
//            imgViewScreenshot.center = self.view.center
//            imgViewScreenshot.frame.size.height = rect.height
//            imgViewScreenshot.frame.size.width = rect.width
//            imgScreenShotWidth.constant = rect.width
//            imgScreenShotHeight.constant = rect.height
//        }
       
        
        print("image view screenshot frame: \(imgViewScreenshot.frame)")
        print("viewtodisplayAR frame: \(viewToDisplayAR.frame)")
//        self.view.layoutIfNeeded()
    }
    //--------------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(endcallTimer), userInfo: nil, repeats: true)
        //        timer?.invalidate()
        //        timer = nil
        
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(saveVidoActionCallEnd), name: Notification.Name("saveVidoActionCallEnd"), object: nil)

        SyncAPI()
        setupARView()
        hideThingsAtFirstTime()
        
       
    }
    
//    func changeAspectRationAtReceiverSide(){
//        let receiverWidth = UIScreen.main.bounds.size.width
//        let receiverHeight = UIScreen.main.bounds.size.height
//        
//        var actualHeight : CGFloat = receiverHeight
//        var actualWidth : CGFloat = receiverWidth
////        DispatchQueue.main.async {
//
//        
//            if(self.senderVideoSize.width != 0.0){
//                if(self.senderVideoSize.width < receiverWidth){
//                    actualWidth = (self.senderVideoSize.width) / (self.senderVideoSize.height) * receiverHeight
////                    self.viewToDisplayAR.frame.size.width = actualWidth
//                    self.viewtodisplayARWidth.constant = actualWidth
//                    self.viewToDisplayAR.center = self.view.center
//                    self.viewToDisplayAR.layoutIfNeeded()
//
//                    print("Actual width : \(actualWidth)")
//                    self.ExtraPaddingX = (receiverWidth - actualWidth) / 2
//                    print("Extrapadding X : \(self.ExtraPaddingX)")
//                    
////                    self.selfSession.videoSession?.hostingView.frame = CGRect(x: self.ExtraPaddingX, y: 0.0, width: actualWidth, height: receiverHeight)
////                    print(self.selfSession.videoSession?.hostingView.frame)
////
////                    self.selfSession.videoSession?.receiverVideoSize = CGRect(x: self.ExtraPaddingX, y: 0.0, width: actualWidth, height: receiverHeight)
////                    print(self.selfSession.videoSession?.receiverVideoSize)
//                    print("TEST TEST")
//                    
//                }else{
//                    actualHeight = (self.senderVideoSize.height) / (self.senderVideoSize.width) * receiverWidth
////                    self.viewToDisplayAR.frame.size.height = actualHeight
//                    self.viewtodisplayARHeight.constant = actualHeight
//                    self.viewToDisplayAR.center = self.view.center
//                    self.viewToDisplayAR.layoutIfNeeded()
//
//                    print("Actual height : \(actualHeight)")
//
//                    self.ExtraPaddingY = (receiverHeight - actualHeight) / 2
//                    print("Extrapadding Y : \(self.ExtraPaddingY)")
////                    self.selfSession.videoSession?.receiverVideoSize = CGRect(x: 0.0, y: self.ExtraPaddingY, width: receiverHeight, height: actualHeight)
//
//                }
//            }
//
////        }
//    }
//    
    func hideThingsAtFirstTime(){
        view_featurePoints.clipsToBounds = true
        view_featurePoints.layer.cornerRadius = 14
        txtCustomText.isHidden = true
        self.btnUndo.isHidden = false
//        self.btnAddCall.isHidden = true
        self.viewColorPicker.isHidden = true
        self.viewLineWidth.isHidden = true
        self.viewLineWidth.clipsToBounds = true
        self.viewLineWidth.layer.cornerRadius = 10
        btnSmallLine.backgroundColor = UIColor.white
        btnMediumLine.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorLightGray.color
        btnLargeLine.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorLightGray.color
        
        if  !call.outgoing {
            self.btnTrash.isHidden = true
            self.btnFlash.isHidden = true
        }else{
            self.btnTrash.isHidden = false
            self.btnFlash.isHidden = false
        }
        view_timer.isHidden = true
        if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
            self.btnRecord.isHidden = true
            self.btnScreenShot.isHidden = true
        }else{
            if AppGlobalManager.sharedInstance.allow_rec_full == 1{
                self.btnRecord.isHidden = false
            }else{
                self.btnRecord.isHidden = true
            }
        }
    }
  
    //--------------------------------------------------------------------------------
    
    //MARK:-  Action Methdos
    
    //--------------------------------------------------    ------------------------------
    
//    @IBAction func btnrotate(_ sender: Any)
    
//    {
//        if call.outgoing == true {
//            if myOrientation == .vertical {
//                myOrientation = .horizontal
//                sideStackView.axis = .horizontal
//            } else {
//                myOrientation = .vertical
//                sideStackView.axis = .vertical
//            }
//            ARController.attemptRotationToDeviceOrientation()
//        }
//    }
    
    
    
//    {
//        if call.outgoing == true {
//
//            if myOrientation == .vertical {
//                AppDelegate.shared.deviceOrientation = .landscapeRight
//                    let value = UIInterfaceOrientation.landscapeRight.rawValue
//                    UIDevice.current.setValue(value, forKey: "orientation")
//                myOrientation = .horizontal
//                let message = "\(Constants.ChannelMessages.orientationLandscape)"
//                let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
//                print("String send for Screen Orientations Horizontal %@",string)
//                self.sendText(string: string)
////
////                UIDevice.current.setValue(UIDeviceOrientation.landscapeLeft.rawValue, forKey: "orientation")
////
//            } else {
//                AppDelegate.shared.deviceOrientation = .portrait
//                    let value = UIInterfaceOrientation.portrait.rawValue
//                    UIDevice.current.setValue(value, forKey: "orientation")
//                myOrientation = .vertical
//                let message = "\(Constants.ChannelMessages.orientationPortrait)"
//                let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
//                print("String send for Screen Orientations Vertical %@",string)
//                self.sendText(string: string)
////
////                UIDevice.current.setValue(UIDeviceOrientation.portrait.rawValue, forKey: "orientation")
////
//            }
////            ARController.attemptRotationToDeviceOrientation()
//        }
//    }
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        AppGlobalManager.sharedInstance.uidAudioArray = []
        AppGlobalManager.sharedInstance.linkURL = ""

        isAnnotationSelected = false
        isTextBoxAnnotation = false
        
        if(isRecordingStarted){
            print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% recording Stopped")
          
            if(recordingSTART){
                view_timer.isHidden = true
                stopwatch.stop()

                if  call.outgoing {
                    let activityData = ActivityData()
                    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
                              
                    //sender
                    //yes no dialog
                    //stop cloud recording
                    
                    //sender recording start |||| sender close
                    
                    let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                        self.Stoprecording(isEndcallPush: false)

                    }
                    let noAction = UIAlertAction(title: "No", style: .default) { (action) in
                        self.callinviteServiceForReject()
                        
                        self.buttonCloseAction(sender)
                    }
                    self.showAlert(withMessage: "User has disconnected call. Do you want to save recording with proper tagging?", customActions: [yesAction,noAction])
                    //store thumb
                    
                    //stop recording means call our stop recording method
                    let message = "\(Constants.ChannelMessages.recording)-OFF"
                    let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
                    self.sendText(string: string)
                }else{
                    //receiver
                   //stop recording message
                    
                    //receiver start recording
                    let activityData = ActivityData()
                    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
                              
                    Stoprecording(isEndcallPush: false)
                    let message = "\(Constants.ChannelMessages.recording)-OFF"
                    let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
                    self.sendText(string: string)
                }
            }else{
                if  call.outgoing {
                    //sender
                    // Me recording start nathi karyu
                  
                    let activityData = ActivityData()
                    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
                              
                    self.callinviteServiceForReject()
                    recordingOnCallEndSender = true
//                    Stoprecording(isEndcallPush: false)
//                    self.buttonCloseAction(sender)

                }else{
                    //reciver
                    // Me recording start nathi karyu
                    //sender start recording and end call by receiver
                    
                    callinviteServiceForReject()
                    self.buttonCloseAction(sender)
                }
            }
        }else{
            // recording is off
            callinviteServiceForReject()
            self.buttonCloseAction(sender)
        }
        
        if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
            let vc = LoginVC.viewController()
            AppDelegate.shared.window?.rootViewController = vc
        }

    }

    func buttonCloseAction(_ sender: UIButton) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()

        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        self.isJoinChannel = false
        self.storeCallApi()
        
        sender.isUserInteractionEnabled = false
        sender.isEnabled = false
        
        if let alertVC = self.presentedViewController as? UIAlertController {
            alertVC.dismiss(animated: true, completion: nil)
        }
        let viewController = self.presentedViewController
        if viewController is SaveSSVC {
            self.dismiss(animated: true, completion: nil)
        }
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {[ weak self] in
        self.leaveChannelSaveRecord { //[weak self] in
            _ = self.selfSession.calls.map{$0.end()}
            _ = self.selfSession.calls.map{AppDelegate.shared.callManager.endCall(call:$0)}
            
            if let cl = self.call {
                AppDelegate.shared.callManager.endCallCXAction(call: cl, completion: nil)
            }
            self.stopAgoraAndDeallocateResources()
            
            self.dismiss(animated: true, completion: {
                AppGlobalManager.sharedInstance.incommingCall.removeAll()
            })
        }
    }
    
    @IBAction func btnFreezeMode(_ sender: Any) {
        convertViewBasedOnAspectRatio()
        
        if(isFreezeModeOn){
           
            isFreezeModeOn = false
            PlayScreen()
            let message = "\(Constants.ChannelMessages.freezeMode)-OFF"
            let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
            print("String send for freeze mode off %@",string)
            self.sendText(string: string)
        }else{
           
            isFreezeModeOn = true
            PauseScreen()
            let message = "\(Constants.ChannelMessages.freezeMode)-ON"
            let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
            print("String send for freeze mode on %@",string)
            self.sendText(string: string)
        }
    }
    
    @IBAction func btnUndoTapped(_ sender: Any) {
        resetTouches()
        self.sliderZoom.isHidden = true
        if isFreezeModeOn {
            if self.selfSession.isSharingSession {
                undoObject(for: self.selfSession)
                drawImage()
            } else {
                let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: Constants.ChannelMessages.undo))
                self.sendText(string: string)
            }
            return
        }
        if self.selfSession.isSharingSession {
            undoObject(for: self.selfSession)
        } else {
            let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: Constants.ChannelMessages.undo))
            self.sendText(string: string)
        }
    }
    
    @IBAction func btnTrashTapped(_ sender: Any) {
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (cancelAction) in
        }
        let okAction = UIAlertAction(title: "Clear", style: .destructive) { (okAction) in
            self.sliderZoom.isHidden = true

            self.clearAllStrokes()
            self.clearAllAnnotations()
            self.selfSession.arUndoManager.clearAllNodes()
            if self.imgViewScreenshot != nil {
                self.imgViewScreenshot.image = self.imgFreeze
            }
            _ = self.tempSenderBufferLocal.removeAll()
            _ = self.tempSenderBufferLocalScreenDrawing?.points2D?.removeAll()
            _ = self.tempSenderAryAnnotationData?.removeAll()
            let allReceiverSession = self.activeSessions.filter{$0.userID != self.selfSession.userID}
            allReceiverSession.map{$0.tempSenderAryAnnotationData?.removeAll()}
            allReceiverSession.map{$0.tempSenderBufferLocal.removeAll()}
            allReceiverSession.map{$0.tempSenderBufferLocalScreenDrawing?.points2D?.removeAll()}
            
            self.tempSenderBufferLocalScreenDrawing?.lineColor?.removeAll()
            _ = self.tempSenderDrawLineColor.removeAll()
            _ = self.tempReceiverDrawLineColor.removeAll()
            _ = allReceiverSession.map{$0.tempSenderBufferLocalScreenDrawing?.lineColor?.removeAll()}
            _ = allReceiverSession.map{$0.tempReceiverDrawLineColor.removeAll()}
        }
        self.showAlert(withMessage: "Clear Drawing", customActions: [cancelAction,okAction])
    }
    
    @IBAction func btnTextBoxTapped(_ sender: Any) {
        isTextBoxAnnotation = true
        isAnnotationSelected = false
        self.showToast("Please tap to screen and type comment", type: .success)
//        txtCustomText.isHidden = false
    }
    //--------------------------------------------------------------------------------
    
    @IBAction func btnAddCallTapped(_ sender: Any) {
        //        guard let contactListVC = self.tabBarController?.viewControllers?[1] as? ContactsVC else {
        //            return
        //        }
        
        if self.activeSessions.count >= 3{
            self.showToast("Only 4 person Allowed in group Call", type: .fail)
            isContactVCOpen = false
        }else{
            isContactVCOpen = true
            let contactListVC = ContactsVC.viewController()
            contactListVC.delegate = self
            contactListVC.contactListType = .chooseContactGroupCall
            let nav = UINavigationController(rootViewController: contactListVC)
            nav.navigationBar.prefersLargeTitles = true
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func btnAnnotationTapped(_ sender: Any) {
        isTextBoxAnnotation = false
        self.createAnimation(card:self.annotationCard, duration: 0.45)
        isAnnotationSelected = true
    }
    
    @IBAction func btnFeaturepointClick(_ sender: Any) {
        if(isSwitchOn){
            btn_feature_point.setBackgroundImage(#imageLiteral(resourceName: "feature_point_on"), for: .normal)
            sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
            isSwitchOn = false
        }else{
            btn_feature_point.setBackgroundImage(#imageLiteral(resourceName: "feature_point_off"), for: .normal)
            sceneView.debugOptions = []
            isSwitchOn = true
        }
    }
    
    @IBAction func btnMutetapped(_ sender: Any) {
        if isAudioMute{
            rtcEngine.muteLocalAudioStream(false)
            isAudioMute = false
            btnMuteUnmute.setImage(#imageLiteral(resourceName: "audio_mute"), for: .normal)
        }else{
            rtcEngine.muteLocalAudioStream(true)
            isAudioMute = true
            btnMuteUnmute.setImage(#imageLiteral(resourceName: "audio_unmute"), for: .normal)
        }
    }
    @IBAction func btnChatTapped(_ sender: Any) {
        self.createAnimation(card:self.chatCard, duration: 0.45)
        viewGreenOnChat.isHidden = true
    }
   
    @IBAction func btnLineWidthTapped(_ sender: Any) {
          if(isLineWidthViewSelected){
              isLineWidthViewSelected = false
              viewLineWidth.isHidden = true
          }else{
              isLineWidthViewSelected = true
              viewLineWidth.isHidden = false
          }
      }
      
    @IBAction func btnSmallLineTapped(_ sender: Any) {
        if  !call.outgoing {
            let message = "\(Constants.ChannelMessages.lineSize)_S"
            let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
            self.sendText(string: string)
        }
        strokeSize = .small
        btnSmallLine.backgroundColor = UIColor.white
        btnMediumLine.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorLightGray.color
        btnLargeLine.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorLightGray.color
        self.viewLineWidth.isHidden = true
    }
    @IBAction func btnMediumLineTapped(_ sender: Any) {
        if  !call.outgoing {
            let message = "\(Constants.ChannelMessages.lineSize)_M"
            let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
            self.sendText(string: string)
        }
        strokeSize = .medium
        btnSmallLine.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorLightGray.color
        btnMediumLine.backgroundColor = UIColor.white
        btnLargeLine.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorLightGray.color
        self.viewLineWidth.isHidden = true
    }
    @IBAction func btnLargeLineTapped(_ sender: Any) {
        if  !call.outgoing {
            let message = "\(Constants.ChannelMessages.lineSize)_L"
            let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
            self.sendText(string: string)
        }
        strokeSize = .large
        btnSmallLine.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorLightGray.color
        btnMediumLine.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorLightGray.color
        btnLargeLine.backgroundColor = UIColor.white
        self.viewLineWidth.isHidden = true
    }
    
    @IBAction func btnRecording(_ sender: Any) {
        view_timer.clipsToBounds = true
        view_timer.layer.cornerRadius = 10
        
        if(canStartRecording){
            // manage related to share screen
            
            if(isRecordingStarted){
                if(recordingSTART){
                    let message = "\(Constants.ChannelMessages.recording)-OFF"
                    let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                    self.sendText(string: string)
                    Stoprecording(isEndcallPush: false)
                }
                else{
                    self.showToast("You haven’t start recording so you are unable to stop", type: .fail)
                }
            }else{
                let message = "\(Constants.ChannelMessages.recording)-ON"
                let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                self.sendText(string: string)
                recordingSTART = true
                
                startRecording()
            }
        }
    }
   
    
    @IBAction func btnScreenShotTapped(_ sender: Any) {
        
        let saveSS = SaveSSVC.viewController()
        if isFreezeModeOn{
            if selfSession.isSharingSession == true{
                saveSS.imageScreenShot = self.imgViewScreenshot.image
            }else{
                saveSS.imageScreenShot = self.viewToDisplayAR.takeScreenshot()
            }
        }else{
            if selfSession.isSharingSession == true{
                saveSS.imageScreenShot = self.sceneView.snapshot()
            }else{
                saveSS.imageScreenShot = self.viewToDisplayAR.takeScreenshot()
            }
        }
        saveSS.mediaType = .saveScreenshot
        saveSS.roomname = self.roomName
        present(saveSS, animated: true, completion: nil)
        
    }
    
    @IBAction func btnShowColorview(_ sender: Any) {
        if(isColorViewSelected){
            isColorViewSelected = false
            viewColorPicker.isHidden = true
        }else{
            isColorViewSelected = true
            viewColorPicker.isHidden = false
        }
    }
    @IBAction func btnFlashOnOff(_ sender: Any) {
        guard let device = AVCaptureDevice.default(for: .video), device.hasTorch else {
            showTorchNotSupported()
            return
        }
        do {
            try device.lockForConfiguration()
            let torchOn = !device.isTorchActive
            try device.setTorchModeOn(level: 1.0)
            device.torchMode = torchOn ? .on : .off
            if(torchOn){
                btnFlash.setImage(#imageLiteral(resourceName: "FlashOff"), for: .normal)
            }else{
                btnFlash.setImage(#imageLiteral(resourceName: "FlashOn"), for: .normal)
            }
            device.unlockForConfiguration()
        } catch {
            showTorchNotSupported()
        }
    }
    
    @IBAction func Slidervaluechange(_ sender: UISlider) {
        
        let sliderValue = sliderZoom.value
        
        guard let nodeToScale = self.selectedNode else {
            return
        }
        let pinchScaleX: CGFloat = CGFloat(sliderValue) //* CGFloat((nodeToScale.scale.x))
        let pinchScaleY: CGFloat = CGFloat(sliderValue) //* CGFloat((nodeToScale.scale.y))
        let pinchScaleZ: CGFloat = CGFloat(sliderValue) //* CGFloat((nodeToScale.scale.z))
        nodeToScale.scale = SCNVector3Make(Float(pinchScaleX), Float(pinchScaleY), Float(pinchScaleZ))
        
        
    }
   
    private func showTorchNotSupported() {
        let alertController = UIAlertController(title: "Flashlight is not supported", message: nil, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true)
    }
    
    //--------------------------------------------------------------------------------
    
    //
    
    //--------------------------------------------------------------------------------
    
    
    @objc func sceneViewTapped(gesture:UITapGestureRecognizer) {
        print("Sceneview Tapped")
        tapLocation = gesture.location(in: self.sceneView)
        self.sliderZoom.isHidden = true
        if(isAnnotationSelected){
            //normal annotation
            touchPoint = .zero
            
            let location = gesture.location(in: self.sceneView)
            
            if isFreezeModeOn {
                //normal annotation : freeze mode
                if self.selfSession.isSharingSession {
                    //normal annotation : freeze mode sender

                    let touchPointgesture = gesture.location(in: self.imgViewScreenshot)
                    let img = selfSession.selectedAnnotationType.freezeModeImage ?? UIImage() // UIImage(named: "09")!
                    tempSenderAryAnnotationData?.append(Annotation2DData(points: touchPointgesture, image: img))
                    var obj = Annotation2D()
                    obj.annotation2D = tempSenderAryAnnotationData
                    //      obj.annotation2D?.append(Annotation2DData(points: touchPointgesture, image: img))
                    selfSession.arUndoManager.addUndoableObject(obj)
                    drawImage()
                    return
                }else{
                    //normal annotation : freeze mode receiver

                    let index = self.selfSession.selectedAnnotationType.index ?? 0
                    let touchPointgesture = gesture.location(in: self.viewToDisplayAR)

                    let message =       "\(Constants.ChannelMessages.singleTap)-\(index)-\(NSCoder.string(for: normalizedCGPoint(point: touchPointgesture)))"
                    //                    let message = "\(Constants.ChannelMessages.singleTap)-\(index)-\(NSCoder.string(for: ConvertLocalScreenCoordinatesToRemoteScreenCoordinates(x: location.x, y: location.y)))"
                    
                    let string =  MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                    
                    self.sendText(string: string)
                    
                    print("annotation String %@",string)
                }
            }else{
                if self.selfSession.isSharingSession {
                    //normal annotation : normal mode sender

                    loadAnnotaion(session: selfSession, at:location)
                } else {
                    //normal annotation : normal mode receiver

                    let index = self.selfSession.selectedAnnotationType.index ?? 0
                    let message = "\(Constants.ChannelMessages.singleTap)-\(index)-\(NSCoder.string(for: normalizedCGPoint(point: location)))"
                    //                    let message = "\(Constants.ChannelMessages.singleTap)-\(index)-\(NSCoder.string(for: ConvertLocalScreenCoordinatesToRemoteScreenCoordinates(x: location.x, y: location.y)))"
                    
                    let string =  MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                    
                    self.sendText(string: string)
                    
                    print("annotation String %@",string)
                }
            }
        }
        else{
            
            if(isTextBoxAnnotation){
                //Text annotation

                if isFreezeModeOn {
                    // Text annotation : freeze mode

                    if self.selfSession.isSharingSession {
                        // Text annotation : freeze mode sender
                        locationTextBoxFreezeMode = gesture.location(in: self.imgViewScreenshot)

                        if isStartingTextAnnotationonFreezmode == false {
                            
                            isStartingTextAnnotationonFreezmode = true
                            txtCustomText.isHidden = false
                            self.toolBar.isHidden = false
                            addToolBar(textField: txtCustomText)
                            txtCustomText.becomeFirstResponder()
                            
                            let img = selfSession.selectedAnnotationTypeLast.freezeModeImage ?? UIImage() // UIImage(named: "09")!
                            img.imageAsset?.setValue("TextBox_F", forKey: "_assetName")
                            tempSenderAryAnnotationData?.append(Annotation2DData(points: locationTextBoxFreezeMode, image: img))
                            
                            var obj = Annotation2D()
                            obj.annotation2D = tempSenderAryAnnotationData
                            //      obj.annotation2D?.append(Annotation2DData(points: touchPointgesture, image: img))
                            selfSession.arUndoManager.addUndoableObject(obj)
                            drawImage()
                        }
                        return
                    }else{
                        // Text annotation : freeze mode receiver
                        locationTextBoxFreezeMode = gesture.location(in: self.viewToDisplayAR)

                        if isStartingTextAnnotationonFreezmode == false {
                            
                            isStartingTextAnnotationonFreezmode = true
                            txtCustomText.isHidden = false
                            self.toolBar.isHidden = false
                            addToolBar(textField: txtCustomText)
                            txtCustomText.becomeFirstResponder()
                            
                            let message = "\(Constants.ChannelMessages.TapTextBox)-\(NSCoder.string(for: normalizedCGPoint(point: locationTextBoxFreezeMode!)))"
                            
                            let string =  MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                            
                            self.sendText(string: string)
                            
                            print("annotation String %@",string)
                        }
                    }
                }else{
                    // Text annotation : normal mode

                    locationTextBox = gesture.location(in: self.sceneView)
                    //
                    ////            self.loadAnnotaionTextBox(session: selfSession, at: location!, textSrting : txtCustomText.text!,isText: false)
                    if locationTextBox != nil{
                        if selfSession.isSharingSession == true{
                            // Text annotation : normal mode sender

                            if self.sceneView.hitTest(tapLocation!, options: nil).first?.node.parent?.name == "EmptyTextBox" {
//                                selectedNode = self.sceneView.hitTest(tapLocation!, options: nil).first?.node.parent
////                                sliderZoom.isHidden = false
//                                sliderZoom.minimumValue = calculatePercentage(value: 0.0015, percentageVal: 30)
//                                sliderZoom.maximumValue = 0.0015 * 5
//                                if selectedNode?.scale.x == 0.0015 {
//                                    sliderZoom.value = 0.0015
//                                } else {
//                                    sliderZoom.value = (selectedNode?.scale.x)!
//                                }
//                                animateNodeSeleted(node: selectedNode!, scaleSize: selectedNode?.scale.x ?? 0)
                                self.view.endEditing(true)
                            } else {
                                self.AddEmptyTextBox(session: selfSession, at: locationTextBox!)
                            }
                        }else{
                            // Text annotation : normal mode receiver

                            let message = "\(Constants.ChannelMessages.TapTextBox)-\(NSCoder.string(for: normalizedCGPoint(point: locationTextBox!)))"
                            let string =  MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                            
                            self.sendText(string: string)
                            
                            print(string)
                        }
                        //slider for annotation zoom - USEFUL CODE DON"T delete
                    
                        if self.sceneView.hitTest(tapLocation!, options: nil).first?.node.parent?.name == "EmptyTextBox" {
//                            selectedNode = self.sceneView.hitTest(tapLocation!, options: nil).first?.node.parent
//                            sliderZoom.isHidden = false
//                            sliderZoom.minimumValue = calculatePercentage(value: 0.0015, percentageVal: 30)
//                            sliderZoom.maximumValue = 0.0015 * 5
//                            if selectedNode?.scale.x == 0.0015 {
//                                sliderZoom.value = 0.0015
//                            } else {
//                                sliderZoom.value = (selectedNode?.scale.x)!
//                            }
        //                    animateNodeSeleted(node: selectedNode!, scaleSize: selectedNode?.scale.x ?? 0)
                            self.view.endEditing(true)
                        }
                        else {
                            txtCustomText.isHidden = false
                            self.toolBar.isHidden = false
                            addToolBar(textField: txtCustomText)
                            txtCustomText.becomeFirstResponder()
                            for gesture in self.view.gestureRecognizers! {
                                gesture.isEnabled = false
                            }
                            if selectedNode == nil { return }
                            sliderZoom.minimumValue = calculatePercentage(value: 0.0015, percentageVal: 30)
                            sliderZoom.maximumValue = 0.0015 * 5
                            if selectedNode?.scale.x == 0.0015 {
                                sliderZoom.value = 0.0015
                            } else {
                                sliderZoom.value = (selectedNode?.scale.x)!
                            }
        //                    animateNodeSeleted(node: selectedNode!, scaleSize: selectedNode?.scale.x ?? 0)
                        }
                    }
                }
            }
        }
    }
    //--------------------------------------------------------------------------------
    
    //MARK:-  Recording Methdos
    
    //--------------------------------------------------------------------------------
    
    private func timeString(from timeInterval: TimeInterval) -> String {
        let seconds = Int(timeInterval.truncatingRemainder(dividingBy: 60))
        let minutes = Int(timeInterval.truncatingRemainder(dividingBy: 60 * 60) / 60)
        //          let hours = Int(timeInterval / 3600)
        //          return String(format: "%.2d:%.2d:%.2d", hours, minutes, seconds)
        return String(format: "%.2d:%.2d", minutes, seconds)
    }
    func startRecording(){
        
              btnRecord.setImage(#imageLiteral(resourceName: "Recording_Start"), for: .normal)
        if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
            view_timer.isHidden = false
            view_timer.clipsToBounds = true
            view_timer.layer.cornerRadius = 10
        }else{
                view_timer.isHidden = false
                view_timer.clipsToBounds = true
                view_timer.layer.cornerRadius = 10
        }

        if  call.outgoing {
            RecordingHelper.sharedInstance.acquireSeriveRecording(roomname: self.roomName, uid: String(selfSession.userID))
        }
        
        isRecordingStarted = true
        stopwatch.start()
    }
    
    func Stoprecording(isEndcallPush : Bool){
        print("stop recording called")
        btnRecord.setImage(#imageLiteral(resourceName: "Recording_Stop"), for: .normal)
        view_timer.isHidden = true
        if  call.outgoing {
            //            RecordingHelper.sharedInstance.stopRECORDING(duration: lblTimer.text ?? "00:00")
            
            let duration = self.lblTimer.text ?? "00:00"
            
            RecordingHelper.sharedInstance.stopRECORDING { (filePath) in
                if(filePath == "Failure"){
                    self.recordingSTART = false
                }
                self.StoreRecordingThumb(recordingPath: filePath,duration : duration, isEndcallPush: isEndcallPush)
            }
        }else{
            self.recordingSTART = false
        }
        isRecordingStarted = false
        stopwatch.stop()
    }
    
    func StoreRecordingThumb(recordingPath : String,duration : String, isEndcallPush : Bool){
        
//        let activityData = ActivityData()
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
//
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        guard let user_call_id = AppGlobalManager.sharedInstance.user_call_id_Global else{
            return
        }
        
        let requestParameters = [ "device_id" : "\(device_id)",
            "recording_path" : "\(recordingPath)",
            "user_call_id" : "\(user_call_id)",
            "device_type" : "0",
            "duration": duration] as [String : Any]
        
        print("store Thumb api parameters : \(requestParameters)")
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.stopRecordingThumb, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    let userResponse =  CommonCodableHelper<saveThumbResponse>.codableObject(from: data)
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast(userResponse?.message ?? "Unable to stop recording. Please try after sometime", type: .fail)
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        return
                    }
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    (AppDelegate.shared.window?.rootViewController as? Toastable)?.showToast(userResponse?.message ?? "Stored", type: .success)
                    
                    if  self.call.outgoing {
                        if(self.recordingSTART){
                            self.openviewForSaveVideo(thumbpath: (userResponse?.output?.thumb_path)!, recordingId: (userResponse?.output?.id)!,isEndcallPush : isEndcallPush)
                        }else{
                            self.sendMessageToReceiver(thumbpath: (userResponse?.output?.thumb_path)!, recordingId: (userResponse?.output?.id)!, userCallId: (userResponse?.output?.user_call_id)!)
//                            self.showToast("Send message to receiver", type: .success)
                            if(self.recordingOnCallEndSender){
                                self.buttonCloseAction(self.btnClose)
                                self.recordingOnCallEndSender = false
                            }
                        }
                        self.recordingSTART = false
                    }
                }
                
            case .failure( let error):
                print("error",error)
            }
        }
    }
    
    func openviewForSaveVideo(thumbpath: String,recordingId: Int,isEndcallPush: Bool){
        let saveSS = SaveSSVC.viewController()
        
        saveSS.mediaType = .saveVideo
        saveSS.thumb_url = thumbpath
        saveSS.recording_id = recordingId
        saveSS.roomname = self.roomName
        if(isEndcallPush){
            saveSS.storecallAction = .saveAndStorecall
        }else{
            saveSS.storecallAction = .normalSave
        }
        
        present(saveSS, animated: true, completion: nil)
    }
    func sendMessageToReceiver(thumbpath:String,recordingId:Int,userCallId:String){
        let dicRecording = [ "user_call_id" : userCallId,
                             "thumb_path" : thumbpath,
                             "id" : recordingId
        ] as [String : Any]
        let dictRequest = [Constants.ChannelMessages.recordingThumb:dicRecording]
        do{
            let data = try JSONSerialization.data(withJSONObject: dictRequest, options: .prettyPrinted)
            if let message = String(data:data,encoding: .utf8) {
                let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                self.sendText(string: string)
                print("Message recordingThumb %@",string)
            }
        }catch {
            print("Exception in parsing json to codable \(error)")
        }
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Custom Methdos
    
    //--------------------------------------------------------------------------------
    
    
    func setupCardView(){
        //    self.btnAnnotaion.setImage(self.selfSession.selectedAnnotationType.image, for: .normal)
        visualEffectView = UIVisualEffectView(frame: self.view.bounds)
        visualEffectView.effect = UIBlurEffect()
        visualEffectView.isUserInteractionEnabled = false
        self.view.addSubview(visualEffectView)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissCardView))
        visualEffectView.addGestureRecognizer(tapGesture)
        // Add Annotaion View to current class
        let annotationVC = self.annotationCard.viewController as! AnnotaionListViewController
        annotationVC.closeAction = { [unowned self] in
            self.createAnimation(card:self.annotationCard, duration: 0.45)
        }
        annotationVC.annotationSelected = {[unowned self] annotaion in
            self.btnAnnotaion.setImage(annotaion.image, for: .normal)
            self.selfSession.selectedAnnotationType = annotaion
            self.createAnimation(card:self.annotationCard, duration: 0.45)
        }
        self.addChild(annotationVC)
        self.view.addSubview(annotationVC.view)
        annotationVC.view.frame = CGRect(x:0,y:self.view.frame.height - self.annotationCard.cardInfo.cardHandleHeight,width:self.view.frame.width,height:self.annotationCard.cardHeight)
        annotationVC.view.clipsToBounds = true
        // Add Chat View to current class
        let chatVC = self.chatCard.viewController as! ChatViewController
        let receiverForChat = AppGlobalManager.sharedInstance.incommingCall.first
        
        var chatUserListVC: ChatUsersListViewController?
        //        if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
        //        }
        //        else {
        chatUserListVC = ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.children)?[3].children.first as? ChatUsersListViewController
        //        }
        
        if receiverForChat?.chatId == "" {
            if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    
                    chatVC.chatId = receiverForChat?.chatId ?? ""
                    chatVC.isFromOnCall = true
                    chatVC.strStatus = ""
                    chatVC.receiverMobileNumber = receiverForChat?.callFrom ?? ""
                    chatVC.receiverName = receiverForChat?.callerName ?? ""
                    chatVC.receiverId = receiverForChat?.callerUserId ?? ""
                    
                    chatVC.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: self.selfSession.calls.map{$0.callName}.first ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                    chatUserListVC?.receiverMobileNumber = receiverForChat?.callFrom ?? ""
                    chatUserListVC?.receiverId = "\(receiverForChat?.callerUserId ?? "")"
                    chatUserListVC?.receiverName = receiverForChat?.callerName ?? ""
                    chatUserListVC?.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: receiverForChat?.callerName ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                    AppGlobalManager.sharedInstance.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: receiverForChat?.callerName ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                    chatVC.isBackFromChatScreen = { (state) in
                        chatUserListVC?.receiverMobileNumber = ""
                        chatUserListVC?.receiverId = ""
                        chatUserListVC?.receiverName = ""
                        chatUserListVC?.receiverUser = nil
                    }
                    chatUserListVC?.aryFilterChatData = chatUserListVC?.aryFilterChatData.map({ (temp) -> chatUsersModel in
                        var currentTemp = temp
                        if currentTemp.mobileNo == receiverForChat?.callFrom ?? "" {
                            currentTemp.unreadCount = 0
                            return currentTemp
                        }
                        return currentTemp
                    }) ?? []
                    chatUserListVC?.tableView?.reloadData()
                }
            }else{
                let num = self.selfSession.calls.map{$0.handle}.first ?? ""
                let chatUser = AppGlobalManager.sharedInstance.aryChatData.filter{$0.mobileNo == num}.first
                if chatUser != nil {
                    chatVC.chatId = AppGlobalManager.sharedInstance.chat_id//chatUser?.chatId ?? ""
                    chatVC.isFromOnCall = true
                    chatVC.strStatus = ""
                    chatVC.receiverMobileNumber = chatUser?.mobileNo ?? ""
                    chatVC.receiverName = chatUser?.name ?? ""
                    chatVC.receiverId = "\(chatUser?.id ?? 0)"
                    chatVC.receiverUser = User(id: Int(chatUser?.chatId ?? "0"), name:self.selfSession.calls.map{$0.callName}.first ?? "" , email: "", is_guest: false, mobileNo: chatUser?.mobileNo ?? "", department: "", company: nil, chatId: AppGlobalManager.sharedInstance.chat_id)
                    
                    chatUserListVC?.receiverMobileNumber = chatUser?.mobileNo ?? ""
                    chatUserListVC?.receiverId = "\(chatUser?.id ?? 0)"
                    chatUserListVC?.receiverName = chatUser?.name ?? ""
                    chatUserListVC?.receiverUser = User(id: chatUser?.id, name: self.selfSession.calls.map{$0.callName}.first ?? "", email: chatUser?.email ?? "", is_guest: false, mobileNo: chatUser?.mobileNo ?? "", department: chatUser?.department ?? "", company: nil, chatId: AppGlobalManager.sharedInstance.chat_id)
                    AppGlobalManager.sharedInstance.receiverUser = User(id: chatUser?.id, name: chatUser?.name ?? "", email: chatUser?.email ?? "", is_guest: false, mobileNo: chatUser?.mobileNo ?? "", department: chatUser?.department ?? "", company: nil, chatId: AppGlobalManager.sharedInstance.chat_id)
                    chatVC.isBackFromChatScreen = { (state) in
                        chatUserListVC?.receiverMobileNumber = ""
                        chatUserListVC?.receiverId = ""
                        chatUserListVC?.receiverName = ""
                        chatUserListVC?.receiverUser = nil
                    }
                    chatUserListVC?.aryFilterChatData = chatUserListVC?.aryFilterChatData.map({ (temp) -> chatUsersModel in
                        var currentTemp = temp
                        if currentTemp.mobileNo == chatUser?.mobileNo ?? "" {
                            currentTemp.unreadCount = 0
                            return currentTemp
                        }
                        return currentTemp
                    }) ?? []
                    chatUserListVC?.tableView?.reloadData()
                }
                else{
                    chatVC.chatId = receiverForChat?.chatId ?? ""
                    chatVC.isFromOnCall = true
                    chatVC.strStatus = ""
                    chatVC.receiverMobileNumber = receiverForChat?.callFrom ?? ""
                    chatVC.receiverName = receiverForChat?.callerName ?? ""
                    chatVC.receiverId = receiverForChat?.callerUserId ?? ""
                    
                    chatVC.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: self.selfSession.calls.map{$0.callName}.first ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                    chatUserListVC?.receiverMobileNumber = receiverForChat?.callFrom ?? ""
                    chatUserListVC?.receiverId = "\(receiverForChat?.callerUserId ?? "")"
                    chatUserListVC?.receiverName = receiverForChat?.callerName ?? ""
                    chatUserListVC?.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: receiverForChat?.callerName ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                    AppGlobalManager.sharedInstance.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: receiverForChat?.callerName ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                    chatVC.isBackFromChatScreen = { (state) in
                        chatUserListVC?.receiverMobileNumber = ""
                        chatUserListVC?.receiverId = ""
                        chatUserListVC?.receiverName = ""
                        chatUserListVC?.receiverUser = nil
                    }
                    chatUserListVC?.aryFilterChatData = chatUserListVC?.aryFilterChatData.map({ (temp) -> chatUsersModel in
                        var currentTemp = temp
                        if currentTemp.mobileNo == receiverForChat?.callFrom ?? "" {
                            currentTemp.unreadCount = 0
                            return currentTemp
                        }
                        return currentTemp
                    }) ?? []
                    chatUserListVC?.tableView?.reloadData()
                }
                
            }
        }
        else {
            if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
                
                chatVC.chatId = receiverForChat?.chatId ?? ""
                chatVC.isFromOnCall = true
                chatVC.strStatus = ""
                chatVC.receiverMobileNumber = receiverForChat?.callFrom ?? ""
                chatVC.receiverName = receiverForChat?.callerName ?? ""
                chatVC.receiverId = receiverForChat?.callerUserId ?? ""
                
                chatVC.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: self.selfSession.calls.map{$0.callName}.first ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                chatUserListVC?.receiverMobileNumber = receiverForChat?.callFrom ?? ""
                chatUserListVC?.receiverId = "\(receiverForChat?.callerUserId ?? "")"
                chatUserListVC?.receiverName = receiverForChat?.callerName ?? ""
                chatUserListVC?.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: receiverForChat?.callerName ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                AppGlobalManager.sharedInstance.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: receiverForChat?.callerName ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                chatVC.isBackFromChatScreen = { (state) in
                    chatUserListVC?.receiverMobileNumber = ""
                    chatUserListVC?.receiverId = ""
                    chatUserListVC?.receiverName = ""
                    chatUserListVC?.receiverUser = nil
                }
                chatUserListVC?.aryFilterChatData = chatUserListVC?.aryFilterChatData.map({ (temp) -> chatUsersModel in
                    var currentTemp = temp
                    if currentTemp.mobileNo == receiverForChat?.callFrom ?? "" {
                        currentTemp.unreadCount = 0
                        return currentTemp
                    }
                    return currentTemp
                }) ?? []
                chatUserListVC?.tableView?.reloadData()
                
            }else{
                chatVC.chatId = receiverForChat?.chatId ?? ""
                chatVC.isFromOnCall = true
                chatVC.strStatus = ""
                chatVC.receiverMobileNumber = receiverForChat?.callFrom ?? ""
                chatVC.receiverName = receiverForChat?.callerName ?? ""
                chatVC.receiverId = receiverForChat?.callerUserId ?? ""
                //        chatVC.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: receiverForChat?.callerName ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                chatVC.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: self.selfSession.calls.map{$0.callName}.first ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                chatUserListVC?.receiverMobileNumber = receiverForChat?.callFrom ?? ""
                chatUserListVC?.receiverId = "\(receiverForChat?.callerUserId ?? "")"
                chatUserListVC?.receiverName = receiverForChat?.callerName ?? ""
                chatUserListVC?.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: receiverForChat?.callerName ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                AppGlobalManager.sharedInstance.receiverUser = User(id: Int(receiverForChat?.callerUserId ?? "0"), name: receiverForChat?.callerName ?? "", email: "", is_guest: false, mobileNo: receiverForChat?.callFrom ?? "", department: "", company: nil, chatId: receiverForChat?.chatId ?? "")
                chatVC.isBackFromChatScreen = { (state) in
                    chatUserListVC?.receiverMobileNumber = ""
                    chatUserListVC?.receiverId = ""
                    chatUserListVC?.receiverName = ""
                    chatUserListVC?.receiverUser = nil
                }
                chatUserListVC?.aryFilterChatData = chatUserListVC?.aryFilterChatData.map({ (temp) -> chatUsersModel in
                    var currentTemp = temp
                    if currentTemp.mobileNo == receiverForChat?.callFrom ?? "" {
                        currentTemp.unreadCount = 0
                        return currentTemp
                    }
                    return currentTemp
                }) ?? []
                chatUserListVC?.tableView?.reloadData()
            }
        }
        
        chatVC.sendChatMessage = { [unowned self] text in
            let message = "\(Constants.ChannelMessages.chat)\(text)"
            //   self.sendText(string: message)
            let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
            self.sendText(string: string)
        }
        self.addChild(chatVC)
        self.view.addSubview(chatVC.view)
        chatVC.view.frame = CGRect(x:0,y:self.view.frame.height - self.chatCard.cardInfo.cardHandleHeight,width:self.view.frame.width,height:self.chatCard.cardHeight)
        chatVC.view.clipsToBounds = true
    }
    
    @objc func dismissCardView() {
        self.view.endEditing(true)
        if self.chatCard.isExpanded {
            self.createAnimation(card: self.chatCard, duration: 0.45)
            viewGreenOnChat.isHidden = true
        } else if self.annotationCard.isExpanded  {
            self.createAnimation(card: self.annotationCard, duration: 0.45)
        }
    }
    
    func createAnimation(card:CardHandler,duration:TimeInterval) {
        
        guard animations.isEmpty else {
            print("Animation not empty")
            return
        }
        let viewController = card.viewController
        print("array count",self.animations.count)
        
        let cardMoveUpAnimation = UIViewPropertyAnimator.init(duration: duration, dampingRatio: 1.0) { [weak self] in
            guard let `self` = self else  {return}
            switch card.cardInfo.nextState {
            case .collapsed:
                viewController.view.frame.origin.y = self.view.frame.height - card.cardInfo.cardHandleHeight
            case .expanded:
                viewController.view.frame.origin.y = self.view.frame.height - card.cardHeight
            }
        }
        cardMoveUpAnimation.addCompletion { [weak self] _ in
            
            self?.animations.removeAll()
            
            if card.cardInfo.nextState == .expanded {
                self?.view.gestureRecognizers?.map{$0.isEnabled = false}
                self?.isDrawingEnable = false
                self?.visualEffectView.isUserInteractionEnabled = true
            } else {
                self?.view.gestureRecognizers?.map{$0.isEnabled = true}
                self?.visualEffectView.isUserInteractionEnabled = false
                self?.isDrawingEnable = true
                
            }
            card.cardInfo.updateToNextState()
        }
        cardMoveUpAnimation.startAnimation()
        animations.append(cardMoveUpAnimation)
        
        let cornerRadiusAnimation = UIViewPropertyAnimator(duration: duration, curve: .linear) { [weak self] in
            switch card.cardInfo.nextState {
            case .expanded:
                viewController.view.layer.cornerRadius = 12
            case .collapsed:
                viewController.view.layer.cornerRadius = 0
            }
        }
        cornerRadiusAnimation.startAnimation()
        animations.append(cornerRadiusAnimation)
        
        
        let visualEffectAnimation = UIViewPropertyAnimator.init(duration: duration, curve: .linear) { [weak self ] in
            switch card.cardInfo.nextState {
            case .expanded:
                if card.cardInfo.cardType == .annotation {
                    let blurEffect = UIBlurEffect(style: .dark)
                    self?.visualEffectView.effect = blurEffect
                    
                }
                
            case .collapsed:
                self?.visualEffectView.effect =  nil
            }
        }
        visualEffectAnimation.startAnimation()
        animations.append(visualEffectAnimation)
    }
    
    
    
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Load agora kit
    
    //--------------------------------------------------------------------------------
    
    func loadAgoraKit() {
        
        rtcEngine = AgoraRtcEngineKit.sharedEngine(withAppId: KeyCenter.AppId, delegate: self)
        //       rtcEngine = AgoraRtcEngineKit.sharedEngine(withAppId: UserDefaults.standard.string(forKey: "AgoraAppId")!, delegate: self)
        rtcEngine.setChannelProfile(.communication)
        rtcEngine.enableVideo()
        //  rtcEngine.enableDualStreamMode(true)
        rtcEngine.enableAudio()
    
        // rtcEngine.setClientRole(.broadcaster)
        rtcEngine.setDefaultAudioRouteToSpeakerphone(true)
        rtcEngine.setVideoSource(videoSource)
        //extra added in agora for testing
        rtcEngine.setLocalPublishFallbackOption(.videoStreamLow)
        rtcEngine.setRemoteSubscribeFallbackOption(.videoStreamLow)
        //        print(AgoraRtcEngineKit.getSdkVersion())
        
        
        
        let videoConfig:AgoraVideoEncoderConfiguration = .init(size:self.view.frame.size, frameRate: .fps15, bitrate: 600, orientationMode: .fixedPortrait)
//        let videoConfig:AgoraVideoEncoderConfiguration = .init(width: 768, height: 1024, frameRate: .fps15, bitrate: 600, orientationMode: .fixedPortrait)

        rtcEngine.setVideoEncoderConfiguration(videoConfig)
        
        rtcEngine.disableExternalAudioSource()
        
//        rtcEngine.startPreview()
        
        //         let user = AppGlobalManager.sharedInstance.loggedInUser?.user
        
        let code = rtcEngine.joinChannel(byToken: nil, channelId: roomName, info: nil, uid: UInt(selfSession.userID)) {[weak self] (message, id, elapsed) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            //        let code = rtcEngine.joinChannel(byUserAccount: (user?.name)!, token: KeyCenter.AppId, channelId: "room_name_15566") {[weak self] (message, id, elapsed) in
            guard let `self` = self else {return }
            //     self?.currentUserID = id
            // STEP2: Init the session
            self.selfSession = ARDrawingSession(userID: Int64(id))
            self.selfSession.calls.appendNewCall(call: self.call)
            
            self.selfSession.userID = Int64(id)
            // STEP3: call toggleARViewForScreenSharingType instead of VIEW DID LOAD
            
            if  self.call.outgoing {
                AppGlobalManager.sharedInstance.uidArray.append(String(self.selfSession.userID))
                
                self.toggleARViewForScreenSharingType()
            }
            AppGlobalManager.sharedInstance.uidAudioArray.append(String(self.selfSession.userID))
            
//            self.rtcEngine.stopPreview()
            
            //changes in lock down
            self.isJoinChannel = true
            self.storeCallApi()
            
            print(self.selfSession.calls.map{$0.callName})
            
//            self.activeUserNames = self.selfSession.calls.map{$0.callName}
//            self.colUserName.reloadData()
//            print("Active user name array : %@",self.activeUserNames)
            
            self.DictActiveUsernames["UserName"] = self.selfSession.calls.map{$0.callName}.first ?? ""
            self.DictActiveUsernames["MobileNumber"] = self.selfSession.calls.map{$0.handle}.first ?? ""
            self.DictActiveUsernames["uid"] = Int64(id)
//            self.arrayUserNames.append(self.DictActiveUsernames)
            if self.arrayUserNames.filter({$0["MobileNumber"] as? String ?? "" == self.selfSession.calls.map{$0.handle}.first ?? ""}).count == 0 {
                self.arrayUserNames.append(self.DictActiveUsernames)
            }
            print("\n &&&&&&&&&&& Join channel Active user name array : %@",self.arrayUserNames)
            self.colUserName.reloadData()
            self.view.setNeedsLayout()
            
//            self.canStartRecording = true
            
            if !self.call.outgoing {
                //receiver
                if ARWorldTrackingConfiguration.isSupported {
                        //When there is only one receiver at that time share my screen alert appears
                        self.resolveScreenSharing(userId:id)
                        
                }
            }else{
//                sender
                self.canStartRecording = true
                //send message for android device sizes issue
//                let messageTRY = "{\(self.sceneView.frame.width),\(self.sceneView.frame.height)}"
//                let message = "\(Constants.ChannelMessages.screensizeshareAndroid)-\(messageTRY)"
//                let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
//                self.sendText(string: string)
            }
        }
        
        if code != 0 {
            DispatchQueue.main.async(execute: {
                self.showAlert(withMessage: "Join channel failed: \(code)")
            })
        }
        rtcEngine.setEnableSpeakerphone(true)
    }
    
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Low network alert method
    
    //--------------------------------------------------------------------------------
    
    func ShowLowNetworkAlert(){
        let ok = UIAlertAction(title: "Disconnect", style: .default) { (act) in
            self.callinviteServiceForReject()
            self.btnCloseTapped(self.btnClose)
            self.dismiss(animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (act) in
            self.rtmChannelManager = nil
            self.setupRTM()
        }
        self.showAlert(withMessage: "Your internet connection is very low.\n Please switch to the better network",customActions: [ok, cancel])
    }
    
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Setup RTM
    
    //--------------------------------------------------------------------------------
    
    func setupRTM(){
        rtmChannelManager = RTMChannelManager()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(message:"Loading ..."))
        rtmChannelManager?.login(phoneNumber: "\(AppGlobalManager.sharedInstance.currentUserID ?? "")", completion: { [unowned self] (code) in
            if(code.rawValue == 1001){
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {[ weak self] in
                    print("RTM ==> AR Controller LOGIN \(code.rawValue)")
                    let ok = UIAlertAction(title: "Disconnect", style: .default) { (act) in
                        self!.callinviteServiceForReject()
                        self!.btnCloseTapped(self!.btnClose)
                    }
                    let cancel = UIAlertAction(title: "Retry", style: .default) { (act) in
                        self!.rtmChannelManager = nil
                        self!.setupRTM()
                    }
                    self?.showAlert(withMessage: "Your internet connection is low.\nSo you can’t perform drawing actions",customActions: [ok, cancel])
                    self!.rtmChannelManager?.joinChannel(string: self!.roomName, completion: { (errorCode) in
                        print("CHANNEL JOINED WITH CODE \(errorCode.rawValue)")
                        self!.loadAgoraKit()
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    })
                }
            }else{
                self.rtmChannelManager?.joinChannel(string: self.roomName, completion: { (errorCode) in
                    print("CHANNEL JOINED WITH CODE \(errorCode.rawValue)")
                    
                    let scale = UIScreen.main.scale

                        let messageTRY = "{\(Int(round(self.sceneView.frame.width))),\(Int(round(self.sceneView.frame.height)))}"
                        let message = "\(Constants.ChannelMessages.screensizeshareSenderFreezeMode)-\(messageTRY)"
                        let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
                        print("Sender message joining RTM : \(string)")
                        self.sendText(string: string)
              
                    self.loadAgoraKit()
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                })
            }
        })
        rtmChannelManager?.messageListener = {[unowned self] message in
            self.handleMessageAction(message.text)
        }
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    func callinviteServiceForReject(){
        AppGlobalManager.sharedInstance.isRejectedBy = true
        self.status = .ideal
        (UIApplication.shared.delegate as! AppDelegate).pushManager.callInviteService(state: .callRejected) { (state) in
            if state {
                AppGlobalManager.sharedInstance.incommingCall.removeAll()
            } else {
                UtilityClass.showAlert(title: "", message: "Something went wrong") { (act) in
                }
            }
        }
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Freeze mode Methdos
    
    //--------------------------------------------------------------------------------
    
    func PauseScreen(){
        btnMainLineWidth.isHidden = true
        SideButtonHideForNonARSender()
        btnFreezeMode.setImage(#imageLiteral(resourceName: "FreezeModeInActive"), for: .normal)
        if selfSession.isSharingSession == false {
            //receiver

        } else {
//            sender
            imgViewScreenshot.image = self.sceneView.snapshot()
            imgViewScreenshot.isHidden = false
            imgFreeze = self.sceneView.snapshot()
            sceneView.isHidden = true
        }

    }
    func PlayScreen(){
        btnMainLineWidth.isHidden = false
        let transform = CGAffineTransform(scaleX: 1, y: 1)
        SideButtonHideForNonARSender()
        if selfSession.isSharingSession == true {
            //sender
            sceneView.isHidden = false
            self.imgViewScreenshot.transform = transform
        }else{
            self.viewToDisplayAR.transform = transform
        }
        locationTextBoxFreezeMode = nil
        btnFreezeMode.setImage(#imageLiteral(resourceName: "FreezeModeActive"), for: .normal)
        imgViewScreenshot.isHidden = true
        tempSenderBufferLocalScreenDrawing?.points2D?.removeAll()
        tempReceiverBufferLocalScreenDrawing.removeAll()
        _ = self.tempSenderBufferLocal.removeAll()
        _ = self.tempSenderBufferLocalScreenDrawing?.points2D?.removeAll()
        _ = self.tempSenderAryAnnotationData?.removeAll()
        let allReceiverSession = activeSessions.filter{$0.userID != self.selfSession.userID}
        _ = allReceiverSession.map{$0.tempSenderAryAnnotationData?.removeAll()}
        _ = allReceiverSession.map{$0.tempSenderBufferLocal.removeAll()}
        _ = allReceiverSession.map{$0.tempSenderBufferLocalScreenDrawing?.points2D?.removeAll()}
        
        tempSenderBufferLocalScreenDrawing?.lineColor?.removeAll()
        _ = self.tempSenderDrawLineColor.removeAll()
        _ = self.tempReceiverDrawLineColor.removeAll()
        _ = allReceiverSession.map{$0.tempSenderBufferLocalScreenDrawing?.lineColor?.removeAll()}
        _ = allReceiverSession.map{$0.tempReceiverDrawLineColor.removeAll()}
    }
    
    func unfortunatelyStopCall() {
        if let alertVC = self.presentedViewController as? UIAlertController {
            alertVC.dismiss(animated: true, completion: nil)
        }
        let viewController = self.presentedViewController
        if viewController is SaveSSVC {
            self.dismiss(animated: true, completion: nil)
        }
        AppGlobalManager.sharedInstance.isRejectedBy = false
        self.isJoinChannel = false
        self.storeCallApi()
        
        self.leaveChannelSaveRecord { [weak self] in
            
            _ = self?.selfSession.calls.map{$0.end()}
            _ = self?.selfSession.calls.map{AppDelegate.shared.callManager.endCall(call:$0)}
            
            AppDelegate.shared.callManager.endCallCXAction(call: self!.call, completion: nil)
            
            self?.stopAgoraAndDeallocateResources()
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  handleMessageAction
    
    //--------------------------------------------------------------------------------
    
    private func handleMessageAction(_ string: String) {
        //  print(" SIGNAL SDK messageReceived ARController \(string)")
        
        if checkJsonValid(jsonString: string){
            if let messageFormat = MessageFormatter.messageFormatObjectFromString(string: string) {
                
//                print("Message in format %@", string)
//                    print("message received %@",string)
//
                let message = messageFormat.message
                let userSession =  self.activeSessions.first(where: {$0.userID == messageFormat.userSettings.userID})
//                print("\n\n ##############################################")
//
//                print("Name = \(messageFormat.userSettings.userName)")
//                print("Mobile = \(messageFormat.userSettings.userLoginID)")
//                print("uid = \(messageFormat.userSettings.userID)")
                
                let currentData = arrayUserNames.filter{$0["MobileNumber"] as? String == messageFormat.userSettings.userLoginID}
                if currentData.count != 0 {
                    if currentData.first?["uid"] as? String != "\(messageFormat.userSettings.userID)" {
                        
                        //                        print("Before arrayUserNames == \(arrayUserNames)")
                        
                        let asdf = arrayUserNames.map { (dict) -> [String: Any] in
                            var dt = dict
                            if dt["MobileNumber"] as? String == currentData.first?["MobileNumber"] as? String {
                                if dt["uid"] as? String != "\(messageFormat.userSettings.userID)" {
                                    dt["uid"] = "\(messageFormat.userSettings.userID)"
                                }
                            }
                            return dt
                        }
                        arrayUserNames = asdf
                        //                        print("After arrayUserNames == \(arrayUserNames)")
                        self.colUserName.reloadData()
                    }
                }
//                if message.contains(Constants.ChannelMessages.screensizeshareSenderFreezeMode){
//                    print(message)
//                    // receiver receives this message
//
//                    print("Screen share sender side message received *************************************")
//
//                    if  let positionStr = message.components(separatedBy: "-").last {
//                        print(positionStr)
//
//                        let position = NSCoder.cgSize(for: positionStr)
//                        let point = CGSize(width: position.width, height: position.height)
//                        senderVideoSize = point
//                     //   if(senderVideoSize.width == 0.0){
//                            if(self.selfSession.isSharingSession){
//                              // sender
//                            }else{
//                                self.changeAspectRationAtReceiverSide()
//                            }
//                            print(senderVideoSize)
//                       // }
//                    }
//                }
//                if message.contains(Constants.ChannelMessages.screensizeshareReceiverFreezeMode){
//                    // Sender receives this message
//
//                    print(message)
//                    print("Screen share REC side message received *************************************")
//
//                    if  let positionStr = message.components(separatedBy: "-").last {
//                        print(positionStr)
//
//                        let position = NSCoder.cgSize(for: positionStr)
//                        let point = CGSize(width: position.width, height: position.height)
//                        rec_w_h = point
//                        print(rec_w_h)
//                    }
//                }
                if message.contains(Constants.ChannelMessages.orientationLandscape) {
                    sideStackView.axis = .horizontal
                    myOrientation = .horizontal
//                    ARController.attemptRotationToDeviceOrientation()
                    AppDelegate.shared.deviceOrientation = .landscapeRight
                        let value = UIInterfaceOrientation.landscapeRight.rawValue
                        UIDevice.current.setValue(value, forKey: "orientation")
                }
                if message.contains(Constants.ChannelMessages.orientationPortrait) {
                    sideStackView.axis = .vertical
                    myOrientation = .vertical
//                    ARController.attemptRotationToDeviceOrientation()
                    AppDelegate.shared.deviceOrientation = .portrait
                        let value = UIInterfaceOrientation.portrait.rawValue
                        UIDevice.current.setValue(value, forKey: "orientation")
                }
                if message.contains(Constants.ChannelMessages.recordingThumb){
                    print("recording thum message received *************************************")
                    
                    let data1 = message.data(using: .utf8)
                    let json1 = try?JSONSerialization.jsonObject(with: data1!, options: .allowFragments)
                    print(json1)
                    
                    if let data = message.data(using: .utf8),  let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any],let thumbResponse = json[Constants.ChannelMessages.recordingThumb] as? [String:Any],!thumbResponse.isEmpty {
                        
                        let thumbpath = thumbResponse["thumb_path"] as? String
                        let recordingID = thumbResponse["id"] as? Int
                        //                        let user_call_id = thumbResponse["user_call_id"] as? String
                        
                        if(recordingOnCallEndReceiver){
                            self.openviewForSaveVideo(thumbpath: thumbpath!, recordingId: recordingID!, isEndcallPush: true)
                        }else{
                            self.openviewForSaveVideo(thumbpath: thumbpath!, recordingId: recordingID!, isEndcallPush: false)
                        }
                    }
                    
                }
                
                if message.contains(Constants.ChannelMessages.screenShareNonAR) {
                    // if sender is android and their device is nonnAR
                    isFromNonARDevice = true
                }
                
                if message.contains(Constants.ChannelMessages.screenShare) {
                    canStartRecording = true
                    if selfSession.isSharingSession == false { return }
                    print(message)
                    // Other person is going to share the screen (we are passsing reverse data)
                    if message.contains(Constants.ChannelMessages.screenShareME) {

                        print(message.components(separatedBy: "-").last)
//
                        if  let positionStr = message.components(separatedBy: "-").last {


                            let position = NSCoder.cgSize(for: positionStr)

                            AppGlobalManager.sharedInstance.SenderWidth = position.width
                            AppGlobalManager.sharedInstance.SenderHeight = position.height
                                                        
                            self.selfSession.isSharingSession = false
                            self.sceneView.session.pause()
                            self.sceneView.isHidden = true
                            self.displayLink?.invalidate()
                            self.sceneView.delegate = nil
                            self.sceneView.session.delegate = nil
                            self.call.outgoing = false
                            self.toggleARViewForScreenSharingType()
                            
                            convertViewBasedOnAspectRatio()

                            AppGlobalManager.sharedInstance.isScreenShareME = true
                        }
//                        self.selfSession.isSharingSession = false
//                        self.sceneView.session.pause()
//                        self.sceneView.isHidden = true
//                        self.displayLink?.invalidate()
//                        self.sceneView.delegate = nil
//                        self.sceneView.session.delegate = nil
//                        self.call.outgoing = false
//                        self.toggleARViewForScreenSharingType()
//
//                        AppGlobalManager.sharedInstance.isScreenShareME = true
                        
                        
                    } else if(message.contains(Constants.ChannelMessages.screenShare)) {
                        if selfSession.isSharingSession == false { return }
                        
                        
                        self.selfSession.isSharingSession = true
                        // We will going to share the screen
                        AppGlobalManager.sharedInstance.isScreenShareME = false
                        
                        //                        let scale = UIScreen.main.scale
                        
                        let messageTRY = "{\(Int(round(self.sceneView.frame.width))),\(Int(round(self.sceneView.frame.height)))}"
                        let message = "\(Constants.ChannelMessages.screensizeshareSenderFreezeMode)-\(messageTRY)"
                        let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
                        self.sendText(string: string)
                        
                    }
                }
               

                if message.contains(Constants.ChannelMessages.freezeMode) {
                    if  let positionStr = message.components(separatedBy: "-").last {
                        if positionStr.uppercased() == "ON" {
                            isFreezeModeOn = true
                            isFreezeModeOnFromOtherTemp = true
                            print("message received on %@",string)
                            PauseScreen()
                        } else if positionStr.uppercased() == "OFF" {
                            isFreezeModeOn = false
                            isFreezeModeOnFromOtherTemp = false
                            tempSenderBufferLocalScreenDrawing?.points2D?.removeAll()
                            print("message received off %@",string)
                            PlayScreen()
                        }
                    }
                }
                
                if isFreezeModeOn {
                    if isFreezeModeOnFromOtherTemp {
                        
                        if self.selfSession.isSharingSession {
                            imgViewScreenshot.image = self.sceneView.snapshot()
                            imgViewScreenshot.isHidden = false
                        }
                        
                        isFreezeModeOnFromOtherTemp = false
                        imgFreeze = self.sceneView.snapshot()
                    }
                    
                    if message.contains(Constants.ChannelMessages.touchBegin) {
                        if selfSession.isSharingSession == false { return }

                        if  let positionStr = message.components(separatedBy: "-").last {
                            
                            let position = NSCoder.cgPoint(for: positionStr)
                            let point = CGPoint(x: position.x * self.imgViewScreenshot.frame.width, y: position.y * self.imgViewScreenshot.frame.height)
//                            let scale = UIScreen.main.scale
//                            let point = CGPoint(x: position.x / scale , y: position.y / scale)

                            print("\n\n touchBegin Receiver :: \(point) \n\n")
                            
                            userSession?.tempSenderBufferLocal.append([point])
                            userSession?.tempReceiverDrawLineColor.append([receiverLineColor])
                            
                            var obj = Points2D()
                            obj.points2D = userSession?.tempSenderBufferLocal
                            obj.lineColor = userSession?.tempReceiverDrawLineColor
                            
                            userSession?.tempSenderBufferLocalScreenDrawing = obj
                            
                        }
                    }
                    else if message.contains(Constants.ChannelMessages.touchMove) {
                        if selfSession.isSharingSession == false { return }

                        print("\n:: Constants.ChannelMessages.touchMove ::\n")
                        
                    }
                    else if  let data = message.data(using: .utf8),  let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any], let points = json[Constants.ChannelMessages.allPoints] as? [String], !points.isEmpty {
                        print("\n:: All Points :: \(points) \n")
                        if selfSession.isSharingSession == false { return }

                        for positionStr in points {
                            let position = NSCoder.cgPoint(for: positionStr)
                            let point = CGPoint(x: position.x * self.imgViewScreenshot.frame.width, y: position.y * self.imgViewScreenshot.frame.height)
//                            let scale = UIScreen.main.scale
//                            let point = CGPoint(x: position.x / scale, y: position.y / scale)

                            var obj = Points2D()
                            guard var lastLine = userSession?.tempSenderBufferLocal.popLast() else { return }
                            lastLine.append(point)
                            userSession?.tempSenderBufferLocal.append(lastLine)
                            obj.points2D = userSession?.tempSenderBufferLocal
                            guard var lastLineColor = userSession?.tempReceiverDrawLineColor.popLast() else { return }
                            lastLineColor.append(receiverLineColor)
                            userSession?.tempReceiverDrawLineColor.append(lastLineColor)
                            obj.lineColor = userSession?.tempReceiverDrawLineColor
                            userSession?.tempSenderBufferLocalScreenDrawing = obj
                            self.drawImage()
                        }
                        //                DispatchQueue.main.async {
                        //                }
                    }
                    else if message.contains(Constants.ChannelMessages.touchEnd) {
                        print("TouchEnd received")
                        if selfSession.isSharingSession == false { return }

                        if let positionStr = message.components(separatedBy: "-").last {
                            if userSession?.tempSenderBufferLocal.last?.count != 1 {
                                var obj = Points2D()
                                obj.points2D = userSession?.tempSenderBufferLocal
                                obj.lineColor = userSession?.tempReceiverDrawLineColor
                                userSession?.tempSenderBufferLocalScreenDrawing = obj
                                userSession?.arUndoManager.addUndoableObject(obj)
                                drawImage()
                                self.sendText(string: Constants.ChannelMessages.drawingFinshed)
                            } else {
                                userSession?.tempSenderBufferLocal.popLast()
                                drawImage()
                            }
                        }
                    }
                    else if message.contains(Constants.ChannelMessages.undo) {
                        if selfSession.isSharingSession == false { return }

                        if let userSession = userSession {
                            self.undoObject(for: userSession)
                        }
                        drawImage()
                    }
                    else if message.contains(Constants.ChannelMessages.lineColor) {
                        if selfSession.isSharingSession == false { return }

                        let positionStrArray = message.components(separatedBy: "-")
                        receiverLineColor = UIColor.init(hexString: positionStrArray.last ?? "ffffff")
                    }
                    else if message.contains(Constants.ChannelMessages.singleTap) {
                        if selfSession.isSharingSession == false { return }

                        let positionStrArray = message.components(separatedBy: "-")
                        guard positionStrArray.count > 2 else{
                            return
                        }
                        print("POS STR \(positionStrArray)")
                        if  let positionStr = positionStrArray.last, let userSession = userSession,let selectedAnnotaion = Int(positionStrArray[1])  {
                            let position = NSCoder.cgPoint(for: positionStr)
                            
                            DispatchQueue.main.async {
                                let loc = CGPoint(x: position.x * self.imgViewScreenshot.frame.width, y: position.y * self.imgViewScreenshot.frame.height)
//                                let scale = UIScreen.main.scale
//                                let loc = CGPoint(x: position.x / scale, y: position.y / scale)

                                let img = AnnotaionType.allCases[selectedAnnotaion].freezeModeImage ?? UIImage(named: "Outward")!
                                let touchPointgesture = loc
                                
                                userSession.tempSenderAryAnnotationData?.append(Annotation2DData(points: touchPointgesture, image: img))
                                var obj = Annotation2D()
                                obj.annotation2D = userSession.tempSenderAryAnnotationData
                                userSession.arUndoManager.addUndoableObject(obj)
                                
                                self.drawImage()
                            }
                        }
                    }
                    else if message.contains(Constants.ChannelMessages.recording) {
                        if selfSession.isSharingSession == false { return }

                        if  let positionStr = message.components(separatedBy: "-").last {
                            if positionStr.uppercased() == "ON" {
                                                                
                                print("message received recording on %@",string)
                                isRecordingStarted = true
                                startRecording()
                                
                            } else if positionStr.uppercased() == "OFF" {
                                
                                print("message received recording off %@",string)
                                
                                isRecordingStarted = false
                                Stoprecording(isEndcallPush: false)
                            }
                        }
                    }
                    else if message.contains(Constants.ChannelMessages.TapTextBox) {
                        if selfSession.isSharingSession == false { return }

                        print(message)
                        let positionStrArray = message.components(separatedBy: "-")
                        print(positionStrArray)
                        guard positionStrArray.count > 1 else{
                            return
                        }
                        print("POS STR textbox \(positionStrArray)")
                        if let positionStr = positionStrArray.last, let userSession = userSession{
                            let position = NSCoder.cgPoint(for: positionStr)
                            let point = CGPoint(x: position.x * self.imgViewScreenshot.frame.width, y: position.y * self.imgViewScreenshot.frame.height)
                            TapTextBoxFreezeMode = point
                            
                            let touchPointgesture = point

                            let img = userSession.selectedAnnotationTypeLast.freezeModeImage ?? UIImage() // UIImage(named: "09")!
                            img.imageAsset?.setValue("TextBox_F", forKey: "_assetName")
                            userSession.tempSenderAryAnnotationData?.append(Annotation2DData(points: touchPointgesture, image: img))
                            
                            var obj = Annotation2D()
                            obj.annotation2D = userSession.tempSenderAryAnnotationData
                            //      obj.annotation2D?.append(Annotation2DData(points: touchPointgesture, image: img))
                            userSession.arUndoManager.addUndoableObject(obj)
                            drawImage(textAnnotationFromFreezeMode: true)
                            print("Single tap text box freezemode")
                        }
                    }
                    else if message.contains(Constants.ChannelMessages.AddTextToTextBox){
                        if selfSession.isSharingSession == false { return }

                        let positionStrArray = message.components(separatedBy: "-")
                        print(positionStrArray)
                        guard positionStrArray.count > 1 else{
                            return
                        }
                        print("POS STR textbox \(positionStrArray)")
                        
                        if let positionStr = positionStrArray.last, let userSession = userSession,let textBoxString: String? = positionStrArray[1] {
                            let position = NSCoder.cgPoint(for: positionStr)
                            let point = CGPoint(x: position.x * self.imgViewScreenshot.frame.width, y: position.y * self.imgViewScreenshot.frame.height)
                            
//                            btnUndoTapped(self.btnUndo)
                            self.undoObject(for: userSession)
                            
                            let img = userSession.selectedAnnotationTypeLast.freezeModeImage ?? UIImage()
                            let imageWithText = self.imageWith(name: textBoxString, fm: CGRect(x: 30, y: 15, width: img.size.width-5, height: img.size.height))
                            
                            let mergerImageWithText = img.mergeImageWithForFreezeModeTextAnnotation(topImage: imageWithText!, position: CGPoint(x: 30, y: 15), sz: img.size)
                            
                            mergerImageWithText.imageAsset?.setValue("TextBox_F", forKey: "_assetName")
                            
                            // ----------------------------------
                            userSession.tempSenderAryAnnotationData?.append(Annotation2DData(points: TapTextBoxFreezeMode, image: mergerImageWithText))
                            //                tempSenderAryAnnotationData?.append(Annotation2DData(points: touchPointgesture, image: imageWithText))
                            var obj = Annotation2D()
                            obj.annotation2D = userSession.tempSenderAryAnnotationData
                            //      obj.annotation2D?.append(Annotation2DData(points: touchPointgesture, image: img))
                            userSession.arUndoManager.addUndoableObject(obj)
                            drawImage(textAnnotationFromFreezeMode: true)
                            print("added text on text box")
                        }
                    }
                    return
                } else {
                    imgViewScreenshot.isHidden = true
                }
                if message.contains(Constants.ChannelMessages.lineColor) {
                    let positionStrArray = message.components(separatedBy: "-")
                    receiverLineColor = UIColor.init(hexString: positionStrArray.last ?? "ffffff")
                }
                if message.contains(Constants.ChannelMessages.touchBegin) && self.selfSession.isSharingSession  {
                    if selfSession.isSharingSession == false { return }
                    if  let positionStr = message.components(separatedBy: "-").last {
                        
                        self.showToast("Other user has started drawing you should keep your device steady ", type: .success)
                        
                        let scale = UIScreen.main.scale
                        
                        let position = NSCoder.cgPoint(for: positionStr)
                        let point = CGPoint(x: position.x * self.sceneView.frame.width, y: position.y * self.sceneView.frame.height)
                        //   self.hanldeTouchBegin(touchInView: point)
                        //TODO: Remove remote points after testing the userSession
                        self.remotePoints.append(point)
                        userSession?.remotePoints.append(point)
                        
                        print("Touch begin")
                        
                    }
                }  else if message.contains(Constants.ChannelMessages.touchMove) && self.selfSession.isSharingSession {
                    if selfSession.isSharingSession == false { return }
                    let scale = UIScreen.main.scale

                    if  let positionStr = message.components(separatedBy: "-").last {
                        let position = NSCoder.cgPoint(for: positionStr)
                        let point = CGPoint(x: position.x * self.sceneView.frame.width, y: position.y * self.sceneView.frame.height)
                        self.remotePoints.append(point)
                        userSession?.remotePoints.append(point)
                        print("Touch Move")
                        
                    }
                }  else if  let data = message.data(using: .utf8),  let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any], let points = json[Constants.ChannelMessages.allPoints] as? [String], !points.isEmpty , self.selfSession.isSharingSession {
                    if selfSession.isSharingSession == false { return }
                    let scale = UIScreen.main.scale

                    for positionStr in points {
                        let position = NSCoder.cgPoint(for: positionStr)
                        let point = CGPoint(x: position.x * self.sceneView.frame.width, y: position.y * self.sceneView.frame.height)
                        self.remotePoints.append(point)
                        userSession?.remotePoints.append(point)
                    }
                    print("Allpoints received")
                    
                } else if message.contains(Constants.ChannelMessages.touchEnd)  && self.selfSession.isSharingSession {
                    print("Touch end Called")
                    let scale = UIScreen.main.scale

                    if selfSession.isSharingSession == false {
                        return }
                    if  let positionStr = message.components(separatedBy: "-").last {
                        let position = NSCoder.cgPoint(for: positionStr)
                        let point = CGPoint(x: position.x * self.sceneView.frame.width, y: position.y * self.sceneView.frame.height)
                        // self.hanleTouchEnd()
                        
                        self.remotePoints.append(point)
                        userSession?.remotePoints.append(point)
                        
                        let stroke = Stroke()
                        stroke.color = receiverLineColor
                        stroke.lineWidth = receiverLineSize.rawValue
                        stroke.mLineWidth = receiverLineSize.rawValue
                        userSession?.strokes.append(stroke)
                        
                        userSession?.arUndoManager.addUndoableObject(stroke)
                        self.draw3DStrokeFromRemotePoints(allPoints: userSession?.remotePoints ?? [], stroke: stroke)
                        userSession?.remotePoints.removeAll()
                        print("Touch end")
                        
                        
                    }
                } else if message.contains(Constants.ChannelMessages.undo) {
                    if let userSession = userSession {
                        self.undoObject(for: userSession)
                    }
                    
                }  else if message.contains(Constants.ChannelMessages.singleTap) {
                    let positionStrArray = message.components(separatedBy: "-")
                    guard positionStrArray.count > 2 else{
                        return
                    }
                    let scale = UIScreen.main.scale

                    print("POS STR \(positionStrArray)")
                    if  let positionStr = positionStrArray.last, let userSession = userSession,let selectedAnnotaion = Int(positionStrArray[1])  {
                        let position = NSCoder.cgPoint(for: positionStr)
                        let point = CGPoint(x: position.x * self.sceneView.frame.width , y: position.y * self.sceneView.frame.height
                        )
                        
//                        let scale = UIScreen.main.scale
//                        let point = CGPoint(x: position.x / scale * self.sceneView.frame.width, y: position.y / scale * self.sceneView.frame.height)
                        //try
                        
//                        let totalPoints = position.x + position.y
//                        let tempHeight = self.sceneView.frame.width * totalPoints / self.sceneView.frame.height
//                        let xPos = position.x * totalPoints
//                        let yPos = position.y * tempHeight
//
//                        print(xPos)
//                        print(yPos)
//
//                        let point = CGPoint(x: position.x, y: position.y)

                        //
                        //FIXME: need to dymanic this per user, annotations
                        userSession.selectedAnnotationType = AnnotaionType.allCases[selectedAnnotaion]
                        //                            self.loadAnnotaion(session: userSession, at:point)
                        self.loadAnnotaion(session: userSession, at:point)
                        
                        print("Single tap")
                        
                    }
                }
                else if message.contains(Constants.ChannelMessages.TapTextBox) {
                    print(message)
                    let positionStrArray = message.components(separatedBy: "-")
                    print(positionStrArray)
                    guard positionStrArray.count > 1 else{
                        return
                    }
                    print("POS STR textbox \(positionStrArray)")
                    if let positionStr = positionStrArray.last, let userSession = userSession {
                        let position = NSCoder.cgPoint(for: positionStr)
                        let point = CGPoint(x: position.x * self.sceneView.frame.width, y: position.y * self.sceneView.frame.height)
                        self.AddEmptyTextBox(session: userSession, at: point)
                        print("Single tap text box")
                    }
                }
                else if message.contains(Constants.ChannelMessages.AddTextToTextBox){
                    let positionStrArray = message.components(separatedBy: "-")
                    print(positionStrArray)
                    guard positionStrArray.count > 1 else{
                        return
                    }
                    print("POS STR textbox \(positionStrArray)")
                    if let positionStr = positionStrArray.last, let userSession = userSession,let textBoxString: String? = positionStrArray[1] {
                        let position = NSCoder.cgPoint(for: positionStr)
                        let point = CGPoint(x: position.x * self.sceneView.frame.width, y: position.y * self.sceneView.frame.height)
                        self.loadAnnotaionTextBox(session: userSession, at: point, textSrting: textBoxString ?? "")
                        print("added text on text box")
                    }
                }
                
                else if message.contains(Constants.ChannelMessages.myCallDuration) {
                    if let duration = message.components(separatedBy: Constants.ChannelMessages.myCallDuration).last {
                        
                        //Here we are getting the duration from other users
                        // Now We need to check that if other user duration is greater than self call duration
                        // That means self.call is joined later so we will keep that lower duration
                        // eg
                        // User 1 & User 2 Join the channel and call for 60 second
                        // User 3 Joins the channel for 10 seconds ; User1 sends duration 70(60 + 10) and So do User2
                        // As we (User3) has only talked 10 seconds so we will kepp 10 seconds
                        // Cheers !!
                        
                        let intDuration =  Int(duration) ?? 0
                        let call =  self.selfSession.calls.first(where: {$0.handle == messageFormat.userSettings.userLoginID})
                        call?.duration = intDuration > self.call.duration ? self.call.duration : intDuration
                    }
                    
                }
                //on call chat with RTM
//                else if message.contains(Constants.ChannelMessages.chat) {
//                    if let chatMessage = message.components(separatedBy: Constants.ChannelMessages.chat).last {
//                        (self.chatCard.viewController as? ChatViewController)?.messageRecv(text: chatMessage, fromUser: messageFormat.userSettings.userName, userID: messageFormat.userSettings.userLoginID)
//                        // create a sound ID, in this case its the tweet sound.
//                        let systemSoundID: SystemSoundID = 1009
//                        // to play sound
//                        AudioServicesPlaySystemSound (systemSoundID)
//
//                    }
//                }
                
                else if message.contains(Constants.ChannelMessages.recording) {
                    if  let positionStr = message.components(separatedBy: "-").last {
                        if positionStr.uppercased() == "ON" {
                                                        
                            print("message received recording on %@",string)
                            isRecordingStarted = true
                            startRecording()
                            
                        } else if positionStr.uppercased() == "OFF" {
                            
                            print("message received recording off %@",string)
                            
                            isRecordingStarted = false
                            Stoprecording(isEndcallPush: false)
                        }
                    }
                }
                else if message.contains(Constants.ChannelMessages.lineSize) {
                    if  let positionStr = message.components(separatedBy: "_").last {
                        if positionStr.uppercased() == "S" {
                            //                                strokeSize = .small
                            receiverLineSize = .small
                            print("message received line size S - %@",string)
                            
                        } else if positionStr.uppercased() == "M" {
                            //                                strokeSize = .medium
                            receiverLineSize = .medium
                            print("message received line size M - %@",string)
                            
                        }else if positionStr.uppercased() == "L" {
                            //                                strokeSize = .large
                            receiverLineSize = .large
                            print("message received line size L - %@",string)
                            
                        }
                    }
                }

            }
        }else{
            let message = string
            print("%@ message string", message)
           
            if message.contains(Constants.ChannelMessages.screenShareNonAR) {
                // if sender is android and their device is nonnAR
                isFromNonARDevice = true
                self.viewToDisplayAR.isUserInteractionEnabled = false
                SideButtonHideForNonARSender()
            }
            
            if message.contains(Constants.ChannelMessages.screenShare) {
                canStartRecording = true
                // Other person is going to share the screen (we are passsing reverse data)
                if message == Constants.ChannelMessages.screenShareME {
                    self.selfSession.isSharingSession = false
                    self.sceneView.session.pause()
                    self.sceneView.isHidden = true
                    self.displayLink?.invalidate()
                    self.sceneView.delegate = nil
                    self.sceneView.session.delegate = nil
                    self.call.outgoing = false
                    self.toggleARViewForScreenSharingType()
                    
                    AppGlobalManager.sharedInstance.isScreenShareME = true
                    
                    // sceneView.removeFromSuperview()
                    //  self.setupViewForScreenShare(addSharingView: true)
                } else {
                   
                    self.selfSession.isSharingSession = true
                    // We will going to share the screen
                    AppGlobalManager.sharedInstance.isScreenShareME = false
                    
//                    let scale = UIScreen.main.scale

                    let messageTRY = "{\(Int(round(self.sceneView.frame.width))),\(Int(round(self.sceneView.frame.height)))}"
                    let message = "\(Constants.ChannelMessages.screensizeshareSenderFreezeMode)-\(messageTRY)"
                    let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
                    self.sendText(string: string)
//
                }
            } else if message.contains(Constants.ChannelMessages.drawingFinshed) {
                // This message recived when we finished drawing all_points
                self.viewToDisplayAR.viewWithTag(1699)?.removeFromSuperview()
            }
                
            else if message.contains(Constants.ChannelMessages.whoIsPublisher) {
                
                print("Check who is publisher message received")
                
                if self.selfSession.isSharingSession {
                    print("check I AM PUBLISHER MESSAGE SENT")
                    self.sendText(string: Constants.ChannelMessages.i_am_publisher + "\(self.selfSession.userID)")
                    
                }
                
            }  else if message.contains(Constants.ChannelMessages.i_am_publisher) {
                print("Check I am publisher message received")
                
                if let userID = message.components(separatedBy: Constants.ChannelMessages.i_am_publisher).last {
                    //self.fullSession = self.videoSessions.first(where:{$0.uid == Int64(userID)})
                    if let sessionIndex = self.activeSessions.firstIndex(where: {$0.userID == Int64(userID)}) {
                        self.activeSessions[sessionIndex].isFullSession = true
                        self.fullSession = self.activeSessions[sessionIndex].videoSession
                        
                        self.rtcEngine.setupRemoteVideo(self.fullSession!.canvas)
                    }
                }
            }   else if message.contains(Constants.ChannelMessages.user_joined) {
                /// This will be called when broadcaster adds the other person and calls the user joined
                /// when we receive this we add to our calls array so we can end it later and save to coredata
                //group call user names
                print("Check user_joined message received")
                
                print("User_joined message %@",message.components(separatedBy: Constants.ChannelMessages.user_joined).last as Any)
                
                if let callInfo = message.components(separatedBy: Constants.ChannelMessages.user_joined).last,
                    let handle = callInfo.components(separatedBy: "-").first,
                    let phoneNumber = callInfo.components(separatedBy: "-").last,
                    handle != AppGlobalManager.sharedInstance.currentUserID {
                    
                    let call  = Call(uuid: UUID(), handle: handle, callName: phoneNumber, date: Date())
                    self.selfSession.calls.appendNewCall(call: call)
                    
//                    self.activeUserNames = self.selfSession.calls.map{$0.callName}
//                    self.colUserName.reloadData()
                    
                    self.DictActiveUsernames["UserName"] = callInfo.components(separatedBy: "-").last
                    self.DictActiveUsernames["MobileNumber"] = callInfo.components(separatedBy: "-").first
                    self.DictActiveUsernames["uid"] = "202020"
//                    self.arrayUserNames.append(self.DictActiveUsernames)
                    if self.arrayUserNames.filter({$0["MobileNumber"] as? String ?? "" == callInfo.components(separatedBy: "-").first}).count == 0 {
                        self.arrayUserNames.append(self.DictActiveUsernames)
                    }
                    print("\n &&&&&&&&&&& User_Joined Active user name array : %@",self.arrayUserNames)
                    self.colUserName.reloadData()
                    self.view.setNeedsLayout()
                    
                    
//                    canStartRecording = true
                }
            }
//            if message.contains(Constants.ChannelMessages.screensizeshareSenderFreezeMode){
//                print(message)
//                // receiver receives this message
//
//                print("Screen share sender side message received *************************************")
//
//                if  let positionStr = message.components(separatedBy: "-").last {
//                    print(positionStr)
//
//                    let position = NSCoder.cgSize(for: positionStr)
//                    let point = CGSize(width: position.width, height: position.height)
//                    senderVideoSize = point
//                    if(senderVideoSize.width == 0.0){
//                        if(self.selfSession.isSharingSession){
//                          // sender
//                        }else{
//                            self.changeAspectRationAtReceiverSide()
//                        }
//                        print(senderVideoSize)
//                    }
//
//                }
//            }
//            if message.contains(Constants.ChannelMessages.screensizeshareReceiverFreezeMode){
//                // Sender receives this message
//
//                print(message)
//                print("Screen share REC side message received *************************************")
//
//                if  let positionStr = message.components(separatedBy: "-").last {
//                    print(positionStr)
//
//                    let position = NSCoder.cgSize(for: positionStr)
//                    let point = CGSize(width: position.width, height: position.height)
//                    rec_w_h = point
//                    print(rec_w_h)
//                }
//            }
        }//else end
    }
 
    //--------------------------------------------------------------------------------
    
    //MARK:-  Setup ARView
    
    //--------------------------------------------------------------------------------
    
    func setupARView () {
        if ARWorldTrackingConfiguration.isSupported {
            
            sceneView.delegate = self
            sceneView.preferredFramesPerSecond = 30
            sceneView.contentScaleFactor = 1
            sceneView.session.delegate = self
            //sceneView.automaticallyUpdatesLighting = true
            //   sceneView.autoenablesDefaultLighting = true
            sceneView.debugOptions = []
            
            //    sceneView.scene.lightingEnvironment.contents = "ARAssest.scnassets/Environment_CUBE.jpg"
            
            let config = ARWorldTrackingConfiguration()
            config.planeDetection = [.horizontal,.vertical]
            
            
            sceneView.session.run(config, options: ARSession.RunOptions.removeExistingAnchors)
            setupDisplayLink()
            
            hitNode = SCNNode()
            
            //            hitNode?.geometry = SCNSphere(radius: 0.1)
            //            hitNode?.geometry?.firstMaterial?.diffuse.contents = UIColor.cyan
            
            hitNode!.position = SCNVector3Make(0, 0, -2)
            sceneView.pointOfView?.addChildNode(hitNode!)
            
        } else {
        
            self.showToast("Your device does not support Augmented reality feature.", type: .fail)
            //  self.showAlert(withMessage: "Your device does not supports Augmented reality feature.")
        }
        
    }
    
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Setup Display Link
    
    //--------------------------------------------------------------------------------
    
    
    private func setupDisplayLink () {
        
        self.displayLink = CADisplayLink(target: self, selector: #selector(displayLinkDidFire(link:)))
        self.displayLink?.preferredFramesPerSecond = self.sceneView.preferredFramesPerSecond
        self.displayLink?.add(to: .main, forMode: .common)
    }
    
    //--------------------------------------------------------------------------------
    
    @objc private func displayLinkDidFire(link:CADisplayLink) {
        var screenShot = UIImage()
//        let scale = UIScreen.main.scale

//        if(isFreezeModeOn){
//            print(rec_w_h.width)
//            if((rec_w_h.width) <= 1920){
//                screenShot = imgViewScreenshot.isHidden ? self.sceneView.snapshot() : (imgViewScreenshot.image ?? self.sceneView.snapshot())
//
//                screenShot = screenShot.resizedImage(width: rec_w_h.width, height: rec_w_h.height) ?? screenShot
//            }else{
//                screenShot = imgViewScreenshot.isHidden ? self.sceneView.snapshot() : (imgViewScreenshot.image ?? self.sceneView.snapshot())
//                screenShot = screenShot.resizedImage(width: rec_w_h.width, height: rec_w_h.height) ?? screenShot
//                print(screenShot.resizedImage(width: rec_w_h.width, height: rec_w_h.height))
//
//            }
//        }else{
            if selfSession.isSharingSession == false {
                //receiver
                screenShot = self.sceneView.snapshot()
            } else {
                //sender
                screenShot = imgViewScreenshot.isHidden ? self.sceneView.snapshot() : (imgViewScreenshot.image ?? self.sceneView.snapshot())
            }
//        }
     
        
//        let scale = UIScreen.main.scale
//         if (1080 < (UIScreen.main.bounds.width * scale)) || (1920 < (UIScreen.main.bounds.height * scale)) {
//            screenShot = screenShot.resizedImage() ?? screenShot
//         }
        
        
        //    screenShot = imgViewScreenshot.isHidden ? self.sceneView.snapshot() : (imgViewScreenshot.image ?? self.sceneView.snapshot())
        guard let cgScreenShotImage = screenShot.cgImage else {
            return
        }
        guard let buffer = self.copyPixelbufferFromCGImageProvider(image: cgScreenShotImage) else {
            return
        }
        if self.selfSession.isSharingSession {
            videoSource.sendBuffer(buffer, timestamp: self.sceneView.session.currentFrame?.timestamp ?? 0)
        }
    }
    
    //--------------------------------------------------------------------------------
   
    
    func copyPixelbufferFromCGImageProvider(image: CGImage) -> CVPixelBuffer? {
        let dataProvider: CGDataProvider? = image.dataProvider
        let data: CFData? = dataProvider?.data
        let baseAddress = CFDataGetBytePtr(data!)
        
        /**
         * We own the copied CFData which will back the CVPixelBuffer, thus the data's lifetime is bound to the buffer.
         * We will use a CVPixelBufferReleaseBytesCallback callback in order to release the CFData when the buffer dies.
         **/
        let unmanagedData = Unmanaged<CFData>.passRetained(data!)
        var pixelBuffer: CVPixelBuffer? = nil
        let status = CVPixelBufferCreateWithBytes(nil,
                                                  image.width,
                                                  image.height,
                                                  kCVPixelFormatType_32BGRA,
                                                  UnsafeMutableRawPointer( mutating: baseAddress!),
                                                  image.bytesPerRow,
                                                  { releaseContext, baseAddress in
                                                    let contextData = Unmanaged<CFData>.fromOpaque(releaseContext!)
                                                    contextData.release() },
                                                  unmanagedData.toOpaque(),
                                                  nil,
                                                  &pixelBuffer)
        if (status != kCVReturnSuccess) {
            return nil;
        }
        
        return pixelBuffer
    }
    
    //--------------------------------------------------------------------------------
    
    func sendText(string:String) {
        rtmChannelManager?.sendString(string: string, completion: { (errorCode) in
            print("RTM MESSAGE SENT WITH \(errorCode.rawValue)")
        })
      
    }
    
    //--------------------------------------------------------------------------------
    
    
    func sendData(data:Data) {
        if let string = String(data:data,encoding: .utf8) {
            sendText(string: string)
        }
    }
    
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Toggle view for screen sharing
    
    //--------------------------------------------------------------------------------
    
    func toggleARViewForScreenSharingType() {
        //STEP4: self session isSharing property
        //FIXME: un comment ARWorldTrackingConfiguration.isSupported
        // if !ARWorldTrackingConfiguration.isSupported {
        if  !call.outgoing {
            //receiver
            self.selfSession.isSharingSession = false
            self.isSharingMyScreen = false
            self.sceneView.session.pause()
            self.sceneView.isHidden = true
            self.displayLink?.invalidate()
            self.btnTrash.isHidden = true
            self.btnFlash.isHidden = true
            self.view_featurePoints.isHidden = true
            self.viewToDisplayAR.isUserInteractionEnabled = true
            
            pinchGestureForFreezeMode = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchRecognized(pinch:)))
            pinchGestureForFreezeMode.delegate = self
            self.viewToDisplayAR.addGestureRecognizer(pinchGestureForFreezeMode)

            panGestureForFreezeMode = UIPanGestureRecognizer(target: self, action: #selector(self.PangestureMethod(gestureRecognizer:)))
            panGestureForFreezeMode.delegate = self
            panGestureForFreezeMode.minimumNumberOfTouches = 2
            panGestureForFreezeMode.maximumNumberOfTouches = 2
            self.viewToDisplayAR.addGestureRecognizer(panGestureForFreezeMode)
           
        } else {
            //sender

            self.isSharingMyScreen = true
            self.selfSession.isSharingSession = true
            self.btnTrash.isHidden = false
            self.btnFlash.isHidden = false
            self.view_featurePoints.isHidden = false
            
            pinchGestureForFreezeMode = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchRecognized(pinch:)))
            pinchGestureForFreezeMode.delegate = self
            self.imgViewScreenshot.addGestureRecognizer(pinchGestureForFreezeMode)

            panGestureForFreezeMode = UIPanGestureRecognizer(target: self, action: #selector(self.PangestureMethod(gestureRecognizer:)))
            panGestureForFreezeMode.delegate = self
            panGestureForFreezeMode.minimumNumberOfTouches = 2
            panGestureForFreezeMode.maximumNumberOfTouches = 2
            self.imgViewScreenshot.addGestureRecognizer(panGestureForFreezeMode)
        }
       
        enableDisasbleGroupCall()
    }
    
    func SideButtonHideForNonARSender(){
        if(isFreezeModeOn){
            btnUndo.isEnabled = true
            btnAnnotaion.isEnabled = true
            btnAddText.isEnabled = true
            btnLineColorChange.isEnabled = true
            btnMainLineWidth.isEnabled = true
            btnFlash.isEnabled = true
        }else{
            if(isFromNonARDevice){
                btnUndo.isEnabled = false
                btnAnnotaion.isEnabled = false
                btnAddText.isEnabled = false
                btnLineColorChange.isEnabled = false
                btnMainLineWidth.isEnabled = false
                btnFlash.isEnabled = false
            }else{
                btnUndo.isEnabled = true
                btnAnnotaion.isEnabled = true
                btnAddText.isEnabled = true
                btnLineColorChange.isEnabled = true
                btnMainLineWidth.isEnabled = true
                btnFlash.isEnabled = true
            }
        }
        
    }
    //--------------------------------------------------------------------------------
    
    //MARK:-  Gesture recognizer(pinch and pan)
    
    //--------------------------------------------------------------------------------
    
    @objc func pinchRecognized(pinch: UIPinchGestureRecognizer) {
        
        // unwrap the view from the gesture
        // AND
        // unwrap that view's superView
        guard let piece = pinch.view,
              let superV = piece.superview
        else {
            return
        }
        
        // this is a bit verbose for clarity
        
        // get the new scale
        let sc = pinch.scale
        
        // get current frame of "piece" view
        let currentPieceRect = piece.frame
        
        // apply scaling transform to the rect
        let futureRect = currentPieceRect.applying(CGAffineTransform(scaleX: sc, y: sc))
        
        // if the resulting rect's width will be
        //  greater-than-or-equal to superView's width
        //        if futureRect.width >= superV.bounds.width {
        //            // go ahead and scale the piece view
        //            piece.transform = piece.transform.scaledBy(x: sc, y: sc)
        //        }
        if futureRect.width >= superV.bounds.width {
            // go ahead and scale the piece view
            piece.transform = piece.transform.scaledBy(x: sc, y: sc)
            if sc >= 1 {
                UIView.animate(withDuration: 0.5) {
                    piece.center = superV.center
                }
            }
        }
        pinch.scale = 1
    
        AppGlobalManager.sharedInstance.extraPaddingXTest = viewToDisplayAR.frame.minX
        AppGlobalManager.sharedInstance.extraPaddingYTest = viewToDisplayAR.frame.minY
        
        print("Img screen shot frame : \(imgViewScreenshot.frame)")
        
        print(viewToDisplayAR.frame)
    }
    @objc func PangestureMethod(gestureRecognizer: UIPanGestureRecognizer){
        
        // unwrap the view from the gesture
        // AND
        // unwrap that view's superView
        
     
        guard let piece = gestureRecognizer.view,
              let superV = piece.superview
        else {
            return
        }
        
     
        let translation = gestureRecognizer.translation(in: superV)
        
        if gestureRecognizer.state == .began {
            self.initialCenter = piece.center
        }
        if gestureRecognizer.state != .cancelled {

            // what the new centerX and centerY will be
            var newX: CGFloat = piece.center.x + translation.x
            var newY: CGFloat = piece.center.y + translation.y

            // MAX centerX is 1/2 the width of the piece's frame
            let mxX = piece.frame.width * 0.5
            
            // MIN centerX is Width of superView minus 1/2 the width of the piece's frame
            let mnX = superV.bounds.width - piece.frame.width * 0.5
            
            // make sure new centerX is neither greater than MAX nor less than MIN
            newX = max(min(newX, mxX), mnX)
            
            // MAX centerY is 1/2 the height of the piece's frame
            let mxY = piece.frame.height * 0.5
            
            // MIN centerY is Height of superView minus 1/2 the height of the piece's frame
            let mnY = superV.bounds.height - piece.frame.height * 0.5
            
            // make sure new centerY is neither greater than MAX nor less than MIN
            newY = max(min(newY, mxY), mnY)
            
            // set the new center
            piece.center = CGPoint(x: newX, y: newY)
            
            // reset recognizer
            gestureRecognizer.setTranslation(.zero, in: superV)

        }
        else {
            piece.center = initialCenter
        }
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Leave channel method
    
    //--------------------------------------------------------------------------------
    
    // Also called from Appdelegate
    func leaveChannelSaveRecord(completion:@escaping (() -> ())) {
        rtcEngine.leaveChannel {[weak self] (status) in
            // guard let self = self else {return }
            print("VIDEO CHANNEL LEAVE CALLED")
            //   CoreDataManager.saveCall(call:self.call, duration: status.duration)
            
            print( "Total Duration is ",self?.selfSession.calls.map{$0.duration})
            
            //            print(self!.call.callName)
            
            _ =  self?.selfSession.calls.map{CoreDataManager.saveCall(call:$0, duration: $0.duration)}
            //CoreDataManager.saveCall(call:self.selfSession.call, duration: status.duration)
            
            //            for session in self.videoSessions {
            //                session.hostingView.removeFromSuperview()
            //            }
            //            self.videoSessions.removeAll()
            
            
            self?.saveSessionCallsAndEmptyArray(duration: status.duration)
            
            completion()
        }
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Extra method
    
    //--------------------------------------------------------------------------------
    
    //FIXME: use selfSession here for group call
    //STEP5: Show Add Call accroding to  selfSession
    func enableDisasbleGroupCall() {
        //    self.btnAddCall.isHidden = true
        
        if AppGlobalManager.sharedInstance.allowGroup_call == 1{
//            self.btnAddCall.isHidden =  !self.selfSession.isSharingSession
//            print(btnAddCall.isHidden)
            self.btnAddCall.isHidden = true
        }else{
           self.btnAddCall.isHidden = true
        }
    }
//
    
    func saveSessionCallsAndEmptyArray(duration:Int) {
        //
        //        for session in videoSessions {
        //            session.hostingView.removeFromSuperview()
        //        }
        //        videoSessions.removeAll()
        
        for session in self.activeSessions {
            session.videoSession?.hostingView.removeFromSuperview()
        }
        self.activeSessions.removeAll()
        
    }
    
    
    func setStreamType(forSessions sessions: [VideoSession], fullSession: VideoSession?) {
        if let fullSession = fullSession {
            for session in sessions {
                rtcEngine.setRemoteVideoStream(UInt(session.uid), type: (session == fullSession ? .high : .low))
            }
        } else {
            for session in sessions {
                rtcEngine.setRemoteVideoStream(UInt(session.uid), type: .high)
            }
        }
    }
    
    private func undoObject(for session:ARDrawingSession) {
        if var undoObject = session.arUndoManager.undo() {
            if undoObject is Stroke {
                if let lastStroke = session.strokes.last {
                    if let anchor = lastStroke.anchor {
                        sceneView.session.remove(anchor: anchor)
                        if session != self.selfSession {
                            lastStroke.node?.removeFromParentNode()
                            lastStroke.cleanup()
                            if let index = session.strokes.firstIndex(of: lastStroke) {
                                session.strokes.remove(at: index)
                            }
                        }
                    }
                }
            }
            else if undoObject is AnnotationNode {
                undoObject.node?.removeFromParentNode()
            }
            else if undoObject is Points2D {
                if self.selfSession.userID == session.userID {
                    self.tempSenderBufferLocalScreenDrawing?.points2D?.popLast()
                    self.tempSenderBufferLocal.popLast()
                    _ = self.tempSenderDrawLineColor.popLast()
                } else {
                    session.tempSenderBufferLocal.popLast()
                    session.tempSenderBufferLocalScreenDrawing?.points2D?.popLast()
                    session.tempReceiverDrawLineColor.popLast()
                }
            }
            else if undoObject is Annotation2D {
                if self.selfSession.userID == session.userID {
                    self.tempSenderAryAnnotationData?.popLast()
                } else {
                    session.tempSenderAryAnnotationData?.popLast()
                }
            }
        }
    }
    
    func Endcall(){
        AppGlobalManager.sharedInstance.linkURL = ""

        if(isRecordingStarted){
            let message = "\(Constants.ChannelMessages.recording)-OFF"
            let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
            self.sendText(string: string)
                              
            Stoprecording(isEndcallPush: false)
        }
        if let alertVC = self.presentedViewController as? UIAlertController {
            alertVC.dismiss(animated: true, completion: nil)
        }
        let viewController = self.presentedViewController
        if viewController is SaveSSVC {
            self.dismiss(animated: true, completion: nil)
        }
        AppGlobalManager.sharedInstance.isRejectedBy = true
        self.status = .ideal
        
        (UIApplication.shared.delegate as! AppDelegate).pushManager.callInviteService(state: .callRejected) { (state) in
            if state {
                AppGlobalManager.sharedInstance.incommingCall.removeAll()
            }else {
                UtilityClass.showAlert(title: "", message: "Something went wrong") { (act) in
                }
            }
        }
        isJoinChannel = false
        self.storeCallApi()
        self.dismiss(animated: true, completion: nil)
    }
   
 
    // handle notification
    @objc func callInviteRejectedAction(_ notification: NSNotification) {
        AppGlobalManager.sharedInstance.linkURL = ""

        print(notification.userInfo ?? "")
        
        //            print(notification.userInfo!["call_frm"] as? String ?? "")
        //            userInfoCallFrom = notification.userInfo!["call_frm"] as? String
        
        if let dict = notification.userInfo as NSDictionary? {
            userInfoCallFrom = dict["call_frm"] as? String
            userInforCallerName = dict["caller_name"] as? String
        }
        // change status rejected from push or not
        
        if(isRecordingStarted){
            print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% recording Stopped through call reject notification")
            view_timer.isHidden = true
            stopwatch.stop()

            if(recordingSTART){
                //Yes no alert
                view_timer.isHidden = true
                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                    
                    let message = "\(Constants.ChannelMessages.recording)-OFF"
                    let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
                    self.sendText(string: string)
                    
                    self.Stoprecording(isEndcallPush: true)

                    if !self.call.outgoing{
                        //receiver
                        self.recordingOnCallEndReceiver = true
                    }
                    
                }
                let noAction = UIAlertAction(title: "No", style: .default) { (action) in
                    let message = "\(Constants.ChannelMessages.recording)-OFF"
                    let string = MessageFormatter.stringFromMessageFormat(object: self.preparePointsHelper(message: message))
                    self.sendText(string: string)
                    
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()

                    self.Stoprecording(isEndcallPush: false)
                    self.callinviteServiceForReject()
                    self.buttonCloseAction(self.btnClose)
                }
                self.showAlert(withMessage: "User has disconnected call. Do you want to save recording with proper tagging?", customActions: [yesAction,noAction])
            }
            
        }else{
            //Group call new code
            
//            selfSession.calls = selfSession.calls.filter{$0.handle != userInfoCallFrom}
//
//            if(AppGlobalManager.sharedInstance.incommingCall.first?.callFrom == userInfoCallFrom){
//                //if rejected by sender
//                if self.selfSession.isSharingSession {
//                    if arrayUserNames.count != 0 {
//                        return
//                    }
//                }
//
//                self.buttonCloseAction(self.btnClose)
//                self.dismiss(animated: true, completion: nil)
//                if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
//                    let vc = LoginVC.viewController()
//                    AppDelegate.shared.window?.rootViewController = vc
//                }
//            }else{
//                if arrayUserNames.count != 0 {
//                    return
//                }
//                let allReceiverSession = activeSessions.filter{$0.userID != Int(userInfoCallFrom)!}
//                if(allReceiverSession.count == 1)
//                {
//                    self.buttonCloseAction(self.btnClose)
//                    self.dismiss(animated: true, completion: nil)
//                    if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
//                        let vc = LoginVC.viewController()
//                        AppDelegate.shared.window?.rootViewController = vc
//                    }
//                }
//            }
//
//            print(activeSessions.count)
            
            
            // one to one call code
            
//                        self.buttonCloseAction(self.btnClose)

            if let alertVC = self.presentedViewController as? UIAlertController {
                       alertVC.dismiss(animated: true, completion: nil)
                   }
            let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                self.buttonCloseAction(self.btnClose)
                self.dismiss(animated: true, completion: nil)
                if AppGlobalManager.sharedInstance.loggedInUser?.user?.is_guest ?? false{
                    let vc = LoginVC.viewController()
                    AppDelegate.shared.window?.rootViewController = vc
                }
            }
            self.showAlert(withMessage: "Thanks for using Plutomen ARMS", title: "\(userInforCallerName ?? "User") has ended the call session.", customActions: [yesAction])
        }
    }
   
    @objc func saveVidoActionCallEnd() {
         self.buttonCloseAction(self.btnClose)
        print("saveVidoActionCallEnd 000000000000000000")
    }

    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
        timer = nil
    }
   
    @objc func endcallTimer() {
        timerCount += 1
        if timerCount == 10 {
            let ok = UIAlertAction(title: "OK", style: .default) { (act) in
                if self.arrayUserNames.count == 0 {
                    self.timerCount = 0
                    print("Timer fired in AR controller 10 sec")
                    self.timer?.invalidate()
                    self.timer = nil
                    self.leaveChannelSaveRecord { [weak self] in
                        _ = self?.selfSession.calls.map{$0.end()}
                        _ = self?.selfSession.calls.map{AppDelegate.shared.callManager.endCall(call:$0)}
                        AppDelegate.shared.callManager.endCallCXAction(call: self!.call, completion: nil)
                        self?.stopAgoraAndDeallocateResources()
                        self?.dismiss(animated: true, completion: nil)
                    }
                }
            }
            self.showAlert(withMessage: "Another user is offline please try after sometime",customActions: [ok])
        } else if timerCount >= 20 {
            if let alertVC = self.presentedViewController as? UIAlertController {
                alertVC.dismiss(animated: true, completion: nil)
            }
            if self.arrayUserNames.count == 0 {
                self.timerCount = 0
                print("Timer fired in AR controller")
                self.timer?.invalidate()
                self.timer = nil
                self.leaveChannelSaveRecord { [weak self] in
                    _ = self?.selfSession.calls.map{$0.end()}
                    _ = self?.selfSession.calls.map{AppDelegate.shared.callManager.endCall(call:$0)}
                    AppDelegate.shared.callManager.endCallCXAction(call: self!.call, completion: nil)
                    self?.stopAgoraAndDeallocateResources()
                    self?.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    //    @objc func endcallTimer() {
    //        print("Timer fired in AR controller")
    //        timer?.invalidate()
    //        timer = nil
    //
    //        self.leaveChannelSaveRecord { [weak self] in
    //            _ = self?.selfSession.calls.map{$0.end()}
    //            _ = self?.selfSession.calls.map{AppDelegate.shared.callManager.endCall(call:$0)}
    //
    //            AppDelegate.shared.callManager.endCallCXAction(call: self!.call, completion: nil)
    //
    //            self?.stopAgoraAndDeallocateResources()
    //            self?.dismiss(animated: true, completion: nil)
    //        }
    //    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
//    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        return .portrait
//    }
    
    deinit {
        print(" AR View controller de allocated ")
        AppGlobalManager.sharedInstance.uidScreenShareArray = []
        AppGlobalManager.sharedInstance.uidArray = []
    }
    func stopAgoraAndDeallocateResources() {
        //Release memory
        if rtcEngine != nil {
            rtcEngine.stopPreview()
            rtcEngine.disableVideo()
            rtcEngine.disableAudio()
            rtcEngine.disableExternalAudioSource()
            rtcEngine.enableDualStreamMode(false)
        }
        self.rtmChannelManager?.leaveChannel(completion: { (errorCode) in
            print("RTM Channel Leaved with code \(errorCode.rawValue)")
        })
        self.rtmChannelManager?.logout()
        self.rtmChannelManager?.messageListener = nil
        self.rtmChannelManager = nil
        // self.agoraSignalManager?.leaveChannel()
        // self.agoraSignalManager?.signalStatusReceived = nil
        //  self.agoraSignalManager = nil
        //    //TODO:
        //    rtcEngine.leaveChannel {[weak self] (status) in
        //      guard let self = self else {return }
        //      print(“VIDEO CHANNEL LEAVE CALLED”)
        //      CoreDataManager.saveCall(call:self.call, duration: status.duration)
        //
        //      AgoraRtcEngineKit.destroy()
        //
        //    }
        if self.sceneView != nil {
            self.sceneView.session.pause()
            self.sceneView.delegate = nil
        }
        self.displayLink?.invalidate()
        AgoraRtcEngineKit.destroy()
        // ADDED THIS LINE TO TEST
    }
    func convertViewBasedOnAspectRatio(){
        if selfSession.isSharingSession == true{
            //sender

            imgViewScreenshot.center = self.view.center
            imgViewScreenshot.frame.size.height = UIScreen.main.bounds.height
            imgViewScreenshot.frame.size.width = UIScreen.main.bounds.width
            imgScreenShotWidth.constant = UIScreen.main.bounds.width
            imgScreenShotHeight.constant = UIScreen.main.bounds.height
            imgViewScreenshot.layoutIfNeeded()
        }else{
            //receiver
            let rect = AppGlobalManager.sharedInstance.changeAspectRationAtReceiverSide()
            viewToDisplayAR.center = self.view.center
            viewToDisplayAR.frame.size.height = rect.height
            viewToDisplayAR.frame.size.width = rect.width
            viewtodisplayARWidth.constant = rect.width
            viewtodisplayARHeight.constant = rect.height
            viewToDisplayAR.layoutIfNeeded()

            imgViewScreenshot.center = self.view.center
            imgViewScreenshot.frame.size.height = rect.height
            imgViewScreenshot.frame.size.width = rect.width
            imgScreenShotWidth.constant = rect.width
            imgScreenShotHeight.constant = rect.height
            imgViewScreenshot.layoutIfNeeded()

        }
        
    }
    
//    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        if call.outgoing == true {
//            if myOrientation == .vertical {
//                sideStackView.axis = .vertical
//                let message = "\(Constants.ChannelMessages.orientationPortrait)"
//                let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
//                print("String send for Screen Orientations Vertical %@",string)
//                self.sendText(string: string)
//            }
//            else {
//                sideStackView.axis = .horizontal
//                let message = "\(Constants.ChannelMessages.orientationLandscape)"
//                let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
//                print("String send for Screen Orientations Horizontal %@",string)
//                self.sendText(string: string)
//            }
//            return UIDevice.current.userInterfaceIdiom == .phone ? [.portrait] : .all
//        } else {
//            return UIDevice.current.userInterfaceIdiom == .phone ? [.portrait] : .all
//        }
//    }
      override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
          return .portrait
      }
}
//extension ARController: UINavigationControllerDelegate {
//    public func navigationControllerSupportedInterfaceOrientations(_ navigationController: UINavigationController) -> UIInterfaceOrientationMask {
//        return navigationController.topViewController!.supportedInterfaceOrientations
//    }
//}
//MARK:- Collection view delegate and data source methods

extension ARController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionViewColorPicker == collectionView {
            return allColorAry.count
        }
        return self.arrayUserNames.count
//        return self.activeUserNames.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionViewColorPicker == collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorPickerCollectionViewCell", for: indexPath) as! ColorPickerCollectionViewCell
            cell.viewColor.tag = indexPath.row
            cell.viewColor.backgroundColor = allColorAry[indexPath.row]
            cell.viewColor.addTarget(self, action: #selector(self.didColorSelect(_:)), for: .touchUpInside)
            UtilityClass.corner(vw: cell.viewColor, size: cell.viewColor.frame.width / 2)
            UtilityClass.cornerWithBorder(vw: cell.viewColor, size: cell.viewColor.frame.width / 2, border: 2, color: UIColor.white)
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "username", for: indexPath) as! UserNameCell
            let username = arrayUserNames[indexPath.row]["UserName"] as? String ?? ""
             cell.lblShortname.text = getFirstLetters(name: username)
             cell.lblShortname.clipsToBounds = true
             cell.lblShortname.layer.cornerRadius = cell.lblShortname.frame.size.height / 2
             let fisrtname = username.components(separatedBy: " ")
             cell.lblUsername.text = fisrtname.first
             return cell
        
//        cell.lblUsername.text = self.activeUserNames[indexPath.row]
//        let username = self.activeUserNames[indexPath.row]
//        cell.lblShortname.text = getFirstLetters(name: username)
//        cell.lblShortname.clipsToBounds = true
//        cell.lblShortname.layer.cornerRadius = cell.lblShortname.frame.size.height / 2
//        let fisrtname = username.components(separatedBy: " ")
//        cell.lblUsername.text = fisrtname.first
//        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionViewColorPicker == collectionView {
            senderLineColor = allColorAry[indexPath.row]
            let colorInString = senderLineColor.toHexString().replacingOccurrences(of: "#", with: "")
            let message = "\(Constants.ChannelMessages.lineColor)-\("#\(colorInString)")"
            let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
            self.sendText(string: string)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionViewColorPicker == collectionView {
            let sz = 25 // (collectionViewColorPicker.frame.width / 5) - 30
            return CGSize(width: sz, height: sz)
        }
        return CGSize(width: 70, height: 75)
    }
    
    @objc func didColorSelect(_ sender: UIButton) {
        print("didColorSelect")
        viewColorPicker.isHidden = true
        isColorViewSelected = false
        
        if  !call.outgoing {
            //receiver
            senderLineColor = allColorAry[sender.tag]
            let colorInString = senderLineColor.toHexString().replacingOccurrences(of: "#", with: "")
            let message = "\(Constants.ChannelMessages.lineColor)-\("#\(colorInString)")"
            let string = MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
            self.sendText(string: string)
            
        }else{
            //sender
            senderLineColor = allColorAry[sender.tag]
        }
        
    }
}
func getFirstLetters(name : String?) -> String{
    var initials = ""
    if let initialsArray = name?.components(separatedBy: " ") {
        if let firstWord = initialsArray.first {
            if let firstLetter = firstWord.first {
                initials += String(firstLetter).capitalized
            }
        }
        if initialsArray.count > 1, let lastWord = initialsArray.last {
            if let lastLetter = lastWord.first {
                initials += String(lastLetter).capitalized
            }
        }
        return initials
    } else {
        return ""
    }
}
extension UIImage {
    //1920 x 1080
    func resized(width:CGFloat,height:CGFloat) -> UIImage? {

//            canvasSize = CGSize(width: 1080, height: 1920)
          let canvasSize = CGSize(width: width, height: height)
        //    if inPixel {
        //      canvasSize = CGSize(width: 1536, height: 2048)
        //    }
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    func resizedImage(width:CGFloat,height:CGFloat) -> UIImage? {
        var resizingImage = self
        resizingImage = resizingImage.resized(width: width, height: height) ?? UIImage()
        return resizingImage
    }
}
//MARK:- Stackview extension

extension UIStackView {
    func addBackground(color: UIColor) {
        if #available(iOS 14.0, *) {
            self.backgroundColor = .clear
            let subView = UIView(frame: self.bounds)
            subView.backgroundColor = color
            subView.alpha = 0.25
            subView.clipsToBounds = true
            subView.layer.cornerRadius = 12
            subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.insertSubview(subView, at: 0)
            
        }
        else {
            let subView = UIView(frame: bounds)
            subView.backgroundColor = color
            subView.alpha = 0.25
            subView.clipsToBounds = true
            subView.layer.cornerRadius = 12
            subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            insertSubview(subView, at: 0)
        }
    }
}
//MARK:- Textfield delegate and datasource methods

extension ARController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn")
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {

       
        print("textFieldDidEndEditing")
    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
////        self.loadAnnotaionTextBox(session: selfSession, at: location!, textSrting:string)
//
//        return true
//    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        if range.length + range.location > currentCharacterCount {
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        if(newLength >= 40)
        {
            self.showToast("You can enter only 40 characters ", type: .info)
        }
        return newLength <= 40
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.text == "Write your custom text here"{
            textField.text = ""
        }
    }
    
    func addToolBar(textField: UITextField){
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = Constants.AppTheme.Colors.ColorsOfApp.green.color
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePressed))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPressed))
        toolBar.setItems([cancelButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true	
        toolBar.sizeToFit()
        
        textField.delegate = self
        textField.inputAccessoryView = toolBar
    }
    @objc func donePressed(){
        for gesture in self.view.gestureRecognizers! {
            gesture.isEnabled = true
        }
        if txtCustomText.text?.count ?? 0 > 0{
            if(isFreezeModeOn){
                if isStartingTextAnnotationonFreezmode == true {
                    isStartingTextAnnotationonFreezmode = false
                    if self.selfSession.isSharingSession {
                        btnUndoTapped(self.btnUndo)
                        
                        let img = selfSession.selectedAnnotationTypeLast.freezeModeImage ?? UIImage()
                        let imageWithText = self.imageWith(name: txtCustomText.text!, fm: CGRect(x: 30, y: 15, width: img.size.width-5, height: img.size.height))
                        
                        let mergerImageWithText = img.mergeImageWithForFreezeModeTextAnnotation(topImage: imageWithText!, position: CGPoint(x: 30, y: 15), sz: img.size)
                        
                        mergerImageWithText.imageAsset?.setValue("TextBox_F", forKey: "_assetName")
                        
                        tempSenderAryAnnotationData?.append(Annotation2DData(points: locationTextBoxFreezeMode, image: mergerImageWithText))
                        var obj = Annotation2D()
                        obj.annotation2D = tempSenderAryAnnotationData
                        selfSession.arUndoManager.addUndoableObject(obj)
                        drawImage(textAnnotationFromFreezeMode: true)
                        
                        self.txtCustomText.isHidden = true
                        self.toolBar.isHidden = true
                        self.txtCustomText.resignFirstResponder()
                        self.txtCustomText.text = ""
                    }else{
                        
                        let message = "\(Constants.ChannelMessages.AddTextToTextBox)-\(txtCustomText.text!)"

                        let string =  MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                        self.sendText(string: string)
                        
                        self.txtCustomText.isHidden = true
                        self.toolBar.isHidden = true
                        self.txtCustomText.resignFirstResponder()
                        self.txtCustomText.text = ""
                    }
                }
               
            }else{
                if self.selfSession.isSharingSession {
                    self.loadAnnotaionTextBox(session: selfSession, at: locationTextBox!, textSrting : txtCustomText.text!)
                    self.txtCustomText.isHidden = true
                    self.toolBar.isHidden = true
                    self.txtCustomText.resignFirstResponder()
                    self.txtCustomText.text = ""
                }else{
                    let message = "\(Constants.ChannelMessages.AddTextToTextBox)-\(txtCustomText.text!)"

                    let string =  MessageFormatter.stringFromMessageFormat(object: preparePointsHelper(message: message))
                    self.sendText(string: string)
                    self.txtCustomText.isHidden = true
                    self.toolBar.isHidden = true
                    self.txtCustomText.resignFirstResponder()
                    self.txtCustomText.text = ""
                    print(string)
                }
            }
        }else{
            self.showToast("Please enter some text", type: .info)
        }
    }
   
    func imageWith(name: String?, fm: CGRect) -> UIImage? {
//         let frame = CGRect(x: 20, y: 20, width: 400, height: 400)
        let frame = fm
         let nameLabel = UILabel(frame: frame)
         nameLabel.textAlignment = .left
         nameLabel.backgroundColor = .clear
         nameLabel.textColor = .white
        nameLabel.lineBreakMode = .byWordWrapping
        nameLabel.numberOfLines = 0
//         nameLabel.font = UIFont(name: Constants.AppTheme.Fonts.ProximaNova.FONT_REGULAR.rawValue,size: 15.0)
        nameLabel.font = UIFont(name:"Georgia-Bold",size: 20.0)
         nameLabel.text = name
         UIGraphicsBeginImageContext(frame.size)
          if let currentContext = UIGraphicsGetCurrentContext() {
             nameLabel.layer.render(in: currentContext)
             let nameImage = UIGraphicsGetImageFromCurrentImageContext()
             return nameImage
          }
          return nil
    }
    @objc func cancelPressed(){
        for gesture in self.view.gestureRecognizers! {
            gesture.isEnabled = true
        }
        
        self.txtCustomText.isHidden = true
        self.toolBar.isHidden = true
        self.txtCustomText.text = ""
        txtCustomText.resignFirstResponder()
        
        isStartingTextAnnotationonFreezmode = false
     
        if(isFreezeModeOn){
            if locationTextBoxFreezeMode != nil {
                btnUndoTapped(self.btnUndo)
            }
        }
        else{
            //sender reciver normal mode
            if locationTextBox != nil{
                btnUndoTapped(self.btnUndo)
            }
        }
    }
}

//Calucate percentage based on given values
public func calculatePercentage(value:Float,percentageVal:Float)->Float{

    let val = value * percentageVal

    return val / 100.0

}
