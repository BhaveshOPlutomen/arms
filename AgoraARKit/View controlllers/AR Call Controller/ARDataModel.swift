//
//  ARModels.swift
//  AgoraARKit
//
//  Created by Prashant on 11/07/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import Foundation

import AgoraRtcKit
import ARKit

enum TrackingMessageType {
    case looking
    case lookingEscalated
    case anchorLost
}

enum DefaultsKeys: String {
    case backgroundDate
}

final class ARDrawingSession:Equatable {
    var calls:[Call] = [] // Only for if current user is sharing the screen.
    var userID:Int64
    var videoSession:VideoSession?
    var strokes = [Stroke]()
    var remotePoints:[CGPoint] = []
    var isFullSession = false
    var isSharingSession = false
    var selectedAnnotationType = ARController.AnnotaionType.allCases.first!
    var selectedAnnotationTypeLast = ARController.AnnotaionType.allCases.last!
    var annotations = [AnnotationNode]()
    var arUndoManager = UndoManager()
    // for Freeze Mode
    var tempSenderBufferLocal:[[CGPoint]] = []
    var tempSenderBufferLocalScreenDrawing: Undoable?
    var tempSenderAryAnnotationData: [Annotation2DData]? = []
    var tempReceiverDrawLineColor:[[UIColor]] = []

    init (userID:Int64,videoSession:VideoSession? = nil) {
        self.userID = userID
        self.videoSession = videoSession
    }
    
    static func == (lhs: ARDrawingSession, rhs: ARDrawingSession) -> Bool {
        return lhs.userID == rhs.userID
    }
}

protocol Undoable {
    var node:SCNNode? {get set}
    var anchor:ARAnchor? {get set}
    var points2D: [[CGPoint]]? {get set}
    var annotation2D: [Annotation2DData]? {get set}
    var lineColor: [[UIColor]]? {get set}
}

struct Points2D: Undoable {
    var lineColor: [[UIColor]]?
    
    var annotation2D: [Annotation2DData]?
    var node: SCNNode?
    var anchor: ARAnchor?
    var points2D: [[CGPoint]]?
    
}

struct Annotation2D: Undoable {
    var lineColor: [[UIColor]]?
    var annotation2D: [Annotation2DData]?
    var node: SCNNode?
    var anchor: ARAnchor?
    var points2D: [[CGPoint]]?
}

struct Annotation2DData {
    var points: CGPoint?
    var image: UIImage?
}

struct AnnotationNode:Undoable {
    var lineColor: [[UIColor]]?
    var annotation2D: [Annotation2DData]?
    var points2D: [[CGPoint]]?
    
    var node: SCNNode?
    var anchor: ARAnchor?
    var annotationType:ARController.AnnotaionType
}

struct UndoManager {
    private var undoableData = [Undoable]()
    
    
    init () {
        self.undoableData = []
    }
    
    var hasObjectToUndo :Bool {
        return !undoableData.isEmpty
    }
    
    
    mutating func addUndoableObject(_ object:Undoable) {
        undoableData.append(object)
    }
    
    mutating func undo () -> Undoable? {
        return  undoableData.popLast()
    }
    
    mutating func clearAllNodes() {
        self.undoableData.removeAll()
        self.undoableData = []
    }
    
    mutating func getAllUndableData() -> [Undoable] {
        return undoableData
    }
    mutating func count() -> Int {
        return undoableData.count
    }
}

extension Array where Element == Call {
    @discardableResult
    mutating func appendNewCall(call:Call) -> Bool {
        if !self.contains(where: {$0.uuid  == call.uuid || $0.handle ==  call.handle}) {
            self.append(call)
            return true
        }
        return false
    }
}

extension Array where Element == ARDrawingSession {
    mutating func findOrInsertSession(userID:Int64) -> ARDrawingSession {
        if let foundSession =  self.first(where: {$0.userID == userID}) {
            return foundSession
        } else {
            let newSession = VideoSession(uid: userID)
            let arDrawingSession = ARDrawingSession(userID: userID, videoSession: newSession)
            self.append(arDrawingSession)
            return arDrawingSession
        }
    }
}


enum ColorDrawing:String ,Codable {
    case white
    case red
    case yellow
    case green
    case blue
    case black
    case gray
}
//TODO: Pending to use this struct
struct UserSettings:Codable {
    
    var userID:Int64
    var selectedColor: ColorDrawing
    var userLoginID:String
    var userName:String
    var userEmail:String
    
}
struct MessageFormat:Codable {
    var userSettings:UserSettings
    var message:String
    
}

struct MessageFormatter {
    
//    static private func messageFormatFromJSON(json:[String:Any]) -> MessageFormat {
//        do {
//            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
//            let object =  try JSONDecoder().decode(MessageFormat.self, from: data)
//            return object
//        } catch {
//            fatalError("Pleae match the JSON with struct format \(error.localizedDescription)")
//        }
//    }
//
//    static private func jsonFromMessageFormat(object:MessageFormat) -> [String:Any]? {
//        do {
//            let jsonData = try JSONEncoder().encode(object)
//            let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String:Any]
//            return json
//        } catch {
//            fatalError("Pleae match the JSON with struct format \(error.localizedDescription)")
//        }
//    }
    
    static func stringFromMessageFormat (object:MessageFormat) -> String {
        do {
            let jsonData = try JSONEncoder().encode(object)
            return String(data: jsonData, encoding: .utf8)!
            
            
        } catch {
            fatalError("Pleae match the JSON with struct format \(error.localizedDescription)")
        }
    }
    
    static func messageFormatObjectFromString (string:String) -> MessageFormat? {
        
        guard let data = string.data(using: .utf8) else {
            return nil
        }
        
        do {
            
            let object = try JSONDecoder().decode(MessageFormat.self, from: data)
            return object
            
        }
        catch let DecodingError.dataCorrupted(context) {
            print(context)
        } catch let DecodingError.keyNotFound(key, context) {
            print("Key '\(key)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch let DecodingError.valueNotFound(value, context) {
            print("Value '\(value)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch let DecodingError.typeMismatch(type, context)  {
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
        } catch {
            print("Unable to decode the Data\(error)")
        }
        catch {
            print("Unable to decode the Data\(error)")
            
        }
        return nil
        
    }
   
    
    
}

extension ARController {
    var prepareUserSettings:UserSettings {
        return UserSettings(userID: self.selfSession.userID,
                            selectedColor: .white,userLoginID:AppGlobalManager.sharedInstance.currentUserID ?? "",
                            userName:AppGlobalManager.sharedInstance.loggedInUser?.user?.name ?? "",
                            userEmail: AppGlobalManager.sharedInstance.loggedInUser?.user?.email ?? "")
    }
    
    func preparePointsHelper(message:String) -> MessageFormat {
       return MessageFormat(userSettings: prepareUserSettings, message: message)
    }
    
    func checkJsonValid(jsonString: String) -> Bool {
//        print("\n\n", #function, "\n \(jsonString) \n")
        if let data = jsonString.data(using: .utf8) {
            do {
                try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
//                 print("Valid Json")
                return true
            } catch {
                print("InValid Json")
                print(error.localizedDescription)
                return false
            }
        }
        print("InValid Json")
        return false
    }
}

/*
 
 private func handleMessageAction(_ string: String) {
 print(" SIGNAL SDK messageReceived ARController \(string)")
 DispatchQueue.main.async {
 
 
 guard let data = string.data(using: .utf8) else {
 return
 }
 
 
 if let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any], let points = json[Constants.ChannelMessages.allPoints] as? [String], !points.isEmpty {
 
 for positionStr in points {
 let position = NSCoder.cgPoint(for: positionStr)
 let point = CGPoint(x: position.x * self.sceneView.frame.width, y: position.y * self.sceneView.frame.height)
 self.remotePoints.append(point)
 }
 
 } else  if let message = String(data: data, encoding: .utf8)   {
 
 if message.contains(Constants.ChannelMessages.screenShare) {
 
 // Other person is going to share the screen (we are passsing reverse data)
 if message == Constants.ChannelMessages.screenShareME {
 self.selfSession.isSharingSession = false
 self.sceneView.session.pause()
 self.sceneView.isHidden = true
 self.displayLink?.invalidate()
 // sceneView.removeFromSuperview()
 
 //  self.setupViewForScreenShare(addSharingView: true)
 } else {
 self.selfSession.isSharingSession = true
 // We will going to share the screen
 
 }
 } else if message.contains(Constants.ChannelMessages.drawingFinshed) {
 // This message recived when we finished drawing all_points
 self.viewToDisplayAR.viewWithTag(1699)?.removeFromSuperview()
 
 }  else if message.contains(Constants.ChannelMessages.touchBegin) {
 if  let positionStr = message.components(separatedBy: "-").last {
 
 self.showToast("Other user has started drawing you should keep your device steady ", type: .success)
 
 let position = NSCoder.cgPoint(for: positionStr)
 let point = CGPoint(x: position.x * self.sceneView.frame.width, y: position.y * self.sceneView.frame.height)
 //   self.hanldeTouchBegin(touchInView: point)
 
 self.remotePoints.append(point)
 
 }
 } else if message.contains(Constants.ChannelMessages.touchMove) {
 if  let positionStr = message.components(separatedBy: "-").last {
 let position = NSCoder.cgPoint(for: positionStr)
 let point = CGPoint(x: position.x * self.sceneView.frame.width, y: position.y * self.sceneView.frame.height)
 self.remotePoints.append(point)
 
 }
 }else if message.contains(Constants.ChannelMessages.touchEnd) {
 if  let positionStr = message.components(separatedBy: "-").last {
 let position = NSCoder.cgPoint(for: positionStr)
 let point = CGPoint(x: position.x * self.sceneView.frame.width, y: position.y * self.sceneView.frame.height)
 // self.hanleTouchEnd()
 
 self.remotePoints.append(point)
 
 self.draw3DStrokeFromRemotePoints(allPoints: self.remotePoints)
 self.remotePoints.removeAll()
 self.remoteStroke = nil
 
 
 
 }
 }  else if message.contains(Constants.ChannelMessages.singleTap) {
 if  let positionStr = message.components(separatedBy: "-").last {
 let position = NSCoder.cgPoint(for: positionStr)
 let point = CGPoint(x: position.x * self.sceneView.frame.width, y: position.y * self.sceneView.frame.height)
 
 //FIXME: need to dymanic this per user, annotations
 self.loadArrowAndAddTo(drawingSessionUserID: self.selfSession.userID, selfSession: true, at:point)
 
 }
 } else if message.contains(Constants.ChannelMessages.endCall) {
 AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
 
 self.leaveChannelSaveRecord {[unowned self] in
 _ = self.selfSession.calls.map{AppDelegate.shared.callManager.endCall(call:$0)}
 //     AppDelegate.shared.callManager.endCall(call: self.selfSession.call)
 //     AppDelegate.shared.callManager.endCall(call: self.call)
 self.stopAgoraAndDeallocateResources()
 
 self.dismiss(animated: true, completion: nil)
 
 }
 
 }  else if message.contains(Constants.ChannelMessages.whoIsPublisher) {
 
 if self.selfSession.isSharingSession {
 print("I AM PUBLISHER MESSAGE SENT")
 self.sendText(string: Constants.ChannelMessages.i_am_publisher + "\(self.selfSession.userID)")
 }
 
 }  else if message.contains(Constants.ChannelMessages.i_am_publisher) {
 
 if let userID = message.components(separatedBy: Constants.ChannelMessages.i_am_publisher).last {
 //self.fullSession = self.videoSessions.first(where:{$0.uid == Int64(userID)})
 if let sessionIndex = self.activeSessions.firstIndex(where: {$0.userID == Int64(userID)}) {
 
 self.activeSessions[sessionIndex].isFullSession = true
 self.fullSession = self.activeSessions[sessionIndex].videoSession
 
 self.rtcEngine.setupRemoteVideo(self.fullSession!.canvas)
 
 }
 
 
 }
 }   else if message.contains(Constants.ChannelMessages.user_joined) {
 /// This will be called when broadcaster adds the other person and calls the user joined
 /// when we receive this we add to our calls array so we can end it later and save to coredata
 
 if let handle = message.components(separatedBy: Constants.ChannelMessages.user_joined).last, handle != AppGlobalManager.sharedInstance.currentUserID {
 //self.fullSession = self.videoSessions.first(where:{$0.uid == Int64(userID)})
 
 //                        let call  = AppDelegate.shared.callManager.startCall(handle: handle, videoEnable: true)
 let call  = Call(uuid: UUID(), handle: handle, date: Date())
 self.selfSession.calls.appendNewCall(call: call)
 
 }
 }
 }
 }
 }
 
 */

