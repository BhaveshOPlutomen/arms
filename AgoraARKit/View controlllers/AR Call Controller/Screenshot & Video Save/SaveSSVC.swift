//
//  SaveSSVC.swift
//  AgoraARKit
//
//  Created by Hetali on 14/10/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SaveSSVC: UIViewController,Toastable {
    
    @IBOutlet weak var imgSS: UIImageView!
    
    @IBOutlet weak var txtSSDescription: UITextField!
    
    @IBOutlet weak var viewMainSS: UIView!
    
    @IBOutlet weak var viewSubSS: UIView!
    
    var imageScreenShot : UIImage?
    
    var roomname : String?
    
    @IBOutlet weak var tblCategory: UITableView!
    @IBOutlet weak var viewCategory: UIView!
    
    @IBOutlet weak var btnSelectcategory: UIButton!
    
    @IBOutlet weak var colTags: UICollectionView!
    
    var selectedCategoryIndex : Int?
    
    var tagsIndexArray : [Int]?
    
    var tagsIndexArrayString : [String]?
    
    var selectedCategory : String?
    
    enum  MediaType {
        case saveScreenshot
        case saveVideo
    }
    enum  StorecallAction {
          case saveAndStorecall
          case normalSave
      }
    
    @IBOutlet weak var btnSaveScreenshot: UIButton!

    @IBOutlet weak var btnShareScreenshot: UIButton!

    @IBOutlet weak var btnSaveVideoRecording: UIButton!
    
    @IBOutlet weak var imgThumbnail: UIImageView!

    var thumb_url : String? = nil
    
    var recording_id : Int? = 0
    
    var mediaType:MediaType = .saveScreenshot
    
    var storecallAction:StorecallAction = .normalSave

    let columnLayout = CustomViewFlowLayout()

    @IBOutlet weak var Cons_subviewHeight: NSLayoutConstraint!
    @IBOutlet weak var Cons_colViewheight: NSLayoutConstraint!
    
    @IBOutlet weak var Cons_btnselectcatheight: NSLayoutConstraint!
    
    override func viewWillAppear(_ animated: Bool) {
         tagsIndexArray = []
        tagsIndexArrayString = []
               
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            self.overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }

        Cons_subviewHeight.constant = 470
        Cons_colViewheight.constant = 80
        Cons_btnselectcatheight.constant = 30
        
        if mediaType == .saveScreenshot{
            btnSaveScreenshot.isHidden = false
            btnShareScreenshot.isHidden = false
            btnSaveVideoRecording.isHidden = true
            imgSS.isHidden = false
            imgThumbnail.isHidden = true
            imgSS.image = imageScreenShot
        }else{
            btnSaveScreenshot.isHidden = true
            btnShareScreenshot.isHidden = true
            btnSaveVideoRecording.isHidden = false
            imgSS.isHidden = true
            imgThumbnail.isHidden = false
            imgThumbnail.sd_setImage(with: URL(string: thumb_url!), placeholderImage:#imageLiteral(resourceName: "placeholder"), options: .highPriority, completed: nil)
        }
        
        txtSSDescription.placeholder = "Write your text"
        txtSSDescription.text = ""
        viewMainSS.isHidden = false
        viewSubSS.clipsToBounds = true
        viewSubSS.layer.cornerRadius = 8
        
        viewCategory.isHidden = true
        viewCategory.clipsToBounds = true
        viewCategory.layer.cornerRadius = 8.0
        viewCategory.layer.borderWidth = 0.8
        viewCategory.layer.borderColor = UIColor.black.cgColor
        
        tblCategory.separatorStyle = .none
        
        selectedCategory = "Select Category"
        txtSSDescription.becomeFirstResponder()
        
        btnSelectcategory.clipsToBounds = true
        btnSelectcategory.layer.cornerRadius = 5.0
        
        tagsIndexArray = []
        tagsIndexArrayString = []
        
        colTags.collectionViewLayout = columnLayout
        colTags.contentInsetAdjustmentBehavior = .always
        
        colTags.reloadData()
        colTags.allowsMultipleSelection = true
        
        btnSelectcategory.isHidden = false
        colTags.isHidden = false
        
        if(AppGlobalManager.sharedInstance.syncLoginResponse?.category?.count == 0){
            btnSelectcategory.isHidden = true
            Cons_subviewHeight.constant = Cons_subviewHeight.constant - Cons_btnselectcatheight.constant
            Cons_btnselectcatheight.constant = 0
        }
        if(AppGlobalManager.sharedInstance.syncLoginResponse?.tags?.count == 0){
            colTags.isHidden = true
            Cons_subviewHeight.constant = Cons_subviewHeight.constant - Cons_colViewheight.constant
            Cons_colViewheight.constant = 0
        }
    }
    class func viewController () -> SaveSSVC {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SaveSSVC") as! SaveSSVC
    }
    
    @IBAction func btnSelectCategoryTapped(_ sender: Any) {
        if (viewCategory.isHidden){
            tblCategory.reloadData()
            viewCategory.isHidden = false
        }else{
            viewCategory.isHidden = true
        }
    }
    
    @IBAction func btnSaveSS(_ sender: Any) {
        if(self.storecallAction == .saveAndStorecall){
            let nc = NotificationCenter.default
            nc.post(name: Notification.Name("saveVidoActionCallEnd"), object: nil)
        }
        StoreScreenShot()
    }
    @IBAction func btnCloseSS(_ sender: Any) {
        txtSSDescription.resignFirstResponder()

        if(self.storecallAction == .saveAndStorecall){
            let nc = NotificationCenter.default
            nc.post(name: Notification.Name("saveVidoActionCallEnd"), object: nil)
        }
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()

        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSaveVideoRecording(_ sender: Any) {
        storeVideoRecording()
    }
    @IBAction func btnShareSS(_ sender: Any) {
        let imageToShare = imgSS.image
        
        guard let textDescription = txtSSDescription.text  else{
            
            return
        }
        guard let username = AppGlobalManager.sharedInstance.loggedInUser?.user?.name else{
            return
        }
        guard let strCategory = selectedCategory, strCategory != "Select Category"else{
            self.showToast("Please select any one category", type: .fail)
            return
        }
        guard let countofTags = colTags.indexPathsForSelectedItems?.count,countofTags >= 1 else{
            self.showToast("Please select any one tag", type: .fail)
            return
        }
        let items = colTags.indexPathsForSelectedItems
        
        for item in items!{
            
            let tag = AppGlobalManager.sharedInstance.tagsArray![item.row]
            tagsIndexArrayString?.append(tag.name!)
        }
        let tagstring = tagsIndexArrayString!.joined(separator: ",")

        let wholeCategoryString = "Category : \(strCategory)"
        let wholeString = "\(username) has shared this ARMS image with you."
        var wholeDesc : String = "Description : \(String(describing: textDescription))"
        if textDescription.isEmpty{
            wholeDesc = ""
        }
        let wholeTags = "Tags: \(tagstring)"
        
        let activityViewController = UIActivityViewController(activityItems: [imageToShare!,wholeString,wholeCategoryString,wholeTags,wholeDesc], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    func StoreScreenShot(){
        
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        guard let user_id = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else{
            return
        }
        guard let image_base64 = imgSS.image?.convertImageToBase64String(img: imgSS.image!) else{
            return
        }
//        guard let textDescription = txtSSDescription.text,!textDescription.isEmpty  else{
        guard let textDescription = txtSSDescription.text  else{

            return
        }
        guard let strCategory = selectedCategoryIndex else{
            self.showToast("Please select any one category", type: .fail)
            return
        }
        guard let countofTags = colTags.indexPathsForSelectedItems?.count,countofTags >= 1 else{
            self.showToast("Please select any one tag", type: .fail)
            return
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
             
        let roomnameArr = self.roomname!.components(separatedBy:"_")
        
        let items = colTags.indexPathsForSelectedItems
        
        for item in items!{
            
            let tag = AppGlobalManager.sharedInstance.tagsArray![item.row]
            tagsIndexArray?.append(tag.id!)
        }
        
        let location = UserDefaults.standard.string(forKey: "Location")
        let Latitude = UserDefaults.standard.double(forKey: "Latitude")
        let Longitude = UserDefaults.standard.double(forKey: "Longitude")

        let requestParameters = [ "device_id" : device_id,
                                  "session_id" : roomnameArr[2],
                                  "user_id" : user_id,
                                  "image" : "data:image/jpeg;base64,\(image_base64)",
            "description" : textDescription,
            "category" : "\(strCategory)",
            "device_type" : "0",
            "tags" : "\(tagsIndexArray!)",
            "location" : location ?? "",
            "latitude" : Latitude,
            "longitude" : Longitude] as [String : Any]
        
        print("Screenshot capture api parameters : \(requestParameters)")
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.saveScreenShot, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    let userResponse =  CommonCodableHelper<GeneralResponse>.codableObject(from: data)
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        self.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        self.selectedCategory = nil
                        self.selectedCategoryIndex = nil
                        self.tagsIndexArray = []
                        self.tagsIndexArrayString = []
                        self.dismiss(animated: true, completion: nil)
                        return
                    }
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                   
                    self.dismiss(animated: true, completion: nil)
                    
                    self.showToast(userResponse!.message ?? "Successfully saved", type: .success)
                    self.txtSSDescription.resignFirstResponder()
                    self.txtSSDescription.text = ""
                    self.txtSSDescription.placeholder = "Write your text"
                    
                    self.selectedCategory = nil
                    self.selectedCategoryIndex = nil
                    self.tagsIndexArray = []
                    self.tagsIndexArrayString = []
                }
                
            case .failure( let error):
                print("error",error)
            }
        }
    }
    func storeVideoRecording(){
        
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
//        guard let textDescription = txtSSDescription.text,!textDescription.isEmpty  else{
        guard let textDescription = txtSSDescription.text  else{
//            self.showToast("Please add some description of your image", type: .fail)
            return
        }
        guard let strCategory = selectedCategoryIndex else{
            self.showToast("Please select any one category", type: .fail)
            return
        }
        guard let countofTags = colTags.indexPathsForSelectedItems?.count,countofTags >= 1 else{
            self.showToast("Please select any one tag", type: .fail)
            return
        }
        
      
        let items = colTags.indexPathsForSelectedItems
        
        for item in items!{
            
            let tag = AppGlobalManager.sharedInstance.tagsArray![item.row]
            tagsIndexArray?.append(tag.id!)
        }
        let location = UserDefaults.standard.string(forKey: "Location")
        let Latitude = UserDefaults.standard.double(forKey: "Latitude")
        let Longitude = UserDefaults.standard.double(forKey: "Longitude")

        let requestParameters = [ "device_id" : device_id,
                                  "thumb_path" : thumb_url as Any,
            "description" : textDescription,
            "recording_id" : recording_id!,
            "category" : "\(strCategory)",
            "device_type" : "0",
            "tags" : "\(tagsIndexArray!)",
            "location" : location ?? "",
            "latitude" : Latitude ,
            "longitude" : Longitude ] as [String : Any]
        
        print("save recording api parameters : \(requestParameters)")
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.saveVideoRecording, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    let userResponse =  CommonCodableHelper<GeneralResponse>.codableObject(from: data)
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        self.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        self.selectedCategory = nil
                        self.selectedCategoryIndex = nil
                        self.tagsIndexArray = []
                        self.recording_id = nil
                        self.thumb_url = nil
                        self.dismiss(animated: true, completion: nil)
                        return
                    }
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    self.dismiss(animated: true, completion: nil)
                    
                    self.showToast(userResponse!.message ?? "Recording Successfully saved", type: .success)
                    self.txtSSDescription.resignFirstResponder()
                    self.txtSSDescription.text = ""
                    self.txtSSDescription.placeholder = "Write your text"
                    
                    self.selectedCategory = nil
                    self.selectedCategoryIndex = nil
                    self.tagsIndexArray = []
                    self.recording_id = nil
                    self.thumb_url = nil
                }
                
            case .failure( let error):
                print("error",error)
            }
        }
    }
}

extension SaveSSVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppGlobalManager.sharedInstance.categoryArray!.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CategoryCell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        
        let category = AppGlobalManager.sharedInstance.categoryArray![indexPath.row]
        CategoryCell.categoryName.text = category.name
        
        return CategoryCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = AppGlobalManager.sharedInstance.categoryArray![indexPath.row]
        btnSelectcategory.setTitle(category.name, for: .normal)
        viewCategory.isHidden = true
        selectedCategoryIndex = category.id
        selectedCategory = category.name
    }
}

extension SaveSSVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AppGlobalManager.sharedInstance.tagsArray!.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let tagCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagsCell", for: indexPath) as! TagsCell
        
        let tag = AppGlobalManager.sharedInstance.tagsArray![indexPath.row]
        tagCell.tagName.text = tag.name
        
        tagCell.layer.cornerRadius = 10
        tagCell.layer.masksToBounds = true
        
        return tagCell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let string = AppGlobalManager.sharedInstance.tagsArray![indexPath.row]
        let yourString = string.name
        let padding = CGSize.init(width: 20, height: 20)
        let textSize = yourString!.size(width: collectionView.frame.width)
        return CGSize.init(width: textSize.width + (padding.width / 2), height: textSize.height + (padding.height / 2))
    }

    
}
extension String {
    func size(width:CGFloat = 220.0, font: UIFont = UIFont.systemFont(ofSize: 17.0, weight: .regular)) -> CGSize {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = self
        
        label.sizeToFit()
        
        return CGSize(width: label.frame.width, height: label.frame.height)
    }
}
