//
//  TagsCell.swift
//  AgoraARKit
//
//  Created by Hetali on 15/10/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit

class TagsCell: UICollectionViewCell {
    
    @IBOutlet weak var tagName: UILabel!
    
    override var isSelected: Bool{
        didSet{
            if self.isSelected{
                self.contentView.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.green.color
            }else{
                self.contentView.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorSecondaryGray.color
            }
        }
    }
    
    
    

}
