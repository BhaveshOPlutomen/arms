//
//  ARController + Stroke.swift
//  AgoraARKit
//
//  Created by Hetali on 29/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import Foundation
import ARKit
import AgoraRtcKit

//--------------------------------------------------------------------------------

//MARK:-  Stroke  Methdos

//--------------------------------------------------------------------------------

extension ARController {
    
    /// Places anchor on hitNode plane at point
    func makeAnchor(at point: CGPoint) -> ARAnchor? {
        
        guard let hitNode = hitNode else {
            return nil
        }
        
        let hitTest = self.sceneView.hitTest(point, types: [.featurePoint,.existingPlane,.estimatedVerticalPlane,.estimatedHorizontalPlane,.existingPlaneUsingExtent])
        
        if let result = hitTest.first  {
            let position = result.worldTransform.columns.3
            
            
            let positionConverted = self.sceneView.pointOfView?.convertPosition(SCNVector3(position.x, position.y, position.z), from: sceneView.scene.rootNode)
            
            hitNode.position.z = positionConverted?.z ?? -0.4
            print("hitNode.position.z",hitNode.position.z)
            hitNode.position.z = abs(positionConverted?.z ?? -0.4) > 3.0 ? -3.0 : (positionConverted?.z ?? -0.4)
            hitNode.position.z = abs(positionConverted?.z ?? -0.4) < 0.1 ? -0.1 : (positionConverted?.z ?? -0.4)
            
            print("hitNode.position.z converted",hitNode.position.z)

//            if abs(hitNode.position.z) < 1 {
//                strokeSize = .small
//            } else if abs(hitNode.position.z) > 1 &&  abs(hitNode.position.z) < 1.5 {
//                strokeSize = .medium
//            } else {
//                strokeSize = .large
//            }
            
//             if strokeSize == .small{
//                    strokeSize = .small
//             }else if strokeSize == .medium{
//                strokeSize = .medium
//             }else{
//                strokeSize = .large
//            }
           /* if strokeSize == .small{
//                strokeSize = .small
                if abs(hitNode.position.z) < 0.2{
                    strokeSize = .small
                    print("Small Small")
                    }else if abs(hitNode.position.z) > 0.2 &&  abs(hitNode.position.z) < 0.6{
                    strokeSize = .medium
                    print("Small medium")
                }else{
                    strokeSize = .large
                    print("Small Large")
                }
            }else if strokeSize == . medium{
//                strokeSize = .medium
                if abs(hitNode.position.z) < 0.2{
                    strokeSize = .small
                    print("medium small")
                    }else if abs(hitNode.position.z) > 0.2 &&  abs(hitNode.position.z) < 0.6{
                    strokeSize = .medium
                    print("medium medium")
                }else{
                    strokeSize = .large
                    print("Medium large")
                }
            }else{
//                strokeSize = .large
                if abs(hitNode.position.z) < 0.2{
                    strokeSize = .small
                    print("Large small")
                }else if abs(hitNode.position.z) > 0.2 &&  abs(hitNode.position.z) < 0.6{
                    strokeSize = .medium
                    print("Large medium")
                }else{
                    strokeSize = .large
                    print("large large")
                }
            }*/
            
            if abs(hitNode.position.z) < 1 {
                strokeSize = .small
            } else if abs(hitNode.position.z) > 1 &&  abs(hitNode.position.z) < 1.5 {
                strokeSize = .medium
            } else {
                strokeSize = .large
            }
            
        } else {
            hitNode.position.z = -2.0
        }
        
        let projectedOrigin = sceneView.projectPoint(hitNode.worldPosition)
        let offset = sceneView.unprojectPoint(SCNVector3Make(Float(point.x), Float(point.y), projectedOrigin.z))
        
        var blankTransform = matrix_float4x4(1)
        //        var transform = hitNode.simdWorldTransform
        blankTransform.columns.3.x = offset.x
        blankTransform.columns.3.y = offset.y
        blankTransform.columns.3.z = offset.z
        
        return ARAnchor(transform: blankTransform)
    }
    
  
    /// Updates stroke with new SCNVector3 point, and regenerates line geometry
    func updateLine(for stroke: Stroke) {
        guard let _ = stroke.points.last, let strokeNode = stroke.node else {
            return
        }
        let offset = unprojectedPosition(for: stroke, at: touchPoint)
        let newPoint = strokeNode.convertPosition(offset, from: sceneView.scene.rootNode)
        
        stroke.lineWidth = strokeSize.rawValue
        if (stroke.add(point: newPoint)) {
            //  pairingManager?.updateStroke(stroke)
            updateGeometry(stroke)
        }
//        print("Total Points: \(stroke.points.count)")
    }
    
    func updateGeometry(_ stroke:Stroke) {
        if stroke.positionsVec3.count > 4 {
            let vectors = stroke.positionsVec3
            let sides = stroke.mSide
            let width = stroke.mLineWidth
            let lengths = stroke.mLength
            let totalLength = (stroke.drawnLocally) ? stroke.totalLength : stroke.animatedLength
            let line = LineGeometry(vectors: vectors,
                                    sides: sides,
                                    width: width,
                                    lengths: lengths,
                                    endCapPosition: totalLength,
                                    selectedColor: stroke.color)
            //    selectedColor: stroke == remoteStroke ? .yellow : .white)
            
            stroke.node?.geometry = line
            // uiViewController?.hasDrawnInSession = true
            //   uiViewController?.hideDrawingPrompt()
        }
    }
    
    // Stroke Helper Methods
    func unprojectedPosition(for stroke: Stroke, at touch: CGPoint) -> SCNVector3 {
        guard let hitNode = self.hitNode else {
            return SCNVector3Zero
        }
        
        let projectedOrigin = sceneView.projectPoint(hitNode.worldPosition)
        let offset = sceneView.unprojectPoint(SCNVector3Make(Float(touch.x), Float(touch.y), projectedOrigin.z))
        
        return offset
    }
    
    //    /// Checks user's strokes for match, then partner's strokes
    func getStroke(for anchor: ARAnchor) -> Stroke? {
        
        // This method is called when didAddNode called.  didAddNode when we add anchor to session
        
        if  let stock = self.selfSession.strokes.first(where: { (stroke) -> Bool in
            return stroke.anchor == anchor
        }) {
            return stock
        } else {
            // Check if we not found the anchor in current list then may be we are drawing remotely using remote stroke
            
            for session in activeSessions {
                if  let stock = session.strokes.first(where: { (stroke) -> Bool in
                    return stroke.anchor == anchor
                }) {
                    return stock
                }
            }
            
        }
        return nil
    }
    
    /// Checks user's strokes for match, then partner's strokes
    func getStroke(for node: SCNNode) -> Stroke? {
        var matchStrokeArray = self.selfSession.strokes.filter { (stroke) -> Bool in
            return stroke.node == node
        }
        
        if matchStrokeArray.isEmpty {
            for session in activeSessions {
                for stroke in session.strokes {
                    if stroke.node == node {
                        matchStrokeArray.append(stroke)
                    }
                }
            }
            
        }
        
        return matchStrokeArray.first
    }
    
    func setStrokeVisibility(isHidden: Bool) {
        self.selfSession.strokes.forEach { stroke in
            stroke.node?.isHidden = isHidden
        }
    }
    
    func resetTouches() {
        touchPoint = .zero
    }
    
    func clearAllStrokes() {
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) in
                 node.removeFromParentNode()
             }
    }
    
    func clearAllAnnotations() {

        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) in
                      node.removeFromParentNode()
        }
    }
    
    //--------------------------------------------------------------------------------
    
    func loadAnnotaionTextBox(session:ARDrawingSession , at  location:CGPoint,textSrting : String)
        {
            sceneView.automaticallyUpdatesLighting = true
            sceneView.autoenablesDefaultLighting = true
        let text = SCNText(string: textSrting, extrusionDepth: 2)
            let material = SCNMaterial()
            material.diffuse.contents = UIColor.white

    //        material.multiply.intensity = 0.8
            material.specular.contents = UIColor.red

    //        material.isDoubleSided = true
            material.locksAmbientWithDiffuse = true
            text.flatness = 0.2
    //        text.chamferRadius = 0.01
        let (min, max) = lastnode!.boundingBox
            let width = CGFloat(max.x - min.x)
            let height = CGFloat(max.y - min.y)
        text.containerFrame = CGRect(x: CGFloat(min.x + 1), y: CGFloat(min.y - 1.5), width: (width - 3) , height: (height + 2))
//            text.containerFrame = CGRect(x: CGFloat(min.x), y: CGFloat(min.y), width: (width) , height: (height))

            text.truncationMode = CATextLayerTruncationMode.middle.rawValue
            text.isWrapped = true
            text.alignmentMode = CATextLayerAlignmentMode.center.rawValue
        text.font = UIFont(name: Constants.AppTheme.Fonts.ProximaNova.FONT_BOLD.rawValue, size: 6)
//        text.font = UIFont(name: "Futura-Bold", size: 0.7)
            text.materials = [material]
    //        self.animateTextGeometry(text, withText: textSrting, completed: {})
    //    guard  let anchor = self.makeAnchor(at: location) else {return}

            let textNode = SCNNode(geometry: text)
            textNode.name = "TextNode"
//        textNode.scale = SCNVector3(0.3, 0.3, 0.3)
//        textNode.position = SCNVector3(anchor.transform.columns.3.x,anchor.transform.columns.3.y,anchor.transform.columns.3.z)
      //   lastnode?.addChildNode(textNode)

            if self.sceneView.scene.rootNode.childNodes.last?.name == "EmptyTextBox" {
                self.sceneView.scene.rootNode.childNodes.last?.addChildNode(textNode)
            }

//        self.sceneView.scene.rootNode.addChildNode(textNode)
    //        self.sceneView.scene.rootNode.childNodes.last?.addChildNode(textNode)

    //        self.sceneView.scene.rootNode.addChildNode(node)

        }
    
    func AddEmptyTextBox(session:ARDrawingSession, at location:CGPoint){
        sceneView.automaticallyUpdatesLighting = true
        sceneView.autoenablesDefaultLighting = true
        
        guard  let anchor = self.makeAnchor(at: location) else {return}
        
        guard let node =  session.selectedAnnotationTypeLast.loadAnnotation() else {
            return
        }
        
//        let path = UIBezierPath()
//
//        path.move(to: CGPoint(x: 2, y: 1))//A
//        path.addLine(to: CGPoint(x: 2, y: -1))//B
//        path.addLine(to: CGPoint(x: 0.5, y: -1))//C
//        path.addLine(to: CGPoint(x: 0, y: -2))//D
//        path.addLine(to: CGPoint(x: -0.5, y: -1))//E
//        path.addLine(to: CGPoint(x: -2, y: -1))//F
//        path.addLine(to: CGPoint(x: -2, y: 1))//G
//        path.close()
//
//        //create geomerty
//        let shape = SCNShape(path: path, extrusionDepth: 0.1)
//        let color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.934424394)
//
//        shape.firstMaterial?.diffuse.contents = color
//        shape.chamferRadius = 0.2
//
//        //create a node
//
//
//        let node1 = SCNNode(geometry: shape)
//        node1.scale = SCNVector3(0.01, 0.01, 0.01)
//        node1.position = SCNVector3(anchor.transform.columns.3.x,anchor.transform.columns.3.y,anchor.transform.columns.3.z)
//        let yawn = sceneView.session.currentFrame?.camera.eulerAngles.y
//
//        node1.eulerAngles.y = (yawn ?? 0) + .pi * 2
//        // node.eulerAngles.z = (xRotate ?? 0) + .pi / 2
//        node1.eulerAngles.x = (sceneView.session.currentFrame?.camera.eulerAngles.x ?? 0)
//
//        node1.name = "EmptyTextBox"
//        lastnode = node1
//        sceneView.scene.rootNode.addChildNode(node1)
        
        
        
        // Create annotaion node and to perticular user's sesssion
        let annotationNode = AnnotationNode(node: node, anchor: anchor, annotationType: session.selectedAnnotationTypeLast)
        
        session.arUndoManager.addUndoableObject(annotationNode)
        session.annotations.append(annotationNode)
        
        node.scale = SCNVector3(0.0015, 0.0015, 0.0015)
        
        node.position = SCNVector3(anchor.transform.columns.3.x,anchor.transform.columns.3.y,anchor.transform.columns.3.z)
        //    node.eulerAngles.y = hitNode!.eulerAngles.y * -1
        let yawn = sceneView.session.currentFrame?.camera.eulerAngles.y
        
        node.eulerAngles.y = (yawn ?? 0) + .pi * 2
        // node.eulerAngles.z = (xRotate ?? 0) + .pi / 2
        node.eulerAngles.x = (sceneView.session.currentFrame?.camera.eulerAngles.x ?? 0)
        
        lastnode = node
        //node.simdTransform = matrix_multiply(currentTransform, node.simdTransform)
        node.name = "EmptyTextBox"
        
        self.sceneView.scene.rootNode.addChildNode(node)
//        (self.currentTextNode?.geometry as! SCNText).string = "strDistance"

    }

    //not in use
//    func animateTextGeometry(_ textGeometry: SCNText, withText text: String, completed: @escaping () -> Void ){
//
//        //1. Get All The Characters From The Text
//        let characters = Array(text)
//
//        //2. Run The Animation
//        animationTimer = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: true) { [weak self] timer in
//
//            //a. If The Current Time Doesnt Equal The Count Of Our Characters Then Continue To Animate Our Text
//            if self?.time != characters.count {
//                let currentText: String = textGeometry.string as! String
//                textGeometry.string = currentText + String(characters[(self?.time)!])
//                self?.time += 1
//            }else{
//                //b. Invalide The Timer, Reset The Variables & Escape
//                timer.invalidate()
//                self?.time = 0
//                completed()
//            }
//        }
//    }
    func loadAnnotaion(session:ARDrawingSession , at  location:CGPoint) {
        sceneView.automaticallyUpdatesLighting = true
        sceneView.autoenablesDefaultLighting = true
        let hits = self.sceneView.hitTest(location, options: nil)
        
        // Zoom annotation slider code
        
     /*   if !hits.isEmpty{
            // retrieved the first clicked object
                let result = hits[0]

            print("Seleted Node = \(result.node.name ?? "")")

            if result.node.name != "Plane" {

                // get its material
//                let material = result.node.geometry!.firstMaterial!

                let oldContent = result.node
//                let oldScale = result.node.scale
                let oldScale = result.node.scale
                self.sliderZoom.isHidden = false
                self.selectedNode = result.node
                DispatchQueue.main.async {
                    if self.selectedNode?.name == "common_button" {
                        self.sliderZoom.minimumValue = calculatePercentage(value: 0.0015, percentageVal: 30)
                        self.sliderZoom.maximumValue = 0.0015 * 5
                        if oldScale.x == 0.0015 {
                            self.sliderZoom.value = 0.0015
                        } else {
                            self.sliderZoom.value = oldScale.x
                        }
                    } else {
                        self.sliderZoom.minimumValue = calculatePercentage(value: 1, percentageVal: 30)
                        self.sliderZoom.maximumValue = 5
                        if oldScale.x == 1 {
                            self.sliderZoom.value = 1
                        } else {
                            self.sliderZoom.value = oldScale.x
                        }
                    }
//                    animateNodeSeleted(node: oldContent, scaleSize: oldScale.x)

    //                // highlight it

                    SCNTransaction.begin()
                    SCNTransaction.animationDuration = 0.5
                    // on completion - unhighlight
                    SCNTransaction.completionBlock = {
                        SCNTransaction.begin()
                        SCNTransaction.animationDuration = 0.5
                        //                    material.emission.contents = UIColor.black
                        oldContent.scale.x = oldScale.x
                        oldContent.scale.y = oldScale.y
                        oldContent.scale.z = oldScale.z
                        SCNTransaction.commit()
                    }
                    oldContent.scale.x = oldScale.x * 2
                    oldContent.scale.y = oldScale.y * 2
                    oldContent.scale.z = oldScale.z * 2
    //                material.emission.contents = UIColor.red
                    SCNTransaction.commit()
                }
                return
            }
        }*/

        self.selectedNode = nil
        sliderZoom.isHidden = true

        guard let anchor = self.makeAnchor(at: location) else {return}

        guard let node =  session.selectedAnnotationType.loadAnnotation() else {
            return
        }

//        let tempNodeClass = freezeModeAnnotation(node: node)

//        node = tempNodeClass
        // Create annotaion node and to perticular user's sesssion

        let annotationNode = AnnotationNode(node: node, anchor: anchor, annotationType: session.selectedAnnotationType)
        session.arUndoManager.addUndoableObject(annotationNode)
        session.annotations.append(annotationNode)
        node.scale = SCNVector3(0.0015, 0.0015, 0.0015)
        //        switch self.strokeSize {

        //        case .small:
        //            node.scale = SCNVector3(0.00003, 0.00003, 0.00003)
        //        case .medium:
        //            node.scale = SCNVector3(0.00006, 0.00006, 0.00006)
        //        case .large:
        //            node.scale = SCNVector3(0.00006, 0.00006, 0.00006)
        node.position = SCNVector3(anchor.transform.columns.3.x,anchor.transform.columns.3.y,anchor.transform.columns.3.z)
        //    node.eulerAngles.y = hitNode!.eulerAngles.y * -1
        let yawn = sceneView.session.currentFrame?.camera.eulerAngles.y
        node.eulerAngles.y = (yawn ?? 0) + .pi * 2
        // node.eulerAngles.z = (xRotate ?? 0) + .pi / 2
        node.eulerAngles.x = (sceneView.session.currentFrame?.camera.eulerAngles.x ?? 0)
        //node.simdTransform = matrix_multiply(currentTransform, node.simdTransform)
        self.sceneView.scene.rootNode.addChildNode(node)
    
        //        let rotateAction = SCNAction.rotateBy(x: 0, y: 0 , z: 0, duration: 0.6)
        //        let reverseAction = SCNAction.repeatForever(SCNAction.group([rotateAction]))
        //        node.runAction(reverseAction)
    }
}
