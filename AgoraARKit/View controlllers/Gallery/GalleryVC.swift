//
//  GalleryVC.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 01/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit

class GalleryVC: UIViewController, Toastable {

    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var btnDocument: UIButton!
    @IBOutlet weak var lblNodata: UILabel!
    
    enum filterGallery: String {
        case image
        case video
        case document
    }
    
    var aryData : [GalleryModel]?
    var aryFilterData : [GalleryModel]?
    var localAry : [GalleryModel]?
    var aryDataItem : [[Date: [GalleryModel]]]?
    var ary = [[GalleryModel]]()
    var selectedCategory: filterGallery? = .image {
        didSet {
            if selectedCategory == .image {
                selectedItem(vw: btnImage)
                unSelectedItem(vw: btnVideo)
                unSelectedItem(vw: btnDocument)
                self.filterCategory(filter: .image)
            } else if selectedCategory == .video {
                selectedItem(vw: btnVideo)
                unSelectedItem(vw: btnImage)
                unSelectedItem(vw: btnDocument)
                self.filterCategory(filter: .video)
            } else if selectedCategory == .document {
                selectedItem(vw: btnDocument)
                unSelectedItem(vw: btnVideo)
                unSelectedItem(vw: btnImage)
                self.filterCategory(filter: .document)
            }
        }
    }
    var documentInteractionController:UIDocumentInteractionController!
    
   var isSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNodata.isHidden = true
        navigationController?.navigationBar.prefersLargeTitles = false

        self.title = "Gallery"
        configureSearchBar()
        selectedCategory = .image
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "GalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GalleryCollectionViewCell")
        collectionView.register(UINib(nibName: "GalleryHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "GalleryHeaderCollectionReusableView")
        
        selectedItem(vw: btnImage)
        unSelectedItem(vw: btnVideo)
        unSelectedItem(vw: btnDocument)
        UtilityClass.corner(vw: btnImage, size: btnImage.frame.height / 2)
        UtilityClass.corner(vw: btnVideo, size: btnVideo.frame.height / 2)
        UtilityClass.corner(vw: btnDocument, size: btnDocument.frame.height / 2)
        
        getScreenShotImages()
        
        let image = UIImage(named: "back")
        let backButton = UIBarButtonItem(image: image, style: .done, target: self, action: #selector(backAction))
        backButton.tintColor = .white
        navigationItem.leftBarButtonItem = backButton
        
    }
    
      class func viewController () -> GalleryVC {
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
          return storyboard.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
      }
      
     @objc func backAction() {
         self.navigationController?.popViewController(animated: true)
     }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.layer.cornerRadius = searchBar.searchTextField.frame.height / 2
            searchBar.searchTextField.layer.masksToBounds = true
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func btnImageAction(_ sender: UIButton) {
        selectedCategory = .image
    }
    
    @IBAction func btnVideoAction(_ sender: UIButton) {
        selectedCategory = .video
    }
    
    @IBAction func btnDocumentAction(_ sender: UIButton) {
        selectedCategory = .document
    }

    func selectedItem(vw: UIButton) {
        vw.backgroundColor = UIColor.init(hexFromString: "53AD67")//"5CD994")
    }
    
    func unSelectedItem(vw: UIButton) {
        vw.backgroundColor = UIColor.clear
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let current = sender as? GalleryModel {
            if current.type == "image"{
                guard let next = segue.destination as? GalleryDetailsViewController else  {
                    return
                }
                if let current = sender as? GalleryModel {
                    let tagstring = current.tagname?.joined(separator: ",")

                    next.strDescription = current.descriptionField ?? ""
                    next.img = current.path ?? ""
                    next.categoryName = current.cat_name ?? ""
                    next.tagName = tagstring ?? ""
                    next.location = current.location ?? ""

                    let strDate = UtilityClass.ConvertUTCDateStringToCurrentDateString(date: current.createdAt ?? "")
                    next.dateAndTime = strDate
                }
            }else{
                guard let next = segue.destination as? GalleryVideoPlayVC else  {
                    return
                }
                if let current = sender as? GalleryModel {
                    let tagstring = current.tagname?.joined(separator: ",")

                    next.strDescription = current.descriptionField ?? ""
                    next.videoPath = current.path ?? ""
                    next.categoryName = current.cat_name ?? ""
                    next.tagName = tagstring ?? ""
                    next.location = current.location ?? ""
                    let strDate =  UtilityClass.ConvertUTCDateStringToCurrentDateString(date: current.createdAt ?? "")
                    next.dateAndTime = strDate
                }
            }
        }
        
     
    }
    
    func convertToDate(date:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        let dt = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strNewDate = dateFormatter.string(from: dt!)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let newDate = dateFormatter.date(from: strNewDate)!
        return newDate
    }
    
//    func convertToString(date:Date) -> String {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MMM-dd"
//        dateFormatter.calendar = NSCalendar.current
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")// TimeZone.current
//
//        let strNewDate = dateFormatter.string(from: date)
////        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
////        dateFormatter.dateFormat = "yyyy-MM-dd"
//
////        print("local to UTC date %@",dateFormatter.string(from: dt!))
////
////        let strNewDate = dateFormatter.string(from: dt!)
//
//        return strNewDate
//    }
//
//    func convertToStringOfDate(date:Date) -> (String,String) {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
//        dateFormatter.calendar = NSCalendar.current
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")// TimeZone.current
//
//        let NewDate = dateFormatter.string(from: date)
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//        dateFormatter.dateFormat = "yyyy-MM-dd"
////        let dtNewDate = dateFormatter.date(from: NewDate)
//
//        let strNewDate = dateFormatter.string(from: date)
//
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//        dateFormatter.dateFormat = "hh:mm:ss"
//        let NewTime = dateFormatter.string(from: date)
////        let tmNewTime = dateFormatter.date(from: NewTime)
//
////        let strNewDate = dateFormatter.string(from: dtNewDate ?? Date())
////        let strNewTime = dateFormatter.string(from: tmNewTime ?? Date())
//
//        return (strNewDate,NewTime)
//    }
//    func tempConvertToTime(date:String) -> String {
//        if date == "" {
//            return ""
//        }
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//        if let date = dateFormatter.date(from: date) {
//            dateFormatter.timeZone = TimeZone.current
//            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
//            return dateFormatter.string(from: date)
//        }
//        return ""
//    }
//    func convertDate(date:String) -> Date {
//           let dateFormatter = DateFormatter()
//           dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//           dateFormatter.calendar = NSCalendar.current
//           dateFormatter.timeZone = TimeZone.current
//
//           let dt = dateFormatter.date(from: date)
//           dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//           dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
//
////           print("local to UTC date %@",dateFormatter.string(from: dt!))
//
//           let strNewDate = dateFormatter.string(from: dt!)
//           let newDate = dateFormatter.date(from: strNewDate)!
//           return newDate
//       }
//
//    func convertToStringForHeader(date:Date) -> String {
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "dd MMM yyyy"
//            dateFormatter.calendar = NSCalendar.current
//            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")// TimeZone.current
//
//            let strNewDate = dateFormatter.string(from: date)
//    //        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//    //        dateFormatter.dateFormat = "yyyy-MM-dd"
//
//    //        print("local to UTC date %@",dateFormatter.string(from: dt!))
//    //
//    //        let strNewDate = dateFormatter.string(from: dt!)
//
//            return strNewDate
//        }
//
    func filterCategory(filter: filterGallery) {
        self.ary.removeAll()
        aryDataItem = [[Date: [GalleryModel]]]()
        self.aryDataItem?.removeAll()
        
        if isSearching {
            self.localAry = self.aryFilterData?.filter{$0.type == filter.rawValue}
        } else {
            self.localAry = self.aryData?.filter{$0.type == filter.rawValue}
        }
        
        if self.localAry?.count == 0 {
            lblNodata.isHidden = false
            lblNodata.text = "No \(filter.rawValue)"
            collectionView.isHidden = true
        } else {
            lblNodata.isHidden = true
            collectionView.isHidden = false
        }
                
          for (i,item) in (self.localAry ?? []).enumerated() {
              let convertedDate = self.convertToDate(date: item.createdAt ?? "")
              self.localAry?[i].sortedDate = convertedDate
          }
                
          let groupData = Dictionary(grouping: self.localAry ?? []) { (gallery) -> Date in
              return gallery.sortedDate ?? Date()
          }
        
        _ = groupData.forEach { (current) in
            
        let dict: [Date:[GalleryModel]] = [current.key:current.value]
            self.aryDataItem?.append(dict)
        }
        
        let adf = self.aryDataItem?.sorted(by: { (_$0, _$1) -> Bool in
            if (_$0.keys.first ?? Date()) > (_$1.keys.first ?? Date()) {
                return true
            }
            return false
        })
        
        self.aryDataItem = adf
        
        self.collectionView.reloadData()
    }

    func getScreenShotImages(defaultSelected : filterGallery = .image){
        
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        guard let user_id = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else{
            return
        }
        
        let requestParameters = [ "device_id" : device_id,
                                  "device_type" : "0",
                                  "user_id" : user_id] as [String : Any]
        
        print("Screenshot list api parameters : \(requestParameters)")
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.screenshotList, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    
                    do {
                    let user = try? JSONDecoder().decode(GalleryModelMain.self, from: data)
                        print(user?.message as Any)
                        self.aryData = user?.output
                        self.selectedCategory = defaultSelected
                        self.filterCategory(filter: defaultSelected)
                    }
                    
//                    let userResponse =  CommonCodableHelper<GalleryModel>.codableObject(from: data)
//                    guard let status = userResponse?.statusCode,status == 1 else {
//                        self.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
//                        return
//                    }
//                    self.showToast(userResponse!.message ?? "Successfully saved", type: .success)
                    
                }
                
            case .failure( let error):
                print("error",error)
            }
        }
    }
    
    func deleteScreenshot(id: String) {
        
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
                    return
                }
        let requestParameters = [ "device_id" : device_id, "id" : id,"device_type" : "0"] as [String : Any]
            print("deleteScreenshot api parameters : \(requestParameters)")
            
            APIHandler.callAPI(endPoint: APIConstants.EndPoints.userScreenshotDelete, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
                
                switch response {
                case .success(let data):
                    DispatchQueue.main.async {
                        let res = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                        print(res as Any)
                        self.getScreenShotImages(defaultSelected: .image)
                    }
                case .failure( let error):
                    print("error",error)
                }
            }
    }
    func deleteRecording(id: String) {
        
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
                    return
                }
        let userId = "\(AppGlobalManager.sharedInstance.loggedInUser?.user?.id ?? 0)"
        let requestParameters = [ "device_id" : device_id, "recording_id" : id, "user_id": userId,"device_type" : "0"] as [String : Any]
            print("deleteRecording api parameters : \(requestParameters)")
            
            APIHandler.callAPI(endPoint: APIConstants.EndPoints.deleteRecording, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
                
                switch response {
                case .success(let data):
                    DispatchQueue.main.async {
                        let res = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                        print(res as Any)
                        self.getScreenShotImages(defaultSelected: .video)
                    }
                case .failure( let error):
                    print("error",error)
                }
            }
    }


}

extension GalleryVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.aryDataItem?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.aryDataItem?[section].values.first?.count ?? 0// self.ary[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as! GalleryCollectionViewCell
        
        let current = self.aryDataItem?[indexPath.section].values.first?[indexPath.row] // self.ary[indexPath.section][indexPath.row]
        
        UtilityClass.gredientColor(vw: cell.viewBottom)
        UtilityClass.corner(vw: cell.btnShare, size: cell.btnShare.frame.height / 2)
        UtilityClass.corner(vw: cell.btnTrash, size: cell.btnTrash.frame.height / 2)
        UtilityClass.corner(vw: cell.viewBG, size: 10)
        cell.viewBG.layer.borderWidth = 0.5
        cell.viewBG.layer.borderColor = UIColor.init(named: "LightGray")?.cgColor
        
        let strURL = URL(string: current?.type == "image" ? current?.path ?? "" : current?.thumbPath ?? "")
//        cell.imgItem.sd_setImage(with: strURL, completed: nil)
        cell.imgItem.sd_setImage(with: strURL, placeholderImage:#imageLiteral(resourceName: "placeholder"), options: .highPriority, completed: nil)
        
        cell.shareButtonTapped = {
            print("Share Button Tapped")
            
            let img = cell.imgItem.image ?? UIImage(named: "placeholder.png")
            let video = current?.path
            
            var param = [Any]()
            
            guard let username = AppGlobalManager.sharedInstance.loggedInUser?.user?.name else{
                return
            }
            
            let tagstring = current?.tagname?.joined(separator: ",")
            let strDescription : String = current?.descriptionField ?? ""
            let categoryName : String = current?.cat_name ?? ""
            let tagName : String = tagstring ?? ""
            
            var wholeCategoryString = "Category : \(categoryName)"
            let wholeString = "\(username) has shared this ARMS image with you."
            let wholeStringVideo = "\(username) has shared this ARMS video with you."
            var wholeDesc = "Description : \(strDescription)"
            var wholeTags = "Tags : \(tagstring ?? "")"
            
            if categoryName.isEmpty{
                wholeCategoryString = ""
            }
            if tagName.isEmpty{
                wholeTags = ""
            }
            if strDescription.isEmpty{
                wholeDesc = ""
            }
            if current?.type == "image" {
                param = ["\(wholeString)" ,"\(wholeCategoryString)" ,"\(wholeTags)","\(wholeDesc)", img as Any]
            }
            else {
                param = ["\(wholeStringVideo)" ,"\(wholeCategoryString)" ,"\(wholeTags)","\(wholeDesc)", URL(string: video ?? "") as Any]
            }
            
            let activityViewController = UIActivityViewController(activityItems: param, applicationActivities: nil)
                   activityViewController.popoverPresentationController?.sourceView = self.view
            
            activityViewController.completionWithItemsHandler = { activity, success, items, error in
            }
            self.present(activityViewController, animated: true, completion: nil)
        }
        
        cell.trashButtonTapped = {
            print("Trash Button Tapped")
            
            if self.selectedCategory == .image {
                UtilityClass.showAlertWithCancel(title: "ARMS", message: "Are you sure you want to delete this image?") { (act) in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.deleteScreenshot(id: current?.id ?? "")
                    }
                }
            } else if self.selectedCategory == .video {
                UtilityClass.showAlertWithCancel(title: "ARMS", message: "Are you sure you want to delete this recording video?") { (act) in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.deleteRecording(id: current?.id ?? "")
                    }
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        let current = self.aryDataItem?[indexPath.section].values.first?[indexPath.row] // self.ary[indexPath.section][indexPath.row]
        
        if current?.type == "image" {
            self.performSegue(withIdentifier: "openImage", sender: current)
        }
        else if current?.type == "video" {
            self.performSegue(withIdentifier: "openVideo", sender: current)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = (self.collectionView.frame.width / 3) - 20
        if (UIDevice.current.userInterfaceIdiom == .pad){
            print("\nIPAD")
            return CGSize(width: w, height: 200)
        }else{
            return CGSize(width: w, height: 130)

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
       return UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            guard
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "GalleryHeaderCollectionReusableView", for: indexPath) as? GalleryHeaderCollectionReusableView else {
                    fatalError("Invalid view type")
            }
            
            let zxc = self.aryDataItem?[indexPath.section].keys.first ?? Date() // self.ary[indexPath.section].first?.sortedDate ?? Date()
            headerView.lblHeader.text = UtilityClass.convertToStringForHeader(date: zxc) // "\(self.aryDataItem?.keys[indexPath.section])"
            return headerView
        default:
//            assert(false, "Invalid element type")
           return UICollectionReusableView()
        }
    }
    
}

extension GalleryVC: UISearchBarDelegate {
    
    func configureSearchBar() {
        // Initialize and perform a minimum configuration to the searchBar.
        searchBar.placeholder = "Search Now"
        searchBar.delegate = self
        searchBar.sizeToFit()
        searchBar.clearBackgroundColor()
        
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorSecondaryGray.color
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
            if let leftView = textfield.leftView as? UIImageView {
                leftView.image = leftView.image?.withRenderingMode(.alwaysTemplate)
                leftView.tintColor = Constants.AppTheme.Colors.ColorsOfApp.colorLightGray.color
            }
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
        collectionView.reloadData()
    }
     
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        filterCategory(filter: selectedCategory!)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        didChangeSearchText(searchText: searchText)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        didChangeSearchText(searchText: searchBar.text ?? "")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        didChangeSearchText(searchText: searchBar.text ?? "")
        searchBar.endEditing(true)
    }
    
    func didChangeSearchText(searchText: String) {
        
        if searchText == "" {
            isSearching = false
        } else {
            isSearching = true
            
            
            aryFilterData = aryData?.filter({ (model) -> Bool in
                let current = model
                
                let tagstring = current.tagname?.joined(separator: ",")
                
                if current.descriptionField?.lowercased().contains(searchText.lowercased()) ?? false ||  current.cat_name?.lowercased().contains(searchText.lowercased()) ?? false || tagstring!.lowercased().contains(searchText.lowercased()) {
                    return true
                }
                return false
            })
        }
        
        filterCategory(filter: selectedCategory!)
        //        collectionView.reloadData()
    }
}


extension UISearchBar {
    func clearBackgroundColor() {
       guard let UISearchBarBackground: AnyClass = NSClassFromString("UISearchBarBackground") else { return }
       for view in subviews {
           for subview in view.subviews where subview.isKind(of: UISearchBarBackground) {
               subview.alpha = 0
           }
       }
   }
}
