//
//  GalleryHeaderCollectionReusableView.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 02/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit

class GalleryHeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var lblHeader: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
