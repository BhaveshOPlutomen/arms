//
//  GalleryCollectionViewCell.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 01/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var btnTrash: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    
    var shareButtonTapped: (() -> ())?
    var trashButtonTapped: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    @IBAction func btnTrashAction(_ sender: UIButton) {
        trashButtonTapped?()
    }
    
    @IBAction func btnShareAction(_ sender: UIButton) {
        shareButtonTapped?()
    }
    
}
