//
//  GalleryDetailsViewController.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 04/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import SDWebImage

class GalleryDetailsViewController: UIViewController {

    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var txtView: ReadMoreTextView!
    
    var img = String()
    var strDescription = String()
    var categoryName = String()
    var dateAndTime = String()
    var tagName = String()
    var location = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        txtView.roundCorners([.bottomLeft,.bottomRight], radius: 10)
        var categoryWholeString = "Category : \(categoryName)"
        var TagsWholeString = "Tags : \(tagName)"
        var dateWholeString = "Date : \(dateAndTime)"
        var descWholeString = "Description : \(strDescription)"
        var locationWholeString = "Location : \(location)"
        
        if categoryName.isEmpty{
            categoryWholeString = ""
        }
        if tagName.isEmpty{
            TagsWholeString = ""
        }
        if dateAndTime.isEmpty{
            dateWholeString = ""
        }
        if strDescription.isEmpty{
            descWholeString = ""
        }
        if location.isEmpty{
            locationWholeString = ""
        }

        imgItem.sd_setImage(with: URL(string: img), completed: nil)
        
        txtView.text = "\(categoryWholeString)\n\(TagsWholeString)\n\(dateWholeString)\n\(descWholeString)\n\(locationWholeString)"
        
        UtilityClass.corner(vw: imgItem, size: 10)
        
        let readMoreTextAttributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.foregroundColor: Constants.AppTheme.Colors.ColorsOfApp.green.color,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)
        ]
        let readLessTextAttributes = [
            NSAttributedString.Key.foregroundColor: Constants.AppTheme.Colors.ColorsOfApp.green.color,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)
        ]
        txtView.attributedReadMoreText = NSAttributedString(string: "... Read more", attributes: readMoreTextAttributes)
        txtView.attributedReadLessText = NSAttributedString(string: " Read less", attributes: readLessTextAttributes)
        txtView.maximumNumberOfLines = 5
        txtView.shouldTrim = true
        
    }
}
