//
//  GalleryVideoPlayVC.swift
//  AgoraARKit
//
//  Created by Hetali on 24/10/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import AVKit

class GalleryVideoPlayVC: UIViewController {

    
    @IBOutlet weak var view_player: UIView!
    @IBOutlet weak var txtView: ReadMoreTextView!

    var videoPath = String()
    var strDescription = String()
    var categoryName = String()
    var dateAndTime = String()
    var tagName = String()
    var location = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        let videoURL = URL(string: current?.path ?? "")
//        let player = AVPlayer(url: videoURL!)
//        let vc = AVPlayerViewController()
//        vc.player = player
//        present(vc, animated: true) {
//            vc.player?.play()
//        }
        
        let url : String = videoPath
        let urlStr : String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let convertedURL : URL = URL(string: urlStr)!
        
        let player = AVPlayer(url: convertedURL as URL)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.addChild(playerController)
        
        // Add your view Frame
        playerController.view.frame = CGRect(x: self.view_player.frame.origin.x, y: self.view_player.frame.origin.y, width: self.view_player.frame.size.width, height: self.view_player.frame.height)
        
        // Add sub view in your view
        self.view_player.addSubview(playerController.view)
        
        player.play()
        
        var categoryWholeString = "Category : \(categoryName)"
        var TagsWholeString = "Tags : \(tagName)"
        var dateWholeString = "Date : \(dateAndTime)"
        var descWholeString = "Description : \(strDescription)"
        var locationWholeString = "Location : \(location)"

        if categoryName.isEmpty{
            categoryWholeString = ""
        }
        if tagName.isEmpty{
            TagsWholeString = ""
        }
        if dateAndTime.isEmpty{
            dateWholeString = ""
        }
        if strDescription.isEmpty{
            descWholeString = ""
        }
        if location.isEmpty{
            locationWholeString = ""
        }
      
        txtView.text = "\(categoryWholeString)\n\(TagsWholeString)\n\(dateWholeString)\n\(descWholeString)\n\(locationWholeString)"
        
        let readMoreTextAttributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.foregroundColor: Constants.AppTheme.Colors.ColorsOfApp.green.color,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)
        ]
        let readLessTextAttributes = [
            NSAttributedString.Key.foregroundColor: Constants.AppTheme.Colors.ColorsOfApp.green.color,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 16)
        ]
        txtView.attributedReadMoreText = NSAttributedString(string: "... Read more", attributes: readMoreTextAttributes)
        txtView.attributedReadLessText = NSAttributedString(string: " Read less", attributes: readLessTextAttributes)
        txtView.maximumNumberOfLines = 5
        txtView.shouldTrim = true
        
    }

}
