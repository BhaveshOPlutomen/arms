//
//  ViewController.swift
//  AgoraARKit
//
//  Created by Prashant on 15/04/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit
import AgoraRtmKit

class ViewController: BaseViewController {

  
    @IBOutlet weak var txtRoomID: UITextField!
    
    var phoneNumberPrefilled:String = ""

    //TEMP
    ///////////////////////////
   // var rtmCallHandler:RTMCallHandler!
   // var localInvite:AgoraRtmLocalInvitation?

    
//    private func setupRTM() {
//
//
//        rtmCallHandler?.login(phoneNumber: AppGlobalManager.sharedInstance.currentUserID ?? "",completion: { [unowned self] code in
//            self.txtRoomID.text = "9874563210"
//            self.localInvite =  self.rtmCallHandler.sendLocalInvite(to: self.txtRoomID.text!) { (errorCode) in
//                print("Local Invite Sent ==> \(errorCode.rawValue)")
//            }
//        })
//
//
//    }
//
    ///////////////////////////

    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  rtmCallHandler = RTMCallHandler(delegate: self)
      //  setupRTM()
        self.txtRoomID.text = phoneNumberPrefilled

        // Do any additional setup after loading the view, typically from a nib.
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//            if UserDefaults.standard.string(forKey: "USER_PHONE") != nil {
//                AppDelegate.shared.window?.rootViewController?.presentingViewController?.dismiss(animated: true, completion:  nil)
//
//                let vc = ARController.viewController()
//                vc.roomName =  "room_name123456" //self.txtRoomID.text ?? ""
//                 AppDelegate.shared.window?.rootViewController?.present(vc, animated: true, completion: nil)
//
//                
//            }
//
//
//        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.txtRoomID.text = phoneNumberPrefilled
        self.txtRoomID.becomeFirstResponder()
        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidChangeAction(_:)), name: UITextField.textDidChangeNotification, object: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: nil)

    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Abstract Methdos
    
    //--------------------------------------------------------------------------------
    
    class func viewController () -> ViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Custom Methdos
    
    //--------------------------------------------------------------------------------

    @objc func textFieldDidChangeAction(_ notification: NSNotification) {
        
        let textField = notification.object as! UITextField
        
        phoneNumberPrefilled = textField.text ?? ""
        
    }
    
    //--------------------------------------------------------------------------------

    public func showCallStatusVC(callerID:String,callerName:String) {
        let callStatusVC = CallStatusViewController.viewController()
        callStatusVC.roomName = "room_name_\(callerID)"
        callStatusVC.delegate = self
        callStatusVC.callerID = callerID
        callStatusVC.callName = callerName // TODO: Fix this with real name of person afte fetching from api
        self.present(callStatusVC, animated: true, completion: nil)

    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Touch Methdos
    
    //--------------------------------------------------------------------------------

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.txtRoomID.resignFirstResponder()
    }
    
    //--------------------------------------------------------------------------------82709016982
    
    //MARK:-  Action Methdos
    
    //--------------------------------------------------------------------------------

    
    
    @IBAction func callTapped(_ sender: Any) {
        self.showCallStatusVC(callerID: self.txtRoomID.text ?? "", callerName: self.txtRoomID.text ?? "" )
     /*   let call = AppDelegate.shared.callManager.startCall(handle: "TEST", name: "TEST", videoEnable: false)
        let vc = ARController.viewController()
        vc.modalPresentationStyle = .fullScreen
        vc.call = call
        vc.roomName =  self.txtRoomID.text! // "room_name_\(userID)" //self.txtRoomID.text ?? ""
        self.present(vc, animated: true, completion: nil)
        */
        /////TEMP
//        self.localInvite =  self.rtmCallHandler.sendLocalInvite(to: self.txtRoomID.text!) { (errorCode) in
//            print("Local Invite Sent ==> \(errorCode.rawValue)")
//        }
        //////
        
        
        
        
        
     //   callStatusVC.roomName = "room_name_\(self.txtRoomID.text ?? "")" //self.txtRoomID.text ?? ""
     //   self.n(callStatusVC, animated: true, completion: nil)
        
        
//        let call = AppDelegate.shared.callManager.startCall(handle: AppGlobalManager.sharedInstance.currentUserID ?? "", videoEnable: true)
//
//        let parameters = [
//            "phone":self.txtRoomID.text ?? "",
//
//        ]
//
//        APIHandler.callAPI(endPoint: "getData", parameters: parameters, method: .POST) { (response) in
//            switch response {
//            case .success(let data):
//                DispatchQueue.main.async {
//
//
//                    /*
//                    print(String(data: data, encoding: .utf8))
//                    let vc = ARController.viewController()
//                    vc.roomName =  "room_name_\(self.txtRoomID.text ?? "")" //self.txtRoomID.text ?? ""
//                    vc.call = call
//                    self.present(vc, animated: true, completion: nil)
//                  //  self.navigationController?.pushViewController(vc, animated: true)
//
//
// */
//                }
//
//            case .failure( let error):
//                print("error",error)
//            }
//        }
//
        
  
    }

    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return  .portrait //UIDevice.current.userInterfaceIdiom == .phone ? [UIInterfaceOrientationMask.portrait, UIInterfaceOrientationMask.portraitUpsideDown] : .all
    }
    
}


extension ViewController: CallStatusDelegate {
    func callDisconntedByReciver(call:Call) {
        CoreDataManager.saveCall(call: call, duration: 0)
        AppDelegate.shared.status = .ideal
        call.end()
        AppDelegate.shared.callManager.remove(call: call) // important , endCallCXAction == CALLS =>   func provider(_ provider: CXProvider, perform action: CXEndCallAction)
        
        
        AppDelegate.shared.callManager.endCallCXAction(call: call, completion: nil)

    }
    
    func callAccpted(call:Call) {
        DispatchQueue.main.async {
           // let call = AppDelegate.shared.callManager.startCall(handle: AppGlobalManager.sharedInstance.currentUserID ?? "", videoEnable: true)
            
            let vc = ARController.viewController()
             vc.modalPresentationStyle = .fullScreen
            vc.roomName =  "room_name_\(self.txtRoomID.text ?? "")" //self.txtRoomID.text ?? ""
            vc.call = call
            self.present(vc, animated: true, completion: nil)
            

        }
    }
    
    func callDisconnectedByCaller(call:Call) {
        CoreDataManager.saveCall(call: call, duration: 0)
        AppDelegate.shared.status = .ideal
        call.end()
        AppDelegate.shared.callManager.remove(call: call) // important , endCallCXAction == CALLS =>   func provider(_ provider: CXProvider, perform action: CXEndCallAction)
        
        AppDelegate.shared.callManager.endCallCXAction(call: call, completion: nil)

    }
    
    
}



extension ViewController:AgoraRtmCallDelegate,Alertable {
    // CALLE GET INVITE
    func rtmCallKit(_ callKit: AgoraRtmCallKit, localInvitationReceivedByPeer localInvitation: AgoraRtmLocalInvitation) {
        showAlert(withMessage: "localInvitationReceivedByPeer")
    
        
    }
    //CALLE ACCEPT THE CALL
    func rtmCallKit(_ callKit: AgoraRtmCallKit, remoteInvitationAccepted remoteInvitation: AgoraRtmRemoteInvitation) {
        showAlert(withMessage: "remoteInvitationAccepted")

    }
    func rtmCallKit(_ callKit: AgoraRtmCallKit, remoteInvitationRefused remoteInvitation: AgoraRtmRemoteInvitation) {
        showAlert(withMessage: "remoteInvitationRefused")

    }
    
    //CALLER CANCELS THE CALL
    func rtmCallKit(_ callKit: AgoraRtmCallKit, remoteInvitationCanceled remoteInvitation: AgoraRtmRemoteInvitation) {
        showAlert(withMessage: "remoteInvitationCanceled")

    }
    
    func rtmCallKit(_ callKit: AgoraRtmCallKit, localInvitationFailure localInvitation: AgoraRtmLocalInvitation, errorCode: AgoraRtmLocalInvitationErrorCode) {
        showAlert(withMessage: "localInvitationFailure \(errorCode.rawValue)")
    }
    
    
    
    
    // APPDELEGATE
    
    
    
    func rtmCallKit(_ callKit: AgoraRtmCallKit, remoteInvitationReceived remoteInvitation: AgoraRtmRemoteInvitation) {
       showAlert(withMessage: "remoteInvitationReceived ")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            if Bool.random() {
                callKit.refuse(remoteInvitation) { (code) in
                    print("Remote Invite REFUSE \(code.rawValue)")
                }
            } else {
                callKit.accept(remoteInvitation) { (code) in
                    print("Remote Invite ACCEPTED \(code.rawValue)")
                }
            }
        }
      
       
    }
    
   
    
    func rtmCallKit(_ callKit: AgoraRtmCallKit, remoteInvitationFailure remoteInvitation: AgoraRtmRemoteInvitation, errorCode: AgoraRtmRemoteInvitationErrorCode) {
        showAlert(withMessage: "remoteInvitationFailure \(errorCode.rawValue)")

    }
}
