//
//  ForgotPasswordVC.swift
//  AgoraARKit
//
//  Created by Hetali on 07/04/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ForgotPasswordVC: UIViewController,Toastable {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnResetPass: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    private func callAPIForgotPassword() {
    guard let userName = self.txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines),!userName.isEmpty,isValidEmail(testStr: userName) else {
            self.showToast("Please enter valid  email address", type: .fail)
            return
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        let requestParameters = ["email":userName]
        print(requestParameters)
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.resetPass, parameters: requestParameters, method: .POST) { (response) in
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    let userResponse =  CommonCodableHelper<GeneralResponse>.codableObject(from: data)
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        self.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                        return
                    }
                    self.showToast(userResponse?.message ?? "We have e-mailed your password reset link!", type: .success)
                    self.dismiss(animated: true, completion: nil)
                }
                
            case .failure( let error):
                print("error",error)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
    }
      
    @IBAction func btnBackTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnResetPassTapped(_ sender: Any) {
        self.callAPIForgotPassword()
    }
   
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
        
    }
       
}
