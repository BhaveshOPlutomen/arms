//
//  SettingsListViewController.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 29/08/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation

class SettingsListViewController: BaseViewController,Alertable,Toastable {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    let geoCoder = CLGeocoder()

    var isSwitchOn = false
    var aryData = [Constants.profileKeys.Profile, Constants.profileKeys.Gallery,Constants.profileKeys.GEOLocation, Constants.profileKeys.ChangePassword, Constants.profileKeys.ContactUs, Constants.profileKeys.Feedback, Constants.profileKeys.AboutUs, Constants.profileKeys.TermsAndConditions, Constants.profileKeys.PrivacyPolicy, Constants.profileKeys.Help]
    
    // MARK: - VC Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        LocationHelper.shared.locationManagerDelegate = self
        NotificationCenter.default.removeObserver(self, name: .locationPermission, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userPermission(_:)), name: .locationPermission, object: nil)
         
    }
    @objc func userPermission(_ notification: Notification) {
            guard let dict = notification.object as? [String:Bool] else { return }
            let permission = dict["permission"] ?? false
            if permission {
                if let cell = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? SettingsListTableViewCell {
                    cell.btnSwitch.isSelected = true
                    LocationHelper.shared.locationManagerDelegate = self
                }
            } else {
                if let cell = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? SettingsListTableViewCell {
                    cell.btnSwitch.isSelected = false
                }
                UserDefaults.standard.removeObject(forKey: "Location")
                UserDefaults.standard.removeObject(forKey: "Latitude")
                UserDefaults.standard.removeObject(forKey: "Longitude")
            }
        }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    // MARK: - Custom Methods
    class func viewController () -> SettingsListViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SettingsListViewController") as! SettingsListViewController
    }
   
}

extension SettingsListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsListTableViewCell", for: indexPath) as! SettingsListTableViewCell
        cell.selectionStyle = .none
        let name = aryData[indexPath.row]
        cell.lblTitle.text = name
      
        if((UserDefaults.standard.string(forKey: "Location")) != nil){
            cell.btnSwitch.isSelected = true
        }
        if aryData[indexPath.row] == Constants.profileKeys.GEOLocation {
            cell.tappedOnSwitch = { status in
                if(status){
                    //switch on
                    LocationHelper.shared.locationManagerDelegate = self
                    LocationHelper.shared.startUpdatingLocation()
                                           
                    if(LocationHelper.shared.isLocationEnable){
                        cell.btnSwitch.isSelected = true
                    }
                }else{
                    //switch off
                   // AppGlobalManager.sharedInstance.latitude = 0.0
                    //AppGlobalManager.sharedInstance.longitude = 0.0
                    //AppGlobalManager.sharedInstance.locationString = ""
                    UserDefaults.standard.removeObject(forKey: "Location")
                    UserDefaults.standard.removeObject(forKey: "Latitude")
                    UserDefaults.standard.removeObject(forKey: "Longitude")
                    cell.btnSwitch.isSelected = false
                }
            }
            cell.btnSwitch.isHidden = false
            cell.imgArrow.isHidden = true
        }else{
            cell.btnSwitch.isHidden = true
            cell.imgArrow.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if aryData[indexPath.row] == Constants.profileKeys.Gallery {
            let vc = GalleryVC.viewController()
            vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(aryData[indexPath.row] == Constants.profileKeys.Profile){
            let vc = SettingsVC.viewController()
            vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
        }else if(aryData[indexPath.row] == Constants.profileKeys.GEOLocation){
            
        }else if(aryData[indexPath.row] == Constants.profileKeys.ChangePassword){
            let next = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.navigationController?.pushViewController(next, animated: true)
        }else if(aryData[indexPath.row] == Constants.profileKeys.Feedback){
                let next = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
                self.navigationController?.pushViewController(next, animated: true)
        }else if(aryData[indexPath.row] == Constants.profileKeys.PrivacyPolicy){
            if UserDefaults.standard.string(forKey: "privacy_policy") != nil {
                let privacy_policy = UserDefaults.standard.string(forKey: "privacy_policy")!
                
                let next = self.storyboard?.instantiateViewController(withIdentifier: "WkWebViewVC") as! WkWebViewVC
                next.openLink(title: Constants.profileKeys.PrivacyPolicy, urlString: privacy_policy)
                self.navigationController?.pushViewController(next, animated: true)
            }
        }else if(aryData[indexPath.row] == Constants.profileKeys.Help){
            if UserDefaults.standard.string(forKey: "help") != nil {
                let help = UserDefaults.standard.string(forKey: "help")!
                
                let next = self.storyboard?.instantiateViewController(withIdentifier: "WkWebViewVC") as! WkWebViewVC
                next.openLink(title: Constants.profileKeys.Help, urlString: help)
                self.navigationController?.pushViewController(next, animated: true)
            }
        }else if(aryData[indexPath.row] == Constants.profileKeys.ContactUs){
            if UserDefaults.standard.string(forKey: "contact_us") != nil {
                
                let contactUs = UserDefaults.standard.string(forKey: "contact_us")!
                
                let next = self.storyboard?.instantiateViewController(withIdentifier: "WkWebViewVC") as! WkWebViewVC
                next.openLink(title: Constants.profileKeys.ContactUs, urlString: contactUs)
                self.navigationController?.pushViewController(next, animated: true)
            }
        }else if(aryData[indexPath.row] == Constants.profileKeys.TermsAndConditions){
            if UserDefaults.standard.string(forKey: "terms_cond") != nil {
                let terms_cond = UserDefaults.standard.string(forKey: "terms_cond")!
                
                let next = self.storyboard?.instantiateViewController(withIdentifier: "WkWebViewVC") as! WkWebViewVC
                next.openLink(title: Constants.profileKeys.TermsAndConditions, urlString: terms_cond)
                self.navigationController?.pushViewController(next, animated: true)
            }
        }else if(aryData[indexPath.row] == Constants.profileKeys.AboutUs){
            if UserDefaults.standard.string(forKey: "about_us") != nil {
                let about_us = UserDefaults.standard.string(forKey: "about_us")!
                
                let next = self.storyboard?.instantiateViewController(withIdentifier: "WkWebViewVC") as! WkWebViewVC
                next.openLink(title: Constants.profileKeys.AboutUs, urlString: about_us)
                self.navigationController?.pushViewController(next, animated: true)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (UIDevice.current.userInterfaceIdiom == .pad){
            return 65
        }
        else{
            return 50
        }
    }
    
    
}

extension SettingsListViewController: LocationManagerDelegate {

    func getLocation(location: CLLocation) {
        AppGlobalManager.sharedInstance.latitude = location.coordinate.latitude
        AppGlobalManager.sharedInstance.longitude = location.coordinate.longitude
        
        geoCoder.reverseGeocodeLocation(location) { placemarks, _ in
            if let place = placemarks?.first {
                let currentCityName : String = place.locality ?? ""
                let streetName : String = place.thoroughfare ?? ""
                let nameofplace : String = place.name ?? ""
                let administrativeArea : String = place.administrativeArea ?? ""
                let postalCode : String = place.postalCode ?? ""
                let combineString = ("\(currentCityName),\(streetName),\(nameofplace),\(administrativeArea),\(postalCode)")
                
                UserDefaults.standard.set(combineString, forKey: "Location")
                UserDefaults.standard.double(forKey: "Latitude")
                UserDefaults.standard.double(forKey: "Longitude")
                
                AppGlobalManager.sharedInstance.locationString = combineString

            }
        }

    }
}
