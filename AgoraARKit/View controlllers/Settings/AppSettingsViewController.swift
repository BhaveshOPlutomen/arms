//
//  AppSettingsViewController.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 30/06/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit

class AppSettingsViewController: UIViewController {

   override func viewDidLoad() {
      super.viewDidLoad()
      DispatchQueue.main.asyncAfter(deadline: .now()) {
        if !Reachability.isConnectedToNetwork() {
          let alert = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: .alert)
          let ok = UIAlertAction(title: "Retry", style: .default) { (act) in
            self.callAPIAppSetting()
          }
          let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
          alert.addAction(ok)
          alert.addAction(cancel)
          self.present(alert, animated: true, completion: nil)
        } else {
          self.callAPIAppSetting()
        }
      }
    }
    override func viewWillAppear(_ animated: Bool) {
    }
  
}


extension AppSettingsViewController {
    
    func callAPIAppSetting(){
        
        guard let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String else {
            return
        }
        var param = [String:Any]()
        param["app_version"] = appVersion
        param["device_type"]  = "0"
        print(param)
        APIHandler.callAPI(endPoint:  APIConstants.EndPoints.appSetting, parameters: param, method: .POST) { (response) in
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    let userResponse =  CommonCodableHelper<AppLinkJoinResponse>.codableObject(from: data)
                    guard let status = userResponse?.statusCode,status == 1 else {
                        if let messageCode = userResponse?.messageCode{
                            if messageCode == "app_ver_match_fail"
                            {
                                let alert = UIAlertController(title: "", message: userResponse?.message, preferredStyle: .alert)
                                let ok = UIAlertAction(title: "Update", style: .default) { (act) in
                                    UIApplication.shared.open((NSURL(string: "https://apps.apple.com/us/app/plutomen-arms/id1502089068")! as URL), options: [:], completionHandler: { (status) in
                                    })
                                }
                                alert.addAction(ok)
                                (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController?.present(alert, animated: true, completion: nil)
                            }
                        }
                        return
                    }
                    if let messageCode = userResponse?.messageCode{
                        if messageCode == "app_ver_match_fail"{
                        }
                        else if messageCode == "app_ver_match"{
                            AppGlobalManager.sharedInstance.setupFlow()
                            
                            UserDefaults.standard.set(userResponse?.output?.privacy_policy,forKey: "privacy_policy")
                            UserDefaults.standard.set(userResponse?.output?.help,forKey: "help")
                            UserDefaults.standard.set(userResponse?.output?.contact_us,forKey: "contact_us")
                            UserDefaults.standard.set(userResponse?.output?.terms_cond,forKey: "terms_cond")
                            UserDefaults.standard.set(userResponse?.output?.about_us,forKey: "about_us")
                            
                            
                        }
                    }
                    
                }
            case .failure( let error):
                print("error",error)
            }
        }
    }
}
