//
//  FeedbackVC.swift
//  AgoraARKit
//
//  Created by Hetali on 07/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class FeedbackVC: UIViewController,Toastable {
    
    @IBOutlet weak var txtfeedback: UITextView!
    
    @IBOutlet weak var btnGood: UIButton!
    
    @IBOutlet weak var btnAverage: UIButton!
    
    @IBOutlet weak var btnBad: UIButton!
    
    var feedbackType : String?
    
    @IBOutlet weak var btnFeedback: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Feedback"

        navigationController?.navigationBar.prefersLargeTitles = false
        
        let image = UIImage(named: "back")
        let backButton = UIBarButtonItem(image: image, style: .done, target: self, action: #selector(backAction))
        backButton.tintColor = .white
        navigationItem.leftBarButtonItem = backButton
        
        txtfeedback.returnKeyType = UIReturnKeyType.done
        
        btnFeedback.isUserInteractionEnabled = false
        btnFeedback.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorLightGray.color

    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnGoodTapped(_ sender: Any) {
        feedbackType = "Good"
        btnGood.setImage(UIImage(named: "Good_smile_green"), for: .normal)
        btnAverage.setImage(UIImage(named: "Avg_smile_white"), for: .normal)
        btnBad.setImage(UIImage(named: "Bad_smile_white"), for: .normal)
        btnFeedback.isUserInteractionEnabled = true
        btnFeedback.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.green.color
    }
    
    @IBAction func btnAverageTapped(_ sender: Any) {
        feedbackType = "Average"
        btnAverage.setImage(UIImage(named: "Avg_smile_geeen"), for: .normal)
        btnGood.setImage(UIImage(named: "Good_smile_white"), for: .normal)
        btnBad.setImage(UIImage(named: "Bad_smile_white"), for: .normal)
        btnFeedback.isUserInteractionEnabled = true
        btnFeedback.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.green.color
    }
    
    @IBAction func btnBadTapped(_ sender: Any) {
        feedbackType = "Bad"
        btnBad.setImage(UIImage(named: "Bad_smile_geeen"), for: .normal)
        btnAverage.setImage(UIImage(named: "Avg_smile_white"), for: .normal)
        btnGood.setImage(UIImage(named: "Good_smile_white"), for: .normal)
        btnFeedback.isUserInteractionEnabled = true
        btnFeedback.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.green.color
    }
    
    @IBAction func btnSendFeedbacktapped(_ sender: Any) {
        if feedbackType != nil  {
            callFeedbackService()
        }
    }
    
    func callFeedbackService(){
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        var strFeedback = txtfeedback.text
        if strFeedback == "Describe your experience here..."{
            strFeedback = ""
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        let requestParameters = ["feedback":feedbackType,
                                 "description" : strFeedback,
                                 "device_id" : device_id
        ]
        print("feedback params %@",requestParameters)
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.appfeedback, parameters: requestParameters as [String : Any], token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    let userResponse =  CommonCodableHelper<GeneralResponse>.codableObject(from: data)
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        self.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        
                        return
                    }
                    
                    self.btnBad.setImage(UIImage(named: "Bad_smile_white"), for: .normal)
                    self.btnAverage.setImage(UIImage(named: "Avg_smile_white"), for: .normal)
                    self.btnGood.setImage(UIImage(named: "Good_smile_white"), for: .normal)
                    self.txtfeedback.text = "Describe your experience here..."
                    
                    self.feedbackType = nil
                    
                    self.btnFeedback.isUserInteractionEnabled = false
                    self.btnFeedback.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorLightGray.color
                    
                    self.showToast(userResponse?.message ?? "Your feedback saved Successfully!", type: .success)
                }
                
            case .failure( let error):
                print("error",error)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
    }
    
    
}
extension FeedbackVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        txtfeedback.text = ""
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
        }
        return true
    }
}
