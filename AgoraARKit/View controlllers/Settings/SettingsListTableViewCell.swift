//
//  SettingsListTableViewCell.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 29/08/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit

class SettingsListTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var btnSwitch: UIButton!
    
    var tappedOnSwitch: ((Bool) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnSwitchAction(_ sender: UIButton) {
        if(btnSwitch.isSelected){
//            btnSwitch.isSelected = false
            tappedOnSwitch?(false)
        }else{
//            btnSwitch.isSelected = true
            tappedOnSwitch?(true)
        }
    }
}

