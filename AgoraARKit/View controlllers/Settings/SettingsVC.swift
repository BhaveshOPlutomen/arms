//
//  SettingsVC.swift
//  AgoraARKit
//
//  Created by Prashant on 30/05/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Firebase

class SettingsVC: BaseViewController , Alertable,Toastable {

    //MARK:- Outlets
    
    @IBOutlet weak var txtTestRoomName: UITextField!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtDepartment: UITextField!
    @IBOutlet weak var lblBuildNO: UILabel!
    
    public typealias Parameters = [String: Any]

    @IBOutlet weak var btnTest: UIButton!
    //MARK:- Var
    
    
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Memory Managment Methods
    
    //--------------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Abstract Methdos
    
    //--------------------------------------------------------------------------------
    
    class func viewController () -> SettingsVC {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
    }
    
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Custom Methdos
    
    //--------------------------------------------------------------------------------

    private func setupData() {
         let user = AppGlobalManager.sharedInstance.loggedInUser?.user
        self.lblUserName.text = user?.name
        self.txtEmailAddress.text = user?.email
        self.txtMobileNo.text = user?.mobileNo
        self.txtDepartment.text = user?.department

        self.lblBuildNO.text =  "Version \(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? "1.12")"
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Action Methdos
    
    //--------------------------------------------------------------------------------

    @IBAction func btnLogoutTapped(_ sender: Any) {
        
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.callLogoutAPI()
//            AppGlobalManager.sharedInstance.logoutAndClearAllCache()
        }
        let noAction = UIAlertAction(title: "No", style: .destructive, handler: nil)
        
        self.showAlert(withMessage: "Are you sure you want to logout?", title: "Logout", customActions: [noAction,yesAction])

    }
    
    //--------------------------------------------------------------------------------

    
    @IBAction func btnTestTapped(_ sender: Any) {
//        
//        let vc = SettingsListViewController.viewController()
//        vc.modalPresentationStyle = .fullScreen
//        self.navigationController?.pushViewController(vc, animated: true)
//        
       let call = AppDelegate.shared.callManager.startCall(handle: "TEST", name: "TEST", videoEnable: false)
        let vc = ARController.viewController()
         vc.modalPresentationStyle = .fullScreen
        vc.call = call
        vc.roomName =  self.txtTestRoomName.text! // "room_name_\(userID)" //self.txtRoomID.text ?? ""
        self.present(vc, animated: true, completion: nil)

//        let x: Int? = nil
//        let y = 5 + x!

    }
    
  
   
    //--------------------------------------------------------------------------------
    
    //MARK:- ViewLifeCycle Methods
    
    //--------------------------------------------------------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtTestRoomName.text = "room_name_4000"
        setupData()
        
        if APIConstants.environment == .local{
            btnTest.isHidden = false
        }else{
            btnTest.isHidden = false
        }
        
        let image = UIImage(named: "back")
        let backButton = UIBarButtonItem(image: image, style: .done, target: self, action: #selector(backAction))
        backButton.tintColor = .white
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //--------------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //--------------------------------------------------------------------------------
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
   
    //--------------------------------------------------------------------------------
    
    //MARK:-  loout service Methdos
    
    //--------------------------------------------------------------------------------
    private func callLogoutAPI(){
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        guard let userID = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else {
            return
        }
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
                  return
        }
            let requestParameters = ["user_id": userID,
                                     "device_id": device_id,
                                     "device_type" : "0"
                ] as [String : Any]
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.userLogout, parameters: requestParameters as [String : Any],token: AppGlobalManager.sharedInstance.token, method: .POST) {[weak self] (response) in
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    let userResponse =  CommonCodableHelper<GeneralResponse>.codableObject(from: data)

                    guard let status = userResponse?.statusCode,status == 1 else {
                        self!.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                        return
                    }
                    if let messageCode = userResponse?.messageCode{
                        if messageCode == "log_out"{
                            AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                            self!.showToast(userResponse?.message ?? "Successfully logged out", type: .success)
                        }
                        if messageCode == "fal_log_out"{
                            self!.showToast(userResponse?.message ??  "Something went wrong !! Please try again later", type: .fail)
                        }
                    }
                  

                }
                
            case .failure( let error):
                print("error",error)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
            
        }
        
    }
}
extension Data {
  mutating func append(_ string: String) {
    if let data = string.data(using: .utf8) {
      append(data)
    }
  }
}
struct Media {
  let key: String
  let filename: String
  let data: Data
  let mimeType: String
  init?(withImage image: UIImage, forKey key: String) {
    self.key = key
    self.mimeType = "image/jpeg"
    self.filename = "kyleleeheadiconimage234567.jpg"
    guard let data = image.jpegData(compressionQuality: 0.7) else { return nil }
    self.data = data
  }
}
