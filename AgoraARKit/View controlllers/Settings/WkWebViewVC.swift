//
//  WkWebViewVC.swift
//  AgoraARKit
//
//  Created by Hetali on 07/09/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import WebKit
import NVActivityIndicatorView

class WkWebViewVC: UIViewController,WKNavigationDelegate {
    
    lazy var webView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationController?.navigationBar.prefersLargeTitles = false
        
        webView.navigationDelegate = self
        
        webView.backgroundColor = Constants.AppTheme.Colors.ColorsOfApp.colorPrimaryDark.color
        
        let image = UIImage(named: "back")
        let backButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(backAction))
        backButton.tintColor = .white
        navigationItem.leftBarButtonItem = backButton
                
    }
    override func viewWillAppear(_ animated: Bool) {
         let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    override func loadView() {
        self.view = webView
    }
    func openLink(title: String,urlString : String){
        self.title = title
        
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
   
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension WKWebView {
    func load(_ urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            load(request)
        }
    }
}
