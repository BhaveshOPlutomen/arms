//
//  LoginVC.swift
//  AgoraARKit
//
//  Created by Prashant on 07/08/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import OTPFieldView

class LoginVC: BaseViewController,Toastable {

    //MARK:- Outlets
    
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var pushManager:AppPushManager!

    @IBOutlet weak var view_guest: UIView!
    
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtMobile: UITextField!
    
    @IBOutlet weak var JoinCodeView: OTPFieldView!
    
    @IBOutlet weak var view_joinCode: UIView!
    
    var code_from_OTPView : String?
    
    var HasEnteredAllOTP : Bool?
    
    @IBOutlet weak var lblConnecting: UILabel!
    
    //MARK:- Var
    
    
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Memory Managment Methods
    
    //--------------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Abstract Methdos
    
    //--------------------------------------------------------------------------------
    
    class func viewController () -> LoginVC {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    }
    
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Custom Methdos
    
    //--------------------------------------------------------------------------------

    private func callAPIToLogin() {
        guard let userParameters = validateEmptyField() else {
            return
        }
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        var deviceToken = UserDefaults.standard.object(forKey: "TOKEN") ?? ""

        #if targetEnvironment(simulator)
        
        deviceToken = "d45f7c51bb506c8bea6649eaffc496b230122084b3bcd3b5b1dbbaeaa8deb10b"
        
        UserDefaults.standard.setValue(deviceToken, forKey: "TOKEN")
        
        #else
        
        
        #endif
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        let requestParameters = ["email":userParameters.userName,
                                 "password":userParameters.password,
                                  "device_type":"0",
                                  "device_id" : device_id,
//                                  "device_token":UserDefaults.standard.object(forKey: "TOKEN") ?? "",
                                  "device_token": deviceToken,
                                  "invite_token":  UserDefaults.standard.object(forKey: "fcmToken") ?? "" ]
        print(requestParameters)
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.userLogin, parameters: requestParameters, method: .POST) { (response) in
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                   let userResponse =  CommonCodableHelper<LoginResponse>.codableObject(from: data)
                                        
                    guard let status = userResponse?.statusCode,status == 1 else {
                        self.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                        return
                    }
                    
                    if let output = userResponse?.output {
                        self.handleResponse(output: output,data:data)
                        
                    } else {
                        self.showToast("Something went wrong !! Please try again  later", type: .fail)
                    }
               
                }
                
            case .failure( let error):
                print("error",error)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
            
        }
    
    }
        //--------------------------------------------------------------------------------

        private func handleResponse(output:LoginResponse,data:Data) {
            AppGlobalManager.sharedInstance.loggedInUser = output
            UserDefaults.standard.set(AppGlobalManager.sharedInstance.currentUserID, forKey: "USER_PHONE")

            if AppGlobalManager.sharedInstance.agoraAppId != nil{
                KeyCenter.AppId = AppGlobalManager.sharedInstance.agoraAppId!
//                print(KeyCenter.AppId)
//                KeyCenter.AppId = "e6ae004e158b473c9154595bcc5ec901"

    //            UserDefaults.standard.set(AppGlobalManager.sharedInstance.agoraAppId,forKey: "AgoraAppId")
            }
    //       pushManager = AppPushManager()
    //        pushManager.setupRTMHandler()
            UserDefaults.standard.set(data, forKey: "UserObject")
            let vc = TabbarController.viewController()
            vc.selectedIndex = 3
            AppDelegate.shared.window?.rootViewController = vc
            
            SocketIOManager.shared.closeConnection()
            SocketIOManager.shared.isSocketOn = false
            SocketIOManager.shared.establishConnection()
            
          //  AppDelegate.shared.agoraSignalManager.login(phoneNumber: AppGlobalManager.sharedInstance.currentUserID!)
            
            //Register Device Token
    //        APIHandler.registerDeviceToken(userID: output.user?.mobileNo ?? "", completion: nil)
        }
    private func handleResponseGuest(output:LoginResponse,data:Data) {
             AppGlobalManager.sharedInstance.loggedInUser = output
             UserDefaults.standard.set(AppGlobalManager.sharedInstance.currentUserID, forKey: "USER_PHONE")
            
             if AppGlobalManager.sharedInstance.agoraAppId != nil{
                 KeyCenter.AppId = AppGlobalManager.sharedInstance.agoraAppId!
                 print(KeyCenter.AppId)
             }
    
     //       pushManager = AppPushManager()
     //        pushManager.setupRTMHandler()
             UserDefaults.standard.set(data, forKey: "UserObject")
           
         }
    private func guestLoginAPI(){
        guard let userParameters = validateEmptyFieldGuest() else {
            return
        }
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        let requestParameters = ["name":userParameters.userName,
                                 "mobile_no":userParameters.mobilenumber,
                                 "email" : userParameters.email,
                                 "device_id" : device_id,
                                  "device_type":"0",
                                  "invite_token":  UserDefaults.standard.object(forKey: "fcmToken") ?? "" ]
        print(requestParameters)
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.createGuest, parameters: requestParameters, method: .POST) { (response) in
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                   let userResponse =  CommonCodableHelper<LoginResponse>.codableObject(from: data)
                                        
                    guard let status = userResponse?.statusCode,status == 1 else {
                        self.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                        self.view_joinCode.isHidden = true

                        return
                    }
                    
                    if let output = userResponse?.output {
                        self.handleResponseGuest(output: output,data:data)
                        self.view_guest.isHidden = true
                    
                        self.showToast(userResponse?.message ?? "Guest user created successfullly.", type: .success)
                        
                        if(!AppGlobalManager.sharedInstance.linkURL.isEmpty){
                            HandleUniversalLink.sharedInstance.universalLinkCreateSession(url:AppGlobalManager.sharedInstance.linkURL)
                            AppGlobalManager.sharedInstance.linkURL = ""
                        }
                        else{
                            self.view_joinCode.isHidden = false
                            self.setupOtpViewTry()
                        }
                        
                    } else {
                        self.showToast("Something went wrong !! Please try again  later", type: .fail)
                    }
                    
                    
                  
                }
                
            case .failure( let error):
                print("error",error)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
            
        }
        
        
        
    }

    //--------------------------------------------------------------------------------

    private func validateEmptyField() -> (userName:String,password:String)? {
        guard let userName = self.txtPhoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines),!userName.isEmpty,isValidEmail(testStr: userName) else {
            self.showToast("Please enter valid  email address", type: .fail)
            return nil
        }
        
        guard let password = self.txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines),!password.isEmpty else {
            self.showToast("Please enter valid password", type: .fail)
            return nil
        }
        
        return (userName,password)
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
        
    }
    
    func isValidMobile(value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]{6,14}$"

        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result = phoneTest.evaluate(with: value)
        return result
    }
    
    private func validateEmptyFieldGuest() -> (userName:String,email:String,mobilenumber:String)? {
        guard let userName = self.txtName.text, !userName.isEmpty  else {
            self.showToast("Please enter your name", type: .fail)
            return nil
        }
        
        guard let email = self.txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines),!email.isEmpty,isValidEmail(testStr: email) else {
            self.showToast("Please enter valid email", type: .fail)
            return nil
        }
        guard let mobilenumber = self.txtMobile.text,!mobilenumber.isEmpty,isValidMobile(value: mobilenumber) else {
            self.showToast("Please enter valid mobilenumber", type: .fail)
            return nil
        }
        
        return (userName,email,mobilenumber)
        
    }
    func setupOtpViewTry(){
           self.JoinCodeView.fieldsCount = 9
           self.JoinCodeView.fieldBorderWidth = 2
           self.JoinCodeView.defaultBorderColor = UIColor.white
           self.JoinCodeView.filledBorderColor = UIColor.init(named: "Green") ?? UIColor.green
           self.JoinCodeView.cursorColor = UIColor.init(named: "Green") ?? UIColor.green
           self.JoinCodeView.displayType = .underlinedBottom
           self.JoinCodeView.fieldSize = 30
           self.JoinCodeView.separatorSpace = 8
           self.JoinCodeView.shouldAllowIntermediateEditing = false
           self.JoinCodeView.delegate = self
           self.JoinCodeView.initializeUI()
       }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Action Methdos
    
    //--------------------------------------------------------------------------------

    @IBAction func btnLoginTapped(_ sender: Any) {
        callAPIToLogin()
    }
    @IBAction func btnGuestLoginTapped(_ sender: Any) {
        guestLoginAPI()
    }
    @IBAction func btnBack(_ sender: Any) {
        self.view_guest.isHidden = true
    }
    @IBAction func btnGuestLohinTappedMain(_ sender: Any) {
        
        if AppGlobalManager.sharedInstance.loggedInUser != nil {
//            SocketIOManager.shared.closeConnection()
//            SocketIOManager.shared.isSocketOn = false
//            SocketIOManager.shared.establishConnection()
//            
            self.view_guest.isHidden = true
            view_joinCode.isHidden = false
            setupOtpViewTry()
            
        }else{
            self.view_guest.isHidden = false
        }
    }
    @IBAction func btnJoinSessionTapped(_ sender: Any) {
        lblConnecting.text = "Connecting"
        if let myString = code_from_OTPView, !myString.isEmpty {
            if(HasEnteredAllOTP == true){

                HandleUniversalLink.sharedInstance.callreceiverLinkJoin(sessionId: myString)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute:  {
                    
                    self.view_joinCode.isHidden = true
                })
            }else{
                self.showToast("Please enter 9 digit code to join a call.", type: .fail)
            }
        }else{
            self.showToast("Please enter 9 digit code", type: .fail)
        }
        
    }
    
    @IBAction func btnBackJoinSessionScreen(_ sender: Any) {
        self.view_joinCode.isHidden = true
        self.JoinCodeView.resignResponder()
    }
    //--------------------------------------------------------------------------------
    
    //MARK:- ViewLifeCycle Methods
    
    //--------------------------------------------------------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
          view_joinCode.isHidden = true
        view_guest.isHidden = true

    }
    
    //--------------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        view_guest.isHidden = true
        lblConnecting.text = ""

//        if(AppGlobalManager.sharedInstance.isGuestModeOn == true){
//            view_guest.isHidden = false
//        }
    }
    
    //--------------------------------------------------------------------------------
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
}

extension LoginVC: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        
        HasEnteredAllOTP = hasEntered
        
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        code_from_OTPView = otpString
    }
}
