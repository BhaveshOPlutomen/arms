//
//  MoreVC.swift
//  AgoraARKit
//
//  Created by Prashant on 09/05/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit

class MoreVC: BaseViewController {

    @IBOutlet weak var viewScreenShot: UIView!
    @IBOutlet weak var imgLikeDisLike: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewScreenShot.layer.cornerRadius = 12
        
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(cardPanned(gesture:)))
        viewScreenShot.addGestureRecognizer(panGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func cardPanned(gesture:UIPanGestureRecognizer) {
        let card = gesture.view!
        let translateValue = gesture.translation(in:view)
        let xDistanceFromCenter = card.center.x - view.center.x
        
        card.center = CGPoint(x: view.center.x + translateValue.x, y: view.center.y + translateValue.y)
        
      //  print("xDistanceFromCenter : \(xDistanceFromCenter),  card.center.x: \(card.center.x) and Alpha = \(abs(xDistanceFromCenter) / view.center.x)  ")

        
        self.imgLikeDisLike.alpha =  abs(xDistanceFromCenter) / view.center.x


        let scale = min(100 / abs(xDistanceFromCenter ),1)
        print("scale == \(scale)")
        NSLog("SCALE ")
        card.transform = CGAffineTransform(rotationAngle: 0.61 * xDistanceFromCenter / view.center.x).scaledBy(x: scale, y: scale)
        
        
        if xDistanceFromCenter > 0 {
                // Move Right
            self.imgLikeDisLike.image = #imageLiteral(resourceName: "undo_white")
        } else if xDistanceFromCenter < 0 {
            // Move left
            self.imgLikeDisLike.image = #imageLiteral(resourceName: "ic_close")
        }
        
        if gesture.state == .ended {
            
            if card.center.x > self.view.frame.width - 75 {
                print("DRAG TO RIGHT")
                
                UIView.animate(withDuration: 0.2 ,animations: {
                    card.center = CGPoint(x: self.view.frame.maxX + card.frame.width / 2 , y: card.center.y + 75 )
                    card.alpha = 0
                }) { completed in
                  //  card.center = CGPoint(x:  -card.frame.width / 2 , y: -card.center.y  )
                    card.center = self.view.center

                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                        self.reestCard()

                    })
                }
               
                
                return
            } else if  (card.center.x < 75) {
                print("DRAG TO LEFT")

                  UIView.animate(withDuration: 0.2 ,animations: {
                    card.center = CGPoint(x: self.view.frame.origin.x - card.frame.width / 2 , y: card.center.y + 75)
                    card.alpha = 0

                  }) { completed in
                    card.center = self.view.center

                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                        self.reestCard()
                        
                    })
                    
                }
                return
            }
           
            self.reestCard()
        }
        
    }
    
    @IBAction func btnTapped(_ sender: Any) {
        
        //TESTING
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        let filename = paths[0].appendingPathComponent("output.txt")
        
        try? FileManager.default.removeItem(at: filename)
        
        
        
        reestCard()
//        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0)
//
//    //    self.viewScreenShot.layer.render(in: UIGraphicsGetCurrentContext()!)
//        self.view.drawHierarchy(in: CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: view.frame.width, height: view.frame.height), afterScreenUpdates: true)
//
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//        // FULL
//
//
//        let cgIImage =  image?.cgImage?.cropping(to: CGRect(origin: CGPoint(x: viewScreenShot.frame.origin.x * UIScreen.main.scale  , y: viewScreenShot.frame.origin.y * UIScreen.main.scale), size: CGSize(width: viewScreenShot.bounds.width * UIScreen.main.scale, height: viewScreenShot.bounds.height * UIScreen.main.scale)))
//
//        let newImage = UIImage(cgImage: cgIImage!, scale: UIScreen.main.scale, orientation: UIImage.Orientation.up)
//
//        (self.view.viewWithTag(90) as? UIImageView)?.image = newImage
//
//        UIGraphicsEndImageContext()
        
        
    }
    
    func reestCard() {
        self.imgLikeDisLike.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.viewScreenShot.transform = CGAffineTransform.identity

            self.viewScreenShot.center = self.view.center
            self.viewScreenShot.alpha = 1
        }
    }
    
   

}
