//
//  ChangePasswordVC.swift
//  AgoraARKit
//
//  Created by Hetali on 07/04/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ChangePasswordVC: UIViewController,Toastable {
    
    @IBOutlet weak var txtOldPass: UITextField!
    @IBOutlet weak var txtNewPass: UITextField!
    @IBOutlet weak var txtConfirmPass: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = false
        
        self.title = "Change Password"
        
        let image = UIImage(named: "back")
        let backButton = UIBarButtonItem(image: image, style: .done, target: self, action: #selector(backAction))
        backButton.tintColor = .white
        navigationItem.leftBarButtonItem = backButton
      

    }
    
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnChangePassTapped(_ sender: Any) {
        guard let oldPassword = self.txtOldPass.text?.trimmingCharacters(in: .whitespacesAndNewlines),!oldPassword.isEmpty else {
            self.showToast("Please enter valid password", type: .fail)
            return
        }
        guard let newPassword = self.txtNewPass.text?.trimmingCharacters(in: .whitespacesAndNewlines),!newPassword.isEmpty else {
            self.showToast("Please enter valid password", type: .fail)
            return
        }
        guard let newPasswordConfirm = self.txtConfirmPass.text?.trimmingCharacters(in: .whitespacesAndNewlines),!newPasswordConfirm.isEmpty else {
            self.showToast("Please enter valid password", type: .fail)
            return
        }
        if(txtOldPass.text!.count < 8){
            self.showToast("Please Enter Password greater than 8 characters", type: .fail)
            return
        }
        if(txtNewPass.text!.count < 8){
            self.showToast("Please Enter Password greater than 8 characters", type: .fail)
            return
        }
        if(txtConfirmPass.text!.count < 8){
            self.showToast("Please Enter Password greater than 8 characters", type: .fail)
            return
        }
        if (txtNewPass.text != txtConfirmPass.text){
            self.showToast("New Password and Confirm Password Should be same", type: .fail)
            return
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        let requestParameters = ["oldpassword":oldPassword,
                                 "password" : newPassword,
                                 "device_type" : "0",
                                 "password_confirmation" : newPasswordConfirm
        ]
        print("Change password params %@",requestParameters)
        
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.changePass, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    let userResponse =  CommonCodableHelper<GeneralResponse>.codableObject(from: data)
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        self.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                        self.txtOldPass.text = ""
                        self.txtNewPass.text = ""
                        self.txtConfirmPass.text = ""
                        return
                    }
                    
                    AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                    self.showToast(userResponse?.message ?? "Password Change Successfully!", type: .success)
                    
                }
                
            case .failure( let error):
                print("error",error)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
