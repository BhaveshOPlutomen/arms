//
//  TabbarController.swift
//  AgoraARKit
//
//  Created by Prashant on 08/05/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class TabbarController: UITabBarController,Toastable,Alertable {

    //MARK:- Outlets
    
    
    //MARK:- Var
    
    
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Memory Managment Methods
    
    //--------------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Abstract Methdos
    
    //--------------------------------------------------------------------------------
    
    class func viewController () -> TabbarController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
    }
    
    
    //--------------------------------------------------------------------------------
    
    //MARK:- ViewLifeCycle Methods
    
    //--------------------------------------------------------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        tabBar.layer.shadowOffset = CGSize(width: 0, height: -1)
//        tabBar.layer.shadowRadius = 2
//        tabBar.layer.shadowColor = UIColor.init(red: 10/255.0, green: 14/255.0, blue: 19/255.0, alpha: 1.0).cgColor
//        tabBar.layer.shadowOpacity = 0.3
        
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                 self.selectedIndex = 0
               })
        
        syncApiCall()
        
     
        if #available(iOS 13.0, *) {
            let appearance = UITabBarAppearance()
            appearance.backgroundColor = UIColor(named: "PrimaryDark")
            appearance.shadowImage = UIImage()
            appearance.shadowColor = UIColor.init(red: 10/255.0, green: 14/255.0, blue: 19/255.0, alpha: 1.0)
            
            appearance.backgroundImage = UIImage.init(color: UIColor(named: "PrimaryDark")!, size: CGSize(width: 1, height: 1))
            self.tabBar.standardAppearance = appearance
            
        } else {
            // Fallback on earlier versions
        }

      
    }
    
    //--------------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //--------------------------------------------------------------------------------
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
       return UIDevice.current.userInterfaceIdiom == .phone ? [.portrait] : .portrait
        
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  sync API call Methdos
    
    //--------------------------------------------------------------------------------

    private func syncApiCall(){
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        guard let userID = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else{
            return
        }
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        let requestParameters = ["user_id" : userID,
                                 "device_type" : "0",
                                 "device_id" : device_id,
                                 "device_token" : UserDefaults.standard.object(forKey: "TOKEN") ?? "",
                                "invite_token":  UserDefaults.standard.object(forKey: "fcmToken") ?? ""
        ]
        print(requestParameters)
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.userSync, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { [weak self](response) in
            switch response{
            case .success(let data):
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    let userResponse =  CommonCodableHelper<SyncResponse>.codableObject(from: data)
                    
                    if let userData =  data as? Data, let userObject = CommonCodableHelper<SyncResponse>.codableObject(from: userData)?.output {
                        AppGlobalManager.sharedInstance.syncLoginResponse = userObject
                    }
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        
                        if let messageCode = userResponse?.messageCode{
                            if messageCode == "log_in_oth_dvc"{
                                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                }
                                
                                self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                            }
                            else if messageCode == "val_fail"{
                                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                }
                                
                                self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                            }else if messageCode == "comp_nt_act" || messageCode == "usr_acnt_dsbl" {
                                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                }
                                
                                self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                                
                                
                            }else if messageCode == "rec_acnt_dsbl" || messageCode == "rec_comp_nt_act"{
                                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    
                                }
                                self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                            }
                        }
                        return
                    }
                    let syncCalloutput = CommonCodableHelper<SyncResponse>.codableObject(from: data)?.output
                    
//                    let categoryOutput = userResponse?.output?.category
                    print(syncCalloutput?.company?.allow_groupcall)
                    
                    AppGlobalManager.sharedInstance.syncLoginResponse = syncCalloutput
                    
                }
                
            case .failure( let error):
                print("error",error)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
    }
    
    
}

//extension UITabBar {
//    
//    override open func sizeThatFits(_ size: CGSize) -> CGSize {
//        var sizeThatFits = super.sizeThatFits(size)
//        sizeThatFits.height = 44 // adjust your size here
//        return sizeThatFits
//    }
//}
