//
//  JoinSessionByCodeVC.swift
//  AgoraARKit
//
//  Created by Hetali on 17/07/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import OTPFieldView
import CoreLocation
import ARKit

class JoinSessionByCodeVC: UIViewController,Toastable,Alertable {


    @IBOutlet weak var view_main: UIView!
    
    @IBOutlet weak var view_inviteCustomer: UIView!
    
    @IBOutlet weak var lblSessionCode: UILabel!
    
    @IBOutlet weak var lblLink: UILabel!
    
    @IBOutlet weak var JoinCodeView: OTPFieldView!
    
    @IBOutlet weak var view_join: UIView!
    
    var channel_key_toShare : String?
    
    var App_link : String?
    
    var code_from_OTPView : String?
    
    var HasEnteredAllOTP : Bool?
    
    let geoCoder = CLGeocoder()

    @IBOutlet weak var lblConnecting: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view_inviteCustomer.isHidden = true
        view_join.isHidden = true
                
        navigationItem.leftBarButtonItem = nil
        
        HasEnteredAllOTP = false
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.leftBarButtonItem = nil
        view_inviteCustomer.isHidden = true
        view_join.isHidden = true
        lblConnecting.text = ""
    }
    @objc func backAction() {
        view_join.isHidden = true
        view_inviteCustomer.isHidden = true
        navigationItem.leftBarButtonItem = nil

        self.JoinCodeView.resignResponder()
    }
    
    @IBAction func btnJoinSessionTapped(_ sender: Any) {
        SyncAPI()
        let image = UIImage(named: "back")
        let backButton = UIBarButtonItem(image: image, style: .done, target: self, action: #selector(backAction))
        backButton.tintColor = .white
        navigationItem.leftBarButtonItem = backButton
        
        view_join.isHidden = false
        setupOtpView()
    }

   private func SyncAPI() {
            let activityData = ActivityData()
    //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            guard let userID = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else{
                return
            }
            guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
                return
            }
            let requestParameters = ["user_id" : userID,
                                     "device_type" : "0",
                                     "device_id" : device_id,
                                     "device_token" : UserDefaults.standard.object(forKey: "TOKEN") ?? "",
                                     "invite_token":  UserDefaults.standard.object(forKey: "fcmToken") ?? ""
                                        ]
            
            print(requestParameters)
            
            APIHandler.callAPI(endPoint: APIConstants.EndPoints.userSync, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { [weak self](response) in
                switch response{
                case .success(let data):
                    DispatchQueue.main.async {
    //                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        let userResponse =  CommonCodableHelper<SyncResponse>.codableObject(from: data)

                        if let userData =  data as? Data, let userObject = CommonCodableHelper<SyncResponse>.codableObject(from: userData)?.output {
                            AppGlobalManager.sharedInstance.syncLoginResponse = userObject
                        }
                        
                        guard let status = userResponse?.statusCode,status == 1 else {
                            //                        self!.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                                if let messageCode = userResponse?.messageCode{
                                if messageCode == "log_in_oth_dvc"{
                                    let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                        AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                    }
                                    
                                    self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                                }else if messageCode == "val_fail"{
                                    let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                        AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                    }
                                    
                                    self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                                }else if messageCode == "comp_nt_act" || messageCode == "usr_acnt_dsbl" {
                                    let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                        AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                    }
                                    
                                    self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                                    
                                    
                                }else if messageCode == "rec_acnt_dsbl" || messageCode == "rec_comp_nt_act"{
                                    let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    }
                                    self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                                }
                            }
                            return
                        }
                        let syncCalloutput = CommonCodableHelper<SyncResponse>.codableObject(from: data)?.output
                        
                        //                    let categoryOutput = userResponse?.output?.category
                        
                        AppGlobalManager.sharedInstance.syncLoginResponse = syncCalloutput
                        
                        
                    }
                    
                case .failure( let error):
                    print("error",error)
    //                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                }
            }
        }
    @IBAction func btnJoinCodeTapped(_ sender: Any) {
        
        if let myString = code_from_OTPView, !myString.isEmpty {
            if(HasEnteredAllOTP == true){
                HandleUniversalLink.sharedInstance.callreceiverLinkJoin(sessionId: myString)
                if ARWorldTrackingConfiguration.isSupported {
                    lblConnecting.text = "Connecting"
                }
                setupOtpView()
               
                navigationItem.leftBarButtonItem = nil
            }else{
                self.showToast("Please enter 9 digit code to join a call.", type: .fail)
            }
        }else{
            self.showToast("Please enter 9 digit code", type: .fail)
        }
    }
    @IBAction func btnCreateNewSessionTapped(_ sender: Any) {
//        LocationHelper.shared.locationManagerDelegate = self
//        LocationHelper.shared.startUpdatingLocation()
//
//        if(LocationHelper.shared.isLocationEnable){
//        if ARWorldTrackingConfiguration.isSupported {
            let image = UIImage(named: "back")
            let backButton = UIBarButtonItem(image: image, style: .done, target: self, action: #selector(backAction))
            backButton.tintColor = .white
            navigationItem.leftBarButtonItem = backButton
                CreateSessionAPI()
//        } else {
//            let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
//            }
//            self.showAlert(withMessage: "Your device does not supports Augmented reality feature.", title: "", customActions: [yesAction])
//        }
        
//        }
    }
    func setupOtpView(){
        self.JoinCodeView.fieldsCount = 9
        self.JoinCodeView.fieldBorderWidth = 2
        self.JoinCodeView.defaultBorderColor = UIColor.white
        self.JoinCodeView.filledBorderColor = UIColor.init(named: "Green") ?? UIColor.green
        self.JoinCodeView.cursorColor = UIColor.init(named: "Green") ?? UIColor.green
        self.JoinCodeView.displayType = .underlinedBottom
        self.JoinCodeView.fieldSize = 30
        self.JoinCodeView.separatorSpace = 8
        self.JoinCodeView.shouldAllowIntermediateEditing = false
        self.JoinCodeView.delegate = self
        self.JoinCodeView.initializeUI()
    }
  
    func CreateSessionAPI(){
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        
        let requestParameters = [
                                 "device_id" : device_id,
                                 "device_type" : "0"
            ] as [String : Any]
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.createSession, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { (response) in
            switch response {
            case .success(let data):
                DispatchQueue.main.async {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    let userResponse =  CommonCodableHelper<createSessionResponse>.codableObject(from: data)
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        if let messageCode = userResponse?.messageCode{
                            if messageCode == "log_in_oth_dvc"{
                                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                }
                                
                                self.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                            }
                        }
                        self.showToast(userResponse?.message ?? "Unable to create a new session. Please try after sometime", type: .fail)
                        return
                    }
                    
                    self.channel_key_toShare = userResponse?.output?.channel_key
                    print("Channel Key %@",userResponse?.output?.channel_key)
                    
                    if(self.channel_key_toShare != nil){
                        self.lblSessionCode.text = self.channel_key_toShare
                        self.view_inviteCustomer.isHidden = false
                    }
                    self.lblLink.text = ""
                    if let linkText = userResponse?.output?.join_app_link{
                        self.App_link = linkText
                        self.lblLink.text = ("\(linkText)\(String(describing: self.channel_key_toShare!))")
                    }
                }
                
            case .failure( let error):
                print("error",error)
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
    }
    @IBAction func btnShareClicked(_ sender: Any) {
        let firstActivityItem = "To join the ARMS call, please click to this link: \(String(describing: App_link!))\(String(describing: channel_key_toShare!)) Or, to join via code, please enter this code from app: \(String(describing: channel_key_toShare!))"
        let strURL : URL?
        
        if let applink = App_link{
            strURL = URL(string: applink)
            let convertedURL : URL = strURL!
            
//            let secondActivityItem : URL = convertedURL
            // If you want to put an image
            //        let image : UIImage = #imageLiteral(resourceName: "ARMS_w_full")
            
            let activityViewController : UIActivityViewController = UIActivityViewController(
                activityItems: [firstActivityItem], applicationActivities: nil)
            
            // This lines is for the popover you need to show in iPad
            activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
            
            // This line remove the arrow of the popover to show in iPad
            activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
            activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
            
            // Anything you want to exclude
            activityViewController.excludedActivityTypes = [
                UIActivity.ActivityType.postToWeibo,
                UIActivity.ActivityType.print,
                UIActivity.ActivityType.assignToContact,
                UIActivity.ActivityType.saveToCameraRoll,
                UIActivity.ActivityType.addToReadingList,
                UIActivity.ActivityType.postToFlickr,
                UIActivity.ActivityType.postToVimeo,
                UIActivity.ActivityType.postToTencentWeibo
            ]
            
            self.present(activityViewController, animated: true, completion: nil)
        }
       
    }
 
    @IBAction func btnCopyTapped(_ sender: Any) {
        
        let strCopyText = "To join the ARMS call, please click to this link: \(String(describing: App_link!))\(String(describing: channel_key_toShare!)) Or, to join via code, please enter this code from app: \(String(describing: channel_key_toShare!))"

        let pasteboard = UIPasteboard.general
        pasteboard.string = strCopyText
        
        self.showToast("Text Copied", type: .success)
    }
}

extension JoinSessionByCodeVC: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        
        HasEnteredAllOTP = hasEntered
        
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        code_from_OTPView = otpString
    }
}
extension JoinSessionByCodeVC: LocationManagerDelegate {

    func getLocation(location: CLLocation) {
        AppGlobalManager.sharedInstance.latitude = location.coordinate.latitude
        AppGlobalManager.sharedInstance.longitude = location.coordinate.longitude
        
        geoCoder.reverseGeocodeLocation(location) { placemarks, _ in
            if let place = placemarks?.first {
                let currentCityName : String = place.locality ?? ""
                let streetName : String = place.thoroughfare ?? ""
                let nameofplace : String = place.name ?? ""
                let administrativeArea : String = place.administrativeArea ?? ""
                let postalCode : String = place.postalCode ?? ""
                let combineString = ("\(currentCityName),\(streetName),\(nameofplace),\(administrativeArea),\(postalCode)")
                
                AppGlobalManager.sharedInstance.locationString = combineString
            }
        }

    }

}
