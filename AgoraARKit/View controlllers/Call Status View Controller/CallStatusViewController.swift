//
//  CallStatusViewController.swift
//  AgoraARKit
//
//  Created by Prashant on 01/05/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit
import AVFoundation
import AgoraRtmKit
import NVActivityIndicatorView

protocol CallStatusDelegate:class {
    func callDisconntedByReciver(call:Call)
    func callAccpted(call:Call)
    func callDisconnectedByCaller(call:Call)
}

class CallStatusViewController: BaseViewController,Alertable,Toastable {
    
    //MARK:- Outlets
    
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgCallerProfile: UIImageView!
    
    @IBOutlet weak var btnEndCall: UIButton!
    
    var timer:Timer? = nil
//    var isRTMlogin : Bool = false

    enum CallStatus {
        case connecting
        case ringing
        case answered
        case endedByOtherUser
        case noAnswer
        case busy
    }
    
    
    //MARK:- Var
    
    weak var delegate:CallStatusDelegate?
    
    //  private  var channelManager = AgoraChannelManager()
    // var agoraSignalManager:AgoraSignalManager? = AgoraSignalManager()
    var rtmCallHandler:RTMCallHandler!
    
    var roomName:String!
    var callerID:String!
    var callName:String = "" // This is the key where we are passing the name related to contact
    var isCallEnded:Bool = false
    
    var call:Call!
    var isForGroupCall = false
    
    let callUUID = UserDefaults.standard.string(forKey: "device_id")
    //    var callUUID = UUID().uuidString
    var callStatus:CallStatus = .connecting {
        didSet {
            DispatchQueue.main.async {
                
                switch self.callStatus {
                    
                case .connecting:
                    self.lblStatus.text = "Connecting"
                case .ringing:
                    self.lblStatus.text = "Ringing.."
                    self.shapeLayer.strokeColor = UIColor(named: "Green")?.cgColor
                //    self.agoraSignalManager?.sendInstantString(string: Constants.ChannelMessages.incomingCallShow, account: self.callerID)
                case .answered:
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    self.lblStatus.text = "Loading ..."
                    self.dismiss(animated: false, completion: {
                        self.delegate?.callAccpted(call: self.call)
                        self.timer?.invalidate()
                        self.timer = nil
                    })
                    
                case .endedByOtherUser:
                    self.lblStatus.text = "Call rejected"
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    self.showAlert(withMessage: "Call rejected", customActions:[ UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.delegate?.callDisconntedByReciver(call: self.call)
                        self.dismiss(animated: true , completion: nil)
                        
                    })])
                    
                case .noAnswer:
//                    self.lblStatus.text = "User is not answering call"
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
//                    self.showAlert(withMessage: "User is offline or  User is not answering the call, Try again later", customActions:[ UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.delegate?.callDisconntedByReciver(call: self.call)
                        self.dismiss(animated: true , completion: nil)

//                    })])
                case .busy:
                    self.lblStatus.text = "Line Busy"
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    self.showAlert(withMessage: "User is busy with other call, Please try again after some time", customActions:[ UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.delegate?.callDisconntedByReciver(call: self.call)
                        self.dismiss(animated: true , completion: nil)
                        if AppGlobalManager.sharedInstance.incommingCall.count != 0{
                        AppGlobalManager.sharedInstance.incommingCall.removeLast()
                        }
                    })])
                }
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                
            }
        }
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Memory Managment Methods
    
    //--------------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Abstract Methdos
    
    //--------------------------------------------------------------------------------
    
    class func viewController () -> CallStatusViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc =  storyboard.instantiateViewController(withIdentifier: "CallStatusViewController") as! CallStatusViewController
        vc.modalPresentationStyle = .fullScreen
        return vc
    }
    
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Action Methdos
    
    //--------------------------------------------------------------------------------
    func endcallTapped(){
        AppGlobalManager.sharedInstance.isRejectedBy = true

        self.isCallEnded = true
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        //  DispatchQueue.main.async {
//        if self.callStatus != .connecting {
//            //  self.agoraSignalManager?.sendInstantString(string: Constants.ChannelMessages.endCall, account: self.callerID)
//            print("END TAPPED \(self.rtmCallHandler.localInvite)")
//            if let localInvite = self.rtmCallHandler.localInvite {
//                
//                self.rtmCallHandler.cancelLocalInvite(invite: localInvite) {[unowned self] (errorCode) in
//                    print("Local Invite Cancel \(errorCode.rawValue)")
//                    
//                    //   self.logoutAndDismiss()
//                }
//            } else {
//                //  self.logoutAndDismiss()
//                
//            }
//        }
        self.delegate?.callDisconnectedByCaller(call: self.call)
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnEndTapped(_ sender: UIButton) {
        sender.isEnabled = false
        endcallTapped()
        
        if AppGlobalManager.sharedInstance.incommingCall.last?.callFrom == "" {
            return
        }
        
        (UIApplication.shared.delegate as! AppDelegate).pushManager.callInviteService(state: .callRejected) { (state) in
            if state {
                AppGlobalManager.sharedInstance.incommingCall.removeAll()
//                print("\n\n \(#function)\n AppGlobalManager.sharedInstance.incommingCall Count :: \t \(AppGlobalManager.sharedInstance.incommingCall.count) \n\n")
            }else {
                UtilityClass.showAlert(title: "", message: "Something went wrong") { (act) in
                }
            }
        }
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Custom Methdos
    
    //--------------------------------------------------------------------------------
    
    fileprivate func sendPushNotification(completion:@escaping((Bool) -> Void)) {
        //    self.agoraSignalManager.joinChannel(channelName: "room_name_call_\(self.callerID!)")
        self.syncApiCall()
        // Send PUSH
        //changes in lock down
        let scale = UIScreen.main.scale
        let parameters = [
            "phone":self.callerID ?? "",
            "caller":AppGlobalManager.sharedInstance.currentUserID ?? "",
            "room_name":self.roomName ?? "",
            "device_id" : callUUID ?? "",
            "call_id":callUUID ?? "",
            "user_call_id" : "",
            "location" : AppGlobalManager.sharedInstance.locationString,
            "latitude" : AppGlobalManager.sharedInstance.latitude,
            "longitude" : AppGlobalManager.sharedInstance.longitude,
            "device_type" : "0",
            "sender_width" : UIScreen.main.bounds.size.width * scale,
            "sender_height" : UIScreen.main.bounds.size.height * scale
            ] as [String : Any]
        print("init call params ",parameters)
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.callToUser, parameters: parameters as [String : Any],token: AppGlobalManager.sharedInstance.token, method: .POST) {[weak self] (response) in
            
            DispatchQueue.main.async {
                switch response {
                case .success(let data):
                    let object = CommonCodableHelper<initCallResponse>.codableObject(from: data)
                    do {
                      print("\n callToUser", try JSONSerialization.jsonObject(with: data, options: .allowFragments),"\n")
                    }
                    catch {
                      print(error.localizedDescription)
                    }
                    guard let status = object?.statusCode,status == 1 else {
                        let message = object?.message
                        let messageCode = object?.messageCode
                        if messageCode == "log_in_oth_dvc"{
                            let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                self!.endcallTapped()
                                AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                            }
                            self!.showAlert(withMessage: object?.message ?? "logged in another device", title: "", customActions: [yesAction])
                            
                            completion(false)
                            
                        }else if messageCode == "comp_nt_act" || messageCode == "usr_acnt_dsbl" {
                            let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                self!.endcallTapped()
                            }
                            
                            self!.showAlert(withMessage: object?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                            
                            completion(false)
                            
                        }else if messageCode == "rec_acnt_dsbl" || messageCode == "rec_comp_nt_act"{
                            let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                self!.endcallTapped()
                            }
                            self!.showAlert(withMessage: object?.message ?? "logged in another device", title: "", customActions: [yesAction])
                            
                            completion(false)
                            
                        }
                        else if messageCode == "usr_nt_lgn"{
                            let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                self!.endcallTapped()
                            }
                            
                            self!.showAlert(withMessage: object?.message ?? "User didn't login to the application!", title: "", customActions: [yesAction])
                            
                            completion(false)
                        }
//                        else if messageCode == "usr_bsy"{
//                          let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
//                             self!.endcallTapped()
//                          }
//                          self!.showAlert(withMessage: object?.message ?? "User busy in call, Try again latter", title: "", customActions: [yesAction])
//                          completion(false)
//                        }
                        else if messageCode == "phone_nt_ext"{
                            let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                self!.endcallTapped()
                            }
                            
                            self!.showAlert(withMessage: object?.message ?? "Phone no not exist !", title: "", customActions: [yesAction])
                            
                            completion(false)
                        }
                        
                      
                        return
                    }
             
                    AppGlobalManager.sharedInstance.user_call_id_Global = object?.output?.user_call_id
                    AppGlobalManager.sharedInstance.chat_id = object?.output?.chat_id ?? ""
                    
                    //                    print(AppGlobalManager.sharedInstance.initcall?.user_call_id)
                    
                    // Suppose before we join user press end call
                    if self?.isCallEnded ?? true {
                        self?.btnEndTapped(UIButton())
                    }
                    completion(true)
                    
                    
                case .failure( let error):
                    print("error",error)
                    
                    self?.lblStatus.text = "Something went wrong !!"
                    completion(false)
                    
                }
            }
        }
    }
    private func syncApiCall() {
        let activityData = ActivityData()
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        guard let userID = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else{
            return
        }
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
            return
        }
        let requestParameters = ["user_id" : userID,
                                 "device_type" : "0",
                                 "device_id" : device_id,
                                 "device_token" : UserDefaults.standard.object(forKey: "TOKEN") ?? "",
                                 "invite_token":  UserDefaults.standard.object(forKey: "fcmToken") ?? ""
                                    ]
        
        print(requestParameters)
        
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.userSync, parameters: requestParameters, token: AppGlobalManager.sharedInstance.token, method: .POST) { [weak self](response) in
            switch response{
            case .success(let data):
                DispatchQueue.main.async {
//                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    let userResponse =  CommonCodableHelper<SyncResponse>.codableObject(from: data)
                    
                    if let userData =  data as? Data, let userObject = CommonCodableHelper<SyncResponse>.codableObject(from: userData)?.output {
                        AppGlobalManager.sharedInstance.syncLoginResponse = userObject
                    }
                    
                    guard let status = userResponse?.statusCode,status == 1 else {
                        //                        self!.showToast(userResponse?.message ?? "Something went wrong !! Please try again later", type: .fail)
                            if let messageCode = userResponse?.messageCode{
                            if messageCode == "log_in_oth_dvc"{
                                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                }
                                
                                self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                            }else if messageCode == "val_fail"{
                                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                }
                                
                                self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                            }else if messageCode == "comp_nt_act" || messageCode == "usr_acnt_dsbl" {
                                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                }
                                
                                self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                                
                                
                            }else if messageCode == "rec_acnt_dsbl" || messageCode == "rec_comp_nt_act"{
                                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                }
                                self!.showAlert(withMessage: userResponse?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                            }
                        }
                        return
                    }
                    let syncCalloutput = CommonCodableHelper<SyncResponse>.codableObject(from: data)?.output
                    
                    //                    let categoryOutput = userResponse?.output?.category
                    
                    AppGlobalManager.sharedInstance.syncLoginResponse = syncCalloutput
                    
                }
                
            case .failure( let error):
                print("error",error)
//                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
    }
        
    private func setupRTM() {
        if UtilityClass.checkInternetConnection(completionHandler: { (act) in
            self.setupRTM()
        }) {
            return
        }
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(message:"Connecting ..."))

        
//        self.rtmCallHandler?.login(phoneNumber: AppGlobalManager.sharedInstance.currentUserID ?? "",completion: {[weak self]  code in
//
//
//            if code.rawValue == 9{
//                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//                self!.showToast("Make sure your device is connected to the internet.", type: .fail)
//                self!.dismiss(animated: true, completion: nil)
//            }
//
//            guard let `self` = self else {return}
//            guard case  .login(_) = self.rtmCallHandler.loginStatus else {
//                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//                self.showToast("Failed to connect to the server, Please try again later", type: .fail)
//                self.dismiss(animated: true, completion: nil)
//                return
//            }
//            self.rtmCallHandler.sendLocalInvite(to: self.callerID, content: self.callUUID!) { (errorCode) in
//                print("Local Invite Sent ==> \(errorCode.rawValue)")
//
//                if errorCode == .ok {
//                    self.sendPushNotification(completion: {  (complete) in
//                        print("PUSH SENT \(complete)")
//
//                        let callUUID:UUID = UUID()
//
//                        let callInfo = CallInfo(callFrom: self.callerID,
//                                   callerName: "",
//                                   callTo: "", roomName: self.roomName,
//                                   callDate: "\(Date())",
//                                   callIdentifier: "",
//                                   callUUID: callUUID)
//
//                        AppGlobalManager.sharedInstance.incommingCall = callInfo
//
////                        self.isRTMlogin = true
//                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//                        if self.isCallEnded {
//                            self.rtmCallHandler.cancelLocalInvite(invite: self.rtmCallHandler.localInvite!) { (errorCode) in
//                                print("Local  Invite Sent ==> \(errorCode.rawValue)")
//
//                            }
//                        }
//
//
//                    })
//                } else {
//                    self.showToast("Failed to connect to the server, Please try again later", type: .fail)
//                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//                    self.dismiss(animated: true, completion: nil)
//
//                }
//
//            }
//        })

//        self.rtmCallHandler.sendLocalInvite(to: self.callerID, content: self.callUUID!) { (errorCode) in
//            print("Local Invite Sent ==> \(errorCode.rawValue)")
//
//            if errorCode == .ok {
        self.sendPushNotification(completion: {  (complete) in
            print("PUSH SENT \(complete)")
            
            let callUUID:UUID = UUID()
            
            let callInfo = CallInfo(callFrom: self.callerID,
                                    callerName: "",
                                    callTo: "", roomName: self.roomName,
                                    callDate: "\(Date())",
                callIdentifier: "",
                callUUID: callUUID)
            
            AppGlobalManager.sharedInstance.incommingCall.append(callInfo)
            
            //                        self.isRTMlogin = true
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//            if self.isCallEnded {
//                self.rtmCallHandler.cancelLocalInvite(invite: self.rtmCallHandler.localInvite!) { (errorCode) in
//                    print("Local  Invite Sent ==> \(errorCode.rawValue)")
//
//                }
//            }
        })
//            }
//            else {
//                self.showToast("Failed to connect to the server, Please try again later", type: .fail)
//                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//                self.dismiss(animated: true, completion: nil)
//
//            }
            
//        }
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()

        //  }
        
        // }
    }
    
    
    let shapeLayer = CAShapeLayer()
    
    private func createPulseAnimation() {
        let path = UIBezierPath(arcCenter: .zero, radius: self.imgCallerProfile.frame.width / 2, startAngle: 0, endAngle: CGFloat.pi * 2, clockwise: true)
        
        imgCallerProfile.superview?.layer.cornerRadius = imgCallerProfile.frame.width / 2
        imgCallerProfile.superview?.layer.masksToBounds = false
        imgCallerProfile.superview?.clipsToBounds = false
        
        // shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor(named: "Red")?.cgColor
        
        shapeLayer.position = CGPoint(x:self.imgCallerProfile.frame.width / 2,y:self.imgCallerProfile.frame.width / 2)
        shapeLayer.lineCap = .round
        shapeLayer.cornerRadius = imgCallerProfile.frame.width / 2
        shapeLayer.lineWidth = 1.5
        shapeLayer.zPosition = -1
        
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.1
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        animation.duration = 2.0
        animation.repeatCount = Float.infinity
        animation.autoreverses = true
        
        let animation1 = CABasicAnimation(keyPath: "opacity")
        animation1.toValue = 1.0
        animation1.fromValue = 0.7
        animation1.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation1.duration = 2.0
        animation1.repeatCount = Float.infinity
        animation1.autoreverses = true
        
        let animation2 = animation.copy() as!  CABasicAnimation
        animation2.toValue = 1.06
        animation2.duration = 2.0
        
        
        imgCallerProfile.superview?.layer.addSublayer(shapeLayer)
        
        
        shapeLayer.add(animation, forKey: "scale")
        shapeLayer.add(animation1, forKey: "opacity")
        imgCallerProfile.layer.add(animation2, forKey: "scale1")
        
        
    }
    
    //--------------------------------------------------------------------------------
    
    
    private func logoutAndDismiss(completion:(() -> Void)? = nil) {
//        self.rtmCallHandler.logout {
//            self.dismiss(animated: true, completion: completion)
//        }
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- ViewLifeCycle Methods
    
    //--------------------------------------------------------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.definesPresentationContext = true
//        rtmCallHandler = RTMCallHandler(delegate: self)
  
        setupRTM()
        // setupSignaling()
        call = AppDelegate.shared.callManager.startCall(handle: callerID, name: callName, videoEnable: true)
        
        NotificationCenter.default.removeObserver(self, name: .callInviteAccepted, object: nil)
        NotificationCenter.default.removeObserver(self, name: .callInviteRejected, object: nil)
        NotificationCenter.default.removeObserver(self, name: .callInviteMiscall, object: nil)
        NotificationCenter.default.removeObserver(self, name: .callInviteRinging, object: nil)
        NotificationCenter.default.removeObserver(self, name: .callInviteBusy, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.callInviteAcceptedAction(_:)), name: .callInviteAccepted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.callInviteRejectedAction(_:)), name: .callInviteRejected, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.callInviteMissCallAction(_:)), name: .callInviteMiscall, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.callInviteRingingCallAction(_:)), name: .callInviteRinging, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.callInviteBusyCallAction(_:)), name: .callInviteBusy, object: nil)

    }

    // handle notification
    @objc func callInviteAcceptedAction(_ notification: NSNotification) {
       print(notification.userInfo ?? "")
        self.callStatus  = .answered
    }
    
    // handle notification
       @objc func callInviteRejectedAction(_ notification: NSNotification) {
          print(notification.userInfo ?? "")
        
//        if response ==  Constants.ChannelMessages.busy {
//            self.callStatus  = .busy
//
//        } else {
            self.callStatus  = .endedByOtherUser
        if AppGlobalManager.sharedInstance.incommingCall.count != 0{
            AppGlobalManager.sharedInstance.incommingCall.removeLast()
        }
//        }
       
       }
    @objc func callInviteBusyCallAction(_ notification: NSNotification) {
        self.callStatus = .busy
    }

    // handle notification
       @objc func callInviteMissCallAction(_ notification: NSNotification) {
          print(notification.userInfo ?? "")
        
        self.callStatus  = .noAnswer
        
       }
    
    @objc func callInviteRingingCallAction(_ notification: NSNotification) {
       print(notification.userInfo ?? "")
        
        self.callStatus  = .ringing
      
    }
    //--------------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ///MUST DO.
        // IF WE DO NOT DO THIS. AGORA WILL SEND MESSAGE TO OLD INSTANCE.
        // CREATE INSTANCE DOES NOT WORK
        
        //  self.agoraSignalManager?.initCallBack()
        
        self.lblPhone.text = callName
        
        timer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)

    }
    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        createPulseAnimation()
        
    }
    
    //--------------------------------------------------------------------------------
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        self.rtmCallHandler.logout()
        
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIDevice.current.userInterfaceIdiom == .phone ? [.portrait] : .portrait
        
    }
    
    deinit {
        print(" CALL STATUS deinit")
    }
    
    @objc func fireTimer() {
        print("Timer fired in callstatus view controller")
        timer?.invalidate()
        timer = nil
        self.delegate?.callDisconntedByReciver(call: self.call)
        self.btnEndTapped(UIButton())
        self.dismiss(animated: true , completion: nil)
    }
    
    
}

extension CallStatusViewController:AgoraRtmCallDelegate {
    // CALLE GET INVITE
    func rtmCallKit(_ callKit: AgoraRtmCallKit, localInvitationReceivedByPeer localInvitation: AgoraRtmLocalInvitation) {
        //   showAlert(withMessage: "localInvitationReceivedByPeer")
        self.callStatus  = .ringing
        
    }
    
    
    func rtmCallKit(_ callKit: AgoraRtmCallKit, localInvitationAccepted localInvitation: AgoraRtmLocalInvitation, withResponse response: String?) {
        self.callStatus  = .answered
        
    }
    
    func rtmCallKit(_ callKit: AgoraRtmCallKit, localInvitationCanceled localInvitation: AgoraRtmLocalInvitation) {
        self.callStatus = .noAnswer
    }
    
    func rtmCallKit(_ callKit: AgoraRtmCallKit, localInvitationFailure localInvitation: AgoraRtmLocalInvitation, errorCode: AgoraRtmLocalInvitationErrorCode) {
        self.callStatus = .noAnswer
    }
    
    func rtmCallKit(_ callKit: AgoraRtmCallKit, localInvitationRefused localInvitation: AgoraRtmLocalInvitation, withResponse response: String?) {
        if response ==  Constants.ChannelMessages.busy {
            self.callStatus  = .busy

        } else {
            self.callStatus  = .endedByOtherUser

        }
        
    }
    
}
