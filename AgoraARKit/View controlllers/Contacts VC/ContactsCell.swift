//
//  ContactsCell.swift
//  AgoraARKit
//
//  Created by Prashant on 22/05/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit
import SwipeCellKit

class ContactsCell: SwipeTableViewCell {

    @IBOutlet weak var lblNameIntials: NameInitalLabel!
      @IBOutlet weak var lblName: UILabel!
      @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblDate: UILabel!

      @IBOutlet weak var chatMessageCounterView: UIView!
      @IBOutlet weak var lblNoOfMsg: UILabel!
      @IBOutlet weak var lblMessages: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
