//
//  ContactsVC.swift
//  AgoraARKit
//
//  Created by Prashant on 08/05/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import UIKit
import Contacts
import SwipeCellKit
import NVActivityIndicatorView
import CoreLocation
import ARKit

struct PhoneNumber:Equatable,Comparable {
    static func < (lhs: PhoneNumber, rhs: PhoneNumber) -> Bool {
       return lhs.name < rhs.name
    }
    
    var name:String
    var phoneNumber:String
    
}

protocol ContactSelectedDelegate :class {
    func contactSelected(contact:String,name:String)
}

class ContactsVC: BaseViewController,Alertable,Toastable {

    //MARK:- Outlets
    
    @IBOutlet weak var tableView: UITableView!
    private let refreshControl = UIRefreshControl()

    let geoCoder = CLGeocoder()

    //MARK:- Var
    
    var contactsPermissionGranted = true
//    var phoneNumbers = [PhoneNumber]() {
//        didSet {
//             filteredPhoneNumbers = phoneNumbers
//
//        }
//    }
   // var filteredPhoneNumbers = [PhoneNumber]()
    var searchController = UISearchController()
    var callPhoneNumber:String = ""
    
    
    enum  ContactListType {
        case chooseContactGroupCall
        case listContactSingleCall
    }
    
    var contactListType:ContactListType = .listContactSingleCall
    weak var delegate:ContactSelectedDelegate? = nil
    
    var companyContacts = [User]()
    var filteredCompanyContact = [User]()
    //--------------------------------------------------------------------------------
    
    //MARK:- Memory Managment Methods
    
    //--------------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- Abstract Methdos
    
    //--------------------------------------------------------------------------------
    
    class func viewController () -> ContactsVC {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ContactsVC") as! ContactsVC
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:-  Custom Methdos
    
    //--------------------------------------------------------------------------------

    private func setupTableView() {
        
     //   self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tableFooterView = UIView()
        
        if self.contactListType == .chooseContactGroupCall {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(dismissVC)   )
        }
        
    }
    
    @objc func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //--------------------------------------------------------------------------------

/*    private func fetchContacts() {
        let contactStore = CNContactStore()
        
        contactStore.requestAccess(for: .contacts) { [unowned self] (success, error) in
            DispatchQueue.main.async {
                self.contactsPermissionGranted = success
                self.tableView.reloadData()
                self.loadContactsFromPhoneBook()
            }
        }
    }
    
    private func loadContactsFromPhoneBook () {
        var contacts = [CNContact]()
        var phoneNumbers = [PhoneNumber]()
        let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                    CNContactPhoneNumbersKey
            ] as [Any]
        let contactStore = CNContactStore()
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
       
        do {
            try contactStore.enumerateContacts(with: request, usingBlock: { (contact, finishedPosinter) in
                contacts.append(contact)
                for phoneNumber in contact.phoneNumbers {
                    if let label = phoneNumber.label {
                        let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
                        phoneNumbers.append(PhoneNumber(name: contact.givenName + " " + contact.familyName + " - " + localizedLabel, phoneNumber: phoneNumber.value.stringValue))
                    } else {
                        phoneNumbers.append(PhoneNumber(name: contact.givenName , phoneNumber: phoneNumber.value.stringValue))
                    }
                }
            })
        } catch {
            print("Exception occured \(error)")
        }
        DispatchQueue.main.async {
            self.phoneNumbers = phoneNumbers.sorted()
            self.tableView.reloadData()
        }
        
    }*/
    
    //--------------------------------------------------------------------------------

    private func setupSearchbar() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Contacts"
  //      searchController.searchBar.barTintColor  = UIColor(named: "LightGray")
        searchController.searchBar.tintColor = .white
     //   searchController.searchBar.isTranslucent = false
        let attributes =  [NSAttributedString.Key.foregroundColor: UIColor.white]
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = attributes

        if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
            if let backgroundview = textfield.subviews.first {
                
                // Background color
                backgroundview.backgroundColor =  #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)  //UIColor(hexFromString: "#2F3740", alpha: 1.0)
                
                // Rounded corner
                backgroundview.layer.cornerRadius = 14;
                backgroundview.clipsToBounds = true;
            }
        }
        navigationItem.hidesSearchBarWhenScrolling = false

        self.navigationItem.searchController = searchController
        self.definesPresentationContext = true


    }
    
    //--------------------------------------------------------------------------------

    private func callAPIToFetchContacts () {
        
        guard let userID = AppGlobalManager.sharedInstance.loggedInUser?.user?.id else {
            return
        }
        guard let device_id = UserDefaults.standard.string(forKey: "device_id") else {
                return
        }
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
      //  let parameters = ["userid":userID]
        APIHandler.callAPI(endPoint: APIConstants.EndPoints.userList + "/" + "\(userID)?" + "device_id=" + "\(device_id)"  + "&" + "device_type=" + "0"  , parameters: nil, token: AppGlobalManager.sharedInstance.token, method: .GET) { (result) in

            
            DispatchQueue.main.async {
                
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                
                switch result {
                case .success( let data):
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()

                    let object = CommonCodableHelper<[User]>.codableObject(from: data)
                    
                    guard let status = object?.statusCode,status == 1 else {
                        if let messageCode = object?.messageCode{
                            if messageCode == "log_in_oth_dvc"{
                                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    AppGlobalManager.sharedInstance.logoutAndClearAllCache()
                                }
                                
                                self.showAlert(withMessage: object?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                            }
                        }
                        //                        self.showToast(object?.message ?? "Something went wrong !! Please try again later", type: .fail)
//                        let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
////                            AppGlobalManager.sharedInstance.logoutAndClearAllCache()
//                        }
//
                        self.showToast(object?.message ?? "Something went wrong !! Please try again later", type: .success)

//                        self.showAlert(withMessage: object?.message ?? "Something went wrong !! Please try again later", title: "", customActions: [yesAction])
                        
                        return
                    }
                    
                    self.companyContacts = object?.output ?? []
                    self.filteredCompanyContact = self.companyContacts.sorted()
                    
                    self.tableView.reloadData()
                    
                case .failure(let error):
                    self.showToast("Something went wrong !!", type: .fail)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()

                }
                
            }
            
        }
        
    }
    
    //--------------------------------------------------------------------------------
    
    //MARK:- ViewLifeCycle Methods
    
    //--------------------------------------------------------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
     //   fetchContacts()
        setupSearchbar()
        callAPIToFetchContacts()
       
        addrefreshControl()
    }
    func addrefreshControl(){
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshContactData(_:)), for: .valueChanged)
        
    }
    @objc private func refreshContactData(_ sender: Any) {
        refreshControl.beginRefreshing()
        callAPIToFetchContacts()
        refreshControl.endRefreshing()
    }
    //--------------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        extendedLayoutIncludesOpaqueBars = true
        //edgesForExtendedLayout = []
        
    }
    
    //--------------------------------------------------------------------------------
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    

}

//--------------------------------------------------------------------------------

//MARK:-  UISearchResultsUpdating Methdos

//--------------------------------------------------------------------------------


extension ContactsVC:UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBarText = searchController.searchBar.text
        
        guard let searchText = searchBarText, !searchText.isEmpty else {
         //   filteredPhoneNumbers = phoneNumbers
            filteredCompanyContact = companyContacts
            self.tableView.reloadData()

            return
        }
        
     //   filteredPhoneNumbers = (phoneNumbers.filter{$0.name.contains(searchText)} ).sorted(by: {$0.name > $1.name})
        filteredCompanyContact = companyContacts.filter{    ($0.name ?? "").lowercased().contains(searchText.lowercased())
                                                        ||  ($0.mobileNo ?? "").lowercased().contains(searchText.lowercased())}
                                                        .sorted(by: (>))
        self.tableView.reloadData()

        
    }
    
    
}

extension ContactsVC:UITableViewDataSource,UITableViewDelegate {
   
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if !contactsPermissionGranted {
            showPermissionButton(tableView)
            
            return 0
        } else if self.companyContacts.isEmpty { //else if self.phoneNumbers.isEmpty {
            
            showNoRecordFoundLabel(tableView)
            return 0

        }
        tableView.backgroundView = nil
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredCompanyContact.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ContactsCell
        
        let contact = filteredCompanyContact[indexPath.row]
        
        cell.lblName.text = contact.name
        cell.lblNameIntials.setText(fullname: contact.name?.capitalized)
    //    cell.lblNameIntials.setText(fullname: contact.name.components(separatedBy: "-").first?.trimmingCharacters(in: .whitespacesAndNewlines))
        cell.lblPhoneNumber.text = contact.mobileNo
        
        if contactListType == .listContactSingleCall {
            cell.delegate = self
        }
        
        return cell
    }
    
    //--------------------------------------------------------------------------------

    
    fileprivate func showNoRecordFoundLabel(_ tableView: UITableView) {
        let noRecordFound: UILabel   = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width  , height: tableView.frame.height))
        noRecordFound.text = "No contacts found."
        noRecordFound.font = Constants.AppTheme.Fonts.font(type: .FONT_REGULAR, size: 16)
        //    noDataLabel.titleLabel?.textColor   = UIColor.red.withAlphaComponent(0.8)
        noRecordFound.textColor = UIColor(named: "LightGray")
        noRecordFound.numberOfLines = 0
        noRecordFound.textAlignment = .center
        noRecordFound.lineBreakMode = .byWordWrapping
        
        tableView.backgroundView  = noRecordFound
    }
    
    fileprivate func showPermissionButton(_ tableView: UITableView) {
        let noPermissionButton: UIButton   = UIButton(frame: CGRect(x: 0, y: 0, width: 120, height: 60))
        noPermissionButton.setTitle("Tap here to change contact permission", for: .normal)
        //    noDataLabel.titleLabel?.textColor   = UIColor.red.withAlphaComponent(0.8)
        noPermissionButton.titleLabel?.font = Constants.AppTheme.Fonts.font(type: .FONT_REGULAR, size: 16)
        noPermissionButton.setTitleColor(UIColor(named: "Red"), for: .normal)
        noPermissionButton.titleLabel?.numberOfLines = 0
        noPermissionButton.titleLabel?.lineBreakMode = .byWordWrapping
        noPermissionButton.addTarget(self, action: #selector(openSetting), for: .touchUpInside)

        tableView.backgroundView  = noPermissionButton
    }
    
    @objc func openSetting() {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if UtilityClass.checkInternetConnection(completionHandler: { (act) in
          
        }) {
          return
        }
//        LocationHelper.shared.locationManagerDelegate = self
//        LocationHelper.shared.startUpdatingLocation()
        
        if self.contactListType == .chooseContactGroupCall{
            let contact = filteredCompanyContact[indexPath.row]

                   let callAction = UIAlertAction(title: "Call", style: .default) {[unowned self] (action) in
                       self.searchController.isActive = false
                       self.dismiss(animated: true, completion: nil)
                       self.delegate?.contactSelected(contact: contact.mobileNo ?? "", name: contact.name ?? "")

                   }

                   let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {[unowned self] (action) in
                       self.searchController.isActive = false
                       self.dismiss(animated: true, completion: nil)

                   }
            self.showAlert(withMessage: "Do you want to add \(String(describing: contact.name ?? "")) to call ?", customActions: [cancelAction,callAction])
        }else{
//            if(LocationHelper.shared.isLocationEnable){
            if ARWorldTrackingConfiguration.isSupported {
                guard  var phoneNumber = self.filteredCompanyContact[indexPath.row].mobileNo,
                    let name = self.filteredCompanyContact[indexPath.row].name else {
                        return
                }
                phoneNumber = phoneNumber.filter{$0.unicodeScalars.allSatisfy{$0.isASCII}}         // Remove special characters if any and white space
                phoneNumber.removeAll(where: {$0.isNewline || $0.isWhitespace})
                
                self.callPhoneNumber = phoneNumber
                
                let roomName = "room_name_\(self.callPhoneNumber)"
                showCallStatusVC(phoneNumber, roomName: roomName,contactName: name)
            } else {
                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                }
                self.showAlert(withMessage: "Your device does not supports Augmented reality feature.", title: "", customActions: [yesAction])
            }
               
//            }
        }
       
    }
    
}

//--------------------------------------------------------------------------------

//MARK:-  Corelocation Delegate Methdos

//--------------------------------------------------------------------------------

extension ContactsVC: LocationManagerDelegate {

    func getLocation(location: CLLocation) {
        AppGlobalManager.sharedInstance.latitude = location.coordinate.latitude
        AppGlobalManager.sharedInstance.longitude = location.coordinate.longitude
        
        geoCoder.reverseGeocodeLocation(location) { placemarks, _ in
            if let place = placemarks?.first {
                let currentCityName : String = place.locality ?? ""
                let streetName : String = place.thoroughfare ?? ""
                let nameofplace : String = place.name ?? ""
                let administrativeArea : String = place.administrativeArea ?? ""
                let postalCode : String = place.postalCode ?? ""
                
                let combineString = ("\(currentCityName),\(streetName),\(nameofplace),\(administrativeArea),\(postalCode)")
                
                AppGlobalManager.sharedInstance.locationString = combineString
            }
        }

    }

}

//--------------------------------------------------------------------------------

//MARK:-  Swipable Tableview cell Delegate Methdos

//--------------------------------------------------------------------------------

extension ContactsVC:SwipeTableViewCellDelegate {
    fileprivate func handleEditAction(_ indexPath: IndexPath) {
      //  var phoneNumber = self.filteredPhoneNumbers[indexPath.row].phoneNumber
        guard var phoneNumber = self.filteredCompanyContact[indexPath.row].mobileNo else {
            return
        }

        phoneNumber = phoneNumber.filter{$0.unicodeScalars.allSatisfy{$0.isASCII}}         // Remove special characters if any and white space

        phoneNumber.removeAll(where: {$0.isNewline || $0.isWhitespace})
        if let nav = self.tabBarController?.viewControllers?.first(where: {($0 as? UINavigationController)?.topViewController is ViewController}) as? UINavigationController,  let vc = nav.topViewController as? ViewController {
            vc.phoneNumberPrefilled = phoneNumber
            self.tabBarController?.selectedViewController = nav
        }
    }
    
    //--------------------------------------------------------------------------------
    
    fileprivate func showCallStatusVC(_ phoneNumber: String,roomName:String,contactName:String) {
        let callStatusVC = CallStatusViewController.viewController()
        callStatusVC.delegate = self
        callStatusVC.roomName = roomName
        callStatusVC.callerID = phoneNumber
        callStatusVC.callName = contactName
        self.present(callStatusVC, animated: true, completion: nil)
    }
    
    fileprivate func handleCallAction(_ indexPath: IndexPath) {
        guard  var phoneNumber = self.filteredCompanyContact[indexPath.row].mobileNo,
                let name = self.filteredCompanyContact[indexPath.row].name else {
            return
        }
        phoneNumber = phoneNumber.filter{$0.unicodeScalars.allSatisfy{$0.isASCII}}         // Remove special characters if any and white space
        phoneNumber.removeAll(where: {$0.isNewline || $0.isWhitespace})
        
        self.callPhoneNumber = phoneNumber

        let roomName = "room_name_\(self.callPhoneNumber)"
        showCallStatusVC(phoneNumber, roomName: roomName,contactName: name)
    }
    
    
    
    //--------------------------------------------------------------------------------

    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        guard orientation == .right else {
            return nil
        }
        
        let edit = SwipeAction(style: .default, title: nil) {[weak self] (action, indexPath) in
            guard let `self` = self else {return }

            self.handleEditAction(indexPath)
        }
        edit.hidesWhenSelected = true
        edit.image = #imageLiteral(resourceName: "edit-call")
        edit.backgroundColor = UIColor(named: "PrimaryDark")
        
        let call = SwipeAction(style: .default, title: nil) { [weak self] (action, indexPath) in
            guard let `self` = self else {return }
            self.handleCallAction(indexPath)
        }
        call.hidesWhenSelected = true
        call.image = #imageLiteral(resourceName: "call-now")
        call.backgroundColor = UIColor(named: "PrimaryDark")

       // return [edit,call]
        
        return nil
        
    }
    
    //--------------------------------------------------------------------------------

    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        
        var swipeOptions = SwipeOptions()
        
        swipeOptions.backgroundColor = UIColor(named: "PrimaryDark")
        swipeOptions.maximumButtonWidth = 60
        swipeOptions.transitionStyle = .border
        swipeOptions.expansionStyle = .none
        swipeOptions.buttonPadding = 8
        swipeOptions.buttonSpacing = 8
        
        
        return swipeOptions
    }
    
}

//--------------------------------------------------------------------------------

//MARK:-  Call Status Delegate  Methdos

//--------------------------------------------------------------------------------


extension ContactsVC: CallStatusDelegate {
    func callDisconntedByReciver(call:Call) {
        CoreDataManager.saveCall(call: call, duration: 0)
        AppDelegate.shared.status = .ideal
        call.end()
        AppDelegate.shared.callManager.remove(call: call) // important , endCallCXAction == CALLS =>   func provider(_ provider: CXProvider, perform action: CXEndCallAction)
        AppDelegate.shared.callManager.endCallCXAction(call: call, completion: nil)
    }
    
    func callAccpted(call:Call) {
        DispatchQueue.main.async {
        //    let call = AppDelegate.shared.callManager.startCall(handle: call.handle/* AppGlobalManager.sharedInstance.currentUserID ?? ""*/, videoEnable: true)
            
            let vc = ARController.viewController()
            vc.modalPresentationStyle = .fullScreen
            vc.roomName =  "room_name_\(self.callPhoneNumber)" //self.txtRoomID.text ?? ""
            vc.call = call
            self.present(vc, animated: true, completion: nil)
            
            
        }
    }
    
    func callDisconnectedByCaller(call:Call) {
        CoreDataManager.saveCall(call: call, duration: 0)
        AppDelegate.shared.status = .ideal
        call.end()
        AppDelegate.shared.callManager.remove(call: call) // important , endCallCXAction == CALLS =>   func provider(_ provider: CXProvider, perform action: CXEndCallAction)
        
        AppDelegate.shared.callManager.endCallCXAction(call: call, completion: nil)

    }
    
    
}


