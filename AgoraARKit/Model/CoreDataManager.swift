//
//  CoreDataManager.swift
//  AgoraARKit
//
//  Created by Prashant on 14/05/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager:NSObject {
    
    
    
    
    class func saveCall(call:Call,duration:Int) {
        
        DispatchQueue.main.async {
            let _ = call.generateCallLog(context: AppDelegate.shared.persistentContainer.viewContext, duration: duration)
            
            AppDelegate.shared.saveContext()

        }
        
    }
    
    
}

extension Call {
    
    func generateCallLog(context:NSManagedObjectContext,duration:Int) -> CallLog {
        let callObject = CallLog(context: AppDelegate.shared.persistentContainer.viewContext)
        callObject.uuid = self.uuid
        callObject.handle = self.handle
        callObject.outgoing = self.outgoing
        callObject.duration = Int32(duration)
        callObject.handle = self.handle
        callObject.date = self.date
        callObject.callName = self.callName
        callObject.userID = Int32(AppGlobalManager.sharedInstance.loggedInUser?.user?.id ?? 0)
        return callObject
        
        
    }
    
}
