//
//  AgoraCallChannelManager.swift
//  AgoraARKit
//
//  Created by Prashant on 01/05/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import Foundation
import AgoraRtcKit

class AgoraChannelManager:NSObject {
    
    public lazy var rtcEngine = AgoraRtcEngineKit.sharedEngine(withAppId: KeyCenter.AppId, delegate: self)
//    public lazy var rtcEngine = AgoraRtcEngineKit.sharedEngine(withAppId: UserDefaults.standard.string(forKey: "AgoraAppId")!, delegate: self)

  //  static let sharedInstance = AgoraChannelManager()
    
    var streamID = -1
    
    var messageRecived:((String) -> Void)?
    
    override init() {
        super.init()
        setupAgoraKit()
        
    }
    
     func setupAgoraKit() {
        rtcEngine = AgoraRtcEngineKit.sharedEngine(withAppId: KeyCenter.AppId, delegate: self)
//       rtcEngine = AgoraRtcEngineKit.sharedEngine(withAppId: UserDefaults.standard.string(forKey: "AgoraAppId")!, delegate: self)

        rtcEngine.setChannelProfile(.communication)
        rtcEngine.enableVideo()
        rtcEngine.enableDualStreamMode(true)
        rtcEngine.disableAudio()
        
        rtcEngine.setClientRole(.broadcaster)
        rtcEngine.setDefaultAudioRouteToSpeakerphone(true)
        rtcEngine.enableDualStreamMode(true)

    }
    
    func joinChannel(channel:String,completion:@escaping()->()) {
        
        rtcEngine.joinChannel(byToken: nil, channelId: channel, info: nil, uid: 0) {[weak self] (string, intvalue, intValue2) in
            guard let self = self else {
                completion()
                NSLog("AGORA CHANNEL FAIL")

                return
            }
            NSLog("AGORA CHANNEL JOINED")
            print("USER JOINED CHANNEL")
            print(intvalue)
            
            self.rtcEngine.createDataStream(&self.streamID, reliable: true, ordered: true)
            
            completion()
        }
        
        
    }
    
    func leaveChannel(completion:@escaping()->()) {
        rtcEngine.stopPreview()
        rtcEngine.disableVideo()
        rtcEngine.disableAudio()
        rtcEngine.disableExternalAudioSource()
        rtcEngine.enableDualStreamMode(false)
        
       let value =  rtcEngine.leaveChannel { (status) in
            
            completion()
        }
        
        print("LEAVE CHANNEL RESPONSE \(value)")
    }
    
    func sendText(string:String) {
       let status =  rtcEngine.sendStreamMessage(streamID, data: string.data(using: .utf8)!)
        print("SEND STATUS \(status)")
    
    }
    
    
}

extension AgoraChannelManager:AgoraRtcEngineDelegate {
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        print("USER JOINED IN TEMP CHAANNEL")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, receiveStreamMessageFromUid uid: UInt, streamId: Int, data: Data) {
        print("STREAM MESSAGE REC. \(String(data:data,encoding:.utf8))")
        messageRecived?("\(String(data:data,encoding:.utf8) ?? "")")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurStreamMessageErrorFromUid uid: UInt, streamId: Int, error: Int, missed: Int, cached: Int) {
        print("STREAM MESSGE ERROR \(error)")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        print("User Offline")
    }
    
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didLeaveChannelWith stats: AgoraChannelStats) {
        print("didLeaveChannelWith")

    }
    
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, streamPublishedWithUrl url: String, errorCode: AgoraErrorCode) {
        print("Published")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, streamInjectedStatusOfUrl url: String, uid: UInt, status: AgoraInjectStreamStatus) {
        print("Stream INJECTION")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        print("ERROR OCCURED",errorCode.rawValue)
    }
    
    
}
