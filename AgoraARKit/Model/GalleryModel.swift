
import Foundation

struct GalleryModel : Codable {

        let createdAt : String?
        let deletedAt : String?
        let descriptionField : String?
        let id : String?
        let path : String?
        let thumbPath : String?
        let sessionId : String?
        let updatedAt : String?
        let userId : String?
        let type : String?
        let cat_name : String?
        let tagname : [String]?
        var sortedDate: Date?
        let location : String?

        enum CodingKeys: String, CodingKey {
                case createdAt = "created_at"
                case deletedAt = "deleted_at"
                case descriptionField = "description"
                case id = "id"
                case path = "path"
                case thumbPath = "thumb_path"
                case sessionId = "session_id"
                case updatedAt = "updated_at"
                case userId = "user_id"
                case type = "type"
                case cat_name = "cat_name"
                case tagname = "tagname"
                case location = "location"
                case sortedDate = "sortedDate"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
                
                descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
                
                path = try values.decodeIfPresent(String.self, forKey: .path)
                
            type = try values.decodeIfPresent(String.self, forKey: .type)
//                sessionId = try values.decodeIfPresent(Int.self, forKey: .sessionId)
                updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
//                userId = try values.decodeIfPresent(Int.self, forKey: .userId)
            
            do {
                thumbPath = try values.decodeIfPresent(String.self, forKey: .thumbPath)
            } catch {
                thumbPath = ""
            }
            
            do {
                deletedAt = try values.decodeIfPresent(String.self, forKey: .deletedAt)
            } catch {
                deletedAt = ""
            }
            
            do {
                let asdf = try values.decodeIfPresent(Int.self, forKey: .id)
                id = "\(asdf ?? 0)"
            } catch {
                id = try values.decodeIfPresent(String.self, forKey: .id)
            }
            
            do {
                let asdf = try values.decodeIfPresent(Int.self, forKey: .sessionId)
                sessionId = "\(asdf ?? 0)"
            } catch {
                do {
                    let asdf = try values.decodeIfPresent(String.self, forKey: .sessionId)
                    sessionId = asdf
                } catch {
                    sessionId = ""
                }
            }
            
            do {
                let asdf = try values.decodeIfPresent(Int.self, forKey: .userId)
                userId = "\(asdf ?? 0)"
            } catch {
                userId = try values.decodeIfPresent(String.self, forKey: .userId)
            }
            
            do {
                cat_name = try values.decodeIfPresent(String.self, forKey: .cat_name)
            } catch {
                cat_name = ""
            }
            
            do {
                tagname = try values.decodeIfPresent([String].self, forKey: .tagname)
            } catch {
                tagname = []
            }
          
            do {
                sortedDate = try values.decodeIfPresent(Date.self, forKey: .sortedDate)
            } catch {
                sortedDate = Date()
            }
            do {
                location = try values.decodeIfPresent(String.self, forKey: .location)
            } catch {
                location = ""
            }
        }

}
