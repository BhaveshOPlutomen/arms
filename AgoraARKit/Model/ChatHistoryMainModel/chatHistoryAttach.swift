//
//  chatHistoryAttach.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 24, 2020

import Foundation

struct chatHistoryAttach : Codable {

        var fileExt : String?
        var fileName : String?
        var filePath : String?
        var fileSize : String?
        var fileThumbnailPath : String?
    
    init(fileExt : String?, fileName : String?, filePath : String?, fileSize : String?, fileThumbnailPath : String?) {
        self.fileExt = fileExt
        self.fileName = fileName
        self.filePath = filePath
        self.fileSize = fileSize
        self.fileThumbnailPath = fileThumbnailPath
    }

        enum CodingKeys: String, CodingKey {
                case fileExt = "file_ext"
                case fileName = "file_name"
                case filePath = "file_path"
                case fileSize = "file_size"
                case fileThumbnailPath = "file_thumbnail_path"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                fileExt = try values.decodeIfPresent(String.self, forKey: .fileExt)
                fileName = try values.decodeIfPresent(String.self, forKey: .fileName)
                filePath = try values.decodeIfPresent(String.self, forKey: .filePath)
                
                fileThumbnailPath = try values.decodeIfPresent(String.self, forKey: .fileThumbnailPath)
            
//            fileSize = try values.decodeIfPresent(String.self, forKey: .fileSize)
            
            do {
                fileSize = try values.decodeIfPresent(String.self, forKey: .fileSize)
                
            } catch {
                do {
                let sz = try values.decodeIfPresent(Int.self, forKey: .fileSize)
                fileSize = "\(sz ?? 0)"
                    }
                catch {
                    let sz = try values.decodeIfPresent(Double.self, forKey: .fileSize)
                    fileSize = "\(sz ?? 0)"
                }
            
            }
        }

}
