//
//  chatHistoryDatum.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 5, 2020

import Foundation

struct chatHistoryDatum : Codable {
    
    var v : Int?
    var id : String?
    var chatId : String?
    var createdAt : String?
    var message : String?
    var sender : String?
    var updatedAt : String?
    var read : Bool?
    var attach : chatHistoryAttach?
    var sortedDate: Date?
    var deliver : Bool?
    var senderName : String?
    var senderUserId : String?
    
    init(v: Int, id: String, chatId: String,createdAt: String, message: String, sender: String, updatedAt: String, read: Bool, deliver: Bool,attach: chatHistoryAttach?, sortedDate: Date, senderName: String?, senderUserId: String?) {
        self.v = v
        self.id = id
        self.chatId = chatId
        self.createdAt = createdAt
        self.message = message
        self.sender = sender
        self.updatedAt = updatedAt
        self.read = read
        self.attach = attach
        self.sortedDate = sortedDate
        self.deliver = deliver
        self.senderName = senderName
        self.senderUserId = senderUserId

    }
    
    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case chatId = "chat_id"
        case createdAt = "createdAt"
        case message = "message"
        case sender = "sender"
        case updatedAt = "updatedAt"
        case read = "read"
        case attach = "attach"
        case sortedDate = "sortedDate"
        case deliver = "deliver" // Bhavesh - 12 Nov 2020
        case senderName = "sender_name"
        case senderUserId = "sender_user_id"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        v = try values.decodeIfPresent(Int.self, forKey: .v)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        chatId = try values.decodeIfPresent(String.self, forKey: .chatId)
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        sender = try values.decodeIfPresent(String.self, forKey: .sender)
        updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
        do {
            let asdf = try values.decodeIfPresent(chatHistoryAttach.self, forKey: .attach)
            attach = asdf
        } catch {
            do {
                if let asdf = try values.decodeIfPresent([chatHistoryAttach].self, forKey: .attach) {
                    attach = asdf.first
                }
            } catch {
                attach = nil
            }
        }
        do {
            let rd = try values.decodeIfPresent(Int.self, forKey: .read)
            read = Int(rd ?? 0) == 0 ? false : true
        }
        catch {
            do {
                let rd = try values.decodeIfPresent(String.self, forKey: .read)
                read = Int(rd ?? "0") == 0 ? false : true
            }
            catch {
                let rd = try values.decodeIfPresent(Bool.self, forKey: .read)
                read = rd == true
            }
        }
        do {
            sortedDate = try values.decodeIfPresent(Date.self, forKey: .sortedDate)
        } catch {
            sortedDate = Date()
        }
        do {
            let dl = try values.decodeIfPresent(String.self, forKey: .deliver)
            deliver = Int(dl ?? "0") == 0 ? false : true
        }
        catch {
            do {
                let rd = try values.decodeIfPresent(Int.self, forKey: .deliver)
                deliver = (rd ?? 0) == 0 ? false : true
            }
            catch {
                let rd = try values.decodeIfPresent(Bool.self, forKey: .deliver)
                deliver = rd == true
            }
        }
        do {
            let rd = try values.decodeIfPresent(Int.self, forKey: .senderUserId)
            senderUserId = "\(rd ?? 0)"
        }
        catch {
            let senderId = try values.decodeIfPresent(String.self, forKey: .senderUserId)
            senderUserId = senderId
        }
        do {
            senderName = try values.decodeIfPresent(String.self, forKey: .senderName)
        }
        catch {
            senderName = ""
        }
    }
}
