//
//  chatHistoryMain.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 5, 2020

import Foundation

struct chatHistoryMain : Codable {

        let message : String?
        let messageCode : String?
        let output : chatHistoryOutput?
        let status : String?
        let statusCode : Int?

        enum CodingKeys: String, CodingKey {
                case message = "message"
                case messageCode = "messageCode"
                case output = "output"
                case status = "status"
                case statusCode = "statusCode"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                messageCode = try values.decodeIfPresent(String.self, forKey: .messageCode)
                output = try values.decodeIfPresent(chatHistoryOutput.self, forKey: .output) // chatHistoryOutput(from: decoder)
                status = try values.decodeIfPresent(String.self, forKey: .status)
                statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        }

}
