//
//  chatHistoryOutput.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on October 5, 2020

import Foundation

struct chatHistoryOutput : Codable {

        var currentPage : Int?
        let data : [chatHistoryDatum]?
        let firstPageUrl : String?
        var from : Int?
        var lastPage : Int?
        let lastPageUrl : String?
        let nextPageUrl : String?
        let path : String?
        var perPage : Int?
        let prevPageUrl : String?
        var to : Int?
        var total : Int?
        var group : chatGroupModel?
    
        enum CodingKeys: String, CodingKey {
                case currentPage = "current_page"
                case data = "data"
                case firstPageUrl = "first_page_url"
                case from = "from"
                case lastPage = "last_page"
                case lastPageUrl = "last_page_url"
                case nextPageUrl = "next_page_url"
                case path = "path"
                case perPage = "per_page"
                case prevPageUrl = "prev_page_url"
                case to = "to"
                case total = "total"
                case group = "group"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                currentPage = try values.decodeIfPresent(Int.self, forKey: .currentPage)
                data = try values.decodeIfPresent([chatHistoryDatum].self, forKey: .data)
                firstPageUrl = try values.decodeIfPresent(String.self, forKey: .firstPageUrl)
                from = try values.decodeIfPresent(Int.self, forKey: .from)
                lastPage = try values.decodeIfPresent(Int.self, forKey: .lastPage)
                lastPageUrl = try values.decodeIfPresent(String.self, forKey: .lastPageUrl)
                nextPageUrl = try values.decodeIfPresent(String.self, forKey: .nextPageUrl)
                path = try values.decodeIfPresent(String.self, forKey: .path)
                perPage = try values.decodeIfPresent(Int.self, forKey: .perPage)
                prevPageUrl = try values.decodeIfPresent(String.self, forKey: .prevPageUrl)
                to = try values.decodeIfPresent(Int.self, forKey: .to)
                total = try values.decodeIfPresent(Int.self, forKey: .total)
            group = try values.decodeIfPresent(chatGroupModel.self, forKey: .group)
            
            do {
                currentPage = try values.decodeIfPresent(Int.self, forKey: .currentPage)
            }
            catch {
                let rd = try values.decodeIfPresent(String.self, forKey: .currentPage)
                currentPage = Int(rd ?? "0")
            }
            
            do {
                from = try values.decodeIfPresent(Int.self, forKey: .from)
            }
            catch {
                let rd = try values.decodeIfPresent(String.self, forKey: .from)
                from = Int(rd ?? "0")
            }
            
            do {
                lastPage = try values.decodeIfPresent(Int.self, forKey: .lastPage)
            }
            catch {
                let rd = try values.decodeIfPresent(String.self, forKey: .lastPage)
                lastPage = Int(rd ?? "0")
            }
            
            do {
                perPage = try values.decodeIfPresent(Int.self, forKey: .perPage)
            }
            catch {
                let rd = try values.decodeIfPresent(String.self, forKey: .perPage)
                perPage = Int(rd ?? "0")
            }
            do {
                group = try values.decodeIfPresent(chatGroupModel.self, forKey: .group)
            }
            catch {
                group = nil
            }
        }
}


func convertIntoDatatype() {
    
}
