//
//  LoginResponse.swift
//  AgoraARKit
//
//  Created by Prashant on 08/08/19.
//  Copyright © 2019 Prashant. All rights reserved.
//

import Foundation


// MARK: - Output
struct LoginResponse: Codable {
    let token: String?
    var user: User?
    let app_id : String?
    
    enum CodingKeys: String, CodingKey {
        case token = "token"
        case user = "user"
        case app_id = "app_id"
    }
}

// MARK: - User
struct User: Codable,Comparable {
    static func == (lhs: User, rhs: User) -> Bool {
                return (lhs.name ?? "").lowercased() < (rhs.name ?? "").lowercased()

    }
    
    static func < (lhs: User, rhs: User) -> Bool {
        return (lhs.name ?? "").lowercased() < (rhs.name ?? "").lowercased()

    }
    
    
    let id: Int?
    let name: String?
    let email: String?
    let is_guest : Bool?
    let mobileNo: String?
    let department: String?
    let company : Company?
    let chatId : String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case email = "email"
        case is_guest = "is_guest"
        case mobileNo = "mobile_no"
        case department = "department"
        case company = "company"
        case chatId = "chat_id"
    }
//
//    static func < (lhs: User, rhs: User) -> Bool {
//        return (lhs.name ?? "").lowercased() < (rhs.name ?? "").lowercased()
//    }
}

//changes in lock down
struct tags : Codable{
    let id : Int?
    let company_id : Int?
    let name: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case company_id = "company_id"
        case name = "name"
    }
}
struct category : Codable{
    let id : Int?
    let name: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }
}
struct Company : Codable{
    let allow_groupcall : Int?
    let name : String?
    let allow_rec_full: Int?

    enum CodingKeys: String, CodingKey {
        case allow_groupcall = "allow_groupcall"
        case name = "name"
        case allow_rec_full = "allow_rec_full"
    }
}
struct SyncResponse: Codable {
    
    
    let id: Int?
    let name: String?
    let email: String?
    let is_guest : Bool?
    let mobileNo: String?
    let department: String?
    let company : Company?
    let tags : [tags]?
    let category : [category]?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case email = "email"
        case is_guest = "is_guest"
        case mobileNo = "mobile_no"
        case department = "department"
        case company = "company"
        case tags = "tags"
        case category = "category"
    }
}

struct saveThumbResponse : Codable{
    let user_call_id : String?
    let thumb_path : String?
    let id : Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case thumb_path = "thumb_path"
        case user_call_id = "user_call_id"
    }
    
}

// MARK: - Output of call log
struct CallLogResponse: Codable {
    let id: Int?
    let created_at: String?
    let user_id : Int?
    let user_call_id : Int?
    let call_role : String?
    let total_time : String?
    let receiver : [Receiver]?
    let user : User?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case created_at = "created_at"
        case user_id = "user_id"
        case user_call_id = "user_call_id"
        case call_role = "call_role"
        case total_time = "total_time"
        case user = "user"
        case receiver = "receiver"
    }
}

// MARK: - receiver
struct Receiver: Codable,Comparable {
    let id: Int?
    let ch_join_time: String?
    let total_time: String?
    let call_role: String?
    let ch_name: String?
    let user_id : Int?
    let user : User?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case ch_join_time = "ch_join_time"
        case total_time = "total_time"
        case call_role = "call_role"
        case ch_name = "ch_name"
        case user_id = "user_id"
        case user = "user"
    }
    static func < (lhs: Receiver, rhs: Receiver) -> Bool {
           return (lhs.ch_name ?? "").lowercased() < (rhs.ch_name ?? "").lowercased()
       }
}
//changes in lock down
struct initCallResponse: Codable {
    var user_call_id: Int?
    var chat_id : String?
       
       enum CodingKeys: String, CodingKey {
           case user_call_id = "user_call_id"
        case chat_id = "chat_id"
       }
}
// MARK: - General Response

struct GeneralResponse : Codable {
    let status: String?
    let message:String?
    let statusCode:Int?

    enum CodingKeys: String, CodingKey {
        case status
        case statusCode
        case message
    }
   
}

struct createSessionResponse: Codable {
    let channel_key: String?
    let join_app_link: String?
     enum CodingKeys: String, CodingKey {
        case channel_key = "channel_key"
        case join_app_link = "join_app_link"
        
    }
}
struct JoinSessionResponse: Codable {
    var user_call_id: Int?
    var chat_id : String?
    var user: User?
    
    enum CodingKeys: String, CodingKey {
        case user = "user"
        case user_call_id = "user_call_id"
        case chat_id = "chat_id"
    }
}
struct RecLinkJoinResponse: Codable {
    var sessCreatedByWeb: Bool?
    
    enum CodingKeys: String, CodingKey {
        case sessCreatedByWeb = "sessCreatedByWeb"
    }
}
struct AppLinkJoinResponse: Codable {
    var privacy_policy: String?
    var help: String?
    var contact_us: String?
    var terms_cond: String?
    var about_us: String?
    
    enum CodingKeys: String, CodingKey {
        case privacy_policy = "privacy_policy"
        case help = "help"
        case contact_us = "contact_us"
        case terms_cond = "terms_cond"
        case about_us = "about_us"
    }
}
