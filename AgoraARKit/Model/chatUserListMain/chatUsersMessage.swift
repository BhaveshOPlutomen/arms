//
//  chatUsers.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 25, 2020

import Foundation

struct chatUsersMessage : Codable {

        var v : Int?
        var id : String?
        var chatId : String?
        var createdAt : chatUsersCreatedAt?
        var message : String?
        var sender : String?
        var updatedAt : chatUsersUpdatedAt?

        enum CodingKeys: String, CodingKey {
                case v = "__v"
                case id = "_id"
                case chatId = "chat_id"
                case createdAt = "createdAt"
                case message = "message"
                case sender = "sender"
                case updatedAt = "updatedAt"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                v = try values.decodeIfPresent(Int.self, forKey: .v)
                id = try values.decodeIfPresent(String.self, forKey: .id)
                chatId = try values.decodeIfPresent(String.self, forKey: .chatId)
                createdAt = try values.decodeIfPresent(chatUsersCreatedAt.self, forKey: .createdAt) // CreatedAt(from: decoder)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                sender = try values.decodeIfPresent(String.self, forKey: .sender)
            updatedAt = try values.decodeIfPresent(chatUsersUpdatedAt.self, forKey: .updatedAt) // UpdatedAt(from: decoder)
        }

}
