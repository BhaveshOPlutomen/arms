//
//  chatUserListMain.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 25, 2020

import Foundation

struct chatUserListMain : Codable {

        let message : String?
        let messageCode : String?
        let output : chatUserTypeMain?
        let status : String?
        let statusCode : Int?

        enum CodingKeys: String, CodingKey {
                case message = "message"
                case messageCode = "messageCode"
                case output = "output"
                case status = "status"
                case statusCode = "statusCode"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                messageCode = try values.decodeIfPresent(String.self, forKey: .messageCode)
                output = try values.decodeIfPresent(chatUserTypeMain.self, forKey: .output)
                status = try values.decodeIfPresent(String.self, forKey: .status)
                statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        }

}
struct chatUserTypeMain : Codable {
        
    let chats : [chatUsersModel]?
    
    let groups : [chatGroupModel]?
    
    
    enum CodingKeys: String, CodingKey {
        case chats = "chats"
        case groups = "groups"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        chats = try values.decodeIfPresent([chatUsersModel].self, forKey: .chats)
        groups = try values.decodeIfPresent([chatGroupModel].self, forKey: .groups)
    }
    
}
