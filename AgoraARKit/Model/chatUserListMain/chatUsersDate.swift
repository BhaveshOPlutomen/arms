//
//  chatUsersDate.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 25, 2020

import Foundation

struct chatUsersDate : Codable {

        let numberLong : String?

        enum CodingKeys: String, CodingKey {
                case numberLong = "$numberLong"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                numberLong = try values.decodeIfPresent(String.self, forKey: .numberLong)
        }

}
