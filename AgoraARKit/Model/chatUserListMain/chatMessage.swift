//
//  chatMessage.swift
//  Created on October 5, 2020

import Foundation

struct chatMessage : Codable {

        let v : Int?
        let id : String?
        let chatId : String?
        var createdAt : String?
        var message : String?
        let sender : String?
        let read : Int?
        let deliver : String?
        var attach : chatHistoryAttach?
    var senderName : String?
            var senderUserId : String?
    
        enum CodingKeys: String, CodingKey {
                case v = "__v"
                case id = "_id"
                case chatId = "chat_id"
                case createdAt = "createdAt"
                case message = "message"
                case sender = "sender"
                case read = "read"
                case deliver = "deliver" // Bhavesh - 12 Nov 2020
            case attach = "attach"
            case senderName = "sender_name"
                        case senderUserId = "sender_user_id"
        }
    
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            v = try values.decodeIfPresent(Int.self, forKey: .v)
            id = try values.decodeIfPresent(String.self, forKey: .id)
            chatId = try values.decodeIfPresent(String.self, forKey: .chatId)
            createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
            message = try values.decodeIfPresent(String.self, forKey: .message)
            sender = try values.decodeIfPresent(String.self, forKey: .sender)
            senderName = try values.decodeIfPresent(String.self, forKey: .senderName)
            do {
                senderUserId = try values.decodeIfPresent(String.self, forKey: .senderUserId)
            }
            catch {
                let rd = try values.decodeIfPresent(Int.self, forKey: .senderUserId)
                senderUserId = "\(rd ?? 0)"
            }
            do {
                attach = try values.decodeIfPresent(chatHistoryAttach.self, forKey: .attach)
            }
            catch {
                print("Attach Nil : ")
                attach = nil
            }
            
               
            do {
                read = try values.decodeIfPresent(Int.self, forKey: .read)
            }
            catch {
                do {
                    let rd = try values.decodeIfPresent(String.self, forKey: .read)
                    read = Int(rd ?? "0")
                }
                catch {
                    let rd = try values.decodeIfPresent(Bool.self, forKey: .read)
                    read = rd == true ? 1 : 0
                }
            }
            
            do {
                deliver = try values.decodeIfPresent(String.self, forKey: .deliver)
            }
            catch {
                do {
                    let rd = try values.decodeIfPresent(Int.self, forKey: .deliver)
                    deliver = "\(rd ?? 0)"
                }
                catch {
                   let temp = try values.decodeIfPresent(Bool.self, forKey: .deliver)
                    deliver = temp == true ? "1" : "0"
                
                }
            }
            
        }

}
