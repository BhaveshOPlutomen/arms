//
//  chatUsers.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 25, 2020

import Foundation

struct chatUsersUpdatedAt : Codable {

        let date : chatUsersDate?

        enum CodingKeys: String, CodingKey {
                case date = "$date"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
//                date = $date(from: decoder)
            date = try values.decodeIfPresent(chatUsersDate.self, forKey: .date)
        }

}
