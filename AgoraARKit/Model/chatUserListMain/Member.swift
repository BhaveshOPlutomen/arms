
import Foundation

struct Member : Codable {
    
    let mobileNo : String?
    let name : String?
    
    enum CodingKeys: String, CodingKey {
        case mobileNo = "mobile_no"
        case name = "name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            let mobileNoStr = try values.decodeIfPresent(String.self, forKey: .mobileNo)
            mobileNo = mobileNoStr
        }
        catch {
            let mobileNoInt = try values.decodeIfPresent(Int.self, forKey: .mobileNo)
            mobileNo = "\(mobileNoInt ?? 0)"
        }
        //                mobileNo = try values.decodeIfPresent(Int.self, forKey: .mobileNo)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
    
}
