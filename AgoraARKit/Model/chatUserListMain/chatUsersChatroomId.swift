//
//  ChatroomId.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 25, 2020

import Foundation

struct chatUsersChatroomId : Codable {

        let id : String?
//        let members : [AnyObject]?

        enum CodingKeys: String, CodingKey {
                case id = "_id"
//                case members = "members"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                id = try values.decodeIfPresent(String.self, forKey: .id)
//                members = try values.decodeIfPresent([AnyObject].self, forKey: .members)
        }

}
