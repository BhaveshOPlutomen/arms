
import Foundation

struct chatGroupModel : Codable {
    
    let id : String?
    var message : chatMessage?
    let createdAt : String?
    let groupDes : String?
    let groupId : String?
    let groupName : String?
    let members : [Member]?
    let updatedAt : String?
    var unreadCount : Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case message = "message"
        case createdAt = "created_at"
        case groupDes = "group_des"
        case groupId = "group_id"
        case groupName = "group_name"
        case members = "members"
        case updatedAt = "updated_at"
        case unreadCount = "unreadCount"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        groupDes = try values.decodeIfPresent(String.self, forKey: .groupDes)
        groupId = try values.decodeIfPresent(String.self, forKey: .groupId)
        groupName = try values.decodeIfPresent(String.self, forKey: .groupName)
        members = try values.decodeIfPresent([Member].self, forKey: .members)
        updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
        unreadCount = try values.decodeIfPresent(Int.self, forKey: .unreadCount)
        
        do {
            message = try values.decodeIfPresent(chatMessage.self, forKey: .message)
        }
        catch {
            message = nil
        }
    }
    
}
