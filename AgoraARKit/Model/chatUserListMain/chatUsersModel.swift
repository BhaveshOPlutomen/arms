//
//  chatUsersModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 25, 2020

import Foundation

struct chatUsersModel : Codable {

        let chatId : String?
        let department : String?
        let email : String?
        let id : Int?
        var message : chatMessage?
        let mobileNo : String?
        let name : String?
        var unreadCount : Int?
        var lastSeen: String?

        enum CodingKeys: String, CodingKey {
                case chatId = "chat_id"
                case department = "department"
                case email = "email"
                case id = "id"
                case message = "message"
                case mobileNo = "mobile_no"
                case name = "name"
                case unreadCount = "unread_count"
                case lastSeen = "last_seen"
            
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                chatId = try values.decodeIfPresent(String.self, forKey: .chatId) // ChatroomId(from: decoder)
                department = try values.decodeIfPresent(String.self, forKey: .department)
                email = try values.decodeIfPresent(String.self, forKey: .email)
                id = try values.decodeIfPresent(Int.self, forKey: .id)
                
                mobileNo = try values.decodeIfPresent(String.self, forKey: .mobileNo)
                name = try values.decodeIfPresent(String.self, forKey: .name)
                unreadCount = try values.decodeIfPresent(Int.self, forKey: .unreadCount)
                lastSeen = try values.decodeIfPresent(String.self, forKey: .lastSeen)
            
            do {
                message = try values.decodeIfPresent(chatMessage.self, forKey: .message) // chatUsersMessage(from: decoder)
            } catch {
                message = nil
            }
        }

}
