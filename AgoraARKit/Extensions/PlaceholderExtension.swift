//
//  PlaceholderExtension.swift
//  AgoraARKit
//
//  Created by Hetali on 13/05/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit

class PlaceholderExtension: UITextField {
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}
