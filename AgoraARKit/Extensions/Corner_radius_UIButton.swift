//
//  Corner_radius_UIButton.swift
//  AgoraARKit
//
//  Created by Hetali on 24/07/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit

@IBDesignable
class Corner_radius_UIButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 10 
    
    override func draw(_ rect: CGRect) {
           layer.cornerRadius = cornerRadius
           layer.masksToBounds = true
       }
}
