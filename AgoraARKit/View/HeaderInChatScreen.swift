//
//  HeaderInChatScreen.swift
//  AgoraARKit
//
//  Created by Bhavesh Odedara on 10/11/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit

class HeaderInChatScreen: UITableViewHeaderFooterView {

    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.lblHeaderTitle.backgroundColor = UIColor.init(named: "SeondaryDark")?.withAlphaComponent(0.6)
        lblHeaderTitle.layer.cornerRadius = 9
        lblHeaderTitle.layer.masksToBounds = true
    }
    
    
   open func setupData(text: String) {
        lblHeaderTitle.text = text + "          "
    }

}
