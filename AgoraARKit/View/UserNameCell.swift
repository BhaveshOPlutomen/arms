//
//  UserNameCell.swift
//  AgoraARKit
//
//  Created by Hetali on 28/04/20.
//  Copyright © 2020 Prashant. All rights reserved.
//

import UIKit

class UserNameCell: UICollectionViewCell {
    
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var imageUser: UIImageView!
    
    @IBOutlet weak var lblShortname: UILabel!
    
    
}
